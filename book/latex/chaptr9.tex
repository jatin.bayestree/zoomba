\chapter{Interacting with Environment}\label{interaction}

{\LARGE E}nvironment is needed to run any system.
In this chapter we discuss about that environment, which is aptly called the Operating System.
We would discuss about how to make calls to the operating system, how to create threads, 
and how to operate in linear ways in case of error happened, and how to have random shuffling generated.  

\begin{section}{Process and Threads}

\begin{subsection}{system() function}
\index{system()}
To run any operating system command from ZoomBA, use the $system()$ function.
It comes in two varieties, one where the whole command is given as single string, 
and in another the commands are separated as words :
\begin{lstlisting}[style=zmbStyle]
status = system(command_to_os)
status = system(word1, word2,...)
\end{lstlisting}
The return status is the exit status of the command.
It returns 0 for success and non zero for failure.
For example, here is the sample :

\begin{lstlisting}[style=zmbStyle]
status = system("whoami") // system prints "noga"
/* status code is 0 */
\end{lstlisting}

In case of unknown command, it would throw an error:

\begin{lstlisting}[style=zmbStyle]
status = system("unknown command") 
/* error will be thrown */
\end{lstlisting}

In case of command is found, but command execution 
reported an error, it status would  be non zero:

\begin{lstlisting}[style=zmbStyle]
status = system("ls -l thisDoesNotExist") 
/* os reprots 
ls: thisDoesNotExist: No such file or directory
status := 1
*/
\end{lstlisting}

In the same way, multiple words can be put into system,
which is of some usefulness when one needs to pass quoted arguments :

\begin{lstlisting}[style=zmbStyle]
// the result of "ls -al ." is not shown at all, use popen()
status = system("ls" , "-al" , "." ) 
/* status code is 0 */
\end{lstlisting}
\end{subsection}

\begin{subsection}{popen() function}
If you want to work with processes, use \emph{popen()} function.

\begin{lstlisting}[style=zmbStyle]
p = popen("ls" , "-al" , "." ) 
p.waitOn(10000) // timeout 
p.status().out // stdout 
p.status().err // stderr 
p.status().code // exit code 
\end{lstlisting}
\emph{popen()} comes with multiple command line arguments, as in :

\begin{lstlisting}[style=zmbStyle]
p = popen(args=["ls" , "-al", "/" ], 
      redirect={ "out" : "out_file" , "err" : "err_file" , "in" : "in_file"  } ) 
\end{lstlisting}

This way, one can redirect input, output and error. A call to \emph{popen()} w/o any input yields
a dummy abstraction of the current running process, which then can be used to extract the \emph{pid} by :

\begin{lstlisting}[style=zmbStyle]
cur = popen( )  // dummy current ZProcess
cur.process.pid // current process id  
\end{lstlisting}

\index{ pid : current process }

\end{subsection}



\begin{subsection}{thread() function}
\index{thread()}
$thread()$ function, creates a thread.
The syntax is simple :
\begin{lstlisting}[style=zmbStyle]
t = thread( args_to_be_passed ) as {  /* thread function body  */  }
current_thread = thread() // gets current thread 
\end{lstlisting}

Thus, simply, use $thread()$ to create a thread:

\begin{lstlisting}[style=zmbStyle]
t = thread( "Hello", "world" ) as {  
   printf('My Id is %d\n', $.i )
   printf('My args are: %s\n', str($.c,'\n' ) )
   printf('I am %s\n', $.o )
  }
/* java isAlive() maps to alive */
while ( t.alive ){
   cur_thread = thread() // returns current thread 
   cur_thread.sleep(1000)
}     
\end{lstlisting}
This would produce some kind of output as below:

\begin{lstlisting}[style=all]
My Id is 10
My args are Hello
world
I am Thread[Thread-1,5,main]
\end{lstlisting}

\end{subsection}

\begin{subsection}{poll() function}
\index{poll()}
Observe the last example where we used a while to wait for a condition to be true.
That would generate an infinite loop if the condition was not true.
So, many times we need to write this custom waiter, where the idea is to wait till a condition is satisfied. 
Of course we need to have a timeout, and of course we need a polling interval. 
To make this work easy, we have $poll()$. 

The syntax is :

\begin{lstlisting}[style=zmbStyle]
poll ( [ timeout-in-ms = 3000, [ polling-interval-in-ms = 100] ]) [ where { condition-body-of-function } ]        
\end{lstlisting}       

So these are explanations :

\begin{lstlisting}[style=zmbStyle]
poll() // this is ok : simple default wait  
poll (4000) // ok too !
poll (4000, 300 ) // not cool : 300 is rejected , no condition!
\end{lstlisting}       

A very mundane example would be :

\begin{lstlisting}[style=zmbStyle]
i = 3 
poll( 1000, 200 ) where { i-= 1 ; i == 0 }// induce a bit of wait 
// this returns true : the operation did not timeout 
i = 100 
poll( 1000, 200 ) where { i+= 1 ; i == 0 } // induce a bit of wait 
// this returns false : timeout happened  
\end{lstlisting}       

and, finally, we replace the while statement 
of the thread example using $poll()$ :

\begin{lstlisting}[style=zmbStyle]
t = thread() as {  
   /* do anything one needs */
  }
/* java isAlive() maps to alive */
poll( 10000, 300 ) where { ! t.alive } 
\end{lstlisting}
\end{subsection}

\begin{subsection}{Atomic Operations}
\index{atomic}
Given there are threads, being atomic is of importance.
The definition of being atomic is :
\begin{center}\emph{Either the block gets totally executed, or it does not at all. }\end{center}
Observe the problem here :

\begin{lstlisting}[style=zmbStyle]
$count = 0 
// create a list of threads to do something?
l = list( [0:10] ) as { thread() as { $count+= 1 } } // increase count...
poll ( 10000, 50 ) where { 
 running = select(l) where { $.o.isAlive } ; empty(running)  }
println($count) // what is the value here?
\end{lstlisting}

You would expect it to be 10, but it would be any number $x \le 10$,
because of the threading and non-atomic issue. To resolve it, 
put it inside an atomic block (\#atomic\{\} ) : \index{ atomic : block }

\begin{center}\begin{minipage}{\linewidth}
\begin{lstlisting}[style=zmbStyle]
$count = 0 
// create a list of threads to do something?
l = list( [0:10] ) as { thread() as { #atomic{ $count+= 1 } } } 
// above tries to increase count...
poll( 10000, 50 ) where { 
  running = select(l) as { $.isAlive } ; empty(running)  
  }
println($count) // the value is 10
\end{lstlisting}
\end{minipage}\end{center}

and now, the count is always 10.

Atomic can be used to revert back any changes in the variables :
\index{ atomic : block : reverting states }
\begin{lstlisting}[style=zmbStyle]
x = 0 
#atomic{
   x = 2 + 2    
}
#atomic{
   x = 10
   println(x) // prints 10 
/* error, no variable as "p', x should revert back to 4 */   
   t = p
}
println(x) // x is 4 
\end{lstlisting}

\end{subsection}

\begin{subsection}{The clock Block}
\index{clock}
Timing is of essence. If one does not grab hold of time, that is the biggest failure one can be.
So, to measure how much time a block of code is taking, there is $clock()$ function.
The syntax is simple :

\begin{lstlisting}[style=zmbStyle]
// times the body
#(time_taken_in_sec_double, result ) = #clock{ /* body  */}
\end{lstlisting}
Thus this is how you tune a long running code:
\begin{center}\begin{minipage}{\linewidth}
\begin{lstlisting}[style=zmbStyle]
m1 = 0 
#(t,r) = #clock{
   // load a large matrix, 0.2 million rows 
   m1 = matrix('test.csv', ',' , false)
   0 // return 0 
}
println(t) // writes the timing in seconds ( double )
println(r) // writes the result : 0  
\end{lstlisting}
\end{minipage}\end{center}

The clock block never throws an error.
That is, if any error gets raised in the block inside,
the error gets assigned to the result variable.
\index{ clock : error }
\begin{lstlisting}[style=zmbStyle]
#(t,r) = #clock{  x = foobar_not_defined_var }
// r would be ZoomBA variable exception
\end{lstlisting}

Note that the timing would always be there, 
no matter what. Even in the case of an error, 
the timing variable would be populated.

\end{subsection}

\end{section}

\begin{section}{Handling of Errors}

We toyed with the idea of try-catch, and then found that : 
\href{http://stackoverflow.com/questions/1736146/why-is-exception-handling-bad}{Why Exception Handling is Bad?} ; 
\href{https://gobyexample.com/errors}{GO} does not have exception handling. 
And we came to  believe it is OK not to have exceptions in a language that has no business becoming a system design language.
In a glue language like ZoomBA, we should code for everything, and never ever eat up error like situations. 
For this specific need, Asserts are in place. 
In particular - the multi part arguments and the return story - should ensure that there is really no exceptional thing that happens. 
After all, trying to build a fool-proof solution is never full-proof. And one of the return values should then be asserted.

\begin{subsection}{error() function}
\index{error()}
But we need errors to be raised. This is done by the $error()$ function.

\begin{lstlisting}[style=zmbStyle]
error( args... ) where [ { /* body, when evaluates to true raise error */} ]   
\end{lstlisting}
The function $error()$ can have multiple modes, 
for example this is how one raise an error with a message :
\begin{lstlisting}[style=zmbStyle]
error( "error message")   
\end{lstlisting}
This is how one raise an error on condition :
\begin{lstlisting}[style=zmbStyle]
error( condition , "error message")   
\end{lstlisting}
when $condition$ would be true, error would raised.
If the condition is false, $error()$ returns $false$.
In case we want to execute complex logic, and guards it against failure:

\begin{lstlisting}[style=zmbStyle]
error("error message") where { /* error condition */ } 
\end{lstlisting}

\end{subsection}


\begin{subsection}{Multiple Assignment}
\index{multiple assignment}
GoLang supports multiple assignments. This feature is picked from GoLang.
Observe the following :

\begin{lstlisting}[style=zmbStyle]
#(a,b) = [ 0 , 1 , 2 ] // a = 0, b = 1 
#(,a,b) = [ 0 , 1 , 2 ] // a = 1, b = 2 
\end{lstlisting}

The idea is that one can assign a collection 
directly mapped to a tuple with proper variable names, 
either from left or from right. 
\index{multiple assignment : splicing left, right}

Note the quirky ``,'' before $a$
in the 2nd assignment, it tells you that the splicing of the array
should happen from right, not from left.

\end{subsection}

\begin{subsection}{Error Assignment}
\index{multiple return : error}
A variant of multiple assignment is error assignment.
The idea is to make the code look as much linear as possible.
The issue with the $try()$ function is that, there is no way to know
that error was raised, actually. It lets one go ahead with the default value.
To solve this issue, there is multiple return, on error.

\begin{lstlisting}[style=zmbStyle]
import 'java.lang.Integer' as Int 
#(n ? e)  =  Int.parseInt('42') //n := 42 int, e := null 
// error will be raised and caught 
#(n ? e) = Int.parseInt('xx42') //n := null, e:= NumberFormatException 
\end{lstlisting}

Observe the ``?'' before the last argument $e$, to tag it as the error container.
Thus, the system knows that the error needs to be filled into that specific variable.
Thus, one can write almost linear code like this:
\begin{center}\begin{minipage}{\linewidth}
\begin{lstlisting}[style=zmbStyle]
import 'java.lang.Integer' as Int 
#(n ? e)  =  Int.parseInt(str_val)
empty(e) || bye('failure in parsing ', str_val )  
\end{lstlisting}
\end{minipage}\end{center}

\index{bye()}
The $bye()$ function, when called, returns from the current calling function or script,
with the same array of argument it was passed with. 

\end{subsection}
\end{section}

\begin{section}{Order and Randomness}

\begin{subsection}{Lexical matching : tokens()}

In some scenarios, one needs to read from a stream of characters (String) and then do something about it.
One such typical problem is to take CSV data, and process it. Suppose one needs to parse CSV into a list of integers. e.g.

\begin{lstlisting}[style=zmbStyle]
s = '11,12,31,34,78,90' // CSV integers 
l = select( s.split(',') )  where { !empty( int($.o) )  } as { int($.o) }
// l := [11, 12, 31, 34, 78, 90] // above works
\end{lstlisting}  
But then, the issue here is : the code iterates over the string once to generate the split array, 
and then again over that array to generate the list of integers, selecting only when it is applicable.
Clearly then, a better approach would be to do it in a single pass, so :

\begin{lstlisting}[style=zmbStyle]
s = '11,12,31,34,78,90'
tmp = ''
l = list()
for ( c : s ){
    continue ( c == ',' ){
        l += int(tmp)  
        tmp = '' 
    }
    tmp += c
}
l += int(tmp) 
println(l)
\end{lstlisting}  
 
However, this is a problem, because we are using too much coding. 
Real developers abhor extra coding. 
The idea is to generate a state machine model based on lexer, in which case, 
the best idea is to use the $tokens()$ function :  \index{tokens()}

\begin{lstlisting}[style=zmbStyle]
tokens( <string> , <regex> , 
match-case = [true|false] ) 
// returns a matcher object 
tokens ( <string> , <regex> , match-case = [true|false] ) as { anon-block }
// calls the anon-block for every  matching group  
\end{lstlisting}  
  
Thus, using this, we can have:  
  
\begin{lstlisting}[style=zmbStyle]
// what to do : string : regex 
l = tokens(s, '\d+') as { int($.o) }
\end{lstlisting}  

and we are done. That is precisely how code is meant to be written.  
  
\end{subsection}

\begin{subsection}{hash() function}
\index{hash()}
Sometimes it is important to generate hash from a string. To do so :

\begin{lstlisting}[style=zmbStyle]
h = hash('abc')
// h  := 900150983cd24fb0d6963f7d28e17f72
\end{lstlisting}  

It defaults to "MD5", so :

\begin{lstlisting}[style=zmbStyle]
hash( 'MD5' ,'abc')
// h := 900150983cd24fb0d6963f7d28e17f72
\end{lstlisting}  

They are the same. One can obviously change the algorithm used :

\begin{lstlisting}[style=zmbStyle]
hash([algo-string , ] <string> )
\end{lstlisting}  

There are these two specific algorithms that one can use to convert to and from base 64 encoding. 
They are ``e64'' to encode and ``d64'' to decode. Thus, to encode a string in the base 64 :
\index{hash : base 64}

\begin{lstlisting}[style=zmbStyle]
h = hash('e64', 'hello, world')
//h := aGVsbG8sIHdvcmxk
//And to decode it back :
s = hash('d64', 'aGVsbG8sIHdvcmxk' )
// s:= "hello, world"
// url encode and decode 
s = hash('eu', string) 
hash('du', s) // decode 
\end{lstlisting}  

\end{subsection}

\begin{subsection}{Anonymous Comparators}
\index{ anonymous comparator }
Ordering requires to compare two elements. 
The standard style of Java comparator is to code something like this :

\begin{lstlisting}[style=zmbStyle]
def compare( a, b ) {
    if ( a < b ) return -1 
    if ( a > b ) return  1
    return 0 
}
// or in short, with sign function :
def compare(a,b) {  sign(a - b) } 
\end{lstlisting}  
Obviously, one needs to define the relation operator accordingly.
For example, suppose we have  student objects :

\begin{lstlisting}[style=zmbStyle]
student1 = { 'name' : 'Anakin' , 'id' : 42  }
student2 = { 'name' : 'Mace' , 'id' : 24  }
\end{lstlisting}  

And we need to compare these two.
We can choose a particular field, say ``id'' to compare these :

\begin{lstlisting}[style=zmbStyle]
def compare( a, b ) {
    if ( a.id < b.id ) return -1 
    if ( a.id > b.id ) return  1
    return 0 
}
\end{lstlisting}  

Or, there is another way to represent the same thing :
 
\begin{lstlisting}[style=zmbStyle]
def compare( a, b ) {
    ( a.id < b.id )
}
\end{lstlisting}  

In ZoomBA compare functions can be of both the types.
Either one choose to generate a triplet of $( -1, 0, 1 )$,
or one can simply return $true$ when $left < right$ and false
otherwise. The triplet is not precisely defined, 
that is, when the comparator returns an integer :

\begin{enumerate}
\item{ The $ result < 0  \implies left < right $  }
\item{ The $ result = 0  \implies left = right $  }
\item{ The $ result > 0  \implies left > right $  }
\end{enumerate}

when it returns a boolean however, the code is :

\begin{lstlisting}[style=zmbStyle]
cmp = comparator(a,b)
if ( cmp ) {
  println( 'a<b' )
}else{
  println( 'a >= b' )
}
\end{lstlisting}  

In the subsequent section anonymous comparators would be used heavily.
The basic syntax of these functions are :

\begin{lstlisting}[style=zmbStyle]
order_function(args... ) where { /* anonymous comparator block */ } 
\end{lstlisting}  

As always $\$.0$ is the left argument $(a)$ and $\$.1$ is the right argument $(b)$.
Thus, one can define the suitable comparator function to be used in order.
\end{subsection}


\begin{subsection}{Sort Functions}
\index{sorting}

Sorting is trivial:
\index{sorta()}\index{sortd()}
\begin{lstlisting}[style=zmbStyle]
cards = ['B','C','A','D' ]
sa = sorta(cards) // ascending 
// sa := [A, B, C, D]
sd = sortd(cards) //descending 
// sd := [D, C, B, A]
\end{lstlisting}  


Now, sorting is anonymous block ( function ) ready, 
hence we can sort on specific attributes. 
Suppose we want to sort a list of complex objects, like a Student with Name and Ids.
And now we are to sort based on "name" attribute: \index{sorting : custom comparator }

\begin{lstlisting}[style=zmbStyle]
students = [ {'roll' : 1, 'name' : 'X' } , 
             {'roll' : 3, 'name' : 'C' } , 
             {'roll' : 2, 'name' : 'Z' } ]
sa = sorta(students) where { $[0].name < $[1].name } 
/* sa := [{roll=3, name=C}, {roll=1, name=X}, 
        {roll=2, name=Z}] */
\end{lstlisting}  

Obviously we can do it using the roll too:

\begin{lstlisting}[style=zmbStyle]
sa = sorta(students) where { $[0].roll < $[1].roll } 
// sa := [{roll=1, name=X}, {roll=2, name=Z}, {roll=3, name=C}]
\end{lstlisting}  
\end{subsection}

\begin{subsection}{Heap}
A heap is an entirely different matter. Heap stores the `k' largest or smallest
entries in a collection that is given to it. This structure let's you find top `k' or
bottom `k' as  you wish. \emph{heap()} function in ZoomBA creates the 
\href{https://en.wikipedia.org/wiki/Heap\_(data\_structure)}{Heap}.


\index{ heap() }

\begin{lstlisting}[style=zmbStyle]
h = heap(3) // It is a min-heap, 3 elements  
for ( [0:10] ) { h += $ } // add stuff up : h<0|[ 2,1,0 ]> // ZHeap 
h.top // 0 // Integer 
\end{lstlisting}  

In case we want to create a max-heap :

\begin{lstlisting}[style=zmbStyle]
h = heap(3,true) // It is a max-heap, 3 elements  
for ( [0:10] ) { h += $ } // add stuff up : h<9|[ 7,8,9 ]> // ZHeap
h.top // 9 // Integer 
\end{lstlisting}  

One can, of course index into a heap :

\begin{lstlisting}[style=zmbStyle]
h[0] // 7 
h[-1] // 9 : last element 
\end{lstlisting}  

And finally, one can always use a comparator to create the Heap,
because that comparison is needed to compare elements:

\begin{lstlisting}[style=zmbStyle]
l = [  [1,2] , [4,1] , [2,10 ], [0,40] , [-1,-3] ] 
h = heap(2) where {  $.l.1 < $.r.1  }  
h += l // consume all of it 
h.top // @[-1,-3] is the top element 
\end{lstlisting}  

\end{subsection}

\begin{subsection}{Priority Queue}
While Heap is a neat structure for top-k, it is a specialisation of 
\href{https://en.wikipedia.org/wiki/Priority\_queue}{Priority Queue}.
In heap there are only some `k' items while in Priority Queue, there can be as many.
ZoomBA implementation wraps around Java 
\href{https://docs.oracle.com/javase/9/docs/api/java/util/PriorityQueue.html}{Priority Queue}.
\index{ pqueue() Priority Queue} 

\begin{lstlisting}[style=zmbStyle]
pq = pqueue() [ where { /* comparator function */ } ] 
\end{lstlisting} 

For example :

\begin{lstlisting}[style=zmbStyle]
l = [10,1,23,12,15,0,3]
pq = pqueue()
for ( l ) { pq += $ } // consume 
pq.peek // 0 
while ( !empty(pq) ){ println(pq.poll) } // 0 1 3 10 12 15 23
\end{lstlisting} 

\end{subsection}



\begin{subsection}{sum() function}
\index{sum()}
Sometimes it is of importance to generate \emph{sum}
of a collection. That is precisely what \emph{sum()} achieves :

\begin{lstlisting}[style=zmbStyle]
l = [0,-1,-3,3,4,10 ]
s = sum(l)
/*  sum := 9  */
\end{lstlisting}  

Obviously, the function takes anonymous function as argument,
thus, given we have :

\begin{lstlisting}[style=zmbStyle]
x = [ 2,3,1,0,1,4,9,13]
s = sum(x) as { $.o * $.o }
/* 281 */
\end{lstlisting}  
 
Thus, it would be very easy to define on what we need to sum over or min or max. 
Essentially the anonymous function must define a scalar to transfer the object into.

\end{subsection}

\begin{subsection}{minmax() function}
\index{minmax()}

It is sometimes important to find min and max of items which can not be cast into numbers directly. 
For example one may need to find if an item is within some range or not, and then finding min and max becomes important. 
Thus, we can have :

\begin{lstlisting}[style=zmbStyle]
x = [ 2,3,1,0,1,4,9,13]
#(m,M) = minmax(x)
// [0, 13]
\end{lstlisting}  

This also supports anonymous functions,thus :

\begin{lstlisting}[style=zmbStyle]
students = [ {'roll' : 1, 'name' : 'X' } , 
             {'roll' : 3, 'name' : 'C' } , 
             {'roll' : 2, 'name' : 'Z' } ]
#(m,M) =  minmax(students) where { $[0].name < $[1].name }
// [{roll=3, name=C}, {roll=2, name=Z}]
\end{lstlisting}  

\end{subsection}


\begin{subsection}{shuffle() function}
\index{shuffle()}
In general, testing requires shuffling of data values. 
Thus, the function comes handy:

\begin{lstlisting}[style=zmbStyle]
cards = [ 'A', 'B' , 'C' , 'D' ] 
// inline shuffling 
happened = shuffle(cards)
// happened := true , returns true/false stating if shuffled
cards
// order changed : [D, A, C, B]
\end{lstlisting}  

\end{subsection}

\begin{subsection}{The random() function}
The $random()$ function is multi-utility function.
With no arguments it returns a 
\href{https://docs.oracle.com/javase/8/docs/api/java/security/SecureRandom.html}{SecureRandom} :

\begin{lstlisting}[style=zmbStyle]
sr = random() // ZRandom instance, a SecureRandom implementation
\end{lstlisting}  

$random()$ function can be used in selecting a single value at random from a collection :
\index{random() : select one}
\begin{lstlisting}[style=zmbStyle]
l = [0,1,2,3,4]
rs = random(l) // rs is a randomly selected element 
\end{lstlisting}  

It can also be used to select a random sub collection from a collection,
we just need to pass the number of elements we want from collection :
\index{random() : select many}

\begin{lstlisting}[style=zmbStyle]
a = [0,1,2,3,4]
rs = random(a,3) // pick 3 items at random without replacement 
// works on strings too....just to array 
rs = random("abcdefghijklmpon".toCharArray ,10) 
\end{lstlisting}  

Given a seed data type, it generates the next random value from 
the same data type. Behold the behaviour: \index{random() : random value with types }

\begin{center}\begin{minipage}{\linewidth}
\begin{lstlisting}[style=zmbStyle]
rand_bool_value = random(true) // generates random boolean 
random_within_0_9 = random(10) // a random integer between 0 to 9
\end{lstlisting}  
\end{minipage}\end{center}

Random instance supports multitudes of other randomized functions like :

\index{random.num(), random.string() }
\begin{lstlisting}[style=zmbStyle]
r = random() // get the random instance 
r.string("\\d+", 10 ) //string matching regex with min size 10 
r.string("[abc]+", 10, 20 ) // string matching regex with min size 10 , max 20 
// now various numeric ones 
r.num() // next random integer
r.num(0.0) // next double
seed = 1
r.num(seed.longValue()) // gets a long random number 
r.num( "2" ) // 2 size large integer as random 
r.num(true) // Next Gaussian 
\end{lstlisting}

\end{subsection}

\end{section}

\begin{section}{Errors in ZoomBA}
ZoomBA was written as a glue language. Therefore, utmost care is taken to report any error, in a precise manner.
For reference, we have used this file called \emph{t.zm} for all the examples.

\begin{subsection}{Unknown Variable Error}

Let's start with a very simple error :

\begin{lstlisting}[style=zmbStyle]
println(x) // x is not defined
\end{lstlisting}

You would see the error comes in as :

\begin{lstlisting}[style=all]
zoomba.lang.core.types.ZException$Variable: x :  --> 
/Codes/zoomba/image_scraper/t.zm at line 1, cols 9:9
|- println --> /Codes/zoomba/image_scraper/t.zm at line 1, cols 1:10
\end{lstlisting}

This is the most common type of error one is going to get. 
\end{subsection}


\begin{subsection}{Unknown Property Error}

In a dynamic language, this is the next error one gets:

\begin{lstlisting}[style=zmbStyle]
obj = { 'a' : 10 }
println( obj.x ) // but there is no property called 'x' !!!
\end{lstlisting}

You would see the error comes in as :

\begin{lstlisting}[style=all]
zoomba.lang.core.types.ZException$Property: 
 Object {a=10} does not have property 'x' of class java.lang.String :  
 --> /Codes/zoomba/image_scraper/t.zm at line 2, cols 14:14
|- println --> /Codes/zoomba/image_scraper/t.zm at line 2, cols 1:16
\end{lstlisting}
Note that the error also tries to showcase what all properties the object has. 
\end{subsection}

\begin{subsection}{Function Errors}

This happens, when you are calling a function that does not exist:

\begin{lstlisting}[style=zmbStyle]
val = 10 
func( val ) // where is this function?
\end{lstlisting}

The error says that there is no such function:
\begin{lstlisting}[style=all]
zoomba.lang.core.types.ZException$Function: func :  
--> /Codes/zoomba/image_scraper/t.zm at line 2, cols 1:11
|- func --> /Codes/zoomba/image_scraper/t.zm at line 2, cols 1:11
\end{lstlisting}
It is simply stating that the function does not exist. Now, there can be issue with parameter passing, as in below:

\begin{lstlisting}[style=zmbStyle]
def func( x ){
  println(x)
}
func() // parameter is not passed!
\end{lstlisting}

Related error is:

\begin{lstlisting}[style=all]
zoomba.lang.core.types.ZException$Function: 
parameter 'x' is not assigned!: in method : func : 
 --> /Codes/zoomba/image_scraper/t.zm at line 4, cols 1:6
|- func --> /Codes/zoomba/image_scraper/t.zm at line 4, cols 1:6
\end{lstlisting}
Wait a bit here. It is OK to have a function where we use implicit args \emph{@ARGS}, 
but not ok to have a parameter and then not using it.

Another related errr is method mismatch, which happens because ZoomBA is a dynamic language:

\begin{lstlisting}[style=zmbStyle]
s ="foobar"
s.foo()
\end{lstlisting}

Clearly, no such method \emph{foo()} exists in the String class and hence, the error comes to be:
\begin{lstlisting}[style=all]
zoomba.lang.core.types.ZException$Function: foo : no such method! : 
foo : in class : class java.lang.String : 
 --> /Codes/zoomba/image_scraper/t.zm at line 2, cols 3:7
|- foo --> /Codes/zoomba/image_scraper/t.zm at line 2, cols 3:7
\end{lstlisting}

What happens when a method exists, but the parameters do not match?

\begin{lstlisting}[style=zmbStyle]
s ="foobar"
s.toString("boohahah")
\end{lstlisting}

Clearly, no such method \emph{toString(String )} exists in the String class and hence, the error comes to be:
\begin{lstlisting}[style=all]
zoomba.lang.core.types.ZException$Function: toString : args did not match any known method! : 
toString : in class : class java.lang.String : 
 --> /Codes/zoomba/image_scraper/t.zm at li
ne 2, cols 3:20
|- toString --> /Codes/zoomba/image_scraper/t.zm at line 2, cols 3:20
\end{lstlisting}


\end{subsection}

\begin{subsection}{Stack Trace}
While we are at functions, functions can and do call other functions. 
\begin{lstlisting}[style=zmbStyle]
def funky(z){
  println(y)
}

def func( x ){
  funky( x )
}
func(42)

\end{lstlisting}
In this case, ZoomBA shows the stack trace:

\begin{lstlisting}[style=all]
zoomba.lang.core.types.ZException$Variable: y :  
--> /Codes/zoomba/image_scraper/t.zm at line 2, cols 11:11
|- println --> /Codes/zoomba/image_scraper/t.zm at line 2, cols 3:12
|- funky --> /Codes/zoomba/image_scraper/t.zm at line 6, cols 3:12
|- func --> /Codes/zoomba/image_scraper/t.zm at line 8, cols 1:8
\end{lstlisting}

Even a for iterated loop comes under this scheme, that is because for-iterator is actually a function, 
calling Java's \emph{hasNext()} and \emph{next()} over an iterator.

\end{subsection}

\begin{subsection}{Arithmetic Errors}
Naturally, there can be arithmetic errors, when some operator we use on some object, that does not support it.
Starting with the trivial :

\begin{lstlisting}[style=zmbStyle]
q = 0
p = 42
println( p/q )
\end{lstlisting}

which is classic divide by zero, culminates in:

\begin{lstlisting}[style=all]
zoomba.lang.core.types.ZException$ArithmeticLogicOperation: 
Invalid Arithmetic Logical Operation [/] : 
 --> /Codes/zoomba/image_scraper/t.zm at line 3, cols 12:12 
 --> ( Can not do operation ( DIVISION ) : ( 42 ) with ( 0 ) ! )
|- println --> /Codes/zoomba/image_scraper/t.zm at line 3, cols 1:14
\end{lstlisting}

\end{subsection}

\begin{subsection}{LValue Error}
What happens when we try to assign value to a constant term?
\begin{lstlisting}[style=zmbStyle]
12 = 42
\end{lstlisting}
this culminates in :
\begin{lstlisting}[style=all]
zoomba.lang.core.types.ZException$Variable: 
Assignment not possible into ASTNumberLiteral :  
--> /Codes/zoomba/image_scraper/t.zm at line 1, cols 1:2
\end{lstlisting}

\end{subsection}

\begin{subsection}{Stack OverFlow }
Pretty common to do:
\begin{lstlisting}[style=zmbStyle]
def fact( n ){
    if ( n == 1 ) return 1 
    n * fact( n  ) // missed the : n - 1
}
fact(2)
\end{lstlisting}

this will produce :

\begin{lstlisting}[style=all]
zoomba.lang.core.types.ZException$Function: StackOverFlow: in method : fact : 
 --> /Codes/zoomba/image_scraper/t.zm at line 3, cols 9:18
|- fact --> /Codes/zoomba/image_scraper/t.zm at line 3, cols 9:18
|- fact --> /Codes/zoomba/image_scraper/t.zm at line 3, cols 9:18
|- fact --> /Codes/zoomba/image_scraper/t.zm at line 3, cols 9:18
|- fact --> /Codes/zoomba/image_scraper/t.zm at line 3, cols 9:18
|- fact --> /Codes/zoomba/image_scraper/t.zm at line 3, cols 9:18
.... <many lines of repeat>
\end{lstlisting}

\end{subsection}


\begin{subsection}{Null Error}
From Java standpoint, pretty common to reference null:
\begin{lstlisting}[style=zmbStyle]
x = null
println( x.method() ) // imagine there is actually something
\end{lstlisting}

this will auto-panic :

\begin{lstlisting}[style=all]
Panic --> [  --> /Codes/zoomba/image_scraper/t.zm at line 2, cols 12:19 ] 
caused by : zoomba.lang.core.types.ZAssertionException: null reference : x
|- println --> /Codes/zoomba/image_scraper/t.zm at line 2, cols 1:21
\end{lstlisting}
Null is treated differently, hence, a Panic gets generated, because it is curable.
\end{subsection}

\end{section}

\begin{section}{Debugging}
It is essential to have debug support in a language. ZoomBA is no exception. ZoomBA supports debugging programmatically.
What does that mean? It means, the breakpoint can be programmatically introduced to the script itself, or manually inserted.

\begin{subsection}{Simple Breakpoint}

\begin{lstlisting}[style=zmbStyle]
x = 10
#bp  // here is how we add a breakpoint as statement!
println(x)
\end{lstlisting}
when we run this with the interpreter ( or even programmatically ) this will happen:

\begin{lstlisting}[style=all]
============= With Power, Comes Responsibility ========
BreakPoint hit:  --> /Codes/zoomba/image_scraper/t.zm at line 2, cols 1:3
============= Please use Power, Carefully..... ========
//h brings this help.
//h key_word : shows help about the keyword
//q quits REPL. In debug mode runs till next BreakPoint
//v shows variables.
//c In debug mode, clear this Breakpoint.
//r loads and runs a script from REPL.
Enjoy ZoomBA...(0.1-beta5-SNAPSHOT-2019-Feb-01 16:58)
(zoomba)
\end{lstlisting}
We are now in the debugger. In here, we can fire any expression, check any variables, upto this point:

\begin{lstlisting}[style=all]
(zoomba)//v
context : {
x => 42 /* class java.lang.Integer */
}
\end{lstlisting}

\end{subsection}

\begin{subsection}{Conditional Breakpoint}
What happens when you want to put a condition on the breakpoint?
\begin{lstlisting}[style=zmbStyle]
x = int(@ARGS[0]) // from command line 
#bp  empty(x) //  breakpoint when x is empty ! need to check how it happened...
println(x)
\end{lstlisting}
when we run this with the interpreter ( or even programmatically ) this will happen:
Syntax of the conditional break point is \emph{ \#bp  [condition] }. 

\end{subsection}

\end{section}




