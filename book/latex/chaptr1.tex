\chapter{About ZoomBA}\label{ZoomBA}

{\LARGE A} philosophy is needed to guide the design of any system. ZoomBA is not much different.
Here we discuss the rationale behind the language, and showcase how it is distinct from its first cousin Java.
While has similarity to \href{https://en.wikipedia.org/wiki/Scala\_(programming\_language)}{Scala} 
should not come as a surprise, that is an example of convergent evolution. \index{scala}
But just as every modern animal is highly evolved, and there is really no general purpose animal,
we sincerely believe there is no real general purpose language. All languages are special purpose, 
some special purposes may seem generic in nature. 
However, $ZoomBA$ preaches a different philosophy than $scala, kotlin$ and even $python$.

\begin{section}{ZoomBA Philosophy}
\index{ZoomBA : philosophy}

To begin with, our own experience in Industry is aptly summarised by \href{https://en.wikipedia.org/wiki/Node.js}{Ryan Dahl}
in \href{http://harmful.cat-v.org/software/node.js}{here} , the creator of Node.js :

\begin{center}
\emph{
I hate almost all software. It's unnecessary and complicated at almost every layer. At best I can congratulate someone for quickly and simply solving a problem on top of the shit that they are given. The only software that I like is one that I can easily understand and solves my problems. The amount of complexity I'm willing to tolerate is proportional to the size of the problem being solved...(continued)...
Those of you who still find it enjoyable to learn the details of, say, a programming language - being able to happily recite off if NaN equals or does not equal null - you just don't yet understand how utterly fucked the whole thing is. If you think it would be cute to align all of the equals signs in your code, if you spend time configuring your window manager or editor, if put unicode check marks in your test runner, if you add unnecessary hierarchies in your code directories, if you are doing anything beyond just solving the problem - you don't understand how fucked the whole thing is. No one gives a fuck about the glib object model.
\emph{The only thing that matters in software is the experience of the user.}   - Ryan Dahl
}
\end{center}

Thus, ZoomBA comes with it's tenets, which are :
\index{ZoomBA : tenets}

\begin{enumerate}
\item{ Reduce the number of lines in the code; \emph{Principle of Percimony} }
\item{ If possible, in every line, reduce the number of characters; }
\item{ Get out of the cycle of bugs and fixes by writing scientific and provable code \emph{Provability} 
    ( see  \href{http://en.wikipedia.org/wiki/Minimum_description_length}{Minimum Description length} ).
\item{The final statement is : Good code is only once written, and then forever forgotten, i.e. : limiting to 0 maintenance. }    
\emph{Maintainability is lack of need to Maintain} } \index{tenet : good code}
\end{enumerate}
That is, 
\begin{center}
     \emph{To boldly go where no developer has gone before - attaining Nirvana in terms of coding}
\end{center}     
Thus we made ZoomBA so that a language exists with it's full focus on \emph{Business Process Automation \& Validation},
not on commercial fads that sucks the profit out of business. Hence it has one singular focus in mind : \emph{brevity} but not at the cost of maintainability. What can be done with 10 people, in 10 days, get it done in 1 day by one person.

It is being demonstrated by firms adapting to it.  

\end{section}

\begin{section}{Design Features}
\index{design}
Here is the important list of features, which make ZoomBA a first choice of the business developers and software testers, alike.

\begin{subsection}{ZoomBA is Embeddable}

ZoomBA scripts are easy to be invoked as stand alone scripts, also from within java code,
thus making integration of external logic into proper code base easy. Thus Java code
can call ZoomBA scripts very easily, and all of ZoomBA functionality is programmatically
accessible by Java caller code. This makes it distinct from Scala, where it is almost impossible
to call scala code from Java. Lots of code of how to call ZoomBA can be found
in the \href{https://gitlab.com/non.est.sacra/ZoomBA/tree/master/lang/src/test/java/com/noga/ZoomBA/lang}{test} directory.
Many scripts are there as samples in \href{https://gitlab.com/non.est.sacra/ZoomBA/tree/master/lang/samples}{samples} folder.
In chapter \ref{java-connectivity} we would showcase how to embed ZoomBA in Java code.
\end{subsection}

\begin{subsection}{Interpreted by JVM}

ZoomBA is interpreted by a program written in Java, and uses Java runtime.
This means that ZoomBA and Java have a common runtime platform.
You can easily move from Java to ZoomBA and vice versa.

\end{subsection}

\begin{subsection}{ZoomBA can Execute any Java Code}

ZoomBA enables you to use all the classes of the Java SDK's in ZoomBA, and also your own, custom Java classes, or your favourite Java open source projects. There are trivial ways to load a jar from path directly from ZoomBA script, and then loading a class as as trivial
as importing the class.

\end{subsection}


\begin{subsection}{ZoomBA can be used Functionally}

ZoomBA is also a functional language in the sense that every function is a value and because every value is an object so ultimately every function is an object. Functions are first class citizens.

ZoomBA provides a lightweight syntax for defining anonymous functions, it supports higher-order functions, it allows functions to be
\href{https://en.wikipedia.org/wiki/Nested_function}{nested}, and supports \href{https://en.wikipedia.org/wiki/Currying}{currying} and \href{https://en.wikipedia.org/wiki/Closure_(computer_programming)}{Closures}. 
It also supports \href{https://en.wikipedia.org/wiki/Operator_overloading}{Operator Overloading}. 

\end{subsection}

\begin{subsection}{ZoomBA is Dynamically Typed}
ZoomBA, unlike statically typed languages, does not expect you to provide type information.
You don't have to specify a type in most cases, and you certainly don't have to repeat it.

\end{subsection}

\begin{subsection}{ZoomBA Vs Java}

Most of the time one can treat ZoomBA as a very tiny shorthand for incredibly fast programming using JVM.
However, ZoomBA has a set of features, which completely differs from Java. Some of these are:

\begin{enumerate}

\item{ All types are objects. An assignment of the form $x = 1 $ makes actually an Integer object in JVM, not a int. }

\item { Type inference :  by that one does not need to cast a variable to a type before accessing its functions. 
    This makes reflective calls intuitive, as $ a.b.f() $ can be written as $ a['b'].f() $ and thus, the value of $'b'$
    itself can come from another variable. Objects are treated like property buckets, as in Dictionaries.  
}

\item { Nested Functions : functions can be nested :

\begin{center}\begin{minipage}{\linewidth}
\begin{lstlisting}[style=zmbStyle]
def parent_function( a ) {
    def child_function (b) {
        // child can use parents args: happily.
    	a + b 
    }
    if ( a != null ) {
      return child_function 
   }
}
\end{lstlisting}
\end{minipage}\end{center}

}
\item { Functions are objects, as the above example aptly shows, they can be returned, and assigned : 

\begin{lstlisting}[style=zmbStyle]
fp = parent_function(10) 
r = fp(32 ) // result will be 42.
\end{lstlisting}
}

\item{ Closures : the above example demonstrates the closure using partial functions, and this is something that is
syntactically new to Java land.}

\end{enumerate}

\end{subsection}

\end{section}

\begin{section}{Setting up Environment}

\begin{subsection}{Installing Java Run Time}

 You need to install Java runtime 1.8 ( 64 bit ) at minimum ( It supports all the way up to Java 11 ) 
 To test whether or not you have successfully installed it try this 
 in your command prompt :

\begin{lstlisting}[style=all]
    $ java -version
    java version "1.8.0_60"
    Java(TM) SE Runtime Environment (build 1.8.0_60-b27)
    Java HotSpot(TM) 64-Bit Server VM (build 25.60-b23, mixed mode)
\end{lstlisting}

\end{subsection}

\begin{subsection}{Download ZoomBA one jar}

Download the latest one-jar.jar file from \href{https://oss.sonatype.org/content/repositories/snapshots/org/zoomba-lang/zoomba.lang.core/0.1-beta5-SNAPSHOT/}{here}.

\end{subsection}

\begin{subsection}{Add to Path}

If you are using \*nix platform, then you should create an alias :

\begin{lstlisting}[style=all]
     alias zmb="java -jar ZoomBA.lang.core-0.1-<time-stamp>-onejar.jar"
\end{lstlisting}

in your .login file.

If you are using Windows, then you should create a batch file that looks like this:

\begin{lstlisting}[style=all]
     @echo off
     rem init.cmd: to be run on every login  
     doskey zmb=java -jar ZoomBA.lang.core-0.1-<time-stamp>-onejar.jar  $*
\end{lstlisting}

and then follow the steps as shown
\href{https://stackoverflow.com/questions/17404165/how-to-run-a-command-on-command-prompt-startup-in-windows}{here}.

\end{subsection}

\begin{subsection}{Test Setup}

Open a command prompt, and type :

\begin{lstlisting}[style=all]
$ZoomBA
//h brings this help.
//h key_word : shows help about the keyword 
//q quits REPL. In debug mode runs till next BreakPoint 
//v shows variables. 
//c In debug mode, clear this Breakpoint. 
//r loads and runs a script from REPL.
Enjoy ZoomBA...(0.1-beta5-SNAPSHOT-2019-Jan-01 13:58)
(zoomba)
\end{lstlisting}     

It should produce the prompt of \href{https://en.wikipedia.org/wiki/Read?eval?print_loop}{REPL} of (ZoomBA).
\index{ZoomBA : repl}

\end{subsection} 

\begin{subsection}{Maven Setup for Java Integration}
In the dependency section (latest release is 0.1-beta4, dated ) : 

\begin{lstlisting}[style=XmlStyle]
<dependency>
  <groupId>org.zoomba-lang</groupId>
  <artifactId>zoomba.lang.core</artifactId>
  <version>0.1-beta4</version> <!-- or 0.1-beta5-SNAPSHOT -->
</dependency>
\end{lstlisting}

That should immediately make your project a ZoomBA supported one.
\end{subsection} 


\begin{subsection}{Setting up Editors}
\index{Sublime Text}\index{Vim}
IDEs are good - and that is why we have minimal editor support, \href{http://www.sublimetext.com}{Sublime Text} is my favourite one. 
You also have access to the syntax highlight file for zoomba and a specially made theme for zoomba editing - ( ES ) both of them can be found :   \href{https://gitlab.com/non.est.sacra/zoomba/blob/master/ide_settings_files/}{here}.
There is also a vim syntax file.
If you use them with your sublime text editor - then typical zoomba script file looks like this : 

\begin{figure}
\begin{center}
\leavevmode
\includegraphics[scale=0.3]{sublime.jpeg}
\end{center}
\caption{Using Sublime Text}
\label{fig_2_1}
\end{figure}

To include for vim :

Create these two files :

\begin{lstlisting}[style=all]
    $HOME/.vim/ftdetect/zmb.vim
    $HOME/.vim/syntax/zmb.vim
\end{lstlisting}

For most \*nix systems it would be same as :

\begin{lstlisting}[style=all]
    mkdir -p ~/.vim/ftdetect/
    touch ~/.vim/ftdetect/zmb.vim 
    touch ~/.vim/syntax/zmb.vim 
\end{lstlisting}

Now on the \$HOME/.vim/ftdetect/zmb.vim  file, put this line :

\begin{lstlisting}[style=all]
    autocmd BufRead,BufNewFile *.zmb,*.z, *.zm  set filetype=zmb
\end{lstlisting}

Note that you should not have blanks between commas.
And then, copy the content of the \href{https://gitlab.com/non.est.sacra/zoomba/blob/master/ide_settings_files/zoomba.vim}{vim syntax file here} in the \$HOME/.vim/syntax/zmb.vim file as is.

If everything is fine, you can now open zmb scripts in vim!

\begin{figure}
\begin{center}
\leavevmode
\includegraphics[scale=0.66]{vim.png}
\end{center}
\caption{Using Vim (MacVim)}
\label{fig_2_1}
\end{figure}

\end{subsection} 

\begin{subsection}{The Proverbial ``Hello, World''}
In any editor of your choice, save this line in a file `hello.zmb' :
\index{Hello, World}
\begin{lstlisting}[style=zmbStyle]
println('Hello, World!')
\end{lstlisting}
and go to the command prompt, and run :
\begin{lstlisting}[style=all]
$ zmb hello.zmb
Hello, World
$
\end{lstlisting}
and that would be the proverbial starting code.
\end{subsection} 

\end{section}




