\chapter{Functional Style}\label{functional-style}

{\LARGE F}unctional style is a misnomer, in essence it boils down to some tenets:
\index{functional style}
\begin{enumerate}
\item{ Functions as first class citizens, they can be used as variables. }
\item{ Avoid modifying objects by calling methods on them. Only method calls returning create objects. }
\item{ Avoid conditional statements, replace them with alternatives. }
\item{ Avoid explicit iteration, replace them with recursion or other 
\href{https://en.wikipedia.org/wiki/Higher-order_function}{higher order functions}. }
\end{enumerate}


\begin{section}{Functions : In Depth}

As the functional style is attributed to functions, in this section
we would discuss functions in depth.

\begin{subsection}{Function Types}
ZoomBA has 3 types of functions.
\begin{enumerate}
\item{Explicit Functions :
\index{function : explicit}
This functions are defined using the \emph{def} keywords.
They are the \emph{normal} sort of functions, which everyone is aware of.
As an example take this :

\begin{lstlisting}[style=zmbStyle]
def my_function( a, b ){ a + b }
\end{lstlisting}
}

\item{Anonymous Functions :
\index{function : anonymous}
This functions are defined as a side-kick to another function, 
other languages generally calls them Lambda functions, but they do very specific 
task, specific to the host function. 
All the collection comprehension functions takes them :
\begin{lstlisting}[style=zmbStyle]
l = list([0:10] ) as { $.o ** 2}
// same with a name-less function ( against anonymous )
l = list([0:10]) as def(_index, _item,_partial,_context) {  _item** 2 } 
// note that , all arguments are optional, so :
 l = list([0:10]) as def() { item = @ARGS[1] ; item **2  } // works too 
\end{lstlisting}
}

\item{Implicit Functions :
\index{function : implicit}
This functions are not even functions, they are the whole script body, 
to be treated as functions. Importing a script and calling it 
as a function qualifies as one : 
\begin{lstlisting}[style=zmbStyle]
import 'foo' as FOO
FOO()
\end{lstlisting}
}
\end{enumerate}

\end{subsection}

\begin{subsection}{Default Parameters}
\index{function : default parameters}

Every defined function in ZoomBA is capable of taking default values
for the parameters. For example :

\begin{lstlisting}[style=zmbStyle]
// default values passed 
def my_function( a = 40 , b = 2 ){ a + b }
// call with no parameters :
println ( my_function() ) // prints 42 
println ( my_function(10) ) // prints 12 
println ( my_function(1,2) ) // prints 3 
\end{lstlisting}
Note that, one can not mix the default and non default arbitrarily.
That is, all the default arguments must be specified from the right side.
Thus, it is legal :
\begin{lstlisting}[style=zmbStyle]
// one of the default values passed 
def my_function( a, b = 2 ){ a + b }
\end{lstlisting}
But it is not :
\begin{lstlisting}[style=zmbStyle]
// default values passed in the wrong order
def my_function( a = 10 , b ){ a + b }
\end{lstlisting}
\end{subsection}

\begin{subsection}{Named Arguments}
\index{functions : named args } 
In ZoomBA, one can change the order of the parameters passed,
provided one uses the named arguments.
See the example:

\begin{lstlisting}[style=zmbStyle]
def my_function( a , b  ){ printf('(a,b) = (%s ,%s)\n' ,a , b ) }
my_function(1,2)  // prints (a,b) = (1,2) 
my_function(b=11,a=10) //  prints (a,b) = (10,11) 
\end{lstlisting}
Note that, named args can not be mixed with unnamed args,
that is, it is illegal to call the method this way :

\begin{lstlisting}[style=zmbStyle]
// only one named values passed 
my_function(b=11,10) //  illegal 
\end{lstlisting}

\end{subsection}


\begin{subsection}{Arbitrary Number of Arguments}
\index{function : \_\_args\_\_ }\index{ function : arbitrary no. of args }
Every ZoomBA function can take arbitrary no of arguments.
To access the arguments, one must use the $@ARGS$ construct, as shown :

\begin{center}\begin{minipage}{\linewidth}
\begin{lstlisting}[style=zmbStyle]
// this can take any no. of arguments 
def my_function(){ 
   // access the arguments, they are collection  
   s = str ( @ARGS , '#' )
   println(s)  
}
my_function(1,2,3,4)
/* prints 1#2#3#4 */
\end{lstlisting}
\end{minipage}\end{center}


This means that, when a function expects $n$
parameters, but is only provided with $m < n $ parameters,
the rest of the expected  parameters not passed is passed as $null$.
\index{function : unassigned parameters}

\begin{lstlisting}[style=zmbStyle]
// this can take any no. of arguments, but named are 3 
def my_function(a,b,c){ 
   // access the arguments, they are collection  
   s = str ( @ARGS , '#' )
   println(s)  
}
my_function(1,2)
/* prints 1#2#null */
\end{lstlisting}

In the same way when a function expects $n$
parameters, but is provided with $m > n $ parameters,
the rest of the passed  parameters can be accessed by the $@ARGS$ construct
\index{function : excess parameters} which was the first example.

\end{subsection}

\begin{subsection}{Arguments Overwriting}

How to generate permutations from a list of object with 
$ ^nP_r$ ? Given a list $l$ of size $3$ We did $ ^3P_3$ :
\index{permutation}

\begin{center}\begin{minipage}{\linewidth}
\begin{lstlisting}[style=zmbStyle]
l = ['a','b','c' ]
perm_3_from_3 = join(l,l,l) where { #|set($.o)| == #|$.o| }
l = ['a','b','c' ,'d' ]
perm_4_from_4 = join(l,l,l,l) where { #|set($.o)| == #|$.o| }
perm_2_from_4 = join(l,l) where { #|set($.o)| == #|$.o| }
\end{lstlisting}
\end{minipage}\end{center}

Thus, how to generate the general permutation?
As we can see, the arguments to the permutation 
is always varying. To fix this problem, 
\emph{argument overwriting} was invented.
That, in general, all the arguments to a function 
can be taken in runtime from a collection.
\index{function : argument overwriting}

\begin{lstlisting}[style=zmbStyle]
l = ['a','b','c' ,'d' ]
// this call overprintlns the args :
perm_2_from_4 = join( @ARGS = [l,l] ) where{ #|set($.o)| == #|$.o| }
\end{lstlisting}

Thus, to collect and permutate $r$ elements from a list $l$ is :
 
\begin{lstlisting}[style=zmbStyle]
perm_r = join(@ARGS = list([0:r]) as {l}) where{ #|set($.o)| == #|$.o| }
\end{lstlisting}

and we are done. This is as declarative as one can get.

\end{subsection}

\begin{subsection}{Recursion}
\index{function : recursion}
It is customary to introduce recursion with \href{https://en.wikipedia.org/wiki/Factorial}{factorial}.
We would not do that, we would introduce the concept delving the very heart of the foundation of mathematics, 
by introducing \href{https://en.wikipedia.org/wiki/Peano_axioms}{Peano Axioms}.
\index{function : Peano Axioms}
Thus, we take the most trivial of them all : Addition is a function that maps two natural numbers (two elements of N) to another one. 
It is defined recursively as:

\begin{lstlisting}[style=zmbStyle]
/* successor function */
def s( n ){
    if ( n == 0 ) return 1 
    return  ( s(n-1) + 1 )  
}
/* addition function */
def add(a,b){
  if ( b == 0 ) return a
  return s ( add(a,b-1) )
}
\end{lstlisting}

These functions do not only show the trivial addition in a non trivial manner, 
it also shows that the natural number system is recursive.
Thus, a system is recursive if and only if it is in 1-1 correspondence with the natural number system.
Observe that the function \emph{add()} does not even have any addition symbol anywhere!
This is obviously cheating, because one can not use minus operator to define plus operation.
A better less cheating solution is to use function composition and power operator :
$a+b := s(...s(s(a)))$ b times, that is $s^b(a)$, which we would discuss later.

Now we test the functions:

\begin{center}\begin{minipage}{\linewidth}
\begin{lstlisting}[style=zmbStyle]
println(s(0)) // prints 1 
println(s(1)) // prints 2
println(s(5)) // prints 6
println(add(1,0)) // prints 1
println(add(1,5)) // prints 6
println(s(5,1)) // prints 6
\end{lstlisting}
\end{minipage}\end{center}

Now, obviously we can do factorial :
\index{factorial}
\begin{lstlisting}[style=zmbStyle]
def fact(n){
   if ( n <= 0 ) return 1 
   return n * fact( n - 1 )
}
\end{lstlisting}

\end{subsection}

\begin{subsection}{Closure}
\index{function : closure}

A \href{https://en.wikipedia.org/wiki/Closure_(computer_programming)}{closure} 
is a function, whose return value depends on the value of one or more variables declared outside this function. 
Consider the following piece of code with anonymous function:

\begin{lstlisting}[style=zmbStyle]
multiplier = def(i) { i * 10 }
\end{lstlisting}

Here the only variable used in the function body, $i * 0$, is $i$, which is defined as a parameter to the function. 
Now let us take another piece of code:

\begin{lstlisting}[style=zmbStyle]
multiplier = def (i) {  i * factor } 
\end{lstlisting}

There are two free variables in multiplier: $i$ and \emph{factor}. One of them, $i$, is a formal parameter to the function. 
Hence, it is bound to a new value each time multiplier is called. 
However, \emph{factor} is not a formal parameter, then what is this? 
Let us add one more line of code:

\begin{lstlisting}[style=zmbStyle]
factor = 3
multiplier = def (i) {  i * factor } 
\end{lstlisting}

Now, factor has a reference to a variable outside the function but in the enclosing scope. Let us try the following example:

\begin{lstlisting}[style=zmbStyle]
  println ( multiplier(1) ) // prints 3 
  println ( multiplier(14) ) // prints 42 
\end{lstlisting}
  
Above function references factor and reads its current value each time. 
If a function has no external references, then it is trivially closed over itself. No external context is required.

\end{subsection}

\begin{subsection}{Partial Function}
\index{function : nested, partial}
Partial functions are functions which lets one implement closure, w/o 
getting into the global variable way. Observe :

\begin{lstlisting}[style=zmbStyle]
// this shows the nested function 
def func(a){
    // here it is : note the name-less-ness of the function
    r = def(b){ // which gets assigned to the variable "r"
       printf("%s + %s ==> %s\n", a,b,a+b)
    }
    return r // returning a function 
}
// get the partial function returned 
x = func(4)
// now, call the partial function 
x(2)
//finally, the answer to life, universe and everything :
x = func("4")
x("2")
\end{lstlisting}
This shows nested functions, as well as partial function.
\end{subsection}

\begin{subsection}{Functions as Parameters : Lambda}
\index{function : as parameter, lambda }

From the theory perspective, lambdas are defined in \href{https://en.wikipedia.org/wiki/Lambda_calculus}{Lambda Calculus}. 
As usual, the jargon guys made a fuss out of it, but as of now, we are using lambdas all along, for example :

\begin{lstlisting}[style=zmbStyle]
list(1,2,3,4) as { $.o ** 2 }
\end{lstlisting}
 
The $\{ \$.o ** 2 \}$ is a lambda. The parameter is implicit albeit, because it is meaningful that way. However, sometimes we need to pass named parameters. Suppose we now want to create a function composition, first step would be to apply it to a single function :

\begin{lstlisting}[style=zmbStyle]
 def  apply ( param , a_function ){
     a_function(param)
 }
\end{lstlisting} 
So, suppose we want to now apply arbitrary function :
\begin{lstlisting}[style=zmbStyle]
 apply( 10, def(a){ a** 2 }  )
\end{lstlisting}
And now, we just created a lambda! The result of such an application would make apply function returning 100.
\end{subsection}


\begin{subsection}{Composition of Functions}
\index{function : composition}
To demonstrate a composition, observe :

\begin{lstlisting}[style=zmbStyle]
def compose (param,  a_function , b_function ){
    // first apply a to param, then apply b to the result 
    // b of a 
    b_function ( a_function( param ) )
 }
\end{lstlisting}
 
Now the application :

\begin{lstlisting}[style=zmbStyle]
 compose( 10, def(a){ a ** 2 } ,  def(b){ b - 58 } )
\end{lstlisting}
 
As usual, the result would be 42!

Now, composition can be taken to extreme ... this is possible due to the arbitrary length argument passing :

\begin{lstlisting}[style=zmbStyle]
def compose(value, func_list){
    for ( f : func_list ){
       value = f(@ARGS=[value])
    }
    value // return it 
}
\end{lstlisting}
 
Formally this is a potential infinite function composition! Thus, this call :

\begin{lstlisting}[style=zmbStyle]
// note the nameless-ness :)
r = compose(6, [def (a){ a** 2} , def (b){ b + 6 }] )
println(r)
\end{lstlisting}

generates, \href{https://www.google.co.in/search?client=safari&rls=en&q=the+answer+to+life,+universe,+and+everything&ie=UTF-8&oe=UTF-8&gfe\_rd=cr&ei=hHDIVsv-BsGL8QedmLnwDg}{the answer to life, universe, and everything}, as expected.

\end{subsection}

\begin{subsection}{Operation on Function}
\index{ function : composition operation}

Functions support composition to generate a new function :
The operator for \href{https://en.wikipedia.org/wiki/Function_composition}{function composition} :
$$
\#< f | g > ( x ) := g( f(x) )  
$$

Let's have a simple demonstration :



\begin{center}\begin{minipage}{\linewidth}
\begin{lstlisting}[style=zmbStyle]
def f(){
    println(str(@ARGS,'#'))
    [ @ARGS[0] ** 2 ]
}
def g(){
    println(str(@ARGS,'#'))
    [ @ARGS[0] - 2 ]
}
h = #< f | g > // g of f   
println( h(10) )
\end{lstlisting}
\end{minipage}\end{center}
The result is :
\begin{lstlisting}[style=all]
10
100
@[ 98 ]
\end{lstlisting}
\end{subsection}

\begin{subsection}{Eventing}
\index{function : eventing }
All ZoomBA functions are Eventing ready.
What are events? For all practical purposes, 
an event is nothing but a hook before or after a method call.
This can easily be achieved by the default handlers for ZoomBA functions.
See the object  
\href{https://gitlab.com/non.est.sacra/ZoomBA/blob/master/lang/src/main/java/com/noga/ZoomBA/lang/extension/oop/ScriptClassBehaviour.java}{Event}.

Here is a sample demonstration of the eventing:
\index{function : eventing : before}\index{function : eventing : after}
\index{event : args} \index{event : when}\index{event : result}\index{event : error}
\begin{center}\begin{minipage}{\linewidth}
\begin{lstlisting}[style=zmbStyle]
def my_func(){
    printf('ARGS : %s %n', str(@ARGS,'#'))
}
def event_hook(event){
    printf('%s %s %n', event.method.name , event.when )
}
my_func.before += event_hook 
my_func.after += event_hook 
my_func('hello', 'world')
\end{lstlisting}
\end{minipage}\end{center}

When one runs it, this is what we will get :

\begin{lstlisting}[style=all]
my_func BEFORE
ARGS : hello#world
my_func AFTER
\end{lstlisting}

\end{subsection}


\end{section}

\begin{section}{Strings as Functions : Currying}

All these idea started first with 
Alan \href{https://en.wikipedia.org/wiki/Turing\_machine}{Turing's Machine}, \index{Turing Machine}
and then the 3rd implementation of a Turing Machine, whose innovator Von Neumann said data is same as executable code. Read more on : 
\index{ Von Neumann Architecture}
\href{https://en.wikipedia.org/wiki/Von\_Neumann\_architecture}{Von Neumann Architecture}.
Thus, one can execute arbitrary string, and call it code, if one may. That brings in how functions are actually executed, or rather what are functions.

\begin{subsection}{Rationale}

The idea of Von Neumann is situated under the primitive notion of alphabets as symbols, and the intuition that any data is representable by a finite collections of them. The formalisation of such an idea was first done by Kurt Godel, and bears his name in 
\href{http://demonstrations.wolfram.com/Goedelization/}{G{\"o}delization}. \index{G{\"o}delization}

For those who came from the Computer Science background, thinks in terms of data as binary streams, which is a general idea of 
\href{https://en.wikipedia.org/wiki/Kleene\_star}{Kleene Closure} : $\{0,1\}^*$. 
Even in this form, data is nothing but a binary String.

Standard languages has String in code. In `C' , we have ``string'' as ``constant char*''. C++ gives us std:string , 
while Java has ``String''. ZoomBA uses Java String. But, curiously, the whole source code, the entire JVM assembly listing can be treated as a String by it's own right! So, while codes has string, code itself is nothing but a string, which is suitable interpreted by a machine, more generally known as a Turing Machine. For example, take a look around this :

\begin{lstlisting}[style=all]
 (zoomba)println('Hello, World!')
\end{lstlisting}

This code printlns `Hello, World!' to the console. From the interpreter perspective, it identifies that it needs to call a function called println, and pass the string literal ``Hello, World!'' to it. But observe that the whole line is nothing but a string.

This brings the next idea, that can strings be, dynamically be interpreted as code? When interpreter reads the file which it wants to interpret, it reads the instructions as strings. So, any string can be suitable interpreted by a suitable interpreter, and thus data which are strings can be interpreted as code.

\end{subsection}

\begin{subsection}{Minimum Description Length}
\index{Minimum Description Length}

With this idea, we now consider this. Given a program is a String, by definition, can program complexity be measured as the length of the string proper? Turns out one can, and that complexity has a name, it is called : \href{https://en.wikipedia.org/wiki/Kolmogorov\_complexity}{Chatin Solomonoff Kolmogorov Complexity}.
\index{Chatin Solomonoff Kolmogorov Complexity}

The study of such is known as 
\href{https://en.wikipedia.org/wiki/Algorithmic\_information\_theory}{Algorithmic Information Theory} 
and the goal of a software developer becomes to println code that reduces this complexity.

As an example, take a look about this string :

\begin{lstlisting}[style=all]
(zoomba)s = 'ababababababababababababababababab'
=>ababababababababababababababababab  ## String
(zoomba)#|s|
=>34 ## Integer
\end{lstlisting}

Now, the string 's' can be easily generated by :

\begin{lstlisting}[style=all]
(zoomba)s1 = 'ab'**17
=>ababababababababababababababababab  ## String 
(zoomba)s == s1
=>true  ## Boolean
\end{lstlisting}

Thus, in ZoomBA, the minimum description length of the string 's' is : 8 :

\begin{lstlisting}[style=all]
(zoomba)code="'ab'**17"
=>'ab'**17  ## String 
(zoomba)#|code|
=>8  ## Integer
\end{lstlisting}

\end{subsection}


\begin{subsection}{Examples}

First problem we would take is that of comparing two doubles to a fixed decimal place. Thus:

\begin{lstlisting}[style=all]
(zoomba)x=1.01125
=>1.01125
(zoomba)y=1.0113
=>1.0113
(zoomba)x == y
=>false
\end{lstlisting}

How about we need to compare these two doubles with till 4th digit of precision (rounding) ? 
How it would work? Well, we can use String format :

\begin{lstlisting}[style=all]
(zoomba)str("%.4f",x)
=>1.0113
(zoomba)str("%.4f",y)
=>1.0113
\end{lstlisting}

But wait, the idea of precision, that is ".4" should it not be a parameter? 
Thus, one needs to pass the precision along, something like this :

\begin{lstlisting}[style=all]
(zoomba)c_string = "%%.%df"  ## This is the original one
=>%%.%df
## apply precision, makes the string into a function 
(zoomba)p_string = str(c_string,4)
=>%.4f
## apply a number, makes the function evaluate into a proper value
(zoomba)str(p_string,y)
=>1.0113  # and now we have the result!
\end{lstlisting}

All we really did, are bloated string substitution, and in the end, that produced what we need. 
Thus in a single line, we have :

\begin{lstlisting}[style=all]
(zoomba)str(str(c_string,4),x)
=>1.0113
(zoomba)str(str(c_string,4),y)
=>1.0113
\end{lstlisting}

In this form, observe that the comparison function takes 3 parameters :
\begin{enumerate}
\item{ The precision, int, no of digits after decimal }
\item{ Float 1 }
\item{ Float 2 }
\end{enumerate}

as the last time, but at the same time, the function is in effect generated by application of partial functions, one function taking the precision as input, generating the actual format string that would be used to format the float further. These sort of taking one parameter at a time and generating partial functions or rather string as function is known as \href{https://en.wikipedia.org/wiki/Currying}{Currying}, 
immortalized the name of \href{https://en.wikipedia.org/wiki/Haskell\_Curry}{Haskell Curry}, 
another tribute to him is the name of the pure functional language 
\href{https://en.wikipedia.org/wiki/Haskell\_(programming\_language)}{Haskell}.

Now to the 2nd problem.
Suppose the task is given to verify calculator functionality. 
A Calculator can do `+' , `-', `*' , ... etc all math operations. 
In this case, how one proposes to println the corresponding test code?
The test code would be, invariably messy :

\begin{lstlisting}[style=zmbStyle]
if ( operation == '+' ) {
    do_plus_check();
} else if ( operation == '-' ) {
    do_minus_check();
}
// some more code ...
\end{lstlisting}

In case the one is slightly smarter, the code would be :

\begin{center}\begin{minipage}{\linewidth}
\begin{lstlisting}[style=zmbStyle]
switch ( operation ){
    case '+' :
       do_plus_check(); break;
    case '-' :
       do_minus_check(); break;
    ...
 }
\end{lstlisting} 
\end{minipage}\end{center}

The insight of the solution to the problem is finding the following :

\begin{center}
We need to test something of the form we call a "binary operator" is working "fine" or not:
$$
operand_1 \; \;  <operator> \; \;  operand_2 
$$
\end{center}

That is a general binary operator. If someone can abstract the operation out - and replace the operation with the symbol - 
and then someone can actually execute that resultant string as code (remember JVMA?) the problem would be solved.
This is facilitated by the back-tick operator ( executable strings) :
\index{currying : back tick operator}

\begin{lstlisting}[style=all]
(zoomba)c_string = #'#{a} #{op} #{b}'
=>#{a} #{op} #{b}
(zoomba)a=10
=>10
(zoomba)c_string = #'#{a} #{op} #{b}'
=>10 #{op} #{b}
(zoomba)op='+'
=>+
(zoomba)c_string = #'#{a} #{op} #{b}'
=>10 + #{b}
(zoomba)b=10
=>10
(zoomba)c_string = #'#{a} #{op} #{b}'
=>20
\end{lstlisting}
\end{subsection}

\begin{subsection}{Reflection}
\index{currying : reflection}

Calling methods can be accomplished using currying. 

\begin{lstlisting}[style=zmbStyle]
import 'java.lang.System.out' as out 
def func_taking_other_function( func ){
    #"#{func}( 'hello!' )"
}
func_taking_other_function('out.println')
\end{lstlisting}
The $func$ is just the name of the function, not the function object at all.
Thus, we can use this to call methods using reflection.
\end{subsection}

\begin{subsection}{Referencing}
\index{currying : referencing}

Let's see how to have a reference like behaviour in ZoomBA.

\begin{lstlisting}[style=all]
(zoomba)x = [1,2]
@[1, 2]
(zoomba)y = { 'x' : x }
{x=@[ 1,2 ]} 
(zoomba)x = [1,3,4]
@[1, 3, 4]
(zoomba)y.x // x is not updated
@[1, 2]
\end{lstlisting}

Suppose you want a different behaviour, and that can be achieved using Pointers/References. 
What you want is this :

\begin{center}\begin{minipage}{\linewidth}
\begin{lstlisting}[style=all]
(zoomba)x = [1,2]
@[1, 2]
(zoomba)y = { 'x' : 'x'  }
{x=x}
(zoomba)#'#{y.x}' // access as in currying stuff
@[1, 2]
(zoomba)x = [1,3,4]
@[1, 3, 4]
(zoomba)#'#{y.x}' // as currying stuff, always updated
@[1, 3, 4]
\end{lstlisting}
\end{minipage}\end{center}

So, in effect we are using a dictionary to hold name of a variable, instead of having a hard variable reference, 
thus, when we are dereferencing it, we would get back the value if such a value exists!

\end{subsection}

\end{section}

\begin{section}{Avoiding Conditions}
\index{conditions : avoiding}

As we can see the tenets of functional programming says to avoid conditional statements.
In the previous section, we have seen some of the applications, how to get rid of the conditional statements.
In this section, we would see in more general how to avoid conditional blocks.


\begin{subsection}{Theory of the Equivalence Class}
\index{Equivalence Class Partitioning}

Conditionals boils down to $if-else$ construct.
Observe the situation for a valid date in the format of $ddMMyyyy$.

\begin{center}\begin{minipage}{\linewidth}
\begin{lstlisting}[style=zmbStyle]
is_valid_date(d_str){
   num = int(d_str) 
   days = num/1000000
   rest = num % 1000000
   mon = rest /10000
   year = rest % 10000 
   max_days = get_days(mon,year)  
   return (  0 < days && days <= max_days  && 
             0 < mon && mon < 13 && 
             year > 0 )      
}
get_days(month,year){
   if ( month == 2  and leap_year(year){
       return 29
   }
   return days_in_month[month] 
}
days_in_month = [31,28,31,... ]
\end{lstlisting}
\end{minipage}\end{center}


This code generates a decision tree. The leaf node of the decision tree are
called \href{https://en.wikipedia.org/wiki/Equivalence\_partitioning}{Equivalent Classes}, 
their path through the source code are truly independent of one another.  
Thus, we can see there are 4 equivalence classes for the days:

\begin{enumerate}
\item{Months with 31 days}
\item{Months with 30 days}
\item{Month with 28 days : Feb - non leap}
\item{Months with 29 days : Feb -leap }
\end{enumerate}
 
And the $if - else$ simply ensures that the correct path is being taken
while reaching the equivalent class. Observe that for two inputs belonging 
to the same equivalent class, the code path remains the same.
That is why they are called equivalent class.

Note from the previous subsection, that the days of the months were simply stored
in an array. That avoided some unnecessary conditional blocks.
Can it be improved further? Can we remove all the other conditionals too?
That can be done, by intelligently tweaking the code.
Observe, we can replace the ifs with this :

\begin{lstlisting}[style=zmbStyle]
get_days(month,year){
   max_days = days_in_month[month] + 
   ( (month == 2 and leap_year(year) )? 1 : 0 )
}
\end{lstlisting} 
But some condition can never be removed even in this way.
\end{subsection}

\begin{subsection}{Dictionaries and Functions}
\index{function : in a dictionary}
Suppose we are given the charter to verify a sorting function.
Observe that we have already verified it, but this time, 
it comes with a twist, sorting can be both ascending and descending.

So, the conditions to verify ascending/descending are :

\begin{center}\begin{minipage}{\linewidth}
\begin{lstlisting}[style=zmbStyle]
sort_a = !exists(collection) where { $.i > 0 and $.c[$.i - 1 ] > $.o } 
sort_d = !exists(collection) where { $.i > 0 and $.c[$.i - 1 ] < $.o }  
\end{lstlisting} 
\end{minipage}\end{center}


Note that both the conditions are the same, except the switch of $>$ in the ascending 
to  $<$ in the descending. How to incorporate such a change?
The answer lies in the dictionary and currying :

\begin{lstlisting}[style=zmbStyle]
op  = { 'ascending' : '>' , 'descending' : '<'  }
sorted = !exists(collection) where { 
    $.i > 0 and #'$.c[$.i - 1 ] #{op} $.o' }
\end{lstlisting} 

In this form, the code written is absolutely generic and devoid of any explicit conditions.
Dictionaries can be used to store functions. 

\end{subsection}

\begin{subsection}{An Application : FizzBuzz}
We already aquatinted ourselves with FizzBuzz.
here, is a purely conditional version:
\index{FizzBuzz : conditional}
\begin{lstlisting}[style=zmbStyle]
/* Pure If/Else */
def fbI(range){
  for ( i : range ){
      if ( i % 3 == 0 ){
          if ( i % 5 == 0 ){ 
              println('FizzBuzz') 
          } else {
              println('Fizz') 
          }
      }else{
          if ( i % 5 == 0 ){
             println('Buzz') 
            }else{
               println(i) 
            }
      }
  }
}
\end{lstlisting} 
However, it can be written in a purely declarative way.
Here is how one can do it, Compare this with the other one:
\index{FizzBuzz : declarative}
\begin{lstlisting}[style=zmbStyle]
def fbD(range){
  d = { 0 : 'FizzBuzz' , 
        3 : 'Fizz' , 
        5 : 'Buzz' ,  
        6 : 'Fizz', 
       10 : 'Buzz' , 
       12 : 'Fizz' }
  for ( x : range ){
       r = x % 15  
       continue ( r @ d ){ println( d[r] ) }
       println(x)  
  }
}
\end{lstlisting}

\end{subsection}

\end{section}

\begin{section}{Avoiding Iterations}

The last tenet is avoiding loops, and thus in this section we would discuss how to avoid loops.
We start with some functions we have discussed, and some functionalities which we did not. 

\begin{subsection}{Range Objects in Detail}
\index{range()} \index{range : date} \index{range : char }
The \emph{for} loop becomes declarative, the moment we put a range in it.
Till this time we only discussed part of the range type, only the numerical one.
We would discuss the $Date$ and the $Symbol$ type range.

A date range can be established by :

\begin{center}\begin{minipage}{\linewidth}
\begin{lstlisting}[style=zmbStyle]
d_start = time()
d_end = d_start + "PD10"
// a range from current to future 
d_range = [d_start : d_end ]
// another range with 2 days spacing 
d_range_2 = [d_start : d_end : 2 ]
\end{lstlisting}
\end{minipage}\end{center}

and this way, we can iterate over the dates or rather time.
The spacing is to be either an integer, in which case it would be 
taken as days, or rather in string as the \href{https://en.wikipedia.org/wiki/ISO_8601#Time_intervals}{ISO-TimeInterval-Format}.
The general format is given by : \index{range : chrono interval format} $PyYmMwWdDThHmMsS$.

The other one is the sequence of symbols, the Symbol range \index{range : symbol} :
\begin{lstlisting}[style=zmbStyle]
s_s = "A"
s_e = "Z" 
// symbol range is inclusive
s_r = [s_s : s_e ]
// str field holds the symbols in a string 
s_r.str == 'ABCDEFGHIJKLMNOPQRSTUVWXYZ' // true 
\end{lstlisting}

All the range types have some specific functions.

\index{range: list(), reverse(), inverse() }
\begin{lstlisting}[style=zmbStyle]
r = [0:5]
// a list of the elements  
r.list()
// reverse of the same [a:b] 
r.reverse() // [4, 3, 2, 1, 0]
// inverse of the range : reversed into [b:a] 
r.inverse() // [5:0:-1]  
\end{lstlisting}

There is the `XRange'. X Range stands for extended range,
which goes beyond standard size of JVM.
This can be created by using long types for one or more end points of a range :

\index{range: xrange }
\begin{lstlisting}[style=zmbStyle]
xr = [0L:5000000000000000000L] // yep, huge range 
xrr = [0D:10D:0.00000001D]  // precise range 
\end{lstlisting}

It is obvious that $list()$ and related functions may or may not be possible with $XRange$.

\end{subsection}


\begin{subsection}{The Fold Functions}
\index{fold functions}
The rationale of the fold function is as follows :

\begin{lstlisting}[style=zmbStyle]
def do_something(item){ /* some code here */ }
for ( i : items ){
   do_something(i)
}
// or use fold
fold(items) as { do_something($.o) }
\end{lstlisting}
The idea can be found here in a more \href{https://en.wikipedia.org/wiki/Fold\_(higher-order\_function)}{elaborative} way.
We must remember that the partial exists, and the general fold function is defined as :
\index{fold(), lfold(),rfold()}
$fold()$ folds from left, so it is also called $lfold()$.

\begin{center}\begin{minipage}{\linewidth}
\begin{lstlisting}[style=zmbStyle]
// folds from left side of the collection 
lfold(items[, seed_value ]) as { /* body */ }
// folds from right side of the collection 
rfold(items[, seed_value ]) as { /* body */ }
\end{lstlisting}
\end{minipage}\end{center}


First we showcase what right to left means in case of $rfold()$ :

\begin{lstlisting}[style=zmbStyle]
items = [0,1,2,3,4]
rfold( items ) as { printf( '%s ', $.o) }
\end{lstlisting}
This prints :
\begin{lstlisting}[style=all]
4 3 2 1 0 
\end{lstlisting}



Let us showcase some functionalities using fold functions,
we start with factorial : \index{fold : factorial}

\begin{lstlisting}[style=zmbStyle]
fact_n = lfold( [2:n+1] , 1 ) as { $.p * $.o  }
\end{lstlisting}

We move onto find Fibonacci : \index{fold : Fibonacci}

\begin{lstlisting}[style=zmbStyle]
fib_n = fold([0:n+1],[0,1]) as { #(p, c) = $.p; [c, p + c]  }
\end{lstlisting}

We now sum the elements up: \index{fold : sum}

\begin{lstlisting}[style=zmbStyle]
sum_n = rfold([2:n+1], 0) as { $.p + $.o }
\end{lstlisting}

Generate a set from a list : \index{fold : set}

\begin{lstlisting}[style=zmbStyle]
my_set = lfold( [1,2,1,2,3,4,5], set() ) as { $.p += $.o }
\end{lstlisting}

Finding min/max of a collection : \index{fold : min,max}

\begin{lstlisting}[style=zmbStyle]
l = [1,8,1,2,3,4,5] 
#(min,max) = lfold( l , [l.0, l.0] ) as { 
                 #(m,M)  = $.p 
                 continue( $.o > M ){ $.p = [ m, $.o] }   
                 continue( $.o < m ){ $.p = [ $.o, M] } 
                 $.p // return it off, when nothing matches   
              }
// (1,8)                    
\end{lstlisting}

Finding match index of a collection : \index{fold : index}

\begin{lstlisting}[style=zmbStyle]
inx = fold( [1,2,1,2,3,4,5] , -1 ) as { 
  break ( $.o  > 3 ){  $.p = $.i } ; $.p 
}
\end{lstlisting}

So, as we can see, the \emph{fold} functions are the basis of every other functions
that we have seen. We show how to replace select function using fold: 
\index{fold : select}
\begin{lstlisting}[style=zmbStyle]
// selects all elements less than than equal to 3
selected = fold( [1,2,1,2,3,4,5] , list() ) as { 
                continue ( $.o  > 3 )  
                $.p += $.o   
           }
\end{lstlisting}

To do an $index()$ function : \index{fold : index}

\begin{lstlisting}[style=zmbStyle]
// first find the element greater than 3
selected = fold( [1,2,1,2,3,4,5] , -1){ 
                break ( $.o  > 3 ) { $.i }
                $.p }
\end{lstlisting}

And to do an $rindex()$ function : \index{fold : rindex}

\begin{lstlisting}[style=zmbStyle]
// first find the element less than 3
selected = rfold( [1,2,1,2,3,4,5] , -1){ 
                break ( $.o  < 3 ) { $.i }   
                $.p }
\end{lstlisting}
\end{subsection}

\begin{subsection}{Matching}\index{match}\index{case}
There is \emph{if}, and there is \emph{else} and switch-case 
seems to be missing in ZoomBA. Fear not, the omniscient \emph{\#match} exists.
To demonstrate :

\begin{center}\begin{minipage}{\linewidth}
\begin{lstlisting}[style=zmbStyle]
// match  item 
item = 10 
selected = switch(item){ 
              case 1 : "I am one" 
              case @$ < 5 :  "I am less than 5"
              /* Default match is @$ itself, note that :
              inside match you must use block comments */
              case @$ : "yes, I could not match anything!"       
           }
\end{lstlisting}
\end{minipage}\end{center}

Observe these important things about match:

\begin{enumerate}
\item{ Match works with case expression after colon, evaluating to \emph{true} 
       or expression using \emph{Object.equals(expr,item)}. Thus, string 0 won't match `0'. 
       This is a design decision chosen to be compatible with switch case. To match such stuff, use \emph{ @\$ == expr },
       which would use ZoomBA expression evaluation. }
\item{ The only comments which are allowed are block comments. }
\item{ The \emph{@\$} signifies the item, and thus a \emph{case} like \emph{ case  @\$ } must be the last item. }
\item{ Any expression is permitted in the case statement, \emph{switch} returns the body value of the matching case.
       Thus, unlike ordinary switch-case, it returns a proper value. Thus, it is more succinct to put it 
       at the end of a choice function.
     }       
\end{enumerate}

\begin{subsection}{Sequences}

Iteration is key component of multitude of algorithms. Let's start with Factorial, Fibonacci, which are recursively defined.
We did find out how to do it with basic function style. Observe, the recursion pattern $F_{n + 1}=n  F_n$, with base case 
$F(0) = 1$. 

\index{seq()}
This is done by the \emph{seq()}  function in ZoomBA, which generates the iterator:

\begin{lstlisting}[style=zmbStyle]
factorial = seq( 1 ) as { size( $.item ) *  $.item[-1]  }
for ( i : [ 1 : 10  ] ){
 // Java Iterator: hasNext() and next() :: prints factorials of 1 to 9 
  printf( '%d %d %n' , i, factorial.next )
} 
println( factorial.history ) // shows the history till this point 
\end{lstlisting}
And that is how we can generate a never ending sequence of factorials.
Moreover, we can see the `history'. Formally, \emph{seq()} is a function that is capable
 of computing fixed point iteration : $X_{n+1} = F(X_n, X_{n-1}, ...) $, each of these $X_{n-i}$ can 
 be tracked by the \emph{\$.item } term, or history.
 
 While Factorial only use one past history term to calculate next, Fibonacci, uses 2:
$$
Fib_n = Fib_{n-1} + Fib_{n-2}
$$

with base cases $Fib(0) = Fib(1) = 1$ and a straightforward mapping is possible using sequences:

\begin{lstlisting}[style=zmbStyle]
fib = seq( 1 , 1 ) as {  $.item[-1] + $.item[-2]  }
for ( i : [ 2 : 10  ] ){
  printf( '%d %d %n' , i, fib.next )
} 
println( fib.history )
\end{lstlisting}

It concludes the sequences. One has to remember that a sequence is a never ending iterator.

\end{subsection}


\end{subsection}



\end{section}
