\chapter{Types and Conversions}\label{type-conversions}

{\LARGE T}ypes are not much useful for general purpose programming, save that they avoid errors.
Sometimes they are necessary, and some types are indeed useful because they let us do 
many useful stuff. In this chapter we shall talk about these types and how to convert
one to another.

The general form for type casting can be written :
\begin{lstlisting}[style=zmbStyle]
val = type_function(value, optional_default_value = null )
\end{lstlisting}

How does this work? The system would try to cast $value$
into the specific type. If it failed, and there is no default value, 
it would return $null$. However, if default is passed, it would return 
that when conversion fails.
This neat design saves a tonnage of $try \; ... \; catch$ stuff. 

\begin{section}{Integer Family}
This section is dedicated to the natural numbers family.
We have :
\begin{enumerate}
\item{bool : \href{https://docs.oracle.com/javase/8/docs/api/java/lang/Boolean.html}{Boolean} }
\item{int : Integer family, automatic size. }
\item{INT : \href{http://jscience.org/api/org/jscience/mathematics/number/LargeInteger.html}{LargeInteger} }
\end{enumerate}

Every type is under the hood is an object in ZoomBA, there is really no primitive here.

\begin{subsection}{Boolean}\index{bool()}
The syntax is :
\begin{lstlisting}[style=zmbStyle]
val = bool(value, optional_default_value = null )
val = bool(value, optional_matching_values[2])
\end{lstlisting}
Observe both in action :
\begin{center}\begin{minipage}{\linewidth}
\begin{lstlisting}[style=zmbStyle]
val = bool("hello") // val is null
val = bool("hello",false) // val is false
val = bool('hi', ['hi' , 'bye' ] ) // val is true 
val = bool('bye', ['hi' , 'bye' ] ) // val is false 
\end{lstlisting}
\end{minipage}\end{center}

\end{subsection}


\begin{subsection}{Integer}\index{int()}
This is very useful and the syntax is :

\begin{lstlisting}[style=zmbStyle]
val = int(value, optional_default_value = null )
\end{lstlisting}

Usage is :

\begin{lstlisting}[style=zmbStyle]
val = int("hello") // val is null
val = int("hello",0) // val is 0
val = int('42') // val is 42 
val = int(42) // val is 42 
val = int ( 10.1 ) // val is 10 
val = int ( 10.9 ) // val is 10 
val = int('100',2, null ) // val is 4, ( string, base, default ) 
\end{lstlisting}

\end{subsection}


\begin{subsection}{Arbitrary Large Integer}\index{INT()}
This is sometimes required, and the syntax is :

\begin{lstlisting}[style=zmbStyle]
val = INT(value, base=10, default_value = null )
\end{lstlisting}
Usage is :

\begin{lstlisting}[style=zmbStyle]
val = INT("hello") // val is null
val = INT('hi',10,42 )// base 10, default 42, val is 42
val = INT('42') // val is 42 
val = INT(54,13 ) // val is 42 
\end{lstlisting}
\end{subsection}

\end{section}


\begin{section}{Rational Numbers Family}
This section is dedicated to the floating point numbers family.
We have :
\begin{enumerate}
\item{float : minimal container for floating point. }
\item{FLOAT : \href{http://jscience.org/api/org/jscience/mathematics/number/Real.html}{Real} }
\end{enumerate}

\begin{subsection}{Float}\index{float()}
The syntax is :

\begin{lstlisting}[style=zmbStyle]
val = float(value, optional_default_value = null )
\end{lstlisting}

Usage is :

\begin{lstlisting}[style=zmbStyle]
val = float("hello") // val is null
val = float("hello",0) // val is 0.0
val = float('42') // val is 42.0 
val = float(42) // val is 42.0 
val = float ( 10.1 ) // val is 10.1 
val = float ( 10.9 ) // val is 10.9 
\end{lstlisting}
Note that, ZoomBA will automatically shrink floating point data into double,
given it can fit the precision in :

\begin{lstlisting}[style=zmbStyle]
val = 0.01 // val is a double type, automatic
\end{lstlisting}


\end{subsection}


\begin{subsection}{FLOAT}\index{FLOAT()}
This is sometimes required, and the syntax is :

\begin{lstlisting}[style=zmbStyle]
val = FLOAT(value,default_value = null )
\end{lstlisting}

Usage is :
\begin{center}\begin{minipage}{\linewidth}
\begin{lstlisting}[style=zmbStyle]
val = FLOAT("hello") // val is null
val = FLOAT('hi', 42 )// val is 42.0
val = FLOAT('42') // val is 42.0 
val = FLOAT(42.00001 ) // val is 42.00001
\end{lstlisting}
\end{minipage}\end{center}
\end{subsection}

\end{section}

\begin{section}{Generic Numeric Functions}

\begin{subsection}{num()}
\index{num()}\index{generic numeric conversion}
For generic numeric conversions, there is \emph{num()} function,
whose job is to convert data types into proper numeric type, 
with least storage. Thus :

\begin{lstlisting}[style=zmbStyle]
val = num(value,default_value)
\end{lstlisting}

Usage is :
\begin{center}\begin{minipage}{\linewidth}
\begin{lstlisting}[style=zmbStyle]
val = num("hello") // val is null
val = num('hi', 42 )// val is 42 int
val = num('42.00') // val is 42 int 
val = num(42.00001 ) // val is 42.00001 double 
val = num(42.000014201014 ) // val is double 
// val is Real
val = num(42.00001420101410801980981092101 )  
\end{lstlisting}
\end{minipage}\end{center}
\end{subsection}

\begin{subsection}{size()}
\index{size() : integers }
\emph{size()} function finds out digits length for an integer, in a given base:
\begin{lstlisting}[style=zmbStyle]
x = 100
size(x) // 100 in 'unary' base : imagine 100s of 1's written  
size(x,2) // 7, base 2 
size(x,10) // 3 , base 10 
size(x,16) // 2 , base 16 
\end{lstlisting}
\end{subsection}

\begin{subsection}{ Float to Integers}
\index{ floor() }
\index{ ceil() }
\index{ round() }
To convert floating points to integers or pure rationals there are 3 functions : \emph{floor(), ceil(), round() }
as shown below:
\begin{center}\begin{minipage}{\linewidth}
\begin{lstlisting}[style=zmbStyle]
// floor of f: max( all integers less than of equal to f ) 
 x = floor(1.1) // x = 1
 x = floor(0.0) // x = 0
 x = floor(-1.5) // x = -2 
 // ceil of f: min ( all integers greater than or equal to f )
 y = ceil(1.1) // x = 2 
 y = ceil(-1.1) // x = -1
 y = ceil(-1.0) // x = -1
 // now round function, with number of digit after decimal precision 
 round(0.5) // 0 digits, produces 1
round(0.51239,3) // 0.512 
round(0.51239,4) // 0.5124 
\end{lstlisting}
\end{minipage}\end{center}
These functions takes care of the sign of the number also, and adjusted.
\end{subsection}

\begin{subsection}{ Signs and Absolute }
\index{ sign() }

Sign function for any number is defined as :

\begin{lstlisting}[style=zmbStyle]
def _sign_(x ){
   if ( x < 0 ) return -1
   if ( x > 0 ) return 1 
   0 // else return 0 
}
// or we can use :
sign(x) // same as _sign_ function 
\end{lstlisting}

Absolute of a number is given by either the \emph{size()}  or the operator \emph{ \#|x|} 
to be read as mod-x. 
\index{ number : absolute }

\begin{lstlisting}[style=zmbStyle]
x = -100
#|x| // 100, absolute value of x 
size(x) // 100, same 
#|null| // 0, mod of nothing is 0 
size(null) // -1, shows that the container is un-initialized
\end{lstlisting}

\end{subsection}

\begin{subsection}{log()}
\index{ log() number }
\emph{log()} function takes logarithm of a number in Napier's base, by default, or if base is provided, on that base:

\begin{lstlisting}[style=zmbStyle]
x = 100
log(x) // 4.6051701859880918  Real
log(10) // 2.0 Double 
\end{lstlisting}

\end{subsection}


\end{section}


\begin{section}{The Chrono Family}
\index{time}\index{format : date and time}

Handling date and time has been a problem, that too with timezones.
ZoomBA simplifies the stuff.
We have Java 8 DateTime to handle date/time:

\begin{subsection}{time()}\index{time()}

This is how you create a DateTime \href{https://docs.oracle.com/javase/8/docs/api/java/time/LocalDateTime.html}{LocalDateTime}:
\begin{lstlisting}[style=zmbStyle]
val = time([ value, date_format , time_zone] )
\end{lstlisting}

With no arguments, it gives the current date time:

\begin{lstlisting}[style=zmbStyle]
today = time()
\end{lstlisting}

The default date format is $yyyyMMdd$, so :
\begin{lstlisting}[style=zmbStyle]
dt = time('20160218') // 2016-02-18T00:00:00.000+05:30
\end{lstlisting}

For all the date formats on dates which are supported, 
see \href{http://www.joda.org/joda-time/apidocs/org/joda/time/format/DateTimeFormat.html}{DateTimeFormat}.

Take for example :

\begin{lstlisting}[style=zmbStyle]
dt = time('2016/02/18', 'yyyy/MM/dd' ) 
// dt := 2016-02-18T00:00:00.000+05:30
dt = time('2016-02-18', 'yyyy-MM-dd' ) 
// dt := 2016-02-18T00:00:00.000+05:30
\end{lstlisting}
\end{subsection}

\begin{subsection}{Adjusting Timezones}
\index{ timezones }
The \emph{at()} function of the \emph{ZDate} object handles the timezones properly.
If you want to convert one time to another timezone, you need to give the time, 
and the \href{http://joda-time.sourceforge.net/timezones.html}{timezone}:

\begin{lstlisting}[style=zmbStyle]
dt = time()
dt_honolulu = dt.at( 'Pacific/Honolulu' ) 
// dt_honolulu := 2016-02-17T17:23:02.754-10:00 
dt_ny = dt.at(  'America/New_York' ) 
// dt_ny := 2016-02-17T22:23:02.754-05:00
\end{lstlisting}

There is a \emph{date()} function to convert into a \emph{Date} object in older Java, 
as well as timezone:

\begin{lstlisting}[style=zmbStyle]
t = time()
dt = t.date() // convert into Date
dt_honolulu = t.date( 'Pacific/Honolulu' ) // timezone and covert  
\end{lstlisting}

\end{subsection}


\begin{subsection}{Comparison on Chronos}\index{operator : compare, chrono }
All these date time types are freely mixable, 
and all comparison operations are defined with them.
Thus :

\begin{lstlisting}[style=zmbStyle]
t1 = time() 
thread().sleep(1000)  // wait for some time 
t2 = time()
// now compare 
c = ( t1 < t2 ) // true 
c = ( t2 > t1 ) // true 
\end{lstlisting}

Tow dates can be equal to one another, but not two instances,
that is a very low probability event, almost never.
Thus, equality makes sense when we know it is date, and not instant :

\begin{center}\begin{minipage}{\linewidth}
\begin{lstlisting}[style=zmbStyle]
t1 = time('19470815') // Indian Day of Independence  
t2 = time('19470815') // Indian Day of Independence  

// now compare 
c = ( t1 == t2 ) // true 
\end{lstlisting}
\end{minipage}\end{center}

\end{subsection}

\begin{subsection}{Arithmetic on Chronos}
\index{arithmetic : chorono}
Dates, obviously can not be multiplied or divided.
That would be a sin. However, dates can be added with other reasonable values, 
dates can be subtracted from one another, and $time()$ can be added or subtracted by days, months or even year.
More of what all date time supports, 
see the manual of \href{https://docs.oracle.com/javase/8/docs/api/java/time/LocalDateTime.html}{LocalDateTime}.

To add time to a date, there is another nifty method :

\begin{lstlisting}[style=zmbStyle]
d = time('19470815') // Indian Day of Independence  
time_delta_in_millis = 24 * 60 * 60 * 1000 // next day 
nd = time( d.time + time_delta_in_millis ) 
// nd := Sat Aug 16 00:00:00 IST 1947 
\end{lstlisting}

Same can easily be achieved by the \href{https://en.wikipedia.org/wiki/ISO\_8601\#Durations}{Durations} string :

\begin{lstlisting}[style=zmbStyle]
d = time('19470815') // Indian Day of Independence  
nd = d + 'P1D' // 1947-08-16T00:00:00.000+05:30 
\end{lstlisting}

And times can be subtracted :
\index{chrono : subtraction}

\begin{lstlisting}[style=zmbStyle]
d = time('19470815') // Indian Day of Independence  
nd = d + 'P1D' // 1947-08-16T00:00:00.000+05:30 
diff = nd - d 
//diff := PT24H ## Duration between two dates!
diff.toMillis // long number of millisecs
\end{lstlisting}

So we can see it generates \href{https://docs.oracle.com/javase/8/docs/api/java/time/Duration.html}{Duration} 
gap between two chrono instances. 

\end{subsection}

\end{section}

\begin{section}{String : Using str}
\index{str()}
Everything is just a string. It is. Thus every object should be able to be converted to and from out of strings.
There are types of strings in ZoomBA:

\begin{subsection}{Types of Strings}
There are 3 types of strings:
\begin{lstlisting}[style=zmbStyle]
general_string = "I am a string"
exec_string = #"2 + 3" // produces string 5 
sub_string = #"variable substitution with #{exec_string}" 
// append scripts folder path to the string  
relocable_string = _/"some_path/some/other/path" 
char = _"I" // character literal 
\end{lstlisting}
\end{subsection}


Converting an object to a string representation is called \href{https://en.wikipedia.org/wiki/Serialization}{Serialization},
and converting a string back to the object format is called \emph{DeSerialization}.
This is generally done in ZoomBA by the function $str()$.


\begin{subsection}{Null}
$str()$ never returns null, by design. Thus:
\begin{lstlisting}[style=zmbStyle]
s = str(null)
s == 'null' // true  
\end{lstlisting}
\end{subsection}


\begin{subsection}{Integer}
\index{str : int,INT}
For general integers family, $str()$ acts normally. However, it takes overload
in case of $INT()$ or $BigInteger$ :
\begin{lstlisting}[style=zmbStyle]
bi = INT(42)
s = str(bi) // '42'
s = str(bi,2) // base 2 representation : 101010
\end{lstlisting}
\end{subsection}

\begin{subsection}{Floating Point}
\index{str : float,double,DEC}
For general floating point family, $str()$ acts normally. 
However, it takes overload, which is defined as :

\begin{lstlisting}[style=zmbStyle]
precise_string = str( float_value, num_of_digits_after_decimal )
\end{lstlisting}

To illustrate the point:

\begin{lstlisting}[style=zmbStyle]
d = 101.091891011
str( d, 0 ) // 101 
str(d,1) // 101.1  
str(d,2) // 101.09 
str(d,3) // 101.092 
\end{lstlisting}

\end{subsection}

\begin{subsection}{Chrono}
\index{str : time }
Given a chrono family instance, $str()$ can convert them to a format of your choice.
These formats have been already discussed earlier, here they are again:
\href{https://docs.oracle.com/javase/8/docs/api/java/text/SimpleDateFormat.html}{SimpleDateFormat}.

The syntax is :

\begin{lstlisting}[style=zmbStyle]
formatted_chrono = str( chrono_value , chrono_format )
\end{lstlisting}

Now, some examples :

\begin{lstlisting}[style=zmbStyle]
t = time()
str( t ) // default is 'yyyyMMdd' : 20160218
str( t , 'dd - MM - yyyy' ) // 18 - 02 - 2016
str( t , 'dd-MMM-yyyy' ) // 18-Feb-2016 
\end{lstlisting}

\end{subsection}

\begin{subsection}{Collections}
\index{ str : collections }
Collections are formatted by str by default using `,'.
The idea is same for all of the collections, thus we would simply showcase some:

\begin{lstlisting}[style=zmbStyle]
l = [1,2,3,4]
s = str(l) // '1,2,3,4'
s == '1,2,3,4' // true 
s = str(l,'#') // '1#2#3#4'
s == '1#2#3#4' // true 
\end{lstlisting}

So, in essence, for $str()$, serialising the collection, the syntax is :

\begin{lstlisting}[style=zmbStyle]
s = str( collection [ ,  seperation_string ] )
\end{lstlisting}

\end{subsection}

\begin{subsection}{Generalised \emph{toString()} }
\index{ str : replacing toString() }

This brings to the point that we can linearise a collection of collections using $str()$.
Observe how:

\begin{lstlisting}[style=zmbStyle]
l = [1,2,3,4]
m = [ 'a' , 'b' , 'c' ] 
j = l * m // now this is a list of lists, really 
s_form = str(j, '&') as { str($.o,'#')  } // lineraize 
// This generates  1#a&1#b&1#c&2#a&2#b&2#c&3#a&3#b&3#c&4#a&4#b&4#c 
\end{lstlisting}

\end{subsection}

\begin{subsection}{JSON}
\index{ jstr() : json }\index{json : jstr() }
Given we have a complex object comprise of primitive types and collection types, 
the \emph{jstr()} function returns a JSON representation of the complex object.
This way, it is very easy to inter-operate with JavaScript type of languages.

\begin{center}\begin{minipage}{\linewidth}
\begin{lstlisting}[style=zmbStyle]
d = { 'a' : 10 , 'b' : 20  } 
s = jstr(d) // as json string
/*  { "a" : 10 , "b" : 20 }  */
// pretty print json string using ZoomBA native capability
jstr(d,false)   
jstr(d,true) // pretty print, using Jsoner capability 
\end{lstlisting}
\end{minipage}\end{center}
\index{ json string pretty print }
\end{subsection}

\begin{subsection}{Yaml}
\index{ ystr() : yaml }\index{yaml : ystr() }
Given we have a complex object comprise of primitive types and collection types, 
the \emph{ystr()} function returns a Yaml representation of the complex object.

\begin{center}\begin{minipage}{\linewidth}
\begin{lstlisting}[style=zmbStyle]
d = { 'a' : 10 , 'b' : 20  } 
s = ystr(d) // as Yaml string
/* Yaml String ...
a: 10
b: 20
*/
\end{lstlisting}
\end{minipage}\end{center}

\end{subsection}



\end{section}

\begin{section}{Playing with Types : Reflection}
It is essential for a dynamic language to dynamically 
inspect types, if at all. Doing so, is termed as 
\href{https://en.wikipedia.org/wiki/Reflection\_(computer\_programming)}{reflection}.
In this section, we would discuss different constructs that lets one move along the types.


\begin{subsection}{type() function} 
\index{type()}
The function $type()$ gets the type of an object.
In essence it gets the $getClass()$ of any 
\href{https://en.wikipedia.org/wiki/Plain\_Old\_Java\_Object}{POJO}.
Notice that the class object is loaded only once under a class loader,
so equality can be done by simply ``==''.
Thus, the following constructs are of importance:

\begin{center}\begin{minipage}{\linewidth}
\begin{lstlisting}[style=zmbStyle]
d = { : }
c1 = type(d) // c1 := java.util.HashMap
i = 42
c2 = type(i) // c2 := java.lang.Integer
c3 = type(20) // c3 := java.lang.Integer
c3 == c2 // true 
c1 == c2 // false 
\end{lstlisting}
\end{minipage}\end{center}

\end{subsection}

\begin{subsection}{The \emph{===} Operator}
\index{operator : === }
From the earlier section, suppose someone wants to 
equal something with another thing, under the condition
that both the objects are of the same type.
Under the tenet of ZoomBA which reads : \emph{ ``find a type at runtime, kill the type'' },
we are pretty open into the type business :

\begin{lstlisting}[style=zmbStyle]
a = [1,2,3] 
ca = type(a) // ZArray 
b = list(1,3,2)
cb = type(b) // ZList type 
a == b // true 
ca == cb // false 
// type equals would be :
ca == cb  && a == b // false  
\end{lstlisting}

But that is a long haul. One should be succinct,
so there is this \href{http://www.w3schools.com/js/js\_operators.asp}{borrowed operator from JavaScript},
known as ``==='' which let's its job in a single go :

\begin{lstlisting}[style=zmbStyle]
a = [1,2,3] 
b = list(1,3,2)
a === b // false 
c = [3,1,2]
c === a // true 
\end{lstlisting}
\end{subsection}

\begin{subsection}{The \emph{isa} operator}
\index{operator : isa }
Arguably, finding a type and matching a type is a tough job.
Given that we need to care about who got derived from whatsoever.
That is an issue a language should take care of, and for that reason, 
we do have $isa$ operator.

\begin{lstlisting}[style=zmbStyle]
null isa null // true 
a = [1,2,3] 
a isa [] // true : a is a type of ZArray?
a isa [0] // true : a is a type of ZArray?
b = list(1,3,2)
b isa 'list' // true : b is a type of list ? 
a isa b // false
{:} isa 'map'
true isa 'bool'
1 isa 'int'
1.0 isa 'float'
"abc".toCharArray() isa 'array'
\end{lstlisting}
This also showcase the issues of completely overhauling the type 
structure found in a JVM. The first two lines are significantly quirky.

There is a distinctly better way of handling $isa$, by using $pattern matcher$ strings,
strings that starts with $\#\#$ and has a type information as expression match: e.g. $\#\#map\#ig$ :

\begin{lstlisting}[style=zmbStyle]
b = list(1,3,2)
b isa ##list#ig // true : b is a type of list ?
{:} isa ##map#ig // true 
4.2 isa ##double##ig // the double
s = set(1,2,3)
s isa ##set#ig // true  
t = time() 
t is a ##date#ig // true : for ZDate
\end{lstlisting}
This regex also is case insensitive, and matches from anywhere, 
so be careful. 
\end{subsection}

\begin{subsection}{Field Access}
\index{reflection : field access}
Once we have found the fields to access upon, 
there should be some way to get to the field, other than this :

\begin{lstlisting}[style=zmbStyle]
d = time()
d.date().fastTime  // some value 
\end{lstlisting}

There is obvious way to access properties
as if the object is a dictionary, or rather than a property bucket:
\index{reflection : property bucket}
\begin{lstlisting}[style=zmbStyle]
d = time().date()
d['fastTime'] == d.fastTime  // true 
\end{lstlisting}
This opens up countless possibilities of all what one can do with this.
Observe however, monsters like spring and hibernate is not required 
given the property injection is this simple. 
\end{subsection}


\begin{subsection}{Nested Field Access}
\index{reflection : Nested field access}
For a simple field like this, it is good. But what happens when we need to access to nested properties?
For example, take this :

\begin{lstlisting}[style=zmbStyle]
m = { "a" : { "b" : 10 }, "c" : { "b" : 20 } }
\end{lstlisting}
Of course, it can actually be a proper java object ( we are simply using JSON map ).
How to access upto the level `m.a.b' ?  Or rather, how to get back the list of all nested fields "b" ?
This is the precise reason ZoomBA has \emph{xpath()} \index{xpath()} and \emph{xelem()} functions.

One can actually parameterize the access "m.a.b" using the \emph{xpath()} function:

\begin{lstlisting}[style=zmbStyle]
m = { "a" : { "b" : 10 }, "c" : { "b" : 20 } }
// from "m" get me the path b, followed by a property
v = xpath(m, "/a/b" )  // v := 10
v = xpath(m, "/a/x" )  // v := null
\end{lstlisting}

What about there are multiple matches? 
This is done by optional third argument sending a boolean `true' :

\begin{lstlisting}[style=zmbStyle]
// from "m" get me the path b, followed by a property, as list, true 
v = xpath(m, "//b", true )  // v := [10, 20]
\end{lstlisting}

But this pose a little problem of the setting property side of the things.
Moreover, the power to traverse the object tree wanes off with only values.
For this, there is the \emph{xelem()} function:

\begin{lstlisting}[style=zmbStyle]
// from "m" get me the path b, followed by a property, as list, true 
p = xelem(m, "/a/x" ) // p := /.[@name='a'][@name='x'] // DynamicPropertyPointer
\end{lstlisting}
Now we can readily set any value pointed by `p': 
\begin{lstlisting}[style=zmbStyle]
p.value = 42
// now if we print m :
println(m) // prints {"a" : {"b" : 10, "x" : 42}, "c" : {"b" : 20} } 
\end{lstlisting}

\emph{xelem()} also takes another optional parameter to get back a list of pointers:

\begin{lstlisting}[style=zmbStyle]
// from "m" get me the path b, followed by a property, as list, true 
pa = xelem(m, "//b", true ) 
// pa := [ /.[@name='a'][@name='b'],/.[@name='c'][@name='b'] ]
\end{lstlisting}

And now we can go about merry ways of getting and setting values in them.
\end{subsection}

\end{section}























