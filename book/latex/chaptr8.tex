\chapter{Input and Output}\label{input-output}

{\LARGE IO} is of very importance for software testing
and business development. There are two parts to it, the generic IO, 
which deals with how to read/write from disk, and to and from url.
There is another part of the data stuff, that is structured data.
ZoomBA let's you do both, and those are the topic of this chapter.

\begin{section}{Reading}

\begin{subsection}{read() function}
Reading is done by this versatile $read()$ function.
\index{read()} \index{ binary read file }
The general syntax is :
\begin{lstlisting}[style=zmbStyle]
value = read(source_to_read)
\end{lstlisting}
The source can actually be a file, an UNC path, an url.
Note that given it is a file, $read()$ reads the whole 
of it at a single go, so, be careful with the file size. 

\begin{lstlisting}[style=zmbStyle]
value = read('my_file.txt') // value contains all the text in my_file.txt 
lines = read('my_file.txt', true) // lines contains lines in the file
bytes = read('my_file.txt', 'b') // bytes contains all bytes of the file 
\end{lstlisting}
\end{subsection}

\begin{subsection}{Reading from url : HTTP GET}
\index{read() : http get}
Read can be used to read from an url.
Simply :

\begin{lstlisting}[style=zmbStyle]
resp = read('http://www.google.co.in')
/* value contains response of the google home page */
resp.body() // string of HTML 
resp.bytes // response bytes
resp.code // responce code 
resp.headers // response headers 
\end{lstlisting}

To handle the timeouts, it comes with overloads:
\index{read() : url timeout }
\begin{center}\begin{minipage}{\linewidth}
\begin{lstlisting}[style=zmbStyle]
// url, connection timeout, read timeout, all in millisec
resp = read('http://www.google.co.in', 10000, 10000 )
/* resp contains all the response data from google */
// it supports named parameters :
resp = read('path' = 'https://www.google.co.in', 
   'conTo'=1000, 'readTo'=1000, headers={:} )
// conTo : Connection Timeout, readTo : Read Timeout, 
// headers : http headers you want to send 
\end{lstlisting}
\end{minipage}\end{center}

\index{read() : http get : url generation }
To generate the url to $get$, the fold function comes handy:
\begin{lstlisting}[style=zmbStyle]
_url_ = 'https://httpbin.org/get'
params = { 'foo' : 'bar' , 'complexity' : 'sucks'  }
// str: is the namespace alias for 'java.lang.String'
data = str(params.entries,'&') as {
   str('%s=%s',$.key,$.value)
}
response = read( str("%s?%s", _url_ , data ) )
\end{lstlisting}
\index{restful api : get}
And that is how you do a restful api call, using $get$.
\end{subsection}

\begin{subsection}{Reading All Lines}
\index{ file() }
The function $file()$ reads all the lines, 
and puts them into different strings, so that it returns 
a list of strings, each string a line.
With no other arguments, it defers the reading of next line, 
so it does not read the whole bunch together.

\begin{lstlisting}[style=zmbStyle]
for ( line : file('my_file.txt') ){
   println(line) // proverbial cat program 
}
\end{lstlisting}
In short, the $file()$ method yields an iterator.
But with this, it reads the whole file together as list of string lines:
\begin{lstlisting}[style=zmbStyle]
ll = read ( 'my_file.txt' , true )
\end{lstlisting}

This is again a synchronous call, and thus, it would 
be advisable to take care.

\end{subsection}

\end{section}

\begin{section}{Writing}

\begin{subsection}{write(), println(), printf() function family}

We are already familiar with the write function.
\index{write()}\index{print(), printf(), eprintf(), eprintln() }
With a single argument, it writes the data back to the standard output.
The function is aliased as \emph{print()} which is same as \emph{write()}:
\begin{lstlisting}[style=zmbStyle]
println('Hello,World!') ; // prints it 
printf("%d", 10) // formatted print 
eprintln("This goes to error stream!" )
eprintf("This goes to error : %d", 10 ) // formatter 
\end{lstlisting}

Given that the first argument does not contain a "\%" symbol,
the write functionality also writes the whole string to a file
with the name :
\index{write() : file }
\begin{lstlisting}[style=zmbStyle]
// creates my_file.txt, writes the string to it 
write('my_file.txt', 'Hello,World!')
// appends to the file 
write('my_file.txt', 'Another line appended', true) 
// write binary to file 
write('my_binary_file.bin', 'This is string data as payload', 'b')  

\end{lstlisting}

The formatting guide for \emph{printf()} can be found 
\href{https://sharkysoft.com/archive/printf/docs/javadocs/lava/clib/stdio/doc-files/specification.htm}{here}. 
\end{subsection}
\end{section}


\begin{section}{File Processing}
We talked about a bit of file processing in $read$ and $write$.
But if we want to read a file line by line, then we have to use something else.

\begin{subsection}{open() function}
\index{open()}
In some rare scenarios, to write optimal code, 
one may choose to read, write, append on files.
The idea is then, using $open()$ function.
The syntax is :

\begin{lstlisting}[style=zmbStyle]
fp = open( path [, mode_string ] )
\end{lstlisting}

The mode strings are defined as such:
\index{open() : open modes}
There are 3 modes :
\begin{enumerate}
\item{ Read : default mode : specified by "r" }
\item{ Write : write mode : specified by "w"  }
\item{ Append : append mode : specified by "a" }
\item{ Binary : binary mode : specified by "b" }

\end{enumerate}

\end{subsection}

\begin{subsection}{Reading}
\index{open() : reading}
To read, one must open the file in read mode.
This returns a java \href{https://docs.oracle.com/javase/8/docs/api/java/io/BufferedReader.html}{BufferedReader} object.
Now, something like \href{https://docs.oracle.com/javase/8/docs/api/java/io/BufferedReader.html#readLine--}{readLine}
can be called to read the file line by line, in a traditional way to implement the $cat$ command : \index{cat}

\begin{center}\begin{minipage}{\linewidth}
\begin{lstlisting}[style=zmbStyle]
// open in read mode : get a reader
fp = open( path ,"r" )
while ( !empty( line = fp.readLine() ) ){
   // call the standard java functions
   println(line)
}
// close the reader
fp.close()
\end{lstlisting}
\end{minipage}\end{center}

\end{subsection}

\begin{subsection}{Writing}
\index{open() : writing}

To write, one must open the file in write or append mode.
This returns a java \href{https://docs.oracle.com/javase/8/docs/api/java/io/PrintStream.html}{PrintStream} object.
Now, something like \href{https://docs.oracle.com/javase/8/docs/api/java/io/PrintStream.html#println-java.lang.Object-}{println()}
can be called to write to the file :

\begin{lstlisting}[style=zmbStyle]
// open in read mode : get a reader
fp = open( "hi.txt" ,"w" )
// call the standard java functions
fp.println("Hi")
// close the Stream
fp.close()
\end{lstlisting}

\end{subsection}

\end{section}

\begin{section}{Web IO}

\begin{subsection}{open() for web}
\index{web open()}
\emph{open()} function is used to open a http or https connection:

\begin{lstlisting}[style=zmbStyle]
// open web connection 
wcon = open( "https://www.google.co.in" )
// wcon has multiple fields:
wcon.readTo // Read timeout 
wcon.conTo // Connection timeout 

\end{lstlisting}

\end{subsection}

\begin{subsection}{get() , post() }
\index{web get() post() }
Once we have \emph{open()} a http or https connection, 
one can get or post into it by appropriate functions :

\begin{lstlisting}[style=zmbStyle]
resp = wcon.get("/", {:} ) // path, headers to send 
resp = wcon.post("/", {:}  ) // path, header map  
resp = wcon.post("/", {:}, body ) // path, headers, body 

\end{lstlisting}

\end{subsection}



\begin{subsection}{Sending to url : send() function}
\index{send()}
To write back to url, or rather for sending anything to a server,
there is a specialised send() function.

\begin{lstlisting}[style=zmbStyle]
_url_ = 'https://httpbin.org'
path = "/post" 
wcon = open(_url_)
params = { 'foo' : 'bar' , 'complexity' : 'sucks'  }
// path ,  protocol,  headers, body  
resp = wcon.send( post  , "POST"  , {:} , jstr(params) )


\end{lstlisting}

The SOAP XML stuff can be easily done with $send$.
See the example soap body we  will send to :

\begin{lstlisting}[style=xmlStyle]
<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
	xmlns:xsd="http://www.w3.org/2001/XMLSchema" 
	xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
    <soap:Body>
        <GetWeather xmlns="http://www.webserviceX.NET">
            <CityName>%s</CityName>
            <CountryName>%s</CountryName>
        </GetWeather>
    </soap:Body>
</soap:Envelope>
\end{lstlisting}

And this is how we $send$ the SOAP: \index{SOAP}

\begin{center}\begin{minipage}{\linewidth}
\begin{lstlisting}[style=zmbStyle]
template_pay_load = read('samples/soap_body.xml')
pay_load = str( template_pay_load , 'Hyderabad', 'India' )
headers = { "SOAPAction" : "http://www.webserviceX.NET/GetWeather" , 
             "Content-Type" : "text/xml; charset=utf-8" }
wcon = open( "http://www.webservicex.net")
resp = wcon.send( "/globalweather.asmx" , "POST" , headers , pay_load ) 
x = xml(resp.body() )
sr = x.element("//GetWeatherResult")
write(sr.text)
\end{lstlisting}
\end{minipage}\end{center}

\end{subsection}

\end{section}



\begin{section}{Working with JSON}

\begin{subsection}{What is JSON?}
\index{json}
JSON is being defined formally in \href{http://www.json.org}{here}.
The idea behind JSON stems from : \emph{JavaScript Object Notation}.
There are many ways to define an object, one of them is very practical :
\begin{center}
\emph{Objects are property buckets.}
\end{center}

That definition ensures that the ordering layout of the properties does not matter at all.
Clearly :
$$
\{  ``x'' : 10 , ``y'' : 42 \} == \{  ``y'' : 42 , ``x'' : 10 \}
$$
Thus, we reach the evident conclusion, JSON are nothing but Dictionaries.
Hence in ZoomBA they are always casted back to a dictionary.

\end{subsection}

\begin{subsection}{json() function}
Suppose the json string is there in a file:

\begin{lstlisting}[style=zmbStyle]
/* sample.json file  */
{
  "zooomba": {
    "dbName": "zooomba",
    "url": "jdbc:postgresql://localhost:5432/zoomba",
    "driverClass" : "org.postgresql.Driver",
    "user": "zooomba",
    "pass": ""
  },
  "some2": {
    "dbName": "dummy",
    "url": "dummy",
    "driverClass" : "class",
    "user": "u",
    "pass": "p"
  },
}
\end{lstlisting}
To read the file ( or the text which has json) :
\begin{lstlisting}[style=zmbStyle]
jo = json('sample.json', true)
\end{lstlisting}
The $json()$ function can read also from the string argument.
The return result object is either a Dictionary object, 
or an array object, based on what sort of json structure was passed.
\end{subsection}

\begin{subsection}{Accessing Fields}
Now, to access any field:
\begin{lstlisting}[style=zmbStyle]
jo.noga.dbName // "zoomba"
jo.some2.driverClass // "class" 
\end{lstlisting}
In effect, one can imagine this is nothing but a big property bucket.
For some, there is this thing called \href{http://goessner.net/articles/JsonPath/}{JSONPath}, \index{JSONPath}
which is clearly not implemented in ZoomBA, because that is not a standard.
In any case, given json is a string, to check whether a text exists or not is a regular string search.
Given whether a particular value is in the hierarchy of the things or not, 
that is where the stuff gets interesting. 

For parameterised access, Currying is a better choice:

\begin{lstlisting}[style=zmbStyle]
prop_value = 'zoomba.dbName'  
value = #`jo.#{prop_value}` // same thing, "zoomba"
\end{lstlisting}
In this way, one can remove the hard coding of accessing JSON objects.
\end{subsection}

\begin{subsection}{Yaml Processing}
ZoomBA supports \href{https://en.wikipedia.org/wiki/YAML}{yaml} processing by the same way:
\index{yaml :  string, file, numerics}
\begin{lstlisting}[style=zmbStyle]
jo = yaml(string) // loads the yaml string into json object 
jo = yaml('file.yaml', true ) // loads the yaml file into json object 
\end{lstlisting}

Only important distinction is, for Yaml processing, the parser does not support
automatic type conversion into primitive type, thus, type hints are needed.

\end{subsection}

\end{section}

\begin{section}{Working with XML}
\index{xml}

Please, do not work with xml. 
There are many reasons why xml is a \href{http://harmful.cat-v.org/software/xml}{terrible idea}.
The best of course is :
\begin{center}
\emph{
XML combines the efficiency of text files with the readability of binary files
 -- unknown
 }
\end{center}

But thanks to many big enterprise companies - it became a norm to abused human intellect - the last are obviously Java EE usage in Hibernate, Springs and Struts. Notwithstanding the complexity and loss of precise network bandwidth - it is a very popular format. 
Thus - against my better judgment we could not avoid XML.

\begin{subsection}{xml() function}
\index{xml()}

Suppose, we have an xml in a file sample.xml :
\begin{lstlisting}[style=xmlStyle]
<slideshow 
title="Sample Slide Show"
date="Date of publication"
author="Yours Truly" >
<!-- TITLE SLIDE -->
<slide type="all">
  <title>Wake up to WonderWidgets!</title>
</slide>
<!-- OVERVIEW -->
<slide type="all">
    <title>Overview</title>
    <item>Why <em>WonderWidgets</em> are great</item>
    <item/>
    <item>Who <em>buys</em> WonderWidgets</item>
</slide>
</slideshow>
\end{lstlisting}
Call $xml()$ To load the xml file ( or a string ) into an 
\href{https://gitlab.com/non.est.sacra/ZoomBA/blob/master/lang/src/main/java/com/noga/ZoomBA/lang/extension/dataaccess/ZXml.java}{ZXml} object :
\begin{lstlisting}[style=zmbStyle]
x = xml('sample.xml', true , 'UTF-8' ) // x is a ZXml object
\end{lstlisting}
The encoding string ( UTF-8 ) is optional. 
Default is UTF-8. \index{xml : encoding}
The full syntax of \emph{xml()} is :
\begin{lstlisting}[style=zmbStyle]
// x is a ZXml object
x = xml( source [, is_source_file? false , encoding=UTF-8, validate? false ) 
\end{lstlisting}
\end{subsection}

\begin{subsection}{Converting to Other Formats}
Given an Xml object, there are handy ways to convert them into other formats.
\index{xml : json} \index{xml : json and yaml string}
\begin{lstlisting}[style=zmbStyle]
// x is a ZXml object
x = xml( ... )
json_object = json(x) // converts xml to json object 
json_string = jstr(x) // converts xml to json string
yaml_string = ystr(x) // converts xml to yaml string 
\end{lstlisting}

\end{subsection}

\begin{subsection}{Accessing Elements}
Given we now have the ZXml object, we should be able to find elements in it.
The elements can be accessed in two different ways.
\index{xml : pojo}
The first is, using the xml as a tree with $root$ and $children$.
That is easy, for example, for the previous json to xml generated :

\begin{lstlisting}[style=zmbStyle]
// XmlMap of the x 
y = xml(x)
y.root.children[0].name //glossary
y.root.children[0].children[0].text // example glossary 
\end{lstlisting}

For the slideshow xml: \index{xml : attribute}

\begin{center}\begin{minipage}{\linewidth}
\begin{lstlisting}[style=zmbStyle]
// XmlMap of the x 
x = xml('sample.xml',true) // slideshow xml 
x.root.name //slideshow
// access attributes 
x.root.children[0].attr.type // "all"
x.root.children[0].attr['type'] // "all"
\end{lstlisting}
\end{minipage}\end{center}

The other way of accessing elements, 
is to use the $element()$ function, 
which uses \href{https://en.wikipedia.org/wiki/XPath}{XPATH} :
\index{xml : element()}
\begin{lstlisting}[style=zmbStyle]
// XmlMap of the x 
x = xml('sample.xml', true ) // slideshow xml 
// get one element  
e = x.element("//slide/title")// first title element 
e.text // "Wake up to WonderWidgets!"
e = x.element("//slide[2]/title" ) // second element 
e.text // "Overview"
\end{lstlisting}

In the same way, for selecting multiple elements, $elements()$
function is used : \index{xml : elements()}

\begin{lstlisting}[style=zmbStyle]
// XmlMap of the x 
x = xml('sample.xml') // slideshow xml 
// get all the elements called titles  
es = x.elements("//slide/title" ) // a list of elements 
// print all titles ?
fold(es) as {  println( $.text ) }
\end{lstlisting}

\end{subsection}

\begin{subsection}{XPATH Formulation}
\index{xml : xpath()}
For evaluation of xpath directly, there is $xpath()$ function 
defined. For example, to find the text of any element, 
one can use the following :

\begin{lstlisting}[style=zmbStyle]
// XmlMap of the x 
x = xml('sample.xml',true) // slideshow xml
// text of the title element 
s = x.xpath("//slide/title/text()" )  
\end{lstlisting}

For a list of all xpath functions see the specification in 
\href{https://www.w3.org/TR/xpath-functions-3}{w3c}.

To check if a condition exists or not, one can use the $exists()$ function.
\index{xml : exists()}
\begin{lstlisting}[style=zmbStyle]
// XmlMap of the x 
x = xml('sample.xml',true) // slideshow xml
// text of the title element, exists? 
b = x.exists("//slide/title/text()" ) // bool : true 
b = x.exists("//slide/title" ) // bool : true 
b = x.exists("//foobar" ) // bool : false 
\end{lstlisting}

Obviously, one can mix and match, that is, give a default to the $xpath()$ function :
\index{xml : xpath() default } 
\begin{lstlisting}[style=zmbStyle]
// string : text comes
t = x.xpath("//slide/title/text()" , 'NONE_EXISTS' )
// string : 'NONE_EXISTS'  
t = x.xpath("//foobar/text()",  'NONE_EXISTS' )  
\end{lstlisting}

\end{subsection}

\end{section}

\begin{section}{Generic XPath, XElement}
In any declarative flavoured language, it is imperative that automatic discovery 
of fields and dynamic accessing of fields to be supported.
For XML, people do have xpath, while for JSON a non standard json path is coming recently 
to the foray. ZoomBA uses apache \href{https://commons.apache.org/proper/commons-jxpath/}{jxpath}
to simplify the whole problem.

JXPath can be used for any complex objects, including Maps and Collections. The syntax is 
trivially same like xpath. 

\begin{subsection}{xpath()}
To access value of an element we use the function $xpath()$ :

\begin{lstlisting}[style=zmbStyle]
v = xpath( object, xpath , [all_items=false] )
// this is how it works out 
d = {'x' : 42 }
v = xpath(d,'/x', false ) // 42 a value 
v = xpath(d,'/x', true ) // [ 42 ] List 
\end{lstlisting}

There is no way to pass a default, when in doubt, use all switch to true,
to ensure that when a field is not there, it returns an empty list.

\end{subsection}  

\begin{subsection}{xelem()}
To access an element using an xpath we use the function $xelem()$ :

\begin{lstlisting}[style=zmbStyle]
v = xelem( object, xpath , [all_items=false] )
// this is how it works out 
d = {'x' : [ 0,1,2] }
e = xelem(d,'/x', false ) // /.[@name='x'] // DynamicPropertyPointer 
e.value // [0,1,2] 
\end{lstlisting}
See more about \href{http://commons.apache.org/proper/commons-jxpath/apidocs/org/apache/commons/jxpath/ri/model/dynamic/DynamicPropertyPointer.html}{DynamicPropertyPointer}.

As usual, pass true to generate a list.
\end{subsection}  



\end{section}



\begin{section}{DataMatrix}
\index{DataMatrix}

A DataMatrix is an abstraction for a table with tuples of data as rows.
Formally, then a data matrix $M = (C,R) $ where $C$ a tuple of columns,
and $R$ a list of rows, such that  $\forall r_i \in R$ , $r_i$ is a tuple
of size $|r_i| = |C|$. In specificity, $r_i[C_j] \to r_{ij}$ , that is, 
the $i$'th rows $j$'th cell contains value which is under column $C_j$.

In software such abstraction is natural for taking data out of data base tables, 
or from spreadsheets, or comma or tab separated values file.   

\begin{subsection}{matrix() function}
\index{ matrix() }
To read a data matrix from a location one needs to use the $matrix()$ function :

\begin{lstlisting}[style=zmbStyle]
m = matrix(data_file_path  [, column_separator_string = '\t' 
               [, header_is_there_boolean = true ]] )
\end{lstlisting}

For example, suppose we have a tab delimited file ``test.tsv''  like this:

\begin{center}\label{matrix}
\begin{tabular}{ |c|c|c|c|c| } 
 \hline
 Number	& First Name & Last Name & Points & Extra \\ 
 \hline 
   1    & Eve	     & Jackson	& 94      & \\
   2    & John       &  Doe	    & 80      &  x \\ 
   3    & Adam       & Johnson	& 67      &  \\	
   4    & Jill       & Smith	& 50      & xx \\  
 \hline
\end{tabular}
\end{center}

To load this table, we should :
\index{DataMatrix : columns} \index{DataMatrix : rows}

\begin{center}\begin{minipage}{\linewidth}
\begin{lstlisting}[style=zmbStyle]
m = matrix("test.tsv", '\t', true ) // loads the matrix 
m.columns // the set of columns 
m.rows // the list of data values
\end{lstlisting}
\end{minipage}\end{center}

The field $columns$ is a Set of string values, depicting the columns.
$rows$, however are the list of data values. 
 
\end{subsection}

\begin{subsection}{Accessing Data}

A data matrix can be accessed by rows or by columns.
For example :

\index{DataMatrix : data access by row }
\begin{lstlisting}[style=zmbStyle]
x = m.rows[0][1] // x is "Eve"
x = m.rows[1][2] // x is "Doe" 
\end{lstlisting}

Now, using column function $c()$ \index{DataMatrix : c() }

\index{DataMatrix : data access by column }
\begin{lstlisting}[style=zmbStyle]
y = (m.c(1))[1] // y is "Eve"
y = (m.c(2))[1] // y is "Doe" 
\end{lstlisting}

\end{subsection}

\begin{subsection}{Tuple Formulation}
\index{DataMatrix : tuple() }
Every row in a data matrix is actually a tuple.
This tuple can be accessed using the $tuple()$ function which returns a specific 
data structure called a Tuple:

\begin{lstlisting}[style=zmbStyle]
t = m.tuple(1) // 2nd row as a Tuple 
t.0 // 2
t["First Name"] // John
t.Points // 80 
\end{lstlisting}

This  tuple object is the one which gets returned as implicit row, 
when select operations on matrices are performed.

\end{subsection}

\begin{subsection}{Project and Select}
\index{DataMatrix : project, c() }
The project operation is defined \href{https://en.wikipedia.org/wiki/Projection\_(relational\_algebra)}{here}.
In essence it can be used to slice the matrix using the columns function $c()$.
The column function $c()$ takes an integer, the column index of slicing, and can take aggregated 
rows, which are a list of objects, which individually can be :

\begin{enumerate}
\item{ An integer, the row index }
\item{ A range: $[a:b]$ , the row indices are between $a$ to $b$. }
\end{enumerate}

Thus :

\begin{lstlisting}[style=zmbStyle]
m.c(0,1) // selects first column, and a cell of 3rd row 
m.c(0, [0:2]) // selects first column, and two cells : 1st and 2nd row 
\end{lstlisting}

\index{DataMatrix : select, select() }
But that is not all we want. We want to run SQL like select query, where
we can specify the columns to pass and appear. That is done using $select()$ function.
It takes the anonymous parameter as an argument. 
The other normal arguments are objects which individually can be :

\begin{enumerate}
\item{ An integer, the column index to project }
\item{ A String, the column name to project }
\item{ A range: $[a:b]$ , the column indices to project, between $a$ to $b$. }
\end{enumerate}

Thus :

\begin{lstlisting}[style=zmbStyle]
x = m.select(0,2)
// x  := [[1, Jackson], [2, Doe], [3, Johnson], [4, Smith]] 
\end{lstlisting}

Same thing can be accomplished by :

\begin{lstlisting}[style=zmbStyle]
x = m.select('Number' , 'Last Name' )
// x  := [[1, Jackson], [2, Doe], [3, Johnson], [4, Smith]] 
\end{lstlisting}

Changing the order of the columns results in a different Tuple, 
as it should :

\begin{lstlisting}[style=zmbStyle]
x = m.select(2 , 0 )
// x :=  [[Jackson, 1], [Doe, 2], [Johnson, 3], [Smith, 4]]
\end{lstlisting}

\index{DataMatrix : select condition }
As always, $select()$ works with condition, so :

\begin{lstlisting}[style=zmbStyle]
x = m.select(2 , 0 , where {  $.Number > 2  } )
// x :=  [[Johnson, 3], [Smith, 4]]
\end{lstlisting}
which demonstrates the conditional aspect of the $select()$ function.

\index{DataMatrix : select transform }
The tuples generated can be transformed, while being selected.
For example, we can change the Last Name to be lowercased :

\begin{lstlisting}[style=zmbStyle]
x = m.select(2 , 0 , where {  
         ( $.Number > 2 ){ 
            $[2] = $[2].toLowerCase() 
           }  
       } ) // x :=  [[johnson, 3], [smith, 4]]
\end{lstlisting}

\end{subsection}

\begin{subsection}{The matrix() function}
\index{ DataMatrix : matrix()}
One can create a matrix out of an existing one.
Observe that, using project and select generates only the list of data values, 
not a new matrix itself. If one wants to create a matrix out of an existing one :

\begin{lstlisting}[style=zmbStyle]
m2 = m.matrix(2 , 0 ,  where ( $.Number > 2 ){ 
            $[2] = $[2].toLowerCase() 
           }  
       ) 
\end{lstlisting}
This generates a new matrix named $m2$, with columns starting from ``Last Name'' and ``Number''.
The rows values are exactly the same as the select query done.

\end{subsection}


\begin{subsection}{Keys}
Given we have 2 matrices $(m1,m2)$ , we can try to figure out if they differ or not.
A Classic case is comparing two database tables from two different databases.
A very naive code can be written as such :
\index{ table comparison }

\begin{center}\begin{minipage}{\linewidth}
\begin{lstlisting}[style=zmbStyle]
def match_all_rows(m1,m2){
   count = 0 
   for ( r1 : m1.rows ){
     // linearise the left tuple
     st1 = str(r1,'#')
     for ( r2 : m2.rows ){
         // linearise the right tuple
         st2 = str(r2,'#')
         // they match 
         if ( r1 == r2 ){ count += 1 } 
     }
   }
   ( count == size(r1.rows) && count == size(r2.rows) )
}
\end{lstlisting}
\end{minipage}\end{center}


The \href{https://en.wikipedia.org/wiki/Computational\_complexity\_theory}{complexity} of this algorithm, 
with respect to the size of the rows of the matrices is : $\Theta(n^2)$, given $n$ is the rows size of the matrices. 
Given both the matrices have $m$ columns, the complexity would become $\Theta(n^2m^2)$, a huge amount considering
a typical $(n,m) := ( 10000, 10 ) $. 

We can obviously improve this setting: \index{ table comparison : O(n) }

\begin{center}\begin{minipage}{\linewidth}
\begin{lstlisting}[style=zmbStyle]
def match_all_rows(m1,m2){
   // exactly m * n 
   l1 = list(m1.rows ) as { str($.o,'#') }
   // exactly m * n
   l2 = list(m2.rows ) as { str($.o,'#') }
   // exactly n
   diff = l1 ^ l2
   empty(diff) // return 
}
\end{lstlisting}

The new complexity comes to $\Theta(nm)$.
The thing we are implicitly using is known as \href{https://en.wikipedia.org/wiki/Unique\_key}{key}.
We are not really using the key, but, stating that key uniquely represents a row. The criterion 
for a set of attributes to be qualified as key can be described as : \index{ table comparison : key }

\begin{lstlisting}[style=zmbStyle]
// a set of attribute indices 
key_attrs = [ index_1 , index_2 , ...  ]
def is_key( m, key_attrs){
   s = set(m.rows) as {  
         r = $.o // store the row
         // generate a list by projecting the attributes 
         l = fold(key_attrs) as { r[$.o] } 
         // linearise the tuple 
         str(l,'#')   
      }
   size(s) == size(m.rows)
}
\end{lstlisting}
\end{minipage}\end{center}


What happens when the keys are non unique? That boils down to the list, 
but then we should be able to remember, which rows have the same keys,
or rather what all rows are marked by the same key.
So, we may have a keys function, assuming keys are not unique : \index{ table comparison : key : non unique }

\begin{center}\begin{minipage}{\linewidth}
\begin{lstlisting}[style=zmbStyle]
// a set of attribute indices 
key_attrs = [ index_1 , index_2 , ...  ]
def key( m, key_attrs, sep){
   keys = fold (m.rows, dict() ) as {  
         r = $.o // store the row
         // generate a list by projecting the attributes 
         l = fold(key_attrs) as { r[$.o] } 
         // linearise the tuple 
         k = str(l, sep)
         if ( k @ $.p ){ // the key already exists 
            $.p[k] += $.i // append the current row index to the rows
          }else{
             // key does not exist, create one and then append 
             $.p[k] = list($.i) // key is the row id 
           }
         $.p    
      }
}
\end{lstlisting}
\end{minipage}\end{center}


This idea is already in-built in the DataMatrix, and is known as the $keys()$ function :
\index{ DataMatrix : key() }

\begin{lstlisting}[style=zmbStyle]
m.keys{  /* how to generate one */ }( args... )
m.keys( columns_for_keys )
\end{lstlisting}

So, to simply put, $keys()$ generate a key dict for the matrix, as shown below, 
using the  matrix in (\ref{matrix}) :  

\begin{lstlisting}[style=zmbStyle]
// the key is the column Number
m.keys( 'Number' )
// see what the keys are ?
m.keys /* {4?=[3], 3?=[2], 2?=[1], 1?=[0]}  */
\end{lstlisting}

We can generate a non unique key too, just to show the practical application :

\begin{lstlisting}[style=zmbStyle]
// the key is the column Number modulo 2: [0,1]
m.keys( as {  $.Number % 2  } ) 
// see what the keys are ?
m.keys /* {0=[1, 3], 1=[0, 2]}  */
\end{lstlisting}

\end{subsection}

\begin{subsection}{Aggregate}
\index{DataMatrix : aggregate() }
Once we establish that the keys function did not generate unique keys, one may want to 
consolidate the rows to ensure that the key points to unique rows.
This is done by the $aggregate()$ function. This function can be surmised to
generate a new matrix from the old matrix, where the same keys pointing to different rows
must be aggregated based on columns.

\begin{center}\begin{minipage}{\linewidth}
\begin{lstlisting}[style=zmbStyle]
def aggregate(m,columns , aggregate_function ){
  agg_rows = list()
  for ( key : m.keys ){
      row = list()
      rows_list = m.keys[key]  
      for ( c : columns ){
          // aggregate the rows on a single column
          col_data = m.c( c, rows_list ) 
          agg_data = aggregate_function( col_data )
          row += agg_data // add a replacement for all the rows   
      }
      // add to the newer row
      agg_rows += row 
  }
  // now we have the new matrix data rows
}
\end{lstlisting}
\end{minipage}\end{center}

\end{subsection}

\end{section}











