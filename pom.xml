<!--
Copyright 2017 zoomba-lang.org

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

-->
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <groupId>org.zoomba-lang</groupId>
    <artifactId>zoomba.lang.core</artifactId>
    <version>0.1-beta5-SNAPSHOT</version>
    <packaging>jar</packaging>
    <name>Zoomba Language Core</name>
    <description>
        ZoomBA is a multi paradigm Micro Language for JVM Scripting
        used for business development and software testing
    </description>
    <url>www.zoomba-lang.org</url>
    <licenses>
        <license>
            <name>Apache 2 License</name>
            <url>https://www.apache.org/licenses/LICENSE-2.0</url>
        </license>
    </licenses>
    <developers>
        <developer>
            <name>ZoomBA Language Org</name>
            <organization>ZoomBA Language Org</organization>
            <organizationUrl>https://www.zoomba-lang.org</organizationUrl>
        </developer>
    </developers>
    <scm>
        <connection>scm:git:git@gitlab.com:non.est.sacra/zoomba.git</connection>
        <developerConnection>scm:git:git@gitlab.com:non.est.sacra/zoomba.git</developerConnection>
        <url>git@gitlab.com:non.est.sacra/zoomba.git</url>
    </scm>
    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <timestamp>${maven.build.timestamp}</timestamp>
        <maven.build.timestamp.format>yyyy-MMM-dd HH:mm</maven.build.timestamp.format>
        <target.jvm>1.8</target.jvm>
        <maven.javadoc.skip>false</maven.javadoc.skip>
        <gpg.skip>true</gpg.skip>
        <deploy.skip>true</deploy.skip>
        <coverage.fraction>0.75</coverage.fraction>
    </properties>
    <distributionManagement>
        <snapshotRepository>
            <id>ossrh</id>
            <url>
                https://oss.sonatype.org/content/repositories/snapshots
            </url>
        </snapshotRepository>
    </distributionManagement>
    <dependencies>
        <!-- Support Arbitrary precision arithmetic -->
        <dependency>
            <groupId>org.jscience</groupId>
            <artifactId>jscience</artifactId>
            <version>4.3.1</version>
        </dependency>
        <!-- for amazingly fast json parsing JSONER source is copied in -->
        <!-- https://github.com/cliftonlabs/json-simple  -->
        <!-- Support YAML Parsing By default Yamlbeans copied in  -->
        <!-- https://github.com/EsotericSoftware/yamlbeans -->
        <!-- Support automation (deterministic and non deterministic ) -->
        <dependency>
            <groupId>dk.brics</groupId>
            <artifactId>automaton</artifactId>
            <version>1.12-1</version>
        </dependency>
        <!-- Support command line CLI No point creating another project -->
        <dependency>
            <groupId>org.jline</groupId>
            <artifactId>jline</artifactId>
            <version>3.9.0</version>
        </dependency>
        <dependency>
            <groupId>commons-jxpath</groupId>
            <artifactId>commons-jxpath</artifactId>
            <version>1.3</version>
        </dependency>
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>4.13-beta-1</version>
            <scope>test</scope>
        </dependency>
    </dependencies>
    <build>
        <resources>
            <resource>
                <directory>src/main/resources</directory>
                <filtering>true</filtering>
            </resource>
        </resources>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>3.0</version>
                <configuration>
                    <source>${target.jvm}</source>
                    <target>${target.jvm}</target>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-dependency-plugin</artifactId>
                <version>2.2</version>
                <executions>
                    <execution>
                        <id>copy-dependencies</id>
                        <phase>package</phase>
                        <goals>
                            <goal>copy-dependencies</goal>
                        </goals>
                        <configuration>
                            <outputDirectory>target/lib</outputDirectory>
                            <includeScope>compile</includeScope>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-surefire-plugin</artifactId>
                <version>2.16</version>
                <configuration>
                    <includes>
                        <include>**/*Test.java</include>
                    </includes>
                </configuration>
            </plugin>
            <plugin>
                <artifactId>maven-assembly-plugin</artifactId>
                <configuration>
                    <descriptors>
                        <descriptor>src/main/assembly/bin.xml</descriptor>
                        <descriptor>src/main/assembly/src.xml</descriptor>
                    </descriptors>
                    <tarLongFileMode>gnu</tarLongFileMode>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.codehaus.mojo</groupId>
                <artifactId>javacc-maven-plugin</artifactId>
                <version>2.6</version>
                <executions>
                    <execution>
                        <id>zmb-jjtree</id>
                        <configuration>
                            <sourceDirectory>src/main/java/zoomba/lang/parser</sourceDirectory>
                            <outputDirectory>${project.build.directory}/generated-sources/java</outputDirectory>
                        </configuration>
                        <goals>
                            <goal>jjtree-javacc</goal>
                        </goals>
                    </execution>
                </executions>
                <dependencies>
                    <dependency>
                        <groupId>net.java.dev.javacc</groupId>
                        <artifactId>javacc</artifactId>
                        <version>7.0.4</version>
                    </dependency>
                </dependencies>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-jar-plugin</artifactId>
                <version>3.0.2</version>
                <configuration>
                    <archive>
                        <manifest>
                            <addClasspath>true</addClasspath>
                            <classpathPrefix>lib</classpathPrefix>
                            <mainClass>zoomba.lang.Main</mainClass>
                            <addDefaultImplementationEntries>true</addDefaultImplementationEntries>
                        </manifest>
                    </archive>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-javadoc-plugin</artifactId>
                <version>3.0.1</version>
                <configuration>
                    <excludePackageNames>zoomba.lang.parser:*.yamlbeans.*</excludePackageNames>
                    <source>8</source> <!-- fix for jdk 11.0.2 https://bugs.openjdk.java.net/browse/JDK-8212233 -->
                </configuration>
                <executions>
                    <execution>
                        <id>package-javadoc</id>
                        <phase>package</phase>
                        <goals>
                            <goal>jar</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>com.jolira</groupId>
                <artifactId>onejar-maven-plugin</artifactId>
                <version>1.4.4</version>
                <executions>
                    <execution>
                        <configuration>
                            <!-- Optional -->
                            <onejarVersion>0.97</onejarVersion>
                            <!-- Optional, default is false -->
                            <attachToBuild>true</attachToBuild>
                            <!-- Optional, default is "onejar" -->
                            <classifier>onejar</classifier>
                        </configuration>
                        <goals>
                            <goal>one-jar</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-deploy-plugin</artifactId>
                <version>2.8.2</version>
                <configuration>
                    <skip>${deploy.skip}</skip>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-source-plugin</artifactId>
                <version>3.0.1</version>
                <executions>
                    <execution>
                        <id>attach-sources</id>
                        <goals>
                            <goal>jar</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.sonatype.plugins</groupId>
                <artifactId>nexus-staging-maven-plugin</artifactId>
                <version>1.6.7</version>
                <extensions>true</extensions>
                <configuration>
                    <serverId>ossrh</serverId>
                    <nexusUrl>https://oss.sonatype.org/</nexusUrl>
                    <autoReleaseAfterClose>true</autoReleaseAfterClose>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-gpg-plugin</artifactId>
                <version>1.6</version>
                <executions>
                    <execution>
                        <id>sign-artifacts</id>
                        <phase>verify</phase>
                        <goals>
                            <goal>sign</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>
    <profiles>
        <!-- Profile for code coverage instrumentation -->
        <profile>
            <id>instrument</id>
            <build>
                <plugins>
                    <plugin>
                        <groupId>org.jacoco</groupId>
                        <artifactId>jacoco-maven-plugin</artifactId>
                        <version>0.8.2</version>
                        <configuration>
                            <excludes>
                                <exclude>**/parser/*</exclude>
                                <exclude>**/json_simple/*</exclude>
                                <exclude>**/yamlbeans/**</exclude>
                            </excludes>
                        </configuration>
                        <executions>
                            <execution>
                                <id>prepare-agent</id>
                                <goals>
                                    <goal>prepare-agent</goal>
                                </goals>
                            </execution>
                            <execution>
                                <id>report</id>
                                <phase>prepare-package</phase>
                                <goals>
                                    <goal>report</goal>
                                </goals>
                            </execution>
                            <execution>
                                <id>check</id>
                                <goals>
                                    <goal>check</goal>
                                </goals>
                                <configuration>
                                    <rules>
                                        <rule>
                                            <element>BUNDLE</element>
                                            <limits>
                                                <limit>
                                                    <counter>INSTRUCTION</counter>
                                                    <value>COVEREDRATIO</value>
                                                    <minimum>${coverage.fraction}</minimum>
                                                </limit>
                                            </limits>
                                        </rule>
                                    </rules>
                                </configuration>
                            </execution>
                        </executions>
                    </plugin>
                </plugins>
            </build>
        </profile>
    </profiles>
</project>
