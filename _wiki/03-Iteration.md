# Conditions and Iterations

Control flow essentially means conditioning and iterating.
Even when we do not have any modularity in the code, addition of features like conditions and iterations make a language Turing Complete ( also 
the capability of doing Arithmetic, and imagining an infinitely limit of the memory). Pure functional people abhor iteration, they prefer recursion.
But that is a dogma - and ZoomBA is not about dogma, but practicality.

## Blocks
As both conditions and iterations controls flow into and from blocks,
we start with blocks of code. Blocks are piece of code between "{" and "}".
The return value of a block is the return value of the last executed statement in the block. That makes it sometimes tricky, but trivial in most of the cases.

```js
 {
    println("Inside the box!")
    x = 42 
 } 
```

This block, has a return value of 42. Thus blocks are 
globally shared anonymous functions with no parameters.
That is a very crucial thing to remember. NOTE: You can not assign 
a block to a variable, though :

```js
v = { /* some code */ } // illegal 
```

## Conditions

### Operators

The operators are as below:

```js
 a == b // equals 
 a === b // type equals and equals
 a < b  // a less than b
 a <= b // a less than or equal to b
 a > b // a greater than b 
 a >= b //  a greater than or equal to b 
 !a // not a 
 a != b // a not equal to b
 a =~ b // a matches b 
 a !~ b // a does not match b 
 a isa b // a is a type of b 
 a ?? b // if a is not defined or null, use b, else a
 ( a ? b : c ) // if expression a is true, then b , else c 
 a @ b // a is in collection/map b
 a !@ b // a is not in collection/map b
 a #^ b // a starts with b 
 a #$ b // a ends with b 
```

Note that *null* is only equal to another *null*, and nothing else.
Thus :

```js
 null == null // true 
 null != null // false 
 empty(null) // true 
 size(null) // -1 
 #|null| // 0 
```

The infix style *contains* works, given we do not know left/right which one would be null : 

```js
null @ null // false 
null @ [ null ] // true 
```

And this is the precise reason having that `@` operator. An equivalent code would be :

```js
if ( b != null ) return b.contains(a) else return false 
// or simply 
a @ b // the mathematical *in* operator     
```

How elegant is that?
​      
#### Starts and Ends With 
This should demonstrate the idea well :

```js
(zoomba)"abc" #^ "a"
true // Boolean
(zoomba)"abc" #$ "bc"
true // Boolean
(zoomba)"abc" #$ "b"
false // Boolean
(zoomba)"abc" #^ "b"
false // Boolean
```

A point, these operators are good even for left and right both being collections! Thus :

```js
(zoomba)a = [0,1]
@[ 0,1 ] // ZArray
(zoomba)b = list(0,1,2,3)
[ 0,1,2,3 ] // ZList
(zoomba)a @ b
false // Boolean ; yes, because a is not in b as an element
(zoomba) b #^ a
true // Boolean
(zoomba)c = [2,3]
@[ 2,3 ] // ZArray
(zoomba) b #$ c
true // Boolean
```

#### Match Operator
The match operator actually takes a regex as string,
or it can be used as in like javascript:

```js
 s = "hello"
 s =~ ##^he.+# // true 
 s =~ ##he.+# // false
 s =~ ##he.+#g // true : global 
 s =~ ##He.+#ig // true : ignore case and global 
```

Now, with strings as regular expression :

```js
(zoomba)s = '\d+'
\d+ // String
(zoomba)"123" =~ s
true // Boolean
```

 Token splitting can be achieved by using java's native split() functionality, there is no need to use anything beyond though we have a function called *tokens()* :

```js 
(zoomba)s = "12,34 45"
12,34,45 // String
(zoomba)s.split("[, ]+")
12,34,45 // String[] 
```

The pattern literal can also take variables and expressions to be evaluated at runtime:

```js
v = "HE"
"hello" =~ ##?v#ig // true 
```
The `?` defines the willingness to evaluate the expression as a code body first, and then does the matching. This is imperative when you want to substitute a string which is passed as a variable, just like here.

##### Not Match 
This is defined as the negation of the match operation :

```js
!( a =~ b ) <-> a !~ b 
```

Thus, 

```js
 "hello" !~ ".+ello"  // false 
 "bo hahaha" !~ ".+ello"  // true 
```


#### isa Operator
*isa* is the loose type check operator. It may use the match syntax :

```js
 0 isa 1 // true, both are same type 
 0 isa ##integer#i#g // true  
 [0,1,2] isa [] // List type 
```

Of course that is a little too much for general purpose computing.
So these works as well:

```js
0 isa 'int' // true 
1 isa 'integer' // true, int -> integer 
0.1 isa 'float' // true 
'0' isa 'str' // yep, true 
true isa 'bool' // yep
[] isa 'list' // true  
{:} isa 'map' // yes, no question 
[0:0] isa 'range' // well, well, true 
```

Not only that, it also finds functions and ZoomBA objects:

```jsh
def f(){} // a function, which does nothing 
f isa 'func' // yes, and true 
def O {} // a ZoomBA object 
O isa 'obj' // yes, and true 
```

#### Type Equals and Equals 
The best way to demonstrate this operation :

```js
(zoomba)1 == 1.0 // equals :: note, java won't agree 
true // Boolean
(zoomba)1 === 1.0 // type equals and equals 
false // Boolean     
(zoomba)1.00 === 1.0 // and they are, indeed equals 
true // Boolean
```

This is a javascript borrowed operator. Sometimes, they are indeed needed.
Expansion of such a construct is as follows :

     a === b <-> type(a) == type(b) && a == b 

The type check is guaranteed to happen first, thus, deferring the comparison operation.
​           
### if-else

This is as trivial as if - else if - else.

```js
x = 10 
if ( x < 10 ) {
   println("<10")
} else if ( x == 10 ) {
   println("==10")
} else {
   println(">10")
}
```

And that is what condition blocks are all about.
Generally, in ZoomBA, condition blocks are frowned upon.
Part of the reason is because conditions branches the 
flow of control, and hence the flow becomes hard to debug.

### Error Handling

ZoomBA does not have exception handling. One of the author believes that sort of design is an exceptionally bad idea while the other one believes exception handling in itself is an exceptionally bad idea. However, both the authors agree that business programming must be made linear. So, ZoomBA has the following error constructs,  which generate error conditions when triggered :

#### Assert

```js
x = 10 
assert( x == 10 , "x should be 10!" ) // nothing happens
x = 101 
assert( x == 10 , "x should be 10!" ) // assertion happens
```

When an assertion condition fails, then the assertion fires.

#### Panic

```js
x = 10 
panic( x == 10 , "x should be 10!" ) // panic  happens
x = 101 
panic( x == 10 , "x should be 10!" ) // nothing happens
```

When panic condition is true, the system panics.

#### Test 

```js
x = 10 
test( x == 10 , "x should be 10!" ) // nothing happens
x = 101 
test( x == 10 , "x should be 10!" ) // test fails 
// now, logging happens to error stream
```

Test is where you want to semi validate your assumption, but you still want to proceed to the next statement. Given that test prints the failure of test to standard error, you can later filter all of these failures to make a call.

As a facade to test automation, these are significant functions.
The underlying structure is event driven, so an event listener/reporter can 
listen to the assertion events and logs appropriate test failure.

#### Raise 
*raise()* raises exceptions. In fact, it can raise any throwable. This was added to be as close to JVM runtime as possible. Without this, some methods won't be able to use full Java functionality. The syntax is :

```js
exception = 'java.lang.Exception'
raise( exception , 'I am being raised!' ) 
```

There is a simpler way to raise exception:

```js
raise('I am being raised')
```

This will raise a ```ZAssertionException```. Of course, to raise a custom exception, you specify the type string, and then the arguments required to raise it :

```js
raise( type_string, args1, args2, ... )
```

Thus *raise()* supports a minimal exception throwing clause.

#### Error Catching

As ZoomBA does not have exception handling, the flow of code becomes surprisingly linear to read. But, errors still needs to be dealt with. 
More often, these are other peoples errors. The underlying runtime (JVM) supports exceptions, so the system may throw exception. Thus - catching error is necessary.

```scala
#(o ? e ) = { assert ( false , "Throw this!" ) ; 42 }
```

In the above code obviously, assert fails. Instead of going boom boom, the error-net catches it. The syntax 

```scala
#(output ? error )
```

reads what it is, if the right side could produce an output, then 
output variable gets it, else error variable gets the error.

This makes ZoomBA significantly go-lang like. Note that, Error Handling is conditional. ZoomBA just makes it from non linear to linear.

One can use object fields for the same :

```scala
import java.lang.Integer as Int
d = { : }
#(d.out ? d.err ) = Int.parseInt("xxx")
println( d ) // out and err are properly set
```



## Iteration Flows

The common flows are *while* and *for* loop. They are imperative construct,
and thus frowned upon in ZoomBA, it lets you control your own iteration, and there is a possibility of mishandling.

### while 
A simple while loop should explain this :

```js
x = 0 
while ( x < 10 ){
  // body here 
    x += 1 // mutable add 
}
```

### for 
The same while loop can be written using for syntax:

#### Imperative
In a minimal imperative form :

```js
for (x = 0 ; x < 10; x+= 1 ){
   // body here 
}
```

##### Multiple Expressions
This is also totally allowed :

```js
for ( i=0,j=0; i < 10; i+=1, j+=2 ){
    println('i=%d, j=%d\n', i,j)
}
```

Note that the scope of the variables (i,j) created in general scope,
not in the local for loop scope. Thus, both i,j are accessible outside 
the for loop. This is an example of leaking reference. Hence, for 
most of the cases imperative for loop is frowned upon. 
The construct is there for historical significance, and as a stepping stone
to more declarative thinking. 


#### Iterator 
In the declarative form :

```js
for ( x : [0:10] ) { // notice the use of range
  // body here 
}
```

Note that, the variable *x* is not accessible outside the scope 
of the for loop. Thus, reference of *x* does not get leaked outside scope.
Finally, what we say omniscient form 
( because we are not even using the loop variable ) :

```js
 for ( [0:10] ) {
    // use body, 
    println($) // $ is the implicit iteration variable
 }
```

We also support (index,object) notation in for loop:

```js
for ( idx,obj : [12:42] ) { // notice the use of index and obejct 
  printf("Index is : %d, Object is %d %n", idx, obj)
}
```

### Break and Continue
We start with the problem of first find. Problem is: *Given a list, 
find first non negative integer*. This should be easy :
​    
```js   
 first_find = false 
 for ( L ) {
    if ( $ >= 0 && ! first_find ){
       item = $
       first_find = true 
    } 
 }
 println( item ?? 'No Non Negative found!' )
```

The problem is, even after the item is found, we continue with the loop, 
which is so bad. The runtime would be exactly *n*, which we do not want.

Is there a way to break out of the iteration?
It is non trivial to break out of iteration, or ignore the rest of the loop body. One way of doing this is:

```js
item = null
for ( i = 0; first_find = false; i < size(L) && !first_find; i+= 1){
    if ( L[i] > 0 ){
        item = L[i]
        first_find = true
    }
}
```

That is too much complexity, inherently associated with this sort of code. Moreover, what if the collection `L` does not even have an indexer, and is iterator based? Then what we need to do?

```js
item = null
for (first_find = false ; L.hasNext() && !first_find ;){
    li = L.next()
    if ( li > 0 ){
        item = li
        first_find = true
    }
}
```

 Thus, *break* and *continue* have their advantages.  They share the same syntax :

```js
break|continue ( ( when expression )? { after body } )?  
```

That is:
​    
     break/continue and
        if condition is specified, 
            then when condition happened, and 
                if body specified, 
                    then after executing the body.

To showcase, we use the [FizzBuzz](https://en.wikipedia.org/wiki/Fizz_buzz) problem.

```js
    // the Fizz Buzz
    for ( [1:n] ){
      div_3 = ( 3 /? $ ) // note the 3 divides $ operator 
      div_5 = ( 5 /? $ ) // note the 5 divides $ operator
      continue ( !div_3 && !div_5 ){ println($) }
      if ( div_3 ){  println("Fizz") }
      if ( div_5 ){  println("Buzz") }
    }
```

To showcase break, take the problem of finding the first
item in a list which is > 10 . 

```js
L = [ 1, 100, 2, 3 , 90 ]
for ( L ){ break ( $ > 10 ){ item = $ } } 
println(item ?? "Nothing Found" )
```

Notice the use of definition coalescing operator *??*.
The only one location where item can be initialised 
is inside the break, and thus, the code is correct.

#### Declarative Reasoning With FizzBuzz

A better, cooler alternative is Equivalence Class Partition on the output set itself. Thus: 

```js
fb_hash = { 0 : 'FizzBuzz' , 3 : 'Fizz' , 5 : 'Buzz' , 
            6 : 'Fizz' , 9 : 'Fizz' , 10 : 'Buzz' , 12 : 'Fizz' }
// improvise 
for ( L ) {
    r = $ % 15 // reminder : compute, cache is faster 
    text = ( r @ fb_hash ? fb_hash[r] : $ )  
    println(text) 
}            
```

### Pattern Matching 
ZoomBA supports *switch* and *case*. One can imagine a simple *C/Java* style switch :


```js
 x = 10 
 switch( x ){
     case 0  : println('zero')
     case 1  : println('one')
     case 10 : println('ten')   
 } 
 // prints ten
```

*break* is not required, because, the first match would exit from the expression branches. Also, switch has return value: 

```js
 x = 10 
 r = switch( x ){
     case 0  : 42
     case 1  : 'hhgg'
     case 10 : 1000   
 } 
 println(r)
 // prints 1000
```

#### Matching with Patterns 

Scala style matching can be done with : 

```js
def use_switch(x){
   switch( x ){
     case @$ isa 0  : println('Integer') 
     case @$ isa '' : println('String')
     case @$ : println('Unknown!')  
    }
 } 
 use_switch( [] ) // prints 'Unknown'
 use_switch( 'hi' ) // prints 'String'
 use_switch( 42 ) // prints 'Integer'
```


It also showcases the default matcher `@$` This matches the expression, no matter what it is, thus, it should be the last line of the switch. 
​     


## Higher Order Functions

Higher order functions are functions which takes one or more
functions as arguments and can return functions.
Take the example of the last *find first* problem: 

> find an element from a list such that a condition is true

This can be implemented as :

```js
for ( L ){ break ( condition ){ item = $ } } 
```

But, how do I pass the condition? This condition is hard coded
in the function itself. So:

```js
find_condition( condition, LIST ) 
```

this function would be a higher order, if there is a way 
pass the condition itself as a function, such that :

```js
def find_condition( condition, LIST ) {
   for ( LIST ) { break ( condition($) ) {  item = $ } }
}    
```
and now, find_condition is of higher order.
Implicitly then, all higher order functions rely on execution 
of some implicit loop. 

### Iteration Variable

For ZoomBA, there is a variable for that implicit loop.
Thus, there would be 4 components to the variable.
The variable is defined as :

>\$ : The Implicit Loop Variable for higher order functions. 

>\$.o : object, or current item of iteration. It is also accessibly using 
>\$.object or $.item.

>\$.i : index of the iteration, also accessible by $.index. 

>\$.c : context / collection on which we are iterating. Also san be accessibly by $.context.

>\$.p : partial result of the method execution, also accessible by $.previous, or $.partial.  

We tend to prefer the succinct `$.x` formulation. Albeit, in prod,
one should use the full expansion, to ensure things are understandable.
You can assign them into any variable in your method body, 
and change the name accordingly.

### Transforms
Take the problem of squaring every item of an integer list.
This would be :

```js
 partial = list()
 for ( item : LIST ) {
     partial.add (  item ** 2 ) 
 }
```

This can be generalised with a higher order function map.

```js    
def map( transform, LIST ) {
       partial = list()
       for ( item : LIST ) { 
          partial.add ( transform( item ) ) 
       }
       return partial
}
```

#### Collectors

But a bigger question is, what is the *collector*?
That is, after mapping where we are holding the items?
Thus, we change the arguments again :

```js
def map( transform, input_collection, collector ) {
    for ( item : input_collection ) { 
        collector.add ( transform( item ) ) 
    }
    return collector
}
```

#### Map

And now, we can describe the map followed by collect in ZoomBA.
Observe that ZoomBA is not for the Jargonists, it is for practical usage scenario.

```js
// create a list of squares from 1-9
l = list( [1:10 ] ) as { $.o ** 2  }
// create a set of squares from 1-9
s = set( [1:10 ] ) as { $.o ** 2  }
// transform all items in a list into integers 
l = list(L) as { int($.o) }
```

##### Printing them All

A better way to understand what the iteration variable is, is to print 
all the fields, during a *list* operation :


```js
l = list( [5:8] ) as {
     s = str( 'index : <%s> item : <%s> partial : %s' , $.i, $.o, $.p )
     println(s)
     $.o ** 2 
 }
// This produces 
index : <0> item : <5> partial : [  ]
index : <1> item : <6> partial : [ 25 ]
index : <2> item : <7> partial : [ 25,36 ]
```

#### From 

So, we see that there are two collectors set() and list().
This syntax is a very short way for this :

```js
l = from ( [1:10] , list() ) as {  $.o ** 2  }
```

to be read as 
> from ( collection , collect into collector) as { body } . 

If no collector is specified, the default collector is
of the type of the collection itself. Thus :

```js
x = from( [ 0 , 1, 2 , 3 ] ) as { $.o - 1 } 
```

will produce a list, but : 

```js
x = from( set( 0 , 1, 2 , 3 ) ) as { $.o - 1 } 
```

will produce a set.
And to please the object fanatics and the jargon guys, we have :

```js
l = [0,1,2,3,4]
x = l.map( as { $.o ** 2  } ) 
```


If you are wondering, then yes, even dictionaries support collecting, specifically, the same problem :

```js
d = dict([0:10] ) as { [ $.o , $.o ** 2 ] }
// key, value pair.
```

The as is a short form of "map as". Jargon guys like to call them *map* and *lambda* and *anonymous function blocks*. Those names do not interest us:

> to call a rose by any other name.

### Mapper Lambda
The syntax *as { function-body }* defines what our jargonists declared as 
*mapper lambda*. The clause *as* defines the next token as a mapper lambda.
The full syntax is :

```js
      ( as | -> ) (  { body } | method_definition | method_identifier ) 
```

 The way to use mapper lambda is, as we have already noted:

      function(args...) mapper_lambda 

 Thus, if you like symbolism, over English ( yes, we know, we do love mathy stuffs ), you can write this sort of code :

```js 
ll = list([0:n]) -> { $.o ** 2 } 
// above is same as 
ll = list([0:n]) as { $.o ** 2 } 
```

### Find
We started with find. So let's see how find works out.
The generic problem of find is :

> find an item in a collection such that a condition related to the item is true


#### Index
One way to do find is to return the index of the item
in a collection, < 0 for item not found. That is tricky.
Generally it is not, but ZoomBA supports -ve index. 
Thus, the return value should ideally be -size(collection).
<0 would guarantee failure, and being larger than the size
of the collection guarantees a failure when blindly 
indexed on the collection. Unfortunately, the underlying VM's
do not agree on this implementation. Thus, ZoomBA returns -1
when index failed.

A collection may be traversed from left or from right.
Thus, there are two index functions:

* left index —> `index`
* right index —> `rindex` 

Thus:

```js
i = index ( [0:10] ) where { $.o > 5 } // i == 5 
ri = rindex ( [0:10] ) where { $.o > 5 } // ri == 9 
i = index ( [0:10] ) where { $.o > 15 } // i == -1 
ri = rindex ( [0:10] ) where { $.o > 15 } // ri == -1 
```

Index is also available in vanilla form :

```js
b = [0,1,2,0,3]
i = index(b,0) // i = 0 
ri = rindex(b,0) // ri = 3
i = index("abc".toCharArray,_'a')  // i = 0, again
```

#### Exists

Given we have *index* we can use it for *exists*. The issuer of
this function is not bothered about where the element exists, 
she is only caring if such case exists or not. Such thing 
can be achieved by :

```js
 there_is = ( index ( [0:10] ) where { $.o > 5 } >= 0 )
```

but, against our better senses, we decidedly wanted to be *cool*, so :

```js
there_is =  exists ( [0:10] ) where { $.o > 5 }
```

to be read as :
> does there exist in ( collection ) such that { where-condition }  

### Predicate Lambda

The syntax *where { function-body }* defines what our jargonists declared as *predicate lambda*. ( We figured out that all useful jargons are already taken by 1960, so people desperately needed to invent jargons like *dependency injection* and *mock vs stubs* to look cool and be famous. But, unfortunately we do have serious degrees in Engineering, and build serious stuff, and thus, can not fall beyond a limit. ) The clause *where* defines the next token as a predicate lambda. Predicates are functions that returns a boolean. Thus, the full syntax is :

      ( where | :: ) (  { body } | method_definition | method_identifier ) 

 The way to use predicate lambda is, as we have already noted:

      function(args...) predicate_lambda 

 Thus, the *mathy* folks can choose to write symbolic code for finding even numbers like this :

```js 
 even_exists = exists( collection ) :: { 2 /? $.o }
 // read exists in ( collection ) such-that { condition } 
```

We are sorry, all useful special symbols were used already. In set builders notation, the math guys tends to use :

    S = { x | condition }
    S = { x : condition }

But, our grammar won't work that way. So, easy fix was replacing ":" with "::" which is by the way, elegant and cool. Peace.
​     
#### Find All

The problem statement of find all is :

>Filter all elements from a collection such that condition is true.

Easily done by :

```js
f = select( [0:10] ) where { $.o > 5 } 
f = from( [0:10] ) where { $.o > 5 }
```

for the filters, we can specify collectors:

```js
f = select([0:10], set() ) where { $.o > 5 } 
f = from ( [0:10] , set()) where { $.o > 5 }
// collect from ( col , into collector ) where condition 
```

Now, they start collecting into sets, instead of list.       
And we can map then after selection :

```js
f = select([0:10], set() ) where { $.o > 5 } as { $.o ** 2 }
```

That should be deemed cool, and much SQL like.  
One can use lambda inside lambda, the outer scope
readable through the syntax 

    $.$ : If outer scope is lambda, get the outer scope variable

That makes it cool.  

#### Partitioning 
A condition induces a partition on an input set. The partition function showcases this :

```js
(zoomba)l = list([0:10]) -> { random(100) }
[ 91,77,20,66,7,55,33,76,26,83 ] // ZList
(zoomba)#(s,r) = partition(l) :: { $.o < 50 }
nil // MonadicContainerBase
(zoomba)s
[ 20,7,33,26 ] // ZList : s is the selected partition 
(zoomba)r
[ 91,77,66,55,76,83 ] // ZList : r is the rejected partition
```

Note that, functional chaining is not possible with partition.

#### Prime Number Detection

Albeit, coolness is not the only thing we seek, we want results. With  *exists*, and *outer scope* we can solve the Sieve of Eratosthenes this way:

```js
x @ from ( [2:x +1] , set() ) where { 
    !exists( $.p ) where { $.$.o % $.o == 0  }  
}
```

without any unusual cluttering of the indices, or any variables, which we do not need at all. Note that, the same can be written as :

```js
x @ from ( [2:x +1] , set() ) where { 
    current = $.o ; primes_as_of_now = $.p 
    !exists( primes_as_of_now ) where { $.o /? current }  
}
```

And with the trick of *\$.\$* and *\$.p*  we avoid the two variables.

But, we can do a lot better on the frugality of characters, 
this *equation* also says if *n* is prime or not:

```js
(select([2:n+1]) :: { !exists($.p) :: { $.o /? $.$.o } })[-1] == n
```

Compare this with *Scala*, that too with overloaded operator, as discussed [here](https://gist.github.com/mkaz/d11f8f08719d6d27bab5):

```scala
(n: Int) => (2 to n) |> (r => r.foldLeft(r.toSet)((ps, x) => if (ps(x)) ps -- (x * x to n by x) else ps))
```

And then Haskell from [SO](http://stackoverflow.com/questions/3596502/lazy-list-of-prime-numbers) :

```haskell
primes :: [Integer]
primes = sieve [2..]
  where
    sieve (p:xs) = p : sieve [x|x <- xs, x `mod` p > 0]
```

### Multiple Element Set 

A set does not repeat elements, while a list can have duplicate elements.
Thus, a nice representation of a list is using a dictionary :

    [0,1,1,2,3,3,3] <-> { 0 : 1, 1:2, 2:1, 3:3 } 

Basically, we are replacing the list with a set of entries, with key as the item and the value as the count of the item in the list. This is done by the function *mset()* :

```js
(zoomba)l = [0,1,1,2,3,3,3]
@[ 0,1,1,2,3,3,3 ] // ZArray
(zoomba)mset(l)
0=1,1=2,2=1,3=3 // HashMap

```

Like always, *mset()* can take mappers - which generates the key for the HashMap :

```js
(zoomba)mset(l) -> {$.o}
0=[0],1=[1, 1],2=[2],3=[3, 3, 3] // HashMap     
```

Obviously, when we use the mapper lambda as key function, the count becomes immaterial, the value gets replaced with a list of items where keys remain the same. This makes total sense if we think of some practical usage.
*mset()* can be imagined as row aggregator for database rows or tuples. 
This function aggregates all the rows which has the same key.

### Group By 

The function *group()* let's use the group followed by aggregation function. The sample should illuminate :

```js
(zoomba)l = [10,1,23,12,15,0,3]
@[ 10,1,23,12,15,0,3 ] // ZArray
(zoomba)size(10,10)
2 // Integer
(zoomba)group(l) as {size($.o,10) } // first as clause is the key function
{1=[1, 0, 3], 2=[10, 23, 12, 15]} // HashMap
(zoomba)group(l) as {size($.o,10) } as { size($.value) } // 2nd as claues is the group 
{1=3, 2=4} // HashMap
(zoomba)
```

while *mset()* let's you count without the key function, it does not count with the key function. *group()* let's you do that. Also, *group()* let's you apply generic aggregators.


#### Application of mset() : Radix Sort 
A simple application of *mset()* is radix sort. Let's do it for decimal. 

```js
def radix_sort( arr ){
  #(min,max) = minmax( arr ) where { size($.left,10) < size($.right,10) } 
  max_digit_size = size(max)
  pos = 0 
  while ( pos < max_digit_size ){
    bucket_at_pos = mset( arr ) as { int( (str($.o))[pos] ?? 0 ) }
    inx = 0 
    for ( digit : [0:10] ){
      continue( !(digit @ bucket_at_pos) )
      for ( item : bucket_at_pos[digit] ){
        arr[inx] = item 
        inx += 1 
     }
    }
    pos += 1   
  }
}
```


### Monadic Containers while Finding Item
*index* and *rindex* works neatly given the collection on which we are searching are indexable. What if they are not? What good it would do, if I want to search for an item from a *set* or a *dict* or any general collection, when none can index the item? Suppose this is the problem :

```js
s = set( 'I' , 'am' , 'a' , 'good' , 'language' )
```

We are to find out, if there is an item in the set that contains 'a'.
In that case, index works. But once the index is there, how can one finds the item, in the first place? One can not. Hence, *find* comes to rescue.

#### find() 

```js
i = index(s) :: { 'a' @ $.o } // get's a non zero index 
r = find(s) :: { 'a' @ $.o } // get's either 'a', or 'am' or 'language' 
```

Now, the interesting thing is, what *find* must return? It has to return 
the object with matching condition, but what about failure cases? *null* does not cut it, because containers can contain *null*. Thus, the only way out is Monadic containers, or optional type, with the following properties:

    * nil : tells if the container is empty or not.
    * value : if the container is non empty, gets the value in the container

Now, clearly then :

    r.nil // false 
    r.value // either 'a', or 'am' or 'language'

To showcase the other side, suppose we search for something not even there:

```js
r = find(s) :: { 'x' @ $.o } //          
```

This *r* is *nil*, and thus :

```js
r.nil // true 
r.value // will raise zoomba.lang.core.types.ZException$Property: value 
```

Thus, *find* works using MonadicContainer.

#### null and nil 

*null* is a terrible design choice. The special constant `null` defines a lack of existence of some object,
and is an universal object. That is a terrible lame design. Surprisingly, it is not used where it should be used, 
for example parsing an integer out. There are no nullables in Java.

Thus, `nil` is introduced. It gets used to precisely define a lack of r-value in any situation.
In ZoomBA, there is no way to assign a variable to `nil`, ever. 


### Tokenization 

*tokens()* function tokenises. Given a string, and a regular expression, 
when that regex is found inside the string, the function isolates them, 
and puts them into a list for later processing. To showcase :

```js
(zoomba)s  = '11, 12 34 45'
11, 12 34 45 // String
(zoomba)tokens(s,'\d+')
11,12,34,45 // ArrayList
(zoomba)l = tokens(s,'\d+') -> { int($.o) }
[ 11,12,34,45 ] // ZList
(zoomba)l.0
11 // Integer     
```

We can chain the operations on token, thus, we can start applying filters 
after the map operation :

```js
(zoomba)l = tokens(s,'\d+') -> { int($.o) } :: { 2 /? $.o }
[ 12,34 ] // ZList
```

With a single input, *tokens()* returns a java Pattern object:

```js
(zoomba)tokens('\d+')
\d+ // Pattern
```

### Order and Comparisons and Scalar

Another problem is that of order. Suppose we want to find min, max of a collection. This requires comparison between two elements.

#### Min and Max
The function one would use is minmax : 

```js
l = [1,0,3,42,2,10 ]
#(min,max) = minmax(l) // min = 0, max = 42
```

But, what about when we need to compare elements?
Suppose, we want to find out the maximum and minimum of the values 
in a dictionary :

```js
d = { 'a' : 'A' , 'b' : 'B' , 'c' : 'C' } 
#(min,max) = minmax(d) :: def(pair){ pair.0.value < pair.1.value } 
#(min,max) = minmax(d) where { $.o.0.value < $.o.1.value } 
// min = (a,A) , max = (c,C) 
```

Observe that the comparison happens assuming : 

    $.o = [ left_item, right_item ] 
    left_item := $.o.0
    right_item := $.o.1

The strategy can be summarised by :   

>The function must return true if left < right, else false.

>-ve return value is taken as left < right.

>0 return value means left == right

>+ve return value means left > right  

This is how compare function [works](https://docs.oracle.com/javase/8/docs/api/java/lang/Comparable.html).

##### Optional and nil

What happens when there are no items in the collection? As usual we can not use `null`, and thus we are resorted to use `nil` again. But:

```js
#(m,M) = minmax([])
// both m,M are assigned nil.
is(m) // false
is(M) // false
```

This is different from null. You can use a comparator that takes care of `null` as the minimal entity. 

```js
x = [ null, 0, 1, 100, 900 -90 ]
#(m,M) = minmax( x ) where { empty($.l) ? -1 : ( empty($.r) ? 1 : $.l - $.r )  }
```

in that case, null is a perfectly valid value for both min, max.

#### Sum 

To sum over a collection, use *sum()* function.
sum() accepts a mapper lambda which returns a scalar value. 
Thus :

```js
  sum( collection ) // sums the collection 
  l = [1,"3" , "4" ,5.1 ] 
  total = sum(l) // 13.1 
  total = sum(l) -> { int($.o) } // 13  
```

*sum()* does not work like reduce. Thus, with no input it generates an integer 0 as the result. 

#### Sorting
In the same way, *sort* functions work.
There are two types, sort ascending *sorta* and descending *sortd*.
They both are higher order functions. Sorting happens in place,
however, `sorta(x)` returns the collection x back.

```js
 l = [ 9,1,4,2,9,6 ]
 sorta(l) 
 // l [ 1,2,4,6,9,9 ]        
 sortd( l ) 
 // [ 9,9,6,4,2,1 ]
```

#### Searching - Binary Search 

We have already discussed *find()* function. For sorted indexable collection ZoomBA has binary search :

```js
i = bsearch(sorted_collection, item) [ where {/* comparator function */} ]
```
an example would be:

```js
(zoomba)l = [10,1,23,12,15,0,3]
@[ 10,1,23,12,15,0,3 ] // ZArray
(zoomba)sorta(l)
true // Boolean
(zoomba)l
@[ 0,1,3,10,12,15,23 ] // ZArray
(zoomba)bsearch(l,12)
4 // Integer
```

*bsearch()* returns the index of the item.
​     
### Folds
Someone said, *fold* is the only function you really need. Folds are the functions generally replacing iteration. Either one reads a collection 
from left to right or right to left. Based on the direction of travel, 
there are two folds :

* fold from left : fold or lfold
* fold from right : rfold 

The syntax is :

    fold|lfold|rfold ( collection (, seed )? ) as { function-body }

#### Sum
To explain, we would discuss sum. Sum can be made with this:

    sum = 0
    for ( LIST ) {  sum += $ }
    println(sum) 

This can be written more succinctly using folds ( either left or right ):

    sum = lfold(LIST,0) -> { $.p += $.o } // this is what sum() does

Do not let the minimal stuff fool you. On the first non fold sample, the variables are global, while in the fold functions, they are local.
​      
#### Factorial
In the same way, we can define a factorial:

```js
factorial = lfold([2:n+1] , 1) as { $.p *= $.o }
```

#### Select using Fold
Can be achieved with the continue trick:

    selected = lfold(LIST, collector) as { 
            continue ( ! condition ) 
            $.p += $.o }

#### Index using Fold
Easily doable :

    index = lfold(LIST,-1) as {
          break( condition ) { $.p = $.i } }

#### Map using Fold
The most trivial one :

    mapped = lfold(LIST, collector ) as {
             $.p += map ( $.o ) }   


### Multiple Iteration - Join
The basic multiple iteration can be written as : 

    for ( x : x_range ){
       for ( y : y_range ){
           printf( "(%d,%d)\n", x, y )
       }
    }

Observe the same is doable by a cartesian product of two index sets:
> for all (x,y) in X * Y 

Obviously one needs to have a condition there, in location of the printf
statement, that would read like filtering in :

> for all (x,y) in X * Y such that ( condition )  

That is exactly what *join* function does. The syntax looks like:

    join(collection1,collection2,...) where { condition } as { mapper }  

#### Combination Tuples 
Let's find all combination tuples :

    A = [ 1, 2, 3 ]
    comb = join(A,A,A) :: { $.o.0 < $.o.1 && $.o.1 < $.o.2 } 

#### Finding Minimal Distance Points
Let's define a point by :
​    
      p = [x,y] // a fixed length list

Now, then, the problem statement is, given a set of *points* , find out
which two points are closest. To do so, we have to define the distance metric:

    // Euclidean Metric
    def d(p1,p2) {  ( ( p1.0 - p2.0 )**2 + ( p1.1 - p2.1 )**2  ) ** 0.5  }

Now we pick the two points out:

      p1 = points[0] ; p2 = points[1] // they must exist
      min = [ p1, p2 , d(p1,p2) ]
      indexer = [2:#|points|].asList()
      join( indexer, indexer ) where {
          continue( $.o.0 >= $.o.1 ) 
          dist = d ( points[ $.o.0 ] , points[ $.o.1 ] )
          continue ( min.2 < dist )
          min.0 = p0 ; min.1 = p1 ; min.2 = dist 
          false // do not add anything to the join
      }
      println( min ) // holds the minimum 

If we look into the code, we notice that the side effect is that of mutability. Albeit the min variable is a immutable reference, what it holds is not! Thus, this way, we can share variables into the lambdas of the higher order functions, through references. This is not much recommended, unless performance is of priority.      

#### Reduce
Fold functions take input, one seed value. The seed value is optional, 
and comes from method argument. Sometimes, a variant can be used, where
the seed value is the first element of the collection itself.
That variant is effectively 

      lfold( col[1:-1] , col[0] ) as { fold-body } 
      // or 
      reduce(col) as { reduce-body }      

 What would be the issue of the collection is empty? Nothing, in fact   *Nothing* would be returned. A specific case of reduce function is 
 the *str* function over a collection. Also note the use of collection splicing using *collection[x:y]*.

#####  str() over Collection

     l = [0,1,2,3]
     s = str(l) // padding is ',' so : 0,1,2,3
     s = str(l,'#') // padding is '#' so : 0#1#2#3
     s = str(l,'#', as { $.o ** 2  } ) // so : 0#1#4#9  
     s = str(l,'#' ) -> { $.o ** 2  } // so again : 0#1#4#9

##### Left and Right Reduce 
Same can be achieved by reduce function:

    s = reduce(l) as { $.p = str($.p) + '#' + $.o } // 0#1#2#3
    s = rreduce(l) as { $.p = str($.p) + '#' + $.o } // 3#2#1#0

##### Min, Max, Sum using Reduction 
The triplet, *min()*, *max()*, and *sum()* can be done using the 
*reduce* function.

     min = reduce(collection) as { $.p <= $.o ? $.p : $.o } 
     max = reduce(collection) as { $.p >= $.o ? $.p : $.o } 
     tot = reduce(collection) as { $.p += $.o } 


## Randomisation 
The function *random()* returns a random no generator. But it has more usage.

    r = random() // get the generator 
    l = [0,1,2,3,4,5]
    item = random(l) // pick one at random 
    items = random(l,3 ) // pick 3 at random, with replacement 
    string = random ( "\\d+" , 10 ) // generate a 10 digit random no  
    f = random(0.0) // pick double 
    il = random(1l) // randomly return a long value 
    // generate a random string of size 2 from a source 
    (zoomba)str ( random('abcdrfgh'.toCharArray , 2 ) ,'')
    dh // String
    random(true|false) // returns a random boolean 

### Random Numbers

    r = random()
    r.num(true) // next Gaussian 
    r.num(100) // random integer between 0 to 100 
    r.num("x") // random unbounded integer 
    r.num("10") // large integer, a concatenation of 10 long values   


### Shuffling
is done by the *shuffle()* function:

    l = [0,1,2,3,4,5]
    shuffle(l) // true : meaning shuffle changed at least 1 
    println(l) // you would see shuffled list     

`shuffle(x)` also returns the in place shuffled collection back. 
This was done such as to ensure function composition over `shuffle()`.

## Sequences
Another very interesting way to look at the sequences provided by ZoomBA.
Sequences are mathematical stuff, generated by this sort of equation :

```
x[n] = f(x[n-1],x[n-2],...x[0])
```

To showcase, start with the basic sequence everyone knows, the integer no. sequence. It is defined as the base *x[0] = 0* and a successor function :

```js
x[n] = x[n-1] + 1 // f(x) := x + 1 : successor(x)             
```

 Now, to showcase a sequence, ZoomBA defines a sequence function as : *seq()* :

```js
z_seq = seq( list(0) ) -> { $.item[-1] + 1 }  
z_seq = seq( 0 ) -> { $.item[-1] + 1 }  // same as above 
```

 The argument can be a collection, or the list of items passed as arguments, generates the initial items in the sequence. 
A sequence is an iterator. Hence, one can use it to iterate inside a for for example :
​      
```js      
max = 10 
for ( z_seq ) { break ( max == $ ) ; println( $ ) } 
```

 It will print 1 to 9. Thus, a sequence is a non terminating iterator.
 The dictum is as follows :

```js
$.item , $.previous : current history of the sequence as List
$.index : holds the index 
$.context : holds the reference to the sequence itself 
```

### Sequences Examples  
Another example goes with factorial :
​      
```js      
// factorial(n) = ((n-1) + 1) * factorial(n-1) with 0->1
factorials = seq( 1,1 ) -> {  $.index * $.item[-1] }
```

And then Fibonacci :

```js
fib = seq( 0, 1 ) -> { $.item[-1] + $.item[-2 ] } 
```

A very complex one, that of [Hamming Numbers](https://rosettacode.org/wiki/Hamming_numbers) :

```js
// Hamming Numbers. 
ugly = seq( 1, 2, 3, 4, 5 ) ->{
  last = $.prev[-1]
  choice = last * 5 
  #(min_2, min_3, min_5 ) = fold ( $.prev , [ choice, choice, choice ] ) ->{
     #(c_2, c_3, c_5 ) = $.prev 
     c2 = 2 * $.item ; c3 = 3 * $.item ; c5 = 5 * $.item 
     if ( c_2 > c2 && c2 > last ) {  $.prev[0] = c2  }
     if ( c_3 > c3 && c3 > last ) {  $.prev[1] = c3  }
     if ( c_5 > c5 && c5 > last ) {  $.prev[2] = c5  }
     $.prev 
  }
  #(next,max) = minmax ( min_2 , min_3, min_5 )
  next 
}
```

A relatively simple one, that of Prime nos:

```js
primes = seq ( 2 ) ->{
   prime_cache = $.prev 
   last = prime_cache[-1] + 1 
   // https://en.wikipedia.org/wiki/Bertrand%27s_postulate 
   end = last * 2 
   last + index( [ last : end ] ) :: {  !exists ( prime_cache ) :: {  $.item /? $.$.item  }  }
}
```

#### Getting Arbitrary Item 
Once we have defined a sequence, it is easy to get the *n'th* item of the sequence ( 0 based index ) :

```js
fib = seq ( 0, 1 ) -> { $.item[-1] + $.item[-2 ] } 
fib[10] // returns 55
```

This can be of help sometimes.

## Combinatorics 
Once you get hold of the idea of sequences, next logical step 
is that of combinatorics. Given a collection the basic ideas of combinatorics are of 3 types :

  * Permnutation
  * Combination 
  * Sub Sequences from the collection 

### Permutations 
The idea of permutation is, enumerate and list all possible ways one can select items from a collection where order matters. How many items to be selected of a collection of size *n* ? That selection size is say : *r*.
Therefore, there will be P(n,r) items for a permutation.

To enumerate over those permutations, one needs to specify the collection, and the selection no (r). If we do not specify the no of items to be selected, it takes r as n itself, i.e. select all items. 

```js
permutations = perm( [1,2,3] )
```
 will yield an Iterable, which then can be iterated over :

```js
for ( permutations ) { println($) }  
```

This will yield :

    @[ 1,2,3 ]
    @[ 1,3,2 ]
    @[ 2,1,3 ]
    @[ 2,3,1 ]
    @[ 3,1,2 ]
    @[ 3,2,1 ]  

One can of course specify the *r* : 

```js
for ( perm([1,2,3], 2 ) ) { println($) }
```

which yields :

    @[ 1,2 ]
    @[ 2,1 ]
    @[ 1,3 ]
    @[ 3,1 ]
    @[ 2,3 ]
    @[ 3,2 ]

Given ZoomBA is DSL for software testing, combinatorics was needed.

### Combinations 
In the same spirit, combinations are where we select items, but do not care about the ordering in them. It is obvious, thus, that without specifying *r*, the combination will yield the collection itself :

```js
c = comb([1,2,3])
for ( c ){ println($) }
```

gives a result of :

    @[ 1,2,3 ]

But:

```js
c = comb([1,2,3], 2)
for ( c ){ println($) }
```

produces a result of:

    @[ 1,2 ]
    @[ 1,3 ]
    @[ 2,3 ]


### Sub Sequences and Power Sets
Another key thing is about power sets. Enumerating all possible sub collections of a collection is of importance. This can be done using the function *sequences* as such : 

```js
ps = sequences( [1,2,3] )
for ( ps ){ println($) }
```

and this yields :

    [3]
    [2]
    [2, 3]
    [1]
    [1, 3]
    [1, 2]
    [1, 2, 3]

This is the generalised notion of a power set. 

### Applications 
Permutations can be used instead of join to get faster access to replace nested loops with indices where we do not want to have the same indices. To showcase, this is how you generate pairs using *for* : 

```js
for ( i = 0 ; i < 4 ; i += 1 ){
 for ( j = 0; j < 4 ; j += 1){
    continue( i == j )
    // do things here 
 }
}
```

This can easily be replaced with a *join* : 

```js
join( [0:4] , [0:4] ) :: { continue($.0 == $.1) ; /* do things */ }     
```

But, the same can be easily achieved with *perm*, observe:

```js
for ( perm([0,4],2) ) { /* do things */ }  
```

And that concludes this section.             