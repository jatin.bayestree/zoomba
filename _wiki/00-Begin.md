# ZoomBA Wiki
 Welcome to <img src="https://gitlab.com/non.est.sacra/zoomba/raw/master/icons/ZoomBA.png" width="100" height="100" /> Wiki .
 
## Design Goals

The language was conceived and written by multiple angry developers, 
after looking at the extensive sad state of the tech for the core businesses. The following issues were found:

* Java is extensively verbose for the amount of work it achieves
* Scala does not play well with Java: 
   * Easy to call Java from Scala, trivial 
   * [Next to impossible](http://lampwww.epfl.ch/~michelou/scala/using-scala-from-java.html) to call [advanced Scala from Java](https://blog.akquinet.de/2011/07/20/integrating-scala-components-in-a-java-application/) code  
* Jython, the next hybrid does not play well between JVM object and Python objects 
* Groovy is ok, but [some design decisions](https://schneide.wordpress.com/2011/04/05/grails-the-good-the-bad-the-ugly/) are not to our liking - those make Groovy a terribly bad language for business programming and software validation.


### Manifesto for ZoomBA Development 

Thus, after working in Software Industry for more than 3 decades,
we have concluded the following :

 * Technology is almost always an aid to business, thus 
   * While we value technology, achieving business goal optimally is what we value more.      
 * While we understand multitudes of OOPs and Design dogmas 
   * We have rarely seen value in it for businesses.  
 * While we feel for the burning desire for the tech managers soul to create empires under them 
   * We have seen how such empires fall, when everyone stopped mapping business value of what tech does.  
 
### Tenets for any Language Development  
 
 Thus, ZoomBA was designed to :
 
 * Remove unnecessary Dogmas ( OOPs, for example )  
 * Being minimal, thus having a code quality that, code that once written, can be forgotten about, should run always 
 * As an aid to inject scripting capabilities into underlying VM
 * As a business development language for SQL aware folks :
    * In Business 
    * In QA   
 * Abiding by strong Pareto optimality : for 90% of the cases for business need a single ZoomBA script file with less than 42 lines would suffice.

While we do not see value in dogma, we see significant value in dollars,
and in the saving of it. 

## Maven Repo 
If you want to programmatically call ZoomBA from JVM - here is the dependency:

```xml
<dependency>
    <groupId>org.zoomba-lang</groupId>
    <artifactId>zoomba.lang.core</artifactId>
    <version>${zoomba.version}</version>
</dependency>
```

Current ```zoomba.version``` is 0.1-beta4, while 0.1-beta5-SNAPSHOT is available in the snapshot repository.


## Compilation from Source 
Please have git and maven. If you have it, then :

    $git clone git@gitlab.com:non.est.sacra/zoomba.git
    $cd zoomba
    $mvn clean install -DskipTests # because some tests might not run 
    $cd target
    $java -jar zoomba.lang.core-<zoomba.version>one-jar.jar
     
You should be able to see the (zoomba) prompt.  
And then, you can do this :

    (zoomba)println ( "I \u2665 ZoomBA!" )
    I ♥ ZoomBA!
    null // Nil   

## Download Latest Binary ( Bleeding Edge ) 

* You need to have Java 8 runtime installed.
* Download the latest binary one jar file from [here](https://oss.sonatype.org/content/repositories/snapshots/org/zoomba-lang/zoomba.lang.core/0.1-beta5-SNAPSHOT/)
* UnZip the archive ( either beta or snapshot )
* Simply run now :

     java -jar zoomba.lang.core-<zoomba.version>.one-jar.jar 
     
You should see the *(zoomba)* prompt.

Alternatively, You can use the following 
[download script](download_latest.sh) to download the latest jar file into your `$HOME/zoomba` directory.


## Setting Up
Setting it up means simply aliasing in \*nix platforms.
Under bash:

    alias zmb='java -jar <repo-path>/target/zoomba.lang.core-<zoomba.version>.one-jar.jar'
    
That should be put into the .bashrc or .profile file.

## IDE Setup

Under the folder ide\_settings\_files, there are 

* Intellij : settings.jar : [import](https://www.jetbrains.com/help/idea/2016.2/exporting-and-importing-settings.html) it in Idea.
* Vim : zoomba.vim file,use appropriately as shown [here](http://superuser.com/questions/844004/creating-a-simple-vim-syntax-highlighting).
* Sublime Text : zoomba.tmLanguage : follow as shown [here](http://sublimetext.info/docs/en/extensibility/syntaxdefs.html)
     
## Language Specification

The following pages would set you up for exploring features of ZoomBA.

* [Introduction to Basic Syntax](01-basic)
* [Types And Conversion](02-types)
* [Conditions and Iterations](03-Iteration) 
* [Functions](04-Functions)
* [Imports and Code ReUse](05-Imports) 
* [Extended Operators](06-Operators)
* [Reading and Writing](07-IO) 
* [Processes and Threads](08-Execution) 
* [Lots of Samples](09-samples)
* [Embedding into other Java source](10-JVM)
* [Introduction to Objects](11-oops)  
* [Advanced Topics in Objects](12-advanced)
* [ZoomBA Gotchas](13-howto)


### Command Line Manual 
ZoomBA has its manual on command line, so to discuss a topic :

```
(zoomba)//h <topic_name>
```

would produce the manual for the topic. 

## Solved Problems
We are using this a/c to solve some problems in CareerCup.  See the solutions listed [here](https://www.careercup.com/user?id=5687896666275840).
