# Imports and Code ReUse

## No Island Policy

Code does not run in isolation. Thus, it is impossible to run a code being oblivious to the fact that it relies on the environment it is running.

### Underlying Runtime

This brings to the point of the underlying runtime.
As of now, ZoomBA is 100% java runtime. Thus, these specifics are all JVM.

### Import Syntax
To import anything, one uses the *import* directive:

```js
 import <string> as <identifier>
 import java_like_import as <identifier>
 import var_identifier as <identifier>
```
     
This string can be an absolute path, or a relocable string.

#### Class
 
To import a class, say java.lang.Integer one can do these :

```js
 import 'java.lang.Integer' as Integer
 import java.lang.Integer as Integer
```
     
and you are up having the variable *Integer* in your memory. To use that:

```js
#(o ? e ) = Integer.parseInt("42")
```
     
That works. One can also store it into a variable, and then import:

```js
 s_class = "".class.name  
 import s_class as String
```  

#### Field 
To import a field of a class, give full name of the field:

```js
import 'java.lang.System.out' as Out
Out.printf("I am imported!")
```

#### Enum 

One can use the *enum()* function to use an enum :

```js
(zoomba)rms = enum('java.math.RoundingMode')
0=UP,1=DOWN,DOWN=DOWN,2=CEILING,3=FLOOR,4=HALF_UP,UNNECESSARY=UNNECESSARY,5=HALF_DOWN,6=HALF_EVEN,7=UNNECESSARY,FLOOR=FLOOR,CEILING=CEILING,HALF_EVEN=HALF_EVEN,UP=UP,HALF_UP=HALF_UP,HALF_DOWN=HALF_DOWN // UnmodifiableMap
```

Now, one can use any *enum* as cardinal no. or using the name :

```js
(zoomba)rms.0
UP // RoundingMode
(zoomba)rms.UP
UP // RoundingMode
```

This was more optimized way of doing it. But, there is another way, a more unoptimal way, by specifying the *enum* as String itself :

```js
(zoomba)rm_up = enum('java.math.RoundingMode' , 'UP' )
UP // RoundingMode  
```

The first parameter can also be a class object, which can be easily created:

```js
(zoomba)import 'java.math.RoundingMode' as RM
class java.math.RoundingMode // Class
(zoomba)rm_up = enum(RM , 'UP' )
UP // RoundingMode
```

## Other ZoomBA Scripts

This is where the relocable string becomes important. 
Note that if your code is dependent on others code, then you need to have
others code installed somewhere. ZoomBA believes in source code sharing, 
and thus expects you to share code.

An import like this :

```js
import _/"foo" as Foo 
```

implies that foo is a ZoomBA file, which is *located in the same location
as that of the importing file*. That way, the specific path is not ever important. Imports do not allow patching of strings or use of variables.

### Using Others Constructs 

### Functions 

Thus, in summary, suppose we want to use the function *bar()* defined 
in the script *foo.zm* which is in the same directory as that of the 
calling script *calling.zm*. In that then :

```js
 import _/"foo" as Foo 
 Foo.bar() // calls the bar() method of imported module foo()
```

### Variables 

Albeit not recommended at all, you can do this:

```js
// in file some.zm 
x = 'This is a bad idea'
```
and then import it 

```js
import 'some' as some 
println(some.x) // use the x from some.zm 
```

### Non Execution while Import 
Just like python, while importing, the whole script gets executed.
That would mean protecting some portion of the script which should not run while used as import module.

While python has :

```python
if __name__ == '__main__' :
   # try something here 
   pass 
```

this is sort of revolting. In ZoomBA, therefore, there is a specific variable, `@SCRIPT` which points to the current executing `ZScript`.
This script has a boolean function telling whether the script is running 
as main script or not:

```js
if ( @SCRIPT.main ){
   // do your stuff here. 
}
```

We thought about a special block, but then choose to ignore it, because too much abstraction is unhealthy. `@SCRIPT` is a special variable, and can not be assigned to.


### Using Others Scripts 
You can use the other whole script as a function by simply doing this :

```js
import _/"foo" as Foo 
Foo.execute() // calls the Foo script as executable
Foo.execute("Hello", "There!" ) // with arguments 
```


