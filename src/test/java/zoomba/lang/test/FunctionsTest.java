package zoomba.lang.test;

import org.junit.Ignore;
import org.junit.Test;
import zoomba.lang.core.collections.ZArray;
import zoomba.lang.core.collections.ZList;
import zoomba.lang.core.collections.ZSet;
import zoomba.lang.core.interpreter.ZScript;
import zoomba.lang.core.operations.Function;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

/**
 */
public class FunctionsTest {

    final String BASIC = "samples/test/functions_basic.zm" ;

    final String EXTENDED = "samples/test/functions_extended.zm" ;

    final String THREADING = "samples/test/sys_thread.zm" ;

    final String PROCESS = "samples/test/sys_proc.zm" ;

    final String PROCESS_WIN = "samples/test/sys_proc_win.zm" ;

    final String DATABASE = "samples/test/sys_db.zm" ;

    final String IO = "samples/test/sys_io.zm" ;

    final String WEB = "samples/test/sys_web.zm" ;

    final String TYPES = "samples/test/functions_types.zm" ;

    final String DATE = "samples/test/date.zm" ;

    @Test
    public void testBasicFunctions()  {
        InterpreterTest.danceWithScript( BASIC );
    }

    @Test
    public void testExtendedFunctions()  {
        InterpreterTest.danceWithScript( EXTENDED );
    }

    @Test
    public void testThreadFunctions()  {
        InterpreterTest.danceWithScript( THREADING );
    }

    @Test
    public void testProcessFunctions()  {
        // different for Unix and Windows...
        final String actualPath = System.getProperty("os.name").toLowerCase().contains("win") ?
                PROCESS_WIN : PROCESS ;
        System.out.println("Detected that the process path should be : " + actualPath );
        InterpreterTest.danceWithScript( actualPath );
    }

    @Test
    @Ignore
    public void testDBFunctions()  {
        InterpreterTest.danceWithScript( DATABASE );
    }

    @Test
    public void testIOFunctions()  {
        InterpreterTest.danceWithScript( IO );
    }

    @Test
    public void testWebFunctions()  {
        InterpreterTest.danceWithScript( WEB );
    }

    @Test
    public void testTypeFunctions()  {
        InterpreterTest.danceWithScript( TYPES );
    }

    @Test
    public void testDateFunctions()  {
        InterpreterTest.danceWithScript( DATE );
    }

    private void threadFunction(ZScript zScript, Map<Integer,Integer> set, int x) {
       zScript.execute(set, x);
    }

    @Test
    public void testThreadingOnZScript() throws Exception {
        Executor executor = Executors.newFixedThreadPool(10);
        ZScript zScript = new ZScript(" #(s,x) = @ARGS ; s[x] = x ;");
        final Map<Integer,Integer> map = new ConcurrentHashMap<>();
        final int size = 10000;
        for ( int i = 0; i < size; i++ ){
            final int x = i ;
            executor.execute(() -> threadFunction( zScript, map, x  ) );
        }
        while ( map.size() != size );
        assertEquals(size, map.size());
    }

    @Test
    public void testStreaming(){
        int[] a = new int[]{1,2,3};
        ZArray za = new ZArray( a );
        za.stream().forEach( x -> System.out.println(x));
        ZList zl = new ZList(za);
        zl.stream().forEach( x -> System.out.println(x));
        ZSet zs = new ZSet(zl);
        zs.stream().forEach( x -> System.out.println(x));
    }
}
