package zoomba.lang.test;

import org.junit.Test;
import zoomba.lang.core.operations.Function;
import zoomba.lang.core.types.ZString;
import zoomba.lang.core.collections.ZList;
import zoomba.lang.core.collections.ZSet;
import zoomba.lang.core.operations.Function;
import zoomba.lang.core.types.ZString;

import java.util.ListIterator;
import static org.junit.Assert.*;

/**
 */
public class ZStringTest {

    static final String HELLO = "Hello, World!" ;

    static final StringBuilder HELLO_BUF = new StringBuilder( HELLO );

    static final StringBuilder HELLO_BUILDER = new StringBuilder( HELLO );

    @Test
    public void testAssignment(){
        ZString zs = new ZString((Object) null);
        assertFalse( zs.isEmpty() );

        zs = new ZString();
        assertTrue(zs.isEmpty());

        zs  = new ZString(HELLO);
        assertFalse( zs.isEmpty() );

        zs  = new ZString(HELLO_BUF);
        assertFalse( zs.isEmpty() );

        zs  = new ZString(HELLO_BUILDER);
        assertFalse( zs.isEmpty() );

        zs  = new ZString(HELLO.toCharArray() );
        assertFalse( zs.isEmpty() );
    }

    @Test
    public void testCollection(){
        ZString zs = new ZString(HELLO);
        int i = 0 ;
        for ( Object c : zs){
            assertTrue( c instanceof Character );
            i++;
        }
        assertEquals( zs.length() , zs.size());
        assertEquals( i , zs.size());

        ListIterator li = zs.listIterator(zs.size());
        i = zs.size() ;
        while ( li.hasPrevious() ){
            Object c = li.previous();
            assertTrue( c instanceof Character );
            --i;
        }
        assertEquals(0,i);
        assertTrue( li.hasNext() );
        ZList l = new ZList(zs);
        i = l.rightIndex(Function.TRUE );
        assertEquals( i, zs.size() - 1 );
        i = l.leftIndex(Function.TRUE );
        assertEquals( i , 0 );

        ZSet s = new ZSet(zs);
        assertEquals( s.size() , 10 );

    }

    @Test
    public void testArithmetic(){
        ZString zs = new ZString(HELLO_BUILDER);
        assertEquals( 0 , zs.compareTo( HELLO ) );
        assertEquals( 0 , zs.compareTo( zs ) );
        assertEquals( 0 , zs.compareTo( HELLO_BUF ) );
        assertEquals( 0 , zs.compareTo( HELLO_BUILDER ) );
        assertEquals( 0 , zs.compareTo( HELLO.toCharArray() ));

        int previousSize = zs.size();
        zs.add_mutable(" Everyone seems to be happy, but not file");
        assertTrue(  previousSize < zs.size() ) ;
        String p = (String) zs._pow_(0) ;
        assertTrue(p.isEmpty());

        // start with a palindrome
        zs = new ZString( "malayalam" );

        p = (String) zs._pow_(1) ;
        assertEquals( 0 , zs.compareTo( p ));

        p = (String) zs._pow_(2) ;
        assertEquals( 2 * zs.size() , p.length() );

        p = (String) zs._pow_(-1) ;
        assertEquals( 0 , zs.compareTo(p));

        p = (String) zs._pow_(-2) ;
        assertEquals( 2 * zs.size() , p.length() );

    }
}
