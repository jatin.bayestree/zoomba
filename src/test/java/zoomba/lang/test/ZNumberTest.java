package zoomba.lang.test;

import org.jscience.mathematics.number.LargeInteger;
import org.jscience.mathematics.number.Real;
import org.junit.Test;
import zoomba.lang.core.collections.ZSet;
import zoomba.lang.core.types.ZNumber;
import java.util.Set;

import static org.junit.Assert.*;

/**
 */
public class ZNumberTest {

    @Test
    public void testAssignment(){
        ZNumber zn = new ZNumber(0);

        assertTrue( zn.actual() instanceof Integer );
        zn = new ZNumber(0.00);

        assertTrue( zn.actual() instanceof Double );

        // test auto assign
        zn = new ZNumber("0.121010011104111973947987917179719219798783798473974398");
        assertTrue( zn.actual() instanceof Real);

        zn = new ZNumber("0.121010011100000");
        assertTrue( zn.actual() instanceof Double);

        zn = new ZNumber(1134234229);
        assertTrue( zn.actual() instanceof Integer );

        zn = new ZNumber(1134234229998989l);
        assertTrue( zn.actual() instanceof Long );

        zn = new ZNumber("1134234229998989979473948759348759834793");
        assertTrue( zn.actual() instanceof LargeInteger);

        ZNumber n0 = new ZNumber(0);
        ZNumber d0 = new ZNumber(0.0);
        assertTrue( n0.equals(d0) );

        ZNumber d01 = new ZNumber(0.01);
        assertFalse( n0.equals(d01) );
        assertFalse( d0.equals(d01) );

        assertEquals( n0.compareTo(d0) , 0 );
        assertEquals( d0.compareTo(d01) , -1 );
        assertEquals( n0.compareTo(d01) , -1 );

    }

    @Test
    public void testAbs(){

        ZNumber n0 = new ZNumber(0);
        ZNumber d0 = new ZNumber(0.0);

        assertEquals( n0._abs_() , 0 );
        assertEquals( d0._abs_() , 0.0 );

        ZNumber d01 = new ZNumber(0.01);
        assertTrue( d01._abs_().doubleValue() - 0.01 < 0.0001);
        d01 = new ZNumber(-0.01);
        assertTrue( d01._abs_().doubleValue() > 0 );
        assertTrue( d01._abs_().doubleValue() - 0.01 < 0.0001);

        ZNumber zn = new ZNumber("0.121010011104111973947987917179719219798783798473974398");
        assertTrue( zn._abs_() instanceof Real);

    }

    @Test
    public void testAdd(){
        ZNumber n0 = new ZNumber(0);
        ZNumber d0 = new ZNumber(0.0);
        assertEquals( 0.0, d0._add_(n0));
        assertEquals( 0.0, n0._add_(d0));
        long ln = -123213131312311131l ;
        ZNumber zln = new ZNumber(ln);
        // must upcast?
        Object r = zln._add_(n0);
        assertEquals( ln , r);
    }

    @Test
    public void testPower(){
        ZNumber zn = new ZNumber(12331201231.12321312312301301d);
        long p = 0 ;
        Object o = zn._pow_(p);
        assertEquals( 1.0d, o );
        zn = new ZNumber("121213123182012810280129824109481098");
        o = zn._pow_(p);
        assertEquals( 1, o );
    }

    @Test
    public void testMultiplication(){
        int i = 6 ;
        ZNumber n = new ZNumber(7);
        Object o = n._mul_(i);
        assertEquals(42,o);
        n = new ZNumber(7.0);
        o = n._mul_(i);
        assertEquals(42.0,o);
        o = n._mul_(6.00);
        assertEquals(42.0,o);
    }

    @Test
    public void testDivision(){
        int i = 6 ;
        ZNumber n = new ZNumber(42);
        Object o = n._div_(i);
        assertEquals(7,o);

        n = new ZNumber(42.0);
        o = n._div_(6);
        assertEquals(7.0,o);

        ZNumber a = new ZNumber("0.11213131010801823013821093221097319873211381981111109099080921");
        ZNumber b = new ZNumber("0.9809809809890119018230981203981309173891481640872630164012746127");
        o = a._div_(b);
        assertTrue( o instanceof Real );
    }

    @Test
    public void testLogic(){
        ZNumber n = new ZNumber(1);
        n.and_mutable(0);
        assertEquals(n,0);

        n.or_mutable(1);
        assertEquals(n,1);

        n.xor_mutable(0);
        assertEquals(n,1);

        n.mod_mutable( 2);
        assertEquals(n,1);

        n.not_mutable();
        // this should work out
        assertEquals( n , -1);
        // now the mutable logic
        n.sub_mutable(-1);
        assertEquals( n , 0);

    }

    @Test
    public void testExtended(){
        ZNumber n = new ZNumber(1.1);
        assertEquals(1, n.floor() );
        assertEquals(2, n.ceil() );
        // test hash code
        Set s = ZSet.set( null, new ZNumber(1), new ZNumber(1) );
        assertEquals( s.size(), 1);
        s.clear();
        s = ZSet.set( null, new ZNumber(1), n );
        assertEquals( s.size(), 2);
    }

    @Test
    public void cielTest(){
        ZNumber zn = new ZNumber(0.1);
        assertEquals(1, zn.ceil());
        zn = new ZNumber(2.1);
        assertEquals(3, zn.ceil());
        zn = new ZNumber(-2.1);
        assertEquals(-2, zn.ceil());
        zn = new ZNumber(-2.0);
        assertEquals(-2, zn.ceil());
        zn = new ZNumber(0.0);
        assertEquals(0, zn.ceil());
    }

    @Test
    public void floorTest(){
        ZNumber zn = new ZNumber(0.1);
        assertEquals(0, zn.floor());
        zn = new ZNumber(2.1);
        assertEquals(2, zn.floor());
        zn = new ZNumber(-2.1);
        assertEquals(-3, zn.floor());
        zn = new ZNumber(-2.0);
        assertEquals(-2, zn.floor());
        zn = new ZNumber(0.0);
        assertEquals(0, zn.floor());
    }

    @Test
    public void roundingTest(){
        Number rn = ZNumber.round(0.2314, 2 );
        assertEquals( 4,rn.toString().length());
        assertEquals( rn, 0.23);
        rn = ZNumber.round(0.2514, 1 );
        assertEquals( 3,rn.toString().length());
        assertEquals( rn, 0.3);
    }

    @Test
    public void powerTest(){
        // -ve power
        ZNumber n = new ZNumber(2.0);
        Number p = (Number) n._pow_(-1);
        assertEquals(p,0.5);
        // 0 power
        n = new ZNumber(2.0);
        p = (Number) n._pow_(0);
        assertEquals(p,1.0);
        // fractional power
        n = new ZNumber(4.0);
        p = (Number) n._pow_(0.5);
        assertEquals(p,2.0);
    }
}
