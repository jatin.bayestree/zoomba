/*
 * Copyright 2017 zoomba-lang.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package zoomba.lang.core.sys;

import zoomba.lang.core.collections.ZArray;
import java.io.*;
import java.lang.management.ManagementFactory;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;


/**
 * A wrapper on Process for ZoomBA
 */
public class ZProcess{

    /**
     * Java does not let you play with the current process,
     * Therefore, this class is required
     */
    public static class CurrentProcess extends Process{

        public final long pid;

        public CurrentProcess(){
            String vmName = ManagementFactory.getRuntimeMXBean().getName();
            int p = vmName.indexOf("@");
            String pidString = vmName.substring(0, p);
            pid = Long.valueOf(pidString);
        }

        @Override
        public OutputStream getOutputStream() {
            return new OutputStream() {
                @Override
                public void write(int b) throws IOException {
                }
            };

        }

        @Override
        public InputStream getInputStream() {
            return new InputStream() {
                @Override
                public int read() throws IOException {
                    return -1;
                }
            };
        }

        @Override
        public InputStream getErrorStream() {
            return new InputStream() {
                @Override
                public int read() throws IOException {
                    return -1;
                }
            };
        }

        @Override
        public int waitFor() throws InterruptedException {
            return 0;
        }

        @Override
        public int exitValue() {
            return 0;
        }

        @Override
        public void destroy() {

        }
    }


    //https://stackoverflow.com/questions/5483830/process-waitfor-never-returns
    public static class AsyncProcessStreamReader implements Runnable{

        private final Process process;

        private final boolean errorStream;

        private final StringBuilder builder;

        private final Thread snoop;

        public AsyncProcessStreamReader(Process p, boolean errorStream){
            process = p ;
            this.errorStream = errorStream ;
            builder = new StringBuilder();
            snoop = new Thread(this);
            snoop.start();
        }

        @Override
        public void run() {
            BufferedReader reader = new BufferedReader( new InputStreamReader(
                    errorStream ? process.getErrorStream() : process.getInputStream()
            ) );

            String line;
            try {
                while ((line = reader.readLine()) != null) {
                    builder.append(line).append("\n");
                }
            }catch (IOException e){

            }
        }

        public String data(){
            return builder.toString();
        }
    }


    /**
     * Process Information
     */
    public static final class ZProcInfo{

        /**
         * The commands which were used
         */
        public final String[] com;

        /**
         * The error string or file name
         */
        public String err;

        /**
         * The output string or file name
         */
        public String out;

        String stdin;

        /**
         * The exit code of the process
         * Making it integer to ensure we do have null
         * In case the process is not over yet
         */
        public Integer code;

        public final boolean redirectError;

        public final boolean redirectOutput;

        public final boolean redirectInput;

        /**
         * Create an info using args
         * @param args the arguments
         */
        public ZProcInfo(String...args){
            if ( args.length == 1 ){
                // can args[0] be tokenized?
                com = args[0].split("\\s+");
            } else {
                com = args;
            }
            code = null ;
            err = "" ;
            out = "" ;
            redirectError = false;
            redirectOutput = false ;
            redirectInput = false ;
        }

        public ZProcInfo(Map map){
            code = null;
            Object args = map.getOrDefault( "args", ZArray.EMPTY_ARRAY );
            ZArray zargs = new ZArray(args,false);
            com = new String[ zargs.length ];
            for ( int i = 0; i < zargs.length; i++ ){
                com[i] = zargs.get(i).toString();
            }
            Map redirects = ((Map)map.getOrDefault("redirect", Collections.emptyMap()));
            if  ( redirects.isEmpty() ){
                redirectOutput = false;
                redirectError = false ;
                redirectInput = false ;
            } else {
                // at least some is non empty ...
                out = String.valueOf(redirects.getOrDefault("stdout", ""));
                err = String.valueOf(redirects.getOrDefault("stderr", ""));
                stdin = String.valueOf(redirects.getOrDefault("stdin", ""));
                redirectError = !err.isEmpty();
                redirectOutput = !out.isEmpty();
                redirectInput = !stdin.isEmpty();
            }
        }

        /**
         * Starts a process
         * @return the process
         */
        public Process process(){
            try {

                ProcessBuilder pb = new ProcessBuilder();
                if ( redirectInput ){
                    pb.redirectInput(new File(stdin) );
                }
                if ( redirectOutput ){
                    pb.redirectOutput(new File(out) );
                }
                if ( redirectError ){
                    pb.redirectError(new File(err) );
                }
                pb.command(new ZArray(com, false));
                return pb.start();
            }catch (Exception e){
                throw new UnsupportedOperationException(e);
            }
        }

        /**
         * Starts a ZProcess
         * @return a ZProcess
         */
        public ZProcess start(){
            return new ZProcess(this);
        }
    }

    /**
     * System command
     * @param args arguments
     * @return exit code
     */
    public static int system(Object...args){
        try {
            ZProcess process = popen(args);
            return process.status().code;
        }catch (Exception ioe){
            return Integer.MIN_VALUE ;
        }
    }


    /**
     * Opens a ZProcess
     * @param args the arguments
     * @return a ZProcess
     */
    public static ZProcess popen(Object...args){
        if ( args.length == 0 ) return new ZProcess(); // can not do much, can we ?
        if ( args.length == 1 ) {
            if ( args[0] instanceof Map ){
                // handle it in a totally different way ...
                return new ZProcess( new ZProcInfo((Map)args[0]));
            }

            if ( args[0].getClass().isArray() ){
                args[0] = new ZArray(args[0],false);
            }
            if ( args[0] instanceof List ) {
                // perhaps we need to unwrap...
                args = ((List) args[0]).toArray();
            }
        } else if ( args.length == 2 ){ // can be exec followed by args...
            List l = new ArrayList();
            l.add(args[0]);
            if ( args[1].getClass().isArray() ){
                args[1] = new ZArray(args[1],false);
            }
            if ( args[1] instanceof List ) {
                // this is the case that one arg (executable) followed by args in an array or list...
                // then we need to unwrap...
                l.addAll( (List)args[1]);
                args = l.toArray();
            }
        }

        String[] commands = new String[args.length];
        for ( int i = 0 ; i < commands.length ; i++ ){
            commands[i] = String.valueOf(args[i]);
        }
        ZProcInfo info = new ZProcInfo(commands);
        return new ZProcess(info);
    }

    /**
     * The underlying process
     */
    public final Process process;

    /**
     * The process info
     */
    public final ZProcInfo procInfo;

    private final AsyncProcessStreamReader outReader;

    private final AsyncProcessStreamReader errReader;


    /**
     * Creates out of a process info
     * @param info the process info
     */
    public ZProcess(ZProcInfo info){
        procInfo = info ;
        process = procInfo.process();
        if ( procInfo.redirectOutput ){
            outReader = null;
        } else {
            outReader = new AsyncProcessStreamReader(process,false);
        }
        if ( procInfo.redirectError ){
            errReader = null;
        } else {
            errReader = new AsyncProcessStreamReader(process,true);
        }
    }

    private ZProcess(){
        process = new CurrentProcess();
        procInfo = null;
        outReader = null;
        errReader = null;
    }

    /**
     * Waits on the process - indefinitely,
     * May hang the system
     */
    public void waitOn(){
        pollWait(1000);
    }

    /**
     * Waits on the process - indefinitely,
     * @param snoopTime the sleep time for each call
     * May hang the system
     */
    public void pollWait(int snoopTime){
        while ( process.isAlive() ){
            try {
                Thread.sleep(snoopTime);
            }catch (Exception e){}
        }
    }

    /**
     * Waits on the process till timeout
     * @param timeOut the Timeout in millisec
     * @return true if process exited, false if it did not
     */
    public boolean waitOn(long timeOut){
        if ( timeOut <= 0 ) {
            waitOn();
            return true ;
        }
        final long sleepTime = timeOut/ 10 ;
        long curTime = System.currentTimeMillis();
        final long endTime = curTime + timeOut ;
        while (  curTime < endTime ){
            try {
                Thread.sleep(sleepTime);
                curTime = System.currentTimeMillis();
                if ( !process.isAlive() ) return true ;
            }catch (Exception e){}
        }
        return false ;
    }

    /**
     * Kills the process
     * @return if the process is killed then true, else false
     */
    public boolean kill(){
        process.destroy();
        // wait for some time to synch the status up?
        try {
            Thread.sleep(1000);
        }catch (Exception e){}
        return !process.isAlive();
    }

    /**
     * Gets the status of the process
     * @param wait 0 or more long - if not specified waits indefinitely for the process to complete,
     *             if specified wait for that many ms for the process to complete
     *             before getting the status
     * @return ZProcInfo
     */
    public ZProcInfo status(long...wait){
        if ( procInfo.code == null ) {
            if ( wait.length == 0 ){
                waitOn();
            } else {
                waitOn( wait[0] );
            }
            if ( !procInfo.redirectError ) {
                procInfo.err = errReader.data();
            }
            if ( !procInfo.redirectOutput ) {
                procInfo.out = outReader.data();
            }
            procInfo.code = process.exitValue();
        }
        return procInfo;
    }

}
