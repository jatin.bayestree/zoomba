/*
 * Copyright 2017 zoomba-lang.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package zoomba.lang.core.sys;

import zoomba.lang.core.collections.ZArray;
import zoomba.lang.core.interpreter.AnonymousFunctionInstance;
import zoomba.lang.core.operations.Function;
import zoomba.lang.core.types.ZNumber;
import zoomba.lang.core.types.ZTypes;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * A Thread wrapper for ZoomBA
 * Capable of returning value back to the main
 * Because it is a MonadicContainer type
 */
public class ZThread extends Thread implements Runnable, Function.MonadicContainer {

    private static final Map<Thread, List<String>> errorStacks = new ConcurrentHashMap<>();

    public static void appendError( List<String> callStack ){
      Thread th = Thread.currentThread();
      if ( !errorStacks.containsKey( th ) ){
         errorStacks.put(th,callStack);
      }
    }

    private static final Function.MonadicContainer THREAD_RUNNING = new Function.MonadicContainerBase();

    protected Function f;

    protected Function.MonadicContainer state;

    protected Object threadArg;

    protected Function after;

    protected List<String> callStackUptoThread;

    private void setupCallStack(){
        if  ( f instanceof AnonymousFunctionInstance ){
            callStackUptoThread = new ArrayList<>( ((AnonymousFunctionInstance) f).interpret.callStack());
        } else {
            callStackUptoThread = Collections.emptyList();
        }
    }

    /**
     * Creates a Thread using
     * @param threadFunction the function to be used as thread
     * @param arg arguments to the thread
     */
    public ZThread(Function threadFunction, Object... arg){
        this.f = threadFunction ;
        setupCallStack();
        // wrap it...
        threadArg = arg ;
        state  =  THREAD_RUNNING ;
        after = Function.NOP ;
    }

    /**
     * Creates a thread with
     * @param threadFunction the thread function
     * @param after a callback function once the thread is done executing
     */
    public ZThread(Function threadFunction, Function after){
        this.f = threadFunction ;
        setupCallStack();
        // wrap it...
        threadArg = ZArray.EMPTY_ARRAY  ;
        state  =  THREAD_RUNNING ;
        this.after = after ;
    }

    @Override
    public boolean isNil() {
        return state.isNil() ;
    }

    @Override
    public Object value() {
        return state.value();
    }

    protected void printDebugStack(Thread thread, Throwable throwable){
        List<String> stack = errorStacks.getOrDefault(thread, Collections.emptyList());
        System.err.printf("Error in Thread-id %d %n", Thread.currentThread().getId());
        System.err.println( throwable.toString() );
        if ( !stack.isEmpty() ){
            System.err.println( ZTypes.string(stack, "\n"));
        }
        System.err.println( ZTypes.string(callStackUptoThread, "\n"));
        System.err.println(".....");
    }

    @Override
    public void run() {
        Thread thread = Thread.currentThread();
        // process args...
        Object[] args = new Object[4];
        // get the id
        args[0] = getId();
        // current
        args[1] = this ;
        // the args are the context
        args[2] = threadArg ;
        // there is no partial
        args[3] = Function.NIL;
        try {
            state = f.execute(args);
        }catch (Throwable throwable){
            state = new Function.MonadicContainerBase(throwable);
            printDebugStack(thread, throwable );
        }finally {
            after.execute( state.value() );
            errorStacks.remove(thread);
        }

    }

    /**
     * Runs a function in asynchronous mode
     * @param threadFunction the function to be run
     * @param after the callback function
     * @return a Runnable which is running
     */
    public static Runnable async( Function threadFunction, Function after){
        Thread t = new ZThread(threadFunction, after );
        t.start();
        return t;
    }

    /**
     * Poll until predicate is true
     * @param predicate the condition
     * @param args integers on no of loops and poll interval
     * @return true when polling successful false otherwise
     */
    public static boolean poll( Function predicate, Object[] args){
        if ( predicate == null ){
            predicate = Function.TRUE ;
        }
        int maxLoop = 42 ; // no of times
        int pollInterval = 420 ; // ms
        if ( args.length > 0 ){
            maxLoop = ZNumber.integer( args[0] , maxLoop ).intValue() ;
            if ( args.length > 1 ){
                pollInterval = ZNumber.integer( args[1] , pollInterval ).intValue() ;
            }
        }
        while( true ) {
            maxLoop--;
            Function.MonadicContainer container = predicate.execute( );
            if (ZTypes.bool( container.value() , false) ){
                return true ;
            }
            if ( maxLoop == 0 ) return false ;
            try { Thread.sleep( pollInterval ); } catch (Throwable t){}
        }
    }
}
