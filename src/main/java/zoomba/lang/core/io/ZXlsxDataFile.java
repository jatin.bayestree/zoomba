/*
 * Copyright 2019 zoomba-lang.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package zoomba.lang.core.io;

import zoomba.lang.core.collections.ZArray;
import zoomba.lang.core.operations.ZMatrix;
import zoomba.lang.core.types.ZTypes;

import java.io.File;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamReader;


/**
 * From :
 * https://sourceforge.net/p/xlsxparserforja/code/ci/master/tree/xlsxparser/Xlsx.java
 * Basic MS XLSX File Handling for reading
 */
public class ZXlsxDataFile {

    public class XlsxMatrix extends ZMatrix.BaseZMatrix{

        final String[][] data ;

        public XlsxMatrix(String sheetName){
            if ( !ZXlsxDataFile.this.sheets.containsKey(sheetName) ){
                throw new UnsupportedOperationException(String.format("Sheet '%s' does not exists!", sheetName));
            }
            data = (String[][])ZXlsxDataFile.this.sheets.get(sheetName);
        }

        @Override
        public int rows() {
            if ( data == null ){ return 0; }
            return ( data.length ) ;
        }

        @Override
        public List row(int rowNum) {
            if ( rowNum >= rows() ) return null;
            return new ZArray( data[rowNum] , false );
        }
    }

    public static class XlsxDataLoader implements ZMatrix.ZMatrixLoader {

        public static XlsxDataLoader LOADER = new XlsxDataLoader();

        private Map<String,ZXlsxDataFile> files = new ConcurrentHashMap<>();

        private XlsxDataLoader(){ }

        @Override
        public Pattern loadPattern() {
            return Pattern.compile("^.+\\.(xlsx)$", Pattern.CASE_INSENSITIVE );
        }

        @Override
        public ZMatrix load(String path, Object... args) {
            if ( args.length < 1 ) return null;
            File f = new File(path);
            try {
               final String key =  f.getAbsolutePath();
               boolean reload = false ;
               if ( args.length > 1 ){
                   reload = ZTypes.bool(args[1],false);
               }
               if ( reload || !files.containsKey(key) ){
                   ZXlsxDataFile zXlsxDataFile = new ZXlsxDataFile(path);
                   files.put(key, zXlsxDataFile);
               }
               return files.get(key).matrix(String.valueOf(args[0]));

            } catch ( Exception e) {
                throw new UnsupportedOperationException("Error in parsing xlsx file : " +  e);
            }
        }
    }

    private String[] sharedStrings;
    private Map<String, Object> sheets = new LinkedHashMap<>();
    private static DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private String[] styles;
    private Map<String, String> numFmts;
    private XMLInputFactory factory;

    public ZXlsxDataFile(String  path) throws Exception {
        parse( new File(path));
    }

    public void parse(File f) throws Exception {
        ZipFile z = new ZipFile(f);
        factory = XMLInputFactory.newInstance();
        parseWorkbook(z.getInputStream(z.getEntry("xl/workbook.xml")));
        ZipEntry ss = z.getEntry("xl/sharedStrings.xml");
        if (ss != null)
            parseSharedStrings(z.getInputStream(ss));
        parseStyles(z.getInputStream(z.getEntry("xl/styles.xml")));
        for (Map.Entry e : this.sheets.entrySet()) {
            e.setValue(parseSheet(z.getInputStream(z.getEntry("xl/worksheets/sheet" + e.getValue() + ".xml"))));
        }
        z.close();
    }

    public Map<String, String[][]> sheets() {
        return (Map<String, String[][]>) (Map) this.sheets;
    }

    public ZMatrix matrix( String sheet){
      return new XlsxMatrix(sheet);
    }

    private int[][] range2num(String r) {
        String[] rs = r.split("[:]");
        int[][] ret = new int[2][];
        ret[0] = ref2num(rs[0]);
        if (rs.length > 1) {
            ret[1] = ref2num(rs[1]);
        } else
            ret[1] = ret[0];
        return ret;
    }

    private int[] ref2num(String r) {
        char c0 = r.charAt(0);
        char c1 = r.charAt(1);
        if (Character.isDigit(c1))
            return new int[]{c0 - 'A', Integer.parseInt(r.substring(1)) - 1};
        else
            return new int[]{(c0 - 'A' + 1) * 26 + (c1 - 'A'), Integer.parseInt(r.substring(2)) - 1};
    }

    private String getSheetId( XMLStreamReader r){

        //  "http://schemas.openxmlformats.org/officeDocument/2006/relationships"

        for ( int inx = 0; inx < r.getAttributeCount(); inx++){
           QName name = r.getAttributeName(inx);
           String ns = name.getNamespaceURI();
           if ( ns != null && ns.endsWith("/relationships") ){
               return r.getAttributeValue(inx).substring(3);
           }
        }
        return "";
    }

    private void parseWorkbook(InputStream inputStream) throws Exception {

        XMLStreamReader r = factory.createXMLStreamReader(inputStream);
        while (r.hasNext()) {
            r.next();
            if (r.getEventType() == XMLStreamConstants.START_ELEMENT) {
                if (r.getLocalName().equals("sheet")) {
                    final String sheetName = r.getAttributeValue(null, "name");
                    String sheetId = getSheetId(r);
                    sheets.put( sheetName, sheetId);
                }
            }
        }
        r.close();
    }

    private void parseStyles(InputStream inputStream) throws Exception {
        XMLStreamReader r = factory.createXMLStreamReader(inputStream);
        while (r.hasNext()) {
            r.next();
            if (r.getEventType() == XMLStreamConstants.START_ELEMENT) {
                if (r.getLocalName().equals("numFmts")) {
                    numFmts = new HashMap<String, String>();
                    while (r.hasNext()) {
                        r.next();
                        if (r.getEventType() == XMLStreamConstants.START_ELEMENT && r.getLocalName().equals("numFmt"))
                            numFmts.put(r.getAttributeValue(null, "numFmtId"), r.getAttributeValue(null, "formatCode"));
                        else if (r.getEventType() == XMLStreamConstants.END_ELEMENT
                                && r.getLocalName().equals("numFmts")) {
                            break;
                        }
                    }
                } else if (r.getLocalName().equals("cellXfs")) {
                    styles = new String[Integer.parseInt(r.getAttributeValue(null, "count"))];
                    int i = 0;
                    while (r.hasNext()) {
                        r.next();
                        if (r.getEventType() == XMLStreamConstants.START_ELEMENT && r.getLocalName().equals("xf"))
                            styles[i++] = r.getAttributeValue(null, "numFmtId");
                        else if (r.getEventType() == XMLStreamConstants.END_ELEMENT
                                && r.getLocalName().equals("cellXfs")) {
                            break;
                        }
                    }
                }
            }
        }
        r.close();
    }

    private boolean isDateStyle(String style) {
        if (this.numFmts == null)
            return false;
        String s = styles[Integer.valueOf(style)];
        if (s.equals("14"))
            return true;
        String numFmt = this.numFmts.get(s);
        if (numFmt != null) {
            return numFmt.contains("d") && numFmt.contains("m") && numFmt.contains("y");
        }
        return false;
    }

    String parseDate(String value){
        try {
            double d = Double.parseDouble(value);
            long myLong = System.currentTimeMillis() + ((long) (d * 1000));
            Date javaDate= new Date(myLong);
            return dateFormat.format(javaDate);
        } catch (Exception e){
        }
        return null;
    }

    private String[][] parseSheet(InputStream is) throws Exception {
        XMLStreamReader r = factory.createXMLStreamReader(is);
        String[][] data = null;
        while (r.hasNext()) {
            r.nextTag();
            if (r.getLocalName().equals("dimension")) {
                int[][] range = range2num(r.getAttributeValue(null, "ref"));
                data = new String[range[1][1] + 1][range[1][0] + 1];
                break;
            }
        }
        Calendar c = Calendar.getInstance();
        while (r.hasNext()) {
            r.next();
            if (r.getEventType() == XMLStreamConstants.START_ELEMENT) {
                if (r.getLocalName().equals("c")) {
                    int[] ref = ref2num(r.getAttributeValue(null, "r"));
                    String type = r.getAttributeValue(null, "t");
                    String style = r.getAttributeValue(null, "s");
                    while (true) {
                        r.next();
                        if (r.getEventType() == XMLStreamConstants.END_ELEMENT && r.getLocalName().equals("c")) {
                            break;
                        } else if (r.getEventType() == XMLStreamConstants.START_ELEMENT) {
                            if (r.getLocalName().equals("v")) {
                                String v = r.getElementText();
                                if (type != null) {
                                    if (type.equals("s")) {
                                        data[ref[1]][ref[0]] = this.sharedStrings[Integer.parseInt(v)];
                                    } else if (type.equals("str")) {
                                        data[ref[1]][ref[0]] = v;
                                    }
                                } else if (style != null) {
                                    if (isDateStyle(style)) {
                                        // TODO 1900-02-29
                                        c.set(1900, 0, Integer.parseInt(v) - 1, 0, 0, 0);
                                        data[ref[1]][ref[0]] = dateFormat.format(c.getTime());
                                    } else {
                                        // there is style, but probably not date, at all...
                                        // in case some later date formatting issue happens will revisit
                                        data[ref[1]][ref[0]] = v;
                                        /*
                                        String dtP = parseDate(v);
                                        if ( dtP != null ){
                                            data[ref[1]][ref[0]] = dtP ;
                                        } else {
                                            data[ref[1]][ref[0]] = v;
                                        }
                                        */

                                    }
                                } else
                                    data[ref[1]][ref[0]] = v;
                            }
                        }
                    }
                }
            }
        }
        r.close();
        return data;
    }

    private void parseSharedStrings(InputStream inputStream) throws Exception {
        XMLStreamReader r = factory.createXMLStreamReader(inputStream);
        int index = 0;
        r.nextTag();
        if (!r.getLocalName().equals("sst"))
            throw new AssertionError();
        String attValue = r.getAttributeValue(null, "count");
        if ( attValue == null ){
            System.err.println("'count' attribute does not exist in the data sheet xml!");
        }
        sharedStrings = new String[Integer.parseInt(attValue)];
        while (r.hasNext()) {
            r.next();
            if (r.getEventType() == XMLStreamConstants.START_ELEMENT) {
                if (r.getLocalName().equals("si")) {
                    StringBuilder sb = new StringBuilder();
                    while (true) {
                        r.next();
                        if (r.getEventType() == XMLStreamConstants.END_ELEMENT && r.getLocalName().equals("si"))
                            break;
                        else if (r.getEventType() == XMLStreamConstants.CHARACTERS) {
                            sb.append(r.getText());
                        }
                    }
                    sharedStrings[index++] = sb.toString();
                }
            }
        }
        r.close();
    }

}
