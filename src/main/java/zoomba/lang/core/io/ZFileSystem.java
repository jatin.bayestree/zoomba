/*
 * Copyright 2017 zoomba-lang.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package zoomba.lang.core.io;

import zoomba.lang.core.collections.ZArray;
import zoomba.lang.core.operations.Function;
import zoomba.lang.core.types.ZTypes;

import java.io.*;
import java.nio.file.*;
import java.util.Iterator;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * A FileSystem utility
 */
public class ZFileSystem {

    /**
     * A File Item iterator.
     * Treats a file as a collection of string lines,
     * TODO given condition, also as chunks of bytes, we should
     * In case of directory, as a collection of child Files
     */
    public static class ZFItemIterator implements Iterator {

        /**
         * The underlying file object
         */
        public final File file;

        /**
         * The underlying iterator
         */
        ZArray.ArrayIterator filesIterator;

        /**
         * Is the object a dictionary
         */
        final boolean isDict;

        public static class WinEnabledReader{

            final BufferedReader br;

            final Reader reader;

            public WinEnabledReader( File f, boolean crlf) throws Exception {
                reader = new FileReader( f );
                if ( crlf ){
                    br = null;
                } else {
                    br = new BufferedReader( reader );
                }
            }

            public String readLine() throws IOException {
                if ( br != null ){ return br.readLine(); }
                StringBuffer buf = new StringBuffer();
                int c;
                while ( true ){
                    c = reader.read();
                    if ( c == -1 ){
                        reader.close();
                        if ( buf.length() == 0 ){
                            return null; // ignore last empty line...
                        }
                        break;
                    }
                    // forward looking?
                    if ( (char)c == '\r' ){
                        c = reader.read();
                        if ( c == -1 ){
                            reader.close();
                            if ( buf.length() == 0 ){
                                return null; // ignore last empty line...
                            }
                            break;
                        }
                        if ( (char)c == '\n'){
                            // newline
                            break;
                        }
                    }
                    buf.append((char)c);
                }
                return buf.toString();
            }
        }

        WinEnabledReader br;

        String line;

        ZFItemIterator(File f, boolean winExcel ) {
            file = f;
            if (!file.exists())
                throw new UnsupportedOperationException("Can not create iterator on non existent file!");
            if (file.isDirectory()) {
                filesIterator = new ZArray.ArrayIterator(file.listFiles());
                isDict = true;
                br = null;
            } else {
                try {
                    br = new WinEnabledReader(file, winExcel );
                    line = br.readLine();
                    isDict = false;
                } catch (Exception e) {
                    throw new UnsupportedOperationException(e);
                }
            }
        }

        @Override
        public boolean hasNext() {
            if (isDict) return filesIterator.hasNext();
            return line != null;
        }

        @Override
        public Object next() {
            if (isDict) return filesIterator.next();
            String oldLine = line;
            line = null;
            try {
                line = br.readLine();
            } catch (Throwable t) {
            }
            return oldLine;
        }

    }

    /**
     * A File Item iterable
     * Treats a file as a collection of string lines,
     * TODO given condition, also as chunks of bytes, we should
     * In case of directory, as a collection of child Files
     */
    public static class ZFile implements Iterable {

        /**
         * Underlying file object
         */
        public final File file;

        /**
         * Should we be using crlf line ending? '\r\n'
         */
        public final boolean crlfLineEnding;

        ZFile(String path) {
            this(path,false);
        }

        ZFile(String path, boolean winFile) {
            this.file = new File(path);
            this.crlfLineEnding = winFile;
        }

        /**
         * Gets the extension of the file, if any
         * @return the extension, wo the dot
         */
        public String extension() {
            String name = file.getName() ;
            int last = name.lastIndexOf( '.' );
            if ( last < 0 ) return "";
            return name.substring( last );
        }

        @Override
        public Iterator iterator() {
            return new ZFItemIterator(file, crlfLineEnding);
        }

        /**
         * The whole of the file as a single String
         * @return the full content of the file
         * @throws Exception in case any errors
         */
        public String all() throws Exception {
            Path p = file.toPath();
            byte[] bytes = Files.readAllBytes(p);
            return new String(bytes);
        }

        /**
         * The whole of the file as a List of Strings, lines
         * @return the content of the file as list of strings
         * @throws Exception in case any errors
         */
        public List<String> lines() throws Exception {
            Path p = file.toPath();
            return Files.readAllLines(p);
        }

        /**
         * Any meta property of the file
         * @param propertyName the property one needs to read
         *                     It does not follow links
         * @return the value of the property
         * @throws Exception in case of error
         */
        public Object get(String propertyName) throws Exception {
            Path p = file.toPath();
            return Files.getAttribute(p, propertyName, LinkOption.NOFOLLOW_LINKS);
        }

        @Override
        public String toString() {
            return file.toString();
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            ZFile zFile = (ZFile) o;

            return file.equals(zFile.file);

        }

        @Override
        public int hashCode() {
            return file.hashCode();
        }

        /**
         * Packs the file into the destination
         * @param destFileName the destination full path
         * @return true if success false if failed
         */
        public boolean pack(String destFileName){
            try {
                if (file.isDirectory()) {
                    packDirectory(file.getAbsolutePath(), destFileName);
                } else {
                    packFile(file, destFileName);
                }
                return true;
            }catch (IOException e){
                return false;
            }
        }
        /**
         * Packs the file into the same directory as the current file
         * With extension fileName.zip
         * @return true if success false if failed
         */
        public boolean pack(){
            String destFileName = file.getParent() + "/" + file.getName() + ".zip" ;
            return pack(destFileName);
        }
    }

    /**
     * The File Abstraction from a path
     * @param args 0 path to the file 1 optional boolean treat '\r\n' as newline and nothing else
     * @return a ZFile object
     */
    public static ZFile file(Object...args) {
        String path = String.valueOf(args[0]);
        if ( args.length > 1 ){
            return new ZFile(path, ZTypes.bool(args[1], false ));
        }
        return new ZFile(path);
    }

    /**
     * Opens a file
     * @param args
     *     0 path
     *     1 mode one of "r"ead,"w"rite,"a"pend
     * @return a PrintStream or BufferedReader in case of no args, returns System.in
     */
    public static Closeable open(Object... args) {
        if (args.length == 0) return System.in;
        String path = "";
        if (args[0] instanceof File) {
            path = args[0].toString();
        } else {
            path = String.valueOf(args[0]);
        }
        String mode = "r";
        if (args.length > 1) {
            mode = String.valueOf(args[1]);
        }
        mode = mode.toLowerCase();
        try {
            switch (mode) {
                case "wb":
                case "bw":
                    return new FileOutputStream(path);

                case "w":
                    return new PrintStream(new FileOutputStream(path));
                case "a":
                    return new PrintStream(new FileOutputStream(path, true), true);

                case "rb":
                case "br":
                    return new FileReader(path);

                case "r":
                default:
                    return new BufferedReader(new FileReader(path));
            }
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    /**
     * Writes into a file
     * @param args
     *   0 path
     *   1 the data
     *   2 mode, if specified "a" then appends, "b" does a binary write
     * @return the path to the file
     */
    public static Path write(Object... args) {

        if (args.length == 0) {
            throw new UnsupportedOperationException("Can not write to nothing!");
        }
        try {
            if (args.length == 1) {
                return Files.write(new File(String.valueOf(args[0])).toPath(), new byte[]{});
            }
            boolean append = false;
            if ( args.length > 2 ){
                if ( "b".equals(args[2] ) ){
                    // binary writing
                    byte[] payLoad;
                    if ( args[1] instanceof byte[] ){
                        payLoad = (byte[])args[1] ;
                    } else {
                        payLoad = String.valueOf(args[1]).getBytes();
                    }
                    return Files.write( new File(String.valueOf(args[0])).toPath(), payLoad ,
                            StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);

                }
                append = ZTypes.bool(args[2], false);
            }

            if (append) {
                return Files.write(new File(String.valueOf(args[0])).toPath(),
                        String.valueOf(args[1]).getBytes(), StandardOpenOption.CREATE, StandardOpenOption.APPEND);
            }
            return Files.write(new File(String.valueOf(args[0])).toPath(), String.valueOf(args[1]).getBytes(),
                    StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Zips a directory
     * @param sourceDirPath source directory
     * @param zipFilePath output directory
     * @throws IOException in error
     */
    public static void packDirectory(String sourceDirPath, String zipFilePath) throws IOException {
        Path p = Files.createFile(Paths.get(zipFilePath));
        try (ZipOutputStream zs = new ZipOutputStream(Files.newOutputStream(p))) {
            Path pp = Paths.get(sourceDirPath);
            Files.walk(pp)
                    .filter(path -> !Files.isDirectory(path))
                    .forEach(path -> {
                        ZipEntry zipEntry = new ZipEntry(pp.relativize(path).toString());
                        try {
                            zs.putNextEntry(zipEntry);
                            Files.copy(path, zs);
                            zs.closeEntry();
                        } catch (IOException e) {
                            System.err.println(e);
                        }
                    });
        }
    }

    /**
     * Zips a File
     * @param sourceFile source file
     * @param zipFilePath output directory
     * @throws IOException in error
     */
    public static void packFile(File sourceFile, String zipFilePath) throws IOException{
        byte[] buffer = new byte[1024];
        FileOutputStream fos = new FileOutputStream(zipFilePath);
        ZipOutputStream zos = new ZipOutputStream(fos);
        ZipEntry ze= new ZipEntry(sourceFile.getName());
        zos.putNextEntry(ze);
        FileInputStream in = new FileInputStream(sourceFile);

        int len;
        while ((len = in.read(buffer)) > 0) {
            zos.write(buffer, 0, len);
        }

        in.close();
        zos.closeEntry();

        //remember close it
        zos.close();
    }

    /**
     * Reads from a file
     * @param args
     *    0 the path
     *    1 if boolean true, reads all the lines as List of Strings
     *      if 1 arg is "b" read all data bytes out of the file
     * @return String or List of Strings the data of the file
     * @throws Exception in case of error
     */
    public static Object read(Object... args) throws Exception {
        if (args.length == 0) return Function.NIL;
        ZFile zf = new ZFile(String.valueOf(args[0]));
        if (args.length == 1) {
            return zf.all();
        }
        if (args[1] instanceof Boolean) {
            if ((boolean) args[1]) {
                return zf.lines();
            }
        }
        if ( "b".equals(args[1]) ){
            // binary, read all bytes
            return Files.readAllBytes( zf.file.toPath());
        }
        return Function.NIL;
    }
}
