/*
 * Copyright 2017 zoomba-lang.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package zoomba.lang.core.io;

import zoomba.lang.core.types.ZTypes;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * An abstraction to talk to the WWW
 */
public class ZWeb {

    /**
     * Default Agent
     */
    public static final String MY_AGENT
            = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11";

    /**
     * An abstraction for Web Communication
     */
    public static final class ZWebCom {

        /**
         * Path of the resource
         */
        public final String path;

        /**
         * The (key,value) pair map
         */
        public final Map map;

        /**
         * Body of the request/response
         * @return request/response stream of bytes as string
         */
        public String body(){
            // possibly handle charset -- later?
           return new String(bytes);
        }

        /**
         * Body of the request/response
         */
        public final byte[] bytes;


        public boolean emptyBody(){
            return bytes == null || bytes.length == 0;
        }

        /**
         * Status of the HTTP response
         */
        public final int status;

        ZWebCom(String p, Map m, byte[] b, int status) {
            path = p;
            map = m;
            bytes = b;
            this.status = status;
        }

        ZWebCom(String p, Map m, byte[] b) {
            this(p, m, b, 0);
        }

        @Override
        public String toString() {
            Map<String,Object> fields = new HashMap<>();
            fields.put("path", path);
            fields.put("map", map);
            fields.put("body", body());
            fields.put("status", status);
            return ZTypes.jsonString(fields);
        }
    }

    /**
     * The URL where we are connecting
     */
    public final URL url;

    /**
     * Create a ZWeb from an object
     *
     * @param o the object
     *          string or URL
     */
    public ZWeb(Object o) {
        if (o instanceof CharSequence) {
            try {
                url = new URL(o.toString());
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        } else if (o instanceof URL) {
            url = (URL) o;
        } else {
            throw new UnsupportedOperationException("Invalid type for URL!");
        }
        headers = new HashMap<>();
        headers.put("User-Agent", MY_AGENT);
    }

    /**
     * Property of the connection -- headers
     */
    public Map<String, String> headers;

    /**
     * The connection time out in millis
     */
    public int conTO;

    /**
     * The read time out in millis
     */
    public int readTO;

    /**
     * Creates a payload ( String ) from
     *
     * @param m the parameter map
     * @return a string
     * @throws Exception in case of error
     */
    public static String payLoad(Map m) throws Exception {
        if (m.isEmpty()) return "";
        Iterator<Map.Entry> i = m.entrySet().iterator();
        StringBuilder buf = new StringBuilder();
        Map.Entry e = i.next();
        String v = URLEncoder.encode(e.getValue().toString(), StandardCharsets.UTF_8.name());
        buf.append(e.getKey()).append("=").append(v);
        while (i.hasNext()) {
            buf.append("&");
            v = URLEncoder.encode(e.getValue().toString(), StandardCharsets.UTF_8.name());
            buf.append(e.getKey()).append("=").append(v);
        }
        return buf.toString();
    }


    /**
     * Gets the byte array body from a stream
     * @param inputStream the stream to be read
     * @return byte array
     * @throws Exception in case of error
     */
    public static byte[] readStream(InputStream inputStream) throws Exception {
        if ( inputStream == null ) return "".getBytes() ;
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byte[] b = new byte[2048];
        int length;

        while ((length = inputStream.read(b)) != -1) {
            byteArrayOutputStream.write(b, 0, length);
        }

        inputStream.close();
        return byteArrayOutputStream.toByteArray();
    }

    /**
     * Reads url and returns a ZWebCom
     *
     * @param urlString the url
     * @param headers headers we need to pass
     * @param conTO     the connection timeout
     * @param readTO    the read timeout
     * @return the ZWebCom
     * @throws Exception in case of error
     */
    public static ZWebCom readUrl(String urlString, Map<String,String> headers, int conTO, int readTO) throws Exception {
        URL url = new URL(urlString);
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setConnectTimeout(conTO);
        con.setReadTimeout(readTO);
        con.setRequestProperty("User-Agent", MY_AGENT);
        for (String prop : headers.keySet()) {
            con.setRequestProperty(prop, headers.get(prop));
        }
        try {
            return new ZWebCom(urlString, con.getHeaderFields(), readStream(con.getInputStream()), con.getResponseCode());
        } catch (Exception e) {
            return new ZWebCom(urlString, con.getHeaderFields(), readStream(con.getErrorStream()), con.getResponseCode());
        }
    }

    /**
     * Given a payload, shoots and gets a ZWebCom
     *
     * @param payLoad the payload, a ZWebCom object
     * @return Another ZWebCom
     * @throws Exception in case of error
     */
    public ZWebCom readUrl(ZWebCom payLoad) throws Exception {
        String myUrl = normalizeToUrlString(url,payLoad);
        String params = payLoad(payLoad.map);
        if (!params.isEmpty()) {
            myUrl += "?" + params;
        }
        return readUrl(myUrl, headers ,conTO, readTO);
    }

    /**
     * Using get protocol reads a path
     *
     * @param path the url path
     * @param m    the client header map
     * @return a ZWebCom
     * @throws Exception in case of error
     */
    public ZWebCom get(String path, Map m) throws Exception {
        return send("get", path, m, "");
    }

    /**
     * Using post protocol posts to a path
     *
     * @param path the url path
     * @param m    the client header map
     * @param body the client body
     * @return a ZWebCom
     * @throws Exception in case of error
     */
    public ZWebCom post(String path, Map m, String body) throws Exception {
        return send("post", path, m, body);
    }

    /**
     * Using post protocol posts to a path
     *
     * @param path the url path
     * @param m    the client header map
     * @return a ZWebCom
     * @throws Exception in case of error
     */
    public ZWebCom post(String path, Map m) throws Exception {
        return post(path, m, "");
    }

    /**
     * Using some x protocol shoots into a path
     *
     * @param protocol the protocol
     * @param path     the url path
     * @param m        the client header map
     * @param body     the client body
     * @return a ZWebCom
     * @throws Exception in case of error
     */
    public ZWebCom send(String protocol, String path, Map m, String body) throws Exception {
        return send(protocol, new ZWebCom(path, m, body.getBytes()));
    }

    private static String normalizeToUrlString(URL baseUrl, ZWebCom payload){
        String url = baseUrl.toString();
        if (!payload.path.isEmpty()) {
            if ( url.endsWith("/") ){
                url = url.substring(0,url.length()-1);
            }
            url += (payload.path.startsWith("/") ? "" : "/") + payload.path;
        }
        return url;
    }

    /**
     * Using some x protocol and a payload shoots into a path
     *
     * @param protocol the protocol
     * @param payLoad  the payload to be used
     * @return a ZWebCom
     * @throws Exception in case of error
     */
    public ZWebCom send(String protocol, ZWebCom payLoad) throws Exception {
        protocol = protocol.toUpperCase();
        String requestMethod = "";
        switch (protocol) {
            case "HEAD":
                requestMethod = "HEAD";
                break;
            case "PUT":
                requestMethod = "PUT";
                break;
            case "POST":
                requestMethod = "POST";
                break;
            case "TRACE":
                requestMethod = "TRACE";
                break;
            case "OPTIONS":
                requestMethod = "OPTIONS";
                break;
            case "CONNECT":
                requestMethod = "CONNECT";
                break;
            case "PATCH":
                requestMethod = "PATCH";
                break;

            case "DELETE":
                requestMethod = "DELETE";
                break;
            case "GET":
            default:
                return readUrl(payLoad);
        }
        final String urlPath =  normalizeToUrlString(url, payLoad);

        String urlParameters = payLoad(payLoad.map);
        URL obj = new URL(urlPath);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        con.setRequestMethod(requestMethod);

        con.setConnectTimeout(conTO);
        con.setReadTimeout(readTO);
        for (String prop : headers.keySet()) {
            con.setRequestProperty(prop, headers.get(prop));
        }
        con.setDoOutput(true);
        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
        if (!urlParameters.isEmpty()) {
            wr.writeBytes(urlParameters);
        }
        if (!payLoad.emptyBody()) {
            wr.write(payLoad.bytes);
        }
        wr.flush();
        wr.close();

        try {

            return new ZWebCom(urlPath, con.getHeaderFields(),
                    readStream(con.getInputStream()), con.getResponseCode());

        } catch (Exception e) {

            return new ZWebCom(urlPath, con.getHeaderFields(), readStream(con.getErrorStream()), con.getResponseCode());
        }
    }

    @Override
    public String toString() {
        return "ZWeb{" +
                "url=" + url +
                ", headers=" + headers +
                ", conTO=" + conTO +
                ", readTO=" + readTO +
                '}';
    }
}
