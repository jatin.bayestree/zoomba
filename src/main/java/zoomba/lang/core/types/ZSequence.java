/*
 * Copyright 2017 zoomba-lang.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package zoomba.lang.core.types;

import zoomba.lang.core.collections.ZArray;
import zoomba.lang.core.collections.ZList;
import zoomba.lang.core.operations.Function;
import java.util.Iterator;
import java.util.List;

/**
 * A mathematical sequence generator, non terminating
 */
public interface ZSequence extends Iterator {

    @Override
    default boolean hasNext() {
        return true;
    }

    ZSequence NULL_SEQUENCE = new BaseSequence( Function.NOP , ZArray.EMPTY_Z_ARRAY );

    /**
     * Retain the last amount of items in the list
     * @param last the amount of items to be retained
     * @return -1 if store data indefinitely, +ve if it has a size
     */
    int retain(int last);

    /**
     * A new sequence
     * @return yielded sequence
     */
    ZSequence yield();

    /**
     * Gets the indexed item from the sequence
     * @param index the index
     * @return the item on that index only +ve integers allowed
     */
    Object get(int index);

    /**
     * A Basic Implementation of such a sequence
     */
    final class BaseSequence implements ZSequence{

        /**
         * History of the sequence
         */
        public final List history;

        /**
         * The generator function which defines the sequence
         */
        public final Function next;

        /**
         * Current index of the sequence - the index of the most recent item
         */
        protected ZNumber index ;

        /**
         * The last item index
         */
        protected int last;

        /**
         * Create a sequence from a generator and a collector
         * @param f the generator function
         * @param initials seed initial values
         */
        public BaseSequence(Function f, List initials){
            history = new ZList(initials);
            next = f ;
            index = new ZNumber( history.size() - 1 );
            last = -1 ;
        }

        /**
         * A sequence with no seed value
         * @param f only the generator function
         */
        public BaseSequence(Function f){
            this( f, new ZList());
        }

        /**
         * A sequence from generator and args as list
         * @param f the generator function
         * @param args the arguments, individual elements as list
         */
        public BaseSequence(Function f, Object...args){
            this( f, new ZList( new ZArray(args,false) ) );
        }

        @Override
        public Object next() {
            index.add_mutable(1); // ++ actually
            Function.MonadicContainer o = next.execute( index , history , this , history );
            if ( o.isNil() ) throw new RuntimeException("A sequence must not return NIL!");
            Object result = o.value();
            history.add( result );
            if ( last >= 0 ){
               while ( history.size() != last ){
                   history.remove(0);
               }
            }

            return result ;
        }

        @Override
        public int retain(int last) {
            int prev = this.last ;
            this.last = last ;
            return prev ;
        }

        @Override
        public ZSequence yield(){
            BaseSequence bs = new BaseSequence( next, history );
            bs.last = last ;
            return bs ;
        }

        @Override
        public Object get(int index) {
            if ( index < 0 ) return null; // only +ve integers works
            while ( this.index.compareTo( index ) < 0 ){ next(); }
            return this.history.get(index);
        }

        /**
         * Creates a sequence from function and arguments
         * @param anon the generator function
         * @param args the arguments
         * @return a sequence
         */
        public static ZSequence sequence(Function anon, Object...args){
            if ( anon == null ) return NULL_SEQUENCE ;
            if ( args.length == 0 ) return new BaseSequence( anon );
            if ( args[0] instanceof List ) return  new BaseSequence( anon, ((List)args[0]) );
            return new BaseSequence( anon, args ) ;
        }

        @Override
        public String toString() {
            Number start = last < 0 ? 0 : (Number) index._add_( - last + 1 );
            return String.format( "[(%s:%s) -> %s]", start, index, history );
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || o.getClass().isAssignableFrom( ZSequence.class ) ) return false;
            BaseSequence that = (BaseSequence) o;
            return toString().equals( that.toString() );
        }

        @Override
        public int hashCode() {
            return toString().hashCode();
        }
    }
}
