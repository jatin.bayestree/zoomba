/*
 * Copyright 2017 zoomba-lang.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package zoomba.lang.core.types;

import static zoomba.lang.core.operations.Function.NIL;

/**
 * Arbitrary large range in ZoomBA
 * With Large Precision, Arbitrary Precision support
 */
public class ZXRange extends ZRange{

    final ZNumber b;

    final ZNumber e;

    final ZNumber s;

    ZNumber c;

    protected final boolean decreasing;

    @Override
    public void reset() {
        c = new ZNumber(b.num);
    }

    /**
     * Creates a ZXRange from ZNumbers
     * @param b begin
     * @param e end
     * @param s step
     */
    public ZXRange(ZNumber b, ZNumber e, ZNumber s) {
        super();
        this.b = b ;
        this.e = e ;
        this.s = s ;
        decreasing = ( b.compareTo( e) > 0 );
        reset();
    }

    /**
     * Creates a ZXRange from ZNumbers
     * @param b begin
     * @param e end
     */
    public ZXRange(ZNumber b, ZNumber e) {
        super();
        this.b = b ;
        this.e = e ;
        decreasing = ( b.compareTo( e) > 0 );
        if ( decreasing ) {
            s = new ZNumber(-1);
        }else{
            s = new ZNumber(1);
        }
        reset();
    }

    @Override
    public ZRange yield() {
        return new ZXRange( b, e, s );
    }

    @Override
    public Object object(long l) {
        return NIL;
    }

    @Override
    public String toString() {
        return String.format("[%s:%s:%s]" , b,e,s);
    }

    @Override
    public void toEnd() {
        c = new ZNumber(e);
        c.sub_mutable( s );
    }

    @Override
    public boolean hasNext() {
        if ( decreasing ) {
            return c.compareTo( e ) > 0 ;
        }
        return c.compareTo(e) < 0 ;
    }

    @Override
    public Object next() {
        Number next = c.actual() ;
        c.add_mutable(s);
        return next ;
    }

    @Override
    public Object get(Number n) {
        // n * s + b
        ZNumber l =   new ZNumber( n );
        l.mul_mutable(s);
        l.add_mutable( b );

        if ( decreasing ){
            if ( l.compareTo( e ) >= 0 ) return l.actual();
            return NIL;
        }
        if ( l.compareTo( e ) <= 0 ) return l.actual();
        return NIL;
    }

    @Override
    public boolean hasPrevious() {
        if ( decreasing ) {
            return c.compareTo( b ) <= 0 ;
        }
        return c.compareTo(b) >= 0 ;
    }

    @Override
    public Object previous() {
        Number prev = c.actual() ;
        c.sub_mutable(s);
        return prev ;
    }

    @Override
    public ZRange reverse() {
        return new ZXRange( new ZNumber( e._sub_(s) ) , new ZNumber( b._sub_(s) ) , new ZNumber( s._not_() ) );
    }

    @Override
    public ZRange inverse() {
        return new ZXRange(e, b, new ZNumber( s._not_() ) );
    }
}
