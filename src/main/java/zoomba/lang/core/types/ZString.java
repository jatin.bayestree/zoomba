/*
 * Copyright 2017 zoomba-lang.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package zoomba.lang.core.types;

import zoomba.lang.core.collections.ZList;
import zoomba.lang.core.operations.Arithmetic.* ;
import java.util.*;

/**
 * Implementation of extensive string operations
 */
public class ZString implements CharSequence, BasicArithmeticAware , List , ZTypes.StringX{

    private static final String EMPTY_STRING = "";

    @Override
    public String string(Object... args) {
        return toString();
    }

    public static class CharSequenceIterator implements ListIterator{

        public final CharSequence sequence ;

        protected int index ;

        public CharSequenceIterator(CharSequence sequence) {
            this.sequence = sequence ;
            index = -1;
        }

        public CharSequenceIterator(CharSequence sequence, int offset) {
            this.sequence = sequence ;
            index = offset;
        }

        @Override
        public boolean hasNext() {
            return ( index + 1 ) < sequence.length() ;
        }

        @Override
        public Object next() {
            return sequence.charAt( ++index );
        }

        @Override
        public boolean hasPrevious() {
            return  index > 0 ;
        }

        @Override
        public Object previous() {
            return sequence.charAt( --index );
        }

        @Override
        public int nextIndex() {
            return (index + 1);
        }

        @Override
        public int previousIndex() {
            return (index-1);
        }

        @Override
        public void remove() {

        }

        @Override
        public void set(Object o) {

        }

        @Override
        public void add(Object o) {

        }
    }

    protected StringBuilder buffer;

    public ZString(){
        buffer = new StringBuilder();
    }

    public ZString(char[] arr){
        if ( arr == null ){ arr = "null".toCharArray() ; }
        buffer = new StringBuilder( new String(arr) );
    }

    public ZString(CharSequence sequence){
        if ( sequence == null ){ sequence = "null" ; }
        buffer = new StringBuilder(sequence);
    }

    public ZString(Object o){
        buffer = new StringBuilder(String.valueOf(o));
    }

    @Override
    public int length() {
        return buffer.length();
    }

    @Override
    public char charAt(int index) {
        return buffer.charAt(index);
    }

    @Override
    public CharSequence subSequence(int start, int end) {
        return buffer.subSequence(start,end);
    }

    @Override
    public Object _add_(Object o) {
        ZString myCopy = new ZString(this.buffer);
        myCopy.buffer.append(String.valueOf(o)) ;
        return myCopy ;
    }

    @Override
    public Object _sub_(Object o) {
        throw new UnsupportedOperationException("Can not subtract from String!");
    }

    @Override
    public Object _mul_(Object o) {
        throw new UnsupportedOperationException("Can not multiply with String!");
    }

    @Override
    public Object _div_(Object o) {
        throw new UnsupportedOperationException("Can not divide with String!");
    }

    @Override
    public Object _pow_(Object o) {
        if ( !(o instanceof Number) ){
            throw new UnsupportedOperationException("Exponentiation is possible only with Number!");
        }
        int p = ((Number) o).intValue();
        if ( p == 0 ) return EMPTY_STRING;
        ZString myCopy = new ZString(this.buffer );
        if ( p < 0 ){
            // reverse the string
            myCopy.buffer.reverse();
            p =-p;
        }
        String appender = myCopy.toString();
        ZString result = new ZString(appender);
        while ( --p > 0 ){
            result.buffer.append( appender );
        }
        return result.toString();
    }

    @Override
    public void add_mutable(Object o) {
       buffer.append(String.valueOf(o));
    }

    @Override
    public void sub_mutable(Object o) {
        throw new UnsupportedOperationException("Can not subtract from String!");
    }

    @Override
    public void mul_mutable(Object o) {
        throw new UnsupportedOperationException("Can not multiply with String!");
    }

    @Override
    public void div_mutable(Object o) {
        throw new UnsupportedOperationException("Can not divide with String!");
    }

    @Override
    public int compareTo(Object o) {
        if ( o == null ) return 1;
        if ( o == this ) return 0 ;
        if ( o instanceof CharSequence ){
            String other = o.toString();
            return buffer.toString().compareTo(other);
        }else{
            if ( o instanceof Collection || o.getClass().isArray() ){
                ZList zl = new ZList(this);
                return zl.compareTo(o);
            }
        }
        throw new UnsupportedOperationException("Can not compare non CharSequence!");
    }

    @Override
    public String toString() {
        return buffer.toString();
    }

    @Override
    public int size() {
        return buffer.length() ;
    }

    @Override
    public boolean isEmpty() {
        return buffer.length() == 0 ;
    }

    @Override
    public boolean contains(Object o) {
        if ( o instanceof CharSequence || o instanceof Character) {
            return buffer.indexOf(o.toString()) >= 0;
        }
        return false ;
    }

    @Override
    public Iterator iterator() {
        return listIterator();
    }

    @Override
    public Object[] toArray() {
        Object[] arr = new Object[ buffer.length() ];
        return toArray(arr);
    }

    @Override
    public Object[] toArray(Object[] a) {
        char[] carr = buffer.toString().toCharArray();
        for ( int i = 0 ; i < carr.length; i++ ){
            a[i] = carr[i];
        }
        return a;
    }

    @Override
    public boolean add(Object o) {
        buffer.append(String.valueOf(o));
        return true;
    }

    @Override
    public boolean remove(Object o) {
        return false;
    }

    @Override
    public boolean containsAll(Collection c) {
        return false;
    }

    @Override
    public boolean addAll(Collection c) {
        return false;
    }

    @Override
    public boolean addAll(int index, Collection c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection c) {
        return false;
    }

    @Override
    public boolean retainAll(Collection c) {
        return false;
    }

    @Override
    public void clear() {
        buffer.setLength(0);
        buffer.trimToSize();
    }

    @Override
    public Object get(int index) {
        return buffer.charAt(index);
    }

    @Override
    public Object set(int index, Object element) {
        char c = buffer.charAt(index);
        if ( element instanceof CharSequence ){
            buffer.setCharAt(index, ((CharSequence) element).charAt(0));
        }
        else if ( element instanceof Character ){
            buffer.setCharAt(index, (Character)element);
        }
        return c;
    }

    @Override
    public void add(int index, Object element) {

    }

    @Override
    public Object remove(int index) {
        return null ;
    }

    @Override
    public int indexOf(Object o) {
        return buffer.indexOf( String.valueOf(o) );
    }

    @Override
    public int lastIndexOf(Object o) {
        return buffer.lastIndexOf( String.valueOf(o) );
    }

    @Override
    public ListIterator listIterator() {
        return listIterator(-1);
    }

    @Override
    public ListIterator listIterator(int index) {
        return new CharSequenceIterator(buffer, index );
    }

    @Override
    public List subList(int fromIndex, int toIndex) {
        ZString zs = new ZString( buffer.subSequence(fromIndex, toIndex ));
        return zs;
    }

    @Override
    public int hashCode() {
        return buffer.toString().hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return ( 0 == compareTo(obj) );
    }
}
