/*
 * Copyright 2017 zoomba-lang.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package zoomba.lang.core.types;

import org.jscience.mathematics.number.Float64;
import org.jscience.mathematics.number.LargeInteger;
import org.jscience.mathematics.number.Real;
import zoomba.lang.core.operations.Arithmetic;
import zoomba.lang.core.operations.Arithmetic.*;
import zoomba.lang.core.operations.Function;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.UnknownFormatConversionException;

/**
 * Wrapper over Numbers, so that we can have unlimited precision Arithmetic
 */
public class ZNumber extends Number implements
        BasicArithmeticAware, NumericAware , LogicAware , ZTypes.StringX {


    public static final DecimalFormatSymbols decimalFormatSymbols = new DecimalFormatSymbols(Locale.getDefault());

    public static final String decimalSperator = String.valueOf(decimalFormatSymbols.getDecimalSeparator());


    private static final String VALUE = "value";

    private static final String BASE = "base";

    private static final String DEFAULT = "default";


    /**
     * From args converts into an Integer type ( can be Long or even Apint also )
     * @param args the arguments
     *      args[0] string or another object
     *      args[1] default integer object, when conversion fails, returns that
     * @return null, or an integer type ( or default )
     */
    public static Number integer(Object...args){
        if ( args.length == 0 ) return null;
        if ( args.length == 3 ){
            // num,base,default
            try{
                BigInteger i = new BigInteger(String.valueOf(args[0]),
                                      integer(args[1]).intValue() );
                return integer(i);
            }catch (Exception e){
                return integer(args[2]);
            }
        }
        if ( args[0] instanceof Map){
            Function.NamedArgs na = Function.NamedArgs.fromMap((Map)args[0]);
            return integer( na.getOrDefault(VALUE,null),
                    na.getOrDefault(BASE,10) , na.getOrDefault(DEFAULT,null));
        }

        if ( args[0] instanceof Number ){
            if ( args[0] instanceof Integer ){
                return (Number)args[0];
            }

            Real f;
            if ( args[0] instanceof Real ){
                f = (Real)args[0];
            }else{
                f = Real.valueOf(args[0].toString());
            }

            LargeInteger ai = LargeInteger.valueOf(new BigDecimal(f.toString()).toBigInteger());
            long l = ai.longValue();
            if ( !ai.equals(l) ) return ai;
            int i = (int)l;
            if ( (long)i == l ) return i;
            return l;
        }
        try {
            Real f = Real.valueOf(args[0].toString());
            return integer(f);
        }catch (Exception e){
            try {
                return integer(args[1]);
            }catch (Exception ee){}
        }
        if ( args[0] instanceof Date ){
            return ((Date)args[0]).getTime() ;
        }
        if ( args[0] instanceof ZDate ){
            return ((ZDate)args[0]).date().getTime() ;
        }
        if ( args[0] instanceof Character ){
            return (int)((Character)args[0]).charValue();
        }
        return null;
    }

    /**
     * From args converts into Large Integer type
     * @param args the arguments
     *      args[0] string or another object
     *      args[1] default integer object, when conversion fails, returns that
     * @return null, or an Apint ( or default )
     */
    public static LargeInteger INT(Object...args){
        Number n = integer(args);
        if ( n == null) return null;
        return LargeInteger.valueOf(n.toString());
    }

    /**
     * From args converts into a Apfloat type
     * @param args the arguments
     *      args[0] string or another object
     *      args[1] default floating object, when conversion fails, returns that
     * @return null, or a floating ( or default )
     */
    public static Real FLOAT(Object...args){
        Number n = floating(args);
        if ( n == null) return null;
        return Real.valueOf(n.toString());
    }

    public Number integer(){
        return integer(num);
    }

    public Number floating(){
        return floating(num);
    }

    /**
     * From args converts into an floating type ( can be Apfloat also )
     * @param args the arguments
     *      args[0] string or another object
     *      args[1] default floating object, when conversion fails, returns that
     * @return null, or a floating ( or default )
     */
    public static Number floating(Object...args){
        if ( args.length == 0 ) return null;
        if ( args[0] instanceof Character){
            Integer i = (int)((Character)args[0]).charValue() ;
            return i.doubleValue();
        }
        if ( args[0] instanceof Number ){
            Number num = (Number)args[0];
            if ( num instanceof Double || num instanceof Float ){
                return num ;
            }
            if ( num instanceof Integer || num instanceof Long || num instanceof Short || num instanceof Byte ){
                return num.doubleValue();
            }

            Real f;
            if ( num instanceof Real ){
                f = (Real) args[0];
            }else{
                f = Real.valueOf( num.doubleValue() );
            }
            double d = f.doubleValue();
            Real df = Real.valueOf( d );
            if ( numberEquals(f,df) ) return d;
            return f;
        }
        try {
            Real f = Real.valueOf(String.valueOf(args[0]));
            return floating(f);
        }catch (Exception e){
            try {
                return floating(args[1]);
            }catch (Exception ee){}
        }
        return null;
    }

    /**
     * From args converts into an numeric type
     * @param args the arguments
     *      args[0] string or another object
     *      args[1] default number object, when conversion fails, returns that
     * @return null, or a numeric ( or default )
     */
    public static Number number(Object...args){
        if ( args.length == 0 ) return null;
        if ( args[0] instanceof Number ){
            return (Number)args[0];
        }
        try {
            ZNumber zn = new ZNumber(args[0]);
            return zn.actual();
        }catch (Exception e){
            String s = String.valueOf(args[0]);
            if ( "inf".equalsIgnoreCase(s) ) return Arithmetic.POSITIVE_INFINITY ;
            if ( "-inf".equalsIgnoreCase(s) ) return Arithmetic.NEGATIVE_INFINITY ;

            try {
                return number(args[1]);
            }catch (Exception ee){}
        }
        return null;
    }

    /**
     * Rounds a number
     * http://mathforum.org/library/drmath/view/71202.html
     * @param n the number
     * @param args optional, num of digits to round into
     * @return the rounded number
     */
    public static Number round(Number n, Object...args){
        if ( n instanceof Integer || n instanceof Long || n instanceof LargeInteger ){
            return n;
        }
        int numDigits = 0;
        if ( args.length > 0 ){
            numDigits = integer( args[0], 0 ).intValue() ;
        }
        if ( n instanceof ZNumber ){
            return round(((ZNumber) n).num, numDigits );
        }
        BigDecimal bd = new BigDecimal(String.valueOf(n));
        if ( numDigits < 0  ){
            return narrowNumber(bd.toBigInteger());
        }
        bd = bd.setScale(numDigits, RoundingMode.HALF_UP );
        return narrowNumber(bd);
    }

    /**
     * Floor of a number
     * @param n the number
     * @return the floored number
     */
    public static Number floor(Number n){
        if ( n instanceof Integer || n instanceof Long || n instanceof LargeInteger ){
            return n;
        }
        if ( n instanceof Real ){
            Real f = ((Real)n);
            if ( f.isPositive() ){
                return integer(f);
            } else if ( f.isNegative() ){
                LargeInteger i1 = f.round();
                Real l = Real.valueOf(i1.toString());
                if ( numberEquals(f, l ) ){
                    // not fractional
                    return integer(i1);
                }
                LargeInteger i2 = i1.minus(1);
                return integer(i2);
            }
            return 0; // it is zero

        }
        if ( n instanceof ZNumber ){
            return floor(((ZNumber) n).num);
        }
        return floor(new ZNumber(n));
    }

    /**
     * Ceiling of a number
     * @param n the number
     * @return the ceil number
     */
    public static Number ceil(Number n){
        if ( n instanceof Integer || n instanceof Long || n instanceof LargeInteger ){
            return n;
        }
        if ( n instanceof Real ){
            LargeInteger i = ((Real)n).round();
            if ( ((Real)n).isPositive() ){
               i = i.plus(1);
            }
            return integer(i);
        }
        if ( n instanceof ZNumber ){
            return ceil(((ZNumber) n).num);
        }
        return ceil(new ZNumber(n));
    }

    public static boolean numberEquals(Number r1, Number r2){
        BigDecimal b1 = new BigDecimal(String.valueOf(r1));
        BigDecimal b2 = new BigDecimal(String.valueOf(r2));
        return 0 == b1.compareTo(b2);
    }

    /**
     * Narrow down a Large float into double precision if possible
     * @param n the large float
     * @return a narrowed down double or the same float if it can not be done
     */
    public static Number narrowNumber(Number n){
        Real f = Real.valueOf(String.valueOf(n));
        LargeInteger api = f.round();
        Real r = Real.valueOf(api.toString());
        if ( f.equals(r) ){
            long l = api.longValue();
            if ( !api.equals(l) ) return api;
            int i = (int)l;
            if ( (long)i == l ) return i;
            return l;
        }
        else{
            // can I get into double?
            double d = f.doubleValue();
            r = Real.valueOf( Double.toString(d) );
            if ( numberEquals(r,f) ) {
                return d;
            }
            return f; // can not narrow
        }
    }

    protected Real num;
    private boolean fractional = false ;


    private void populateNumber (Number o){

        if ( o instanceof ZNumber ){
            num = ((ZNumber) o).num ;
            fractional = ((ZNumber) o).fractional ;
        } else{
            fractional = !( o instanceof Integer || o instanceof Short || o instanceof Byte || o instanceof Long
                    || o instanceof BigInteger || o instanceof LargeInteger );
            num = Real.valueOf(String.valueOf(o));
        }
    }

    private void init(Object o){
        if ( o instanceof Number ){
            populateNumber((Number) o);
        }else if ( o instanceof Date ){
            num = Real.valueOf(((Date) o).getTime());
        } else if ( o instanceof Character ){
            num = Real.valueOf ( (int) (Character) o);
        }
        else{
            String v = String.valueOf(o);
            num = Real.valueOf(v);
            if ( o instanceof CharSequence ){
                fractional = v.contains( decimalSperator ) ;
                return;
            }
            // assign floating..
            LargeInteger l = num.round();
            fractional = !numberEquals( Real.valueOf(l.toString()), num);
        }
    }

    /**
     * Creates a Wrapper number from an object
     * @param o the object
     */
    public ZNumber(Object o){
        try {
            init(o);
        }catch (Exception e){
            throw new UnknownFormatConversionException("Object is not a Number : " + String.valueOf(o));
        }
    }

    @Override
    public int intValue() {
        return num.intValue();
    }

    @Override
    public long longValue() {
        return num.longValue();
    }

    @Override
    public float floatValue() {
        return num.floatValue();
    }

    @Override
    public double doubleValue() {
        return num.doubleValue();
    }

    @Override
    public Object _add_(Object o) {
        ZNumber zn = new ZNumber(o);
        Real f = num.plus(zn.num);
        return narrowNumber(f);
    }

    @Override
    public Object _sub_(Object o) {
        ZNumber zn = new ZNumber(o);
        Real f = num.minus(zn.num);
        return narrowNumber(f);
    }

    @Override
    public Object _mul_(Object o) {
        ZNumber zn = new ZNumber(o);
        Real f = num.times(zn.num);
        return narrowNumber(f);
    }

    @Override
    public Object _div_(Object o) {
        ZNumber zn = new ZNumber(o);
        Real f = num.divide(zn.num);
        if ( fractional || zn.fractional ){
            return floating(f);
        }
        return integer(f);
    }

    @Override
    public Object _pow_(Object o) {
        ZNumber zn = new ZNumber(o);
        if ( Real.ZERO.equals( zn.num) ){
            if ( fractional() ){
                return 1.0d;
            }
            return 1;
        }
        Real r = num;
        double p = zn.num.doubleValue();
        if ( p < 0 ){
            // do something else
            r = r.inverse();
            zn.num = zn.num.opposite();
        }
        if ( zn.fractional ){
            // do with log
            int iPower = zn.num.round().intValue();
            double fPower = zn.num.minus( Real.valueOf(iPower) ).doubleValue();
            Real fRes = Real.valueOf( String.valueOf(Math.pow( r.doubleValue(), fPower ) ) );
            if ( iPower == 0 ){
                return narrowNumber(fRes);
            }
            Real iRes = r.pow( iPower );
            Real res = iRes.times(fRes) ;
            return narrowNumber( res );

        } else {
            Real f = r.pow( zn.num.round().intValue());
            return narrowNumber(f);
        }
    }

    @Override
    public void add_mutable(Object o) {
        Object r = _add_(o);
        init(r);
    }

    @Override
    public void sub_mutable(Object o) {
        Object r = _sub_(o);
        init(r);
    }

    @Override
    public void mul_mutable(Object o) {
        Object r = _mul_(o);
        init(r);
    }

    @Override
    public void div_mutable(Object o) {
        Object r = _div_(o);
        init(r);
    }

    @Override
    public Object _and_(Object o) {
        BigInteger bi = new BigInteger(String.valueOf(o));
        BigInteger me = bigInteger();
        return  integer( me.and(bi) ) ;
    }

    @Override
    public Object _or_(Object o) {
        BigInteger bi = new BigInteger(String.valueOf(o));
        BigInteger me = bigInteger();
        return  integer( me.or( bi )) ;
    }

    @Override
    public Object _xor_(Object o) {
        BigInteger bi = new BigInteger(String.valueOf(o));
        BigInteger me = bigInteger();
        return  integer( me.xor( bi )) ;
    }

    @Override
    public void and_mutable(Object o) {
        BigInteger bi = new BigInteger(String.valueOf(o));
        BigInteger me = bigInteger();
        num = Real.valueOf( me.and(bi).toString() );
        fractional = false;
    }

    @Override
    public void or_mutable(Object o) {
        BigInteger bi = new BigInteger(String.valueOf(o));
        BigInteger me = bigInteger();
        num = Real.valueOf( me.or(bi).toString() );
        fractional = false;
    }

    @Override
    public void xor_mutable(Object o) {
        BigInteger bi = new BigInteger(String.valueOf(o));
        BigInteger me = bigInteger();
        num = Real.valueOf( me.xor(bi).toString() );
        fractional = false;
    }

    @Override
    public Number _abs_() {
        return narrowNumber(num.abs());
    }

    @Override
    public Number _not_() {
        return narrowNumber(num.opposite());
    }

    @Override
    public void not_mutable() {
        num = num.opposite();
    }

    @Override
    public Number _mod_(Number o ) {
        if ( fractional() ){
            return Real.ZERO  ;
        }
        BigInteger cur = new BigInteger(num.toString());
        BigInteger bi = new BigInteger(o.toString());
        return integer(cur.mod(bi));
    }

    @Override
    public void mod_mutable(Number o) {
        if ( fractional() ){ return; }
        num = Real.valueOf( _mod_(o ).toString() );
        fractional = false;
    }

    @Override
    public Number actual() {
        if ( fractional ){
            return floating();
        }
        return integer();
    }

    @Override
    public BigDecimal bigDecimal() {
        return new BigDecimal(num.toString() ) ;
    }

    @Override
    public BigInteger bigInteger() {
        return new BigInteger( num.round().toString() );
    }

    @Override
    public boolean fractional() {
        return fractional;
    }

    @Override
    public int compareTo(Object o) {
        if ( o == null ) return -1;
        if ( o == this ) return 0 ;
        ZNumber zn = new ZNumber(o);
        return num.compareTo( zn.num );
    }

    @Override
    public Number ceil() {
        return narrowNumber( ceil(num ) ) ;
    }

    @Override
    public Number floor() {
        return narrowNumber( floor(num) );
    }

    @Override
    public String toString() {
        BigDecimal bd = new BigDecimal(num.toString());
        if ( fractional() ) {
            return bd.toString();
        }
        return bd.toBigInteger().toString();
    }

    @Override
    public String string(Object... args) {
        if ( args.length == 0 ) return toString();
        if ( args[0] instanceof Number ){
            int i = ((Number)args[0]).intValue() ;
            if ( !fractional() ){
                BigInteger bi = bigInteger();
                return bi.toString(i);
            }
            // the precision float thingie
            BigDecimal bd = new BigDecimal( num.toString() );
            String format = "%%.%df" ;
            format = String.format(format, i);
            String v = String.format(format,bd);
            return v;
        }
        if ( args[0] instanceof String ){
            String pattern = (String)args[0];
            DecimalFormat df = new DecimalFormat(pattern);
            return df.format( num );
        }

        return toString();
    }

    @Override
    public int hashCode() {
        Number n = narrowNumber(num);
        return n.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if ( obj == this ) return true ;
        if ( obj == null ) return false ;
        ZNumber o = new ZNumber(obj);
        return numberEquals(num, o.num );
    }

    /**
     * Log base e
     * @return log(this,e)
     */
    public Number ln(){
        Float64 float64 = Float64.valueOf(num.toString());
        return narrowNumber(float64.log());
    }

    /**
     * Log of this base b
     * @param b the base
     * @return log(this,b)
     */
    public Number log(Object b){
        Float64 cur = Float64.valueOf(num.toString());
        Float64 other = Float64.valueOf( String.valueOf(b));
        Float64 res = cur.log().divide( other.log() );
        return narrowNumber(res);
    }

    /**
     * Log of this base b
     * @param args the optional base
     *     if not specified treats base as e,
     *     else takes args[0] as base b
     * @return log(this,b)
     */
    public static Number log(Object...args){
        if  ( args.length == 0 ) return Math.E ;
        ZNumber zn = new ZNumber( args[0]);
        if ( args.length == 1 ) return zn.ln();
        return zn.log(args[1]);
    }

    public static int sign(Object ... args){
        if ( args.length == 0 ) return 0;
        ZNumber zn = new ZNumber( args[0] );
        if ( zn.num.isPositive() ){
            return 1;
        } else if ( zn.num.isNegative() ){
            return -1;
        }
        return 0;
    }
}
