/*
 * Copyright 2017 zoomba-lang.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package zoomba.lang.core.interpreter;

import zoomba.lang.Main;
import zoomba.lang.core.collections.ZArray;
import zoomba.lang.core.collections.BaseZCollection;
import zoomba.lang.core.collections.ZMap;
import zoomba.lang.core.oop.ZObject;
import zoomba.lang.core.operations.*;
import zoomba.lang.parser.*;
import zoomba.lang.core.types.ZException;
import zoomba.lang.core.types.ZException.*;
import zoomba.lang.core.types.ZString;
import zoomba.lang.core.types.ZTypes;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static zoomba.lang.core.interpreter.ZContext.NOTHING;

import zoomba.lang.core.operations.Function;

import static zoomba.lang.core.operations.Function.*;
import static zoomba.lang.core.types.ZException.Break.BREAK;
import static zoomba.lang.core.types.ZException.Continue.CONTINUE;
import static zoomba.lang.core.types.ZException.Return.RETURN;


/**
 * The Interpreter
 */
public class ZInterpret implements ParserVisitor, ZMethodInterceptor {

    public static String MAIN_SCRIPT_VAR = "@SCRIPT" ;

    /**
     * Get's type of an object
     *
     * @param args the arguments
     * @return the type of the arguments
     */
    public static Class type(Object... args) {
        if (args.length == 0) return Function.NIL.getClass();
        Object o = args[0];
        if (o == null) return Function.NIL.getClass();
        if (o instanceof Class) return Class.class;
        return o.getClass();
    }

    public Object assignToNode(ZoombaNode node, ZoombaNode left, Object right) {
        // determine initial object & property:
        ZoombaNode objectNode = null;
        Object object = null;
        ZoombaNode propertyNode;
        Object property = null;
        // 1: follow children till penultimate, resolve dot/array
        int last = left.jjtGetNumChildren() - 1;
        if (last < 0) {
            throw new ZException.Variable(left,
                    "Assignment not possible into " + left.getClass().getSimpleName());
        }
        // start at 1 if register
        for (int c = 0; c < last; ++c) {
            objectNode = (ZoombaNode) left.jjtGetChild(c);
            // evaluate the property within the object
            object = objectNode.jjtAccept(this, object);
            if (object != null) {
                continue;
            }
            /*****
             TODO : We are not sure if this is useful anymore...?
             isVariable &= objectNode instanceof ASTIdentifier
             || (objectNode instanceof ASTNumberLiteral && ((ASTNumberLiteral) objectNode).isInteger());

             // if we get null back as a result, check for an ant variable
             if (isVariable) {

             if (v == 0) {
             variableName = new StringBuilder( ((ZoombaNode)left.jjtGetChild(0)).image);
             v = 1;
             }
             for (; v <= c; ++v) {
             variableName.append('.');
             variableName.append(((ZoombaNode)left.jjtGetChild(v)).image);
             }
             MonadicContainer mc = frames.peek().get( variableName.toString() );
             if ( ! mc.isNil() ){
             object = mc.value() ;
             isVariable = false;
             }
             } else {
             throw new ZException.Property(objectNode, "illegal assignment form");
             }
             */
        }
        // 2: last objectNode will perform assignment in all cases
        propertyNode = (ZoombaNode) left.jjtGetChild(last);
        if (propertyNode instanceof ASTIdentifier) {
            property = propertyNode.image;
            if (object == null) {
                // global or local ?
                if (ZScript.IS_GLOBAL_VAR((String) property)) {
                    danceMaster.globals.put((String) property, right);
                } else {
                    // the variable node, set it
                    frames.peek().set((String) property, right);
                }
                return right;
            }

        } else if (propertyNode instanceof ASTNumberLiteral && ((ASTNumberLiteral) propertyNode).isInteger()) {
            property = ((ASTNumberLiteral) propertyNode).getLiteral();
        } else if (propertyNode instanceof ASTArrayAccess) {
            // first objectNode is the identifier
            objectNode = propertyNode;
            ASTArrayAccess narray = (ASTArrayAccess) objectNode;
            Object nobject = narray.jjtGetChild(0).jjtAccept(this, object);
            if (nobject == null) {
                throw new ZException.Property(objectNode, "array element is null");
            } else {
                object = nobject;
            }
            // can have multiple nodes - either an expression, integer literal or
            // reference
            last = narray.jjtGetNumChildren() - 1;
            for (int i = 1; i < last; i++) {
                objectNode = (ZoombaNode) narray.jjtGetChild(i);
                if (objectNode instanceof ZoombaNode.Literal<?>) {
                    object = objectNode.jjtAccept(this, object);
                } else {
                    Object index = objectNode.jjtAccept(this, null);
                    MonadicContainer mc = ZJVMAccess.getProperty(object, index);
                    if (mc.isNil()) throw new ZException.Property(objectNode, object, index);
                    object = mc.value();
                }
            }
            property = narray.jjtGetChild(last).jjtAccept(this, null);
        }

        if (property == null) {
            // no property, we fail
            throw new ZException.Property(node, "property is absent, invalid L-value for Assignment!");
        }
        if (object == null) {
            // no object, we fail
            throw new ZException.Property(node, "object is null");
        }
        MonadicContainer mc = ZJVMAccess.setProperty(object, property, right);
        if (mc.isNil()) throw new ZException.Property(objectNode, object, property);

        return right;
    }

    protected final Stack<ZContext> frames;

    protected final Arithmetic arithmetic = new Arithmetic();

    public void prepareCall(ZContext newContext) {
        frames.push(newContext);
    }

    public ZContext endCall() {
        // this can only happen in a threaded environment...
        return frames.empty()? ZContext.EMPTY_CONTEXT : frames.pop();
    }

    protected ZScript danceMaster;

    final long threadId;

    /**
     * Gets the script of the interpreter
     *
     * @return the script
     */
    public ZScript script() {
        return danceMaster;
    }

    /**
     * Given an owner creates a child Interpreter object
     *
     * @param owner the parent
     */
    public ZInterpret(ZScript owner) {
        danceMaster = owner;
        frames = new Stack<>();
        threadId = Thread.currentThread().getId();
        arithmetic.strictOnAmbiguity = !owner.lenient;
    }

    /**
     * Creates a new Interpreter from from kind of clone
     *
     * @param from the original one
     */
    public ZInterpret(ZInterpret from) {
        danceMaster = from.danceMaster;
        // clone it, not assign it...
        frames = (Stack<ZContext>)from.frames.clone();
        threadId = Thread.currentThread().getId();
        arithmetic.strictOnAmbiguity = from.arithmetic.strictOnAmbiguity;
    }

    // this should be synchronized across all instances of ZInterpret

    private final static Object _LOCK_OBJECT = new Object();

    @Override
    public Object visit(ASTDebugBreak node, Object data) {
        synchronized (_LOCK_OBJECT) { // because for multithreading, things should run one after another
            // disabled? Not thread specific now, should be, really
            if (Boolean.FALSE == node.jjtGetValue()) return false;

        /*
          The breakpoint magic happens here
          The statement returns true, if bp got hit, false if not
        * */
            int numChild = node.jjtGetNumChildren();
            boolean debug = true;
            if (numChild > 0) {
                debug = ZTypes.bool(node.jjtGetChild(0).jjtAccept(this, data), false);
            }
            if (debug) {
                System.out.println("============= With Power, Comes Responsibility ========");
                System.out.printf("BreakPoint hit: %s\n", node.locationInfo());
                System.out.println("============= Please use Power, Carefully..... ========");
                try {
                    Main.stepDance((ZContext.FunctionContext) this.frames.peek());
                } catch (Throwable t) {
                    System.err.printf("%s\n", t.getMessage());
                } finally {
                    if (Main.bpToBeCleared(this.frames.peek())) {
                        Main.clearFromContext(this.frames.peek());
                        node.jjtSetValue(false);
                        System.out.println("Clearing the break point, will not be hit, ever");
                    }
                }
            }
            return debug;
        }
    }

    /**
     * Dance around the given script/expression.
     *
     * @param node the script or expression to interpret.
     * @return the result of the interpretation.
     * @throws ZException if any error occurs during interpretation.
     */
    public Object dance(Node node) {
        return dance(node, false);
    }

    private Object dance(Node node, boolean preserveStack){
        try {
            if ( !preserveStack) { callStack.clear(); }
            return node.jjtAccept(this, null);
        } catch (Return xreturn) {
            if (xreturn.isNil()) {
                return NIL;
            }
            return xreturn.value;
        } catch (ZException xz) {
            throw xz;
        } finally {
        }
    }

    @Override
    public Object visit(ASTCaseExpression node, Object data) {
        boolean matched = false;
        try {
            Node condition = node.jjtGetChild(0);
            Object curThis = frames.peek().get(Function.SWITCH_VALUE).value();
            Object value = condition.jjtAccept(this, data);
            if (condition instanceof ASTNumberLiteral) {
                matched = ZTypes.equals(curThis, value);
            } else if (condition instanceof ASTPatternLiteral) {
                matched = ((ASTPatternLiteral) condition).matches(String.valueOf(curThis),this);
            } else {
                if (condition instanceof ASTReference) {
                    if (condition.jjtGetChild(0) instanceof ASTIdentifier &&
                            Function.SWITCH_VALUE.equals(((ASTIdentifier) condition.jjtGetChild(0)).image)) {
                        matched = true;
                    } else if (condition.jjtGetChild(0) instanceof ASTZStringLiteral) {
                        matched = ZTypes.equals(curThis, value);
                    }
                } else {
                    matched = ZTypes.bool(value, false);
                }
            }
        } catch (Throwable t) {
            throw t;
        }
        if (!matched) return NOTHING;
        try {
            int numChild = node.jjtGetNumChildren();
            Object ret = NIL;
            for (int i = 1; i < numChild; i++) {
                ret = node.jjtGetChild(i).jjtAccept(this, data);
            }
            if (ret != NIL) {
                return new MonadicContainerBase(ret);
            }
        } catch (Throwable t) {
            throw t;
        }
        return NOTHING;
    }

    @Override
    public Object visit(ASTSwitchStatement node, Object data) {
        ZContext ctx = frames.peek();
        Object oldThis = ctx.get(Function.SWITCH_VALUE).value();

        int numChild = node.jjtGetNumChildren();
        // evaluate first node...
        try {
            Object var = node.jjtGetChild(0).jjtAccept(this, data);
            ctx.set(Function.SWITCH_VALUE, var);
        } catch (Throwable t) {
            return NIL;
        }
        Object retVal = NIL;
        for (int i = 1; i < numChild; i++) {
            MonadicContainer ret = (MonadicContainer) node.jjtGetChild(i).jjtAccept(this, data);
            if (!ret.isNil()) {
                retVal = ret.value();
                break;
            }
        }
        if (oldThis != NIL) {
            ctx.set(Function.SWITCH_VALUE, oldThis);
        }
        return retVal; // return back
    }

    @Override
    public Object visit(ASTDeleteStatement node, Object data) {
        String name = ((ASTIdentifier) node.jjtGetChild(0)).image;
        if (ZScript.IS_GLOBAL_VAR(name)) {
            if (danceMaster.globals.containsKey(name)) {
                return danceMaster.globals.remove(name);
            }
            // $$ are not allowed in any case ...
            return false;
        }
        ZContext ctx = frames.peek();
        if (ctx.has(name)) {
            return ctx.unSet(name);
        }
        return false;
    }

    @Override
    public Object visit(ASTAdditiveNode node, Object data) {
        ZoombaNode leftNode = (ZoombaNode) node.jjtGetChild(0);
        Object left = leftNode.jjtAccept(this, data);

        int num = node.jjtGetNumChildren();
        int i = 1;
        while (i < num) {
            String op = ((ASTAdditiveOperator) node.jjtGetChild(i++)).image;
            Object right = node.jjtGetChild(i++).jjtAccept(this, data);
            try {
                switch (op) {
                    case "+":
                        left = arithmetic.add(left, right);
                        break;
                    case "-":
                        left = arithmetic.sub(left, right);
                        break;
                    case "+=":
                        left = arithmetic.addMutable(left, right);
                        assignToNode(node, leftNode, left);
                        break;
                    case "-=":
                        left = arithmetic.subMutable(left, right);
                        assignToNode(node, leftNode, left);
                        break;
                    case "*=":
                        left = arithmetic.mulMutable(left, right);
                        assignToNode(node, leftNode, left);
                        break;
                    case "/=":
                        left = arithmetic.divMutable(left, right);
                        assignToNode(node, leftNode, left);
                        break;
                    case "&=":
                        left = arithmetic.andMutable(left, right);
                        assignToNode(node, leftNode, left);
                        break;
                    case "|=":
                        left = arithmetic.orMutable(left, right);
                        assignToNode(node, leftNode, left);
                        break;
                    case "^=":
                        left = arithmetic.xorMutable(left, right);
                        assignToNode(node, leftNode, left);
                        break;
                    case "%=":
                        left = arithmetic.modMutable(left, right);
                        assignToNode(node, leftNode, left);
                        break;
                    default:
                        throw new UnsupportedOperationException("Operator : (" + op + ") is unsupported!");
                }
            }catch (Throwable t){
                throw new ArithmeticLogicOperation(node,t,op);
            }
        }
        return left;
    }

    @Override
    public Object visit(SimpleNode node, Object data) {
        return null;
    }

    @Override
    public Object visit(ASTZoombaScript node, Object data) {
        Object r = NIL;
        int n = node.jjtGetNumChildren();
        for (int i = 0; i < n; i++) {
            Node child = node.jjtGetChild(i);
            r = NIL;
            r = child.jjtAccept(this, data);
        }
        return r;
    }

    @Override
    public Object visit(ASTErrorCatch node, Object data) {
        return NIL; // none should call it
    }

    @Override
    public Object visit(ASTLeftCatch node, Object data) {
        return NIL; // none should call it
    }

    @Override
    public Object visit(ASTRightCatch node, Object data) {
        return NIL; // none should call it
    }

    @Override
    public Object visit(ASTMultipleReturnStatement node, Object data) {
        ZContext ctx = frames.peek();
        Node n = node.jjtGetChild(0);
        int num = n.jjtGetNumChildren();

        if (n instanceof ASTErrorCatch) {

            try {
                Object r = node.jjtGetChild(1).jjtAccept(this, data);
                assignToNode(node, (ZoombaNode) n.jjtGetChild(0), r);
                assignToNode(node, (ZoombaNode) n.jjtGetChild(1), null);
            } catch (Throwable t) {
                assignToNode(node, (ZoombaNode) n.jjtGetChild(0), null);
                assignToNode(node, (ZoombaNode) n.jjtGetChild(1), t);
            }
            return NOTHING;
        }

        Object r = node.jjtGetChild(1).jjtAccept(this, data);
        if (r == null) {
            r = ZArray.EMPTY_Z_ARRAY;
        }
        if (r.getClass().isArray()) {
            r = new ZArray(r, false);
        }
        if (!(r instanceof Iterable)) {
            r = ZArray.EMPTY_Z_ARRAY;
        }
        if (n instanceof ASTLeftCatch) {
            Iterator iterator = ((Iterable) r).iterator();
            for (int i = 0; i < num ; i++) {
                ZoombaNode leftNode = (ZoombaNode)n.jjtGetChild(i);
                // assign...
                if (iterator.hasNext()) {
                    Object o = iterator.next();
                    assignToNode(node, leftNode, o);
                } else {
                    System.err.printf("assignment mismatch --> no r-value for container %s @ %s %n",
                            leftNode.body(danceMaster.body), node.locationInfo());
                    /*
                    if (ZScript.IS_GLOBAL_VAR(ids[i])) {
                        danceMaster.globals.put(ids[i], NIL);
                    } else {
                        ctx.set(ids[i], NIL);
                    }
                    */
                }
            }
        } else {
            if (!(r instanceof List)) {
                r = ZArray.EMPTY_Z_ARRAY;
            }
            List l = (List) r;
            int j = l.size() - 1;
            for (int i = num - 1; i >= 0; i--) {
                ZoombaNode leftNode = (ZoombaNode)n.jjtGetChild(i);
                if (j >= 0) {
                    Object o = l.get(j);
                    assignToNode(node, leftNode, o);

                } else {
                    System.err.printf("assignment mismatch --> no r-value for container %s @ %s %n",
                            leftNode.body(danceMaster.body), node.locationInfo());
                    /*
                    if (ZScript.IS_GLOBAL_VAR(ids[i])) {
                        danceMaster.globals.put(ids[i], NIL);
                    } else {
                        ctx.set(ids[i], NIL);
                    }
                    */
                }
                j--;
            }
        }
        return NOTHING;
    }

    @Override
    public Object visit(ASTAtomicStatement node, Object data) {
        synchronized (this) {
            Node n = node.jjtGetChild(0);
            synchronized (n) { // lock the piece of code block
                // preserve state ??
                // get a new Context
                ZContext current = frames.peek();
                ZContext sandBox = new ZContext.MapContext(current);
                frames.push(sandBox);
                // save globals too?
                Map<String, Object> globalState = new HashMap<>(danceMaster.globals);
                try {
                    Object o = n.jjtAccept(this, data);
                    // if things are right then
                    frames.pop(); // remove sandBox
                    frames.pop(); // remove parent
                    frames.push(sandBox); // put sandBox as Ok one
                    // clear globals
                    globalState.clear();
                    return o;
                } catch (Throwable t) {
                    frames.pop(); // remove sandBox
                    // reset global
                    danceMaster.globals.clear();
                    danceMaster.globals.putAll(globalState);
                    return t;
                }
            }
        }
    }

    public static final double NANO_TO_SEC = 0.000000001;

    @Override
    public Object visit(ASTClockStatement node, Object data) {
        long start = System.nanoTime();
        Object o;
        try {
            o = node.jjtGetChild(0).jjtAccept(this, data);
        } catch (Throwable t) {
            o = t;
        }
        long end = System.nanoTime();
        return new Object[]{NANO_TO_SEC * (end - start), o};
    }

    @Override
    public Object visit(ASTFunctionComposition node, Object data) {
        int num = node.jjtGetNumChildren();
        ZoombaNode n = (ZoombaNode) node.jjtGetChild(0);
        Object l = n.jjtAccept(this, data);
        if (!(l instanceof ZScriptMethod)) throw new ZException.Function(node, n.image);
        ZScriptMethod f = (ZScriptMethod) l;
        StringBuilder buf = new StringBuilder("def(){ _r = @ARGS ; _r = ").append(f.name).append("(@ARGS=_r); ");
        for (int i = 1; i < num; i++) {
            n = (ZoombaNode) node.jjtGetChild(i);
            l = n.jjtAccept(this, data);
            if (!(l instanceof ZScriptMethod)) {
                throw new ZException.Function(n, n.image);
            }
            buf.append(" _r = ").append(((ZScriptMethod) l).name).append("(@ARGS=_r); ");
        }
        buf.append(" }");
        return execInline(buf.toString());
    }

    @Override
    public Object visit(ASTBreakStatement node, Object data) {
        int num = node.jjtGetNumChildren();
        if (num == 0) throw BREAK;
        boolean condition = ZTypes.bool(node.jjtGetChild(0).jjtAccept(this, data), false);
        if (!condition) return Void;
        if (num == 2) {
            Object v = node.jjtGetChild(1).jjtAccept(this, data);
            throw new ZException.Break(v);
        }
        throw BREAK;
    }

    @Override
    public Object visit(ASTContinueStatement node, Object data) {
        int num = node.jjtGetNumChildren();
        if (num == 0) throw CONTINUE;
        boolean condition = ZTypes.bool(node.jjtGetChild(0).jjtAccept(this, data), false);
        if (!condition) return Void;
        if (num == 2) {
            Object v = node.jjtGetChild(1).jjtAccept(this, data);
            throw new ZException.Continue(v);
        }
        throw CONTINUE;
    }

    @Override
    public Object visit(ASTBlock node, Object data) {
        int num = node.jjtGetNumChildren();
        Object r = NIL;
        for (int i = 0; i < num; i++) {
            Node n = node.jjtGetChild(i);
            r = n.jjtAccept(this, data);
        }
        return r;
    }

    @Override
    public Object visit(ASTCharacterString node, Object data) {
        String s = ((ASTStringLiteral) node.jjtGetChild(0)).image;
        switch (s.length()) {
            case 0:
                throw new ZException.Property(node, "No char in char literal!");
            case 1:
                return s.charAt(0);
            default:
                throw new ZException.Property(node, "Multiple char in char literal!");
        }
    }

    @Override
    public Object visit(ASTRelocableString node, Object data) {
        String s = ((ASTStringLiteral) node.jjtGetChild(0)).image;
        String myLocation = node.sourceLocation();
        if ( myLocation == null ){
            throw new UnsupportedOperationException(
                    "Can not use relocable string with no source location information!");

        }
        File f = new File(myLocation);
        final String relocatedValue = f.getParent() + "/" + s ; // debug purpose only
        return relocatedValue;
    }

    public Object execInline(String text) {
        try {
            ASTZoombaScript node = danceMaster.parse(text);
            return dance(node,true);
        } catch (Throwable t) {
            return text;
        }
    }

    public Object curry(String s) {
        String r = s;
        Matcher m = ZScript.CURRY_PATTERN.matcher(r);
        while (m.find()) {
            String expr = m.group("p");
            Object v = execInline(expr);
            String sub = String.valueOf(v);
            if (sub.equals(expr)) return r;
            r = r.replace(m.group(), sub);
            m = ZScript.CURRY_PATTERN.matcher(r);
        }
        return execInline(r);
    }

    @Override
    public Object visit(ASTExecutableString node, Object data) {
        String s = ((ASTStringLiteral) node.jjtGetChild(0)).image;
        return curry(s);
    }

    @Override
    public Object visit(ASTZStringLiteral node, Object data) {
        return node.jjtGetChild(0).jjtAccept(this, data);
    }

    private String findZMFileFromName(String name){
        File file = new File(name + ".zm");
        if ( file.exists() ){
            try {
                return file.getCanonicalPath();
            }catch (Exception e){
                return file.getAbsolutePath();
            }
        }
        return "" ;
    }

    MonadicContainer zImport(String name, String as) {
        String possibleZMFile = findZMFileFromName(name);
        if ( !possibleZMFile.isEmpty() ) {
            // file import
            ZScript zs = new ZScript(possibleZMFile, danceMaster, as);
            ZContext.FunctionContext frozen = new ZContext.FunctionContext( frames.peek(),
                    ZContext.ArgContext.EMPTY_ARGS_CONTEXT);
            zs.setExternalContext(frozen);
            // run and execute if needed ?
            zs.execute();
            return new MonadicContainerBase(zs);
        }

        try {
            Class c = Class.forName(name);
            return new MonadicContainerBase(c);
        } catch (Exception e) {
            // perhaps a static field?
            int i = name.lastIndexOf('.');
            if ( i < 0 ) { return NOTHING; }
            String className = name.substring(0, i);
            String fieldName = name.substring(i + 1);
            try {
                Class cl = Class.forName(className);
                MonadicContainer c = ZJVMAccess.getProperty(cl, fieldName);
                if (!c.isNil()) return c;
            } catch (Exception ee) {

            }
        }
        return NOTHING;
    }

    @Override
    public Object visit(ASTHardImport node, Object data) {
        StringBuilder builder = new StringBuilder();
        int n = node.jjtGetNumChildren();
        ZoombaNode zn = (ZoombaNode) node.jjtGetChild(0);
        builder.append(zn.image);
        if (n == 1) {
            // we should do some special casing
            return frames.peek().get(zn.image).value();
        }
        for (int i = 1; i < n; i++) {
            zn = (ZoombaNode) node.jjtGetChild(i);
            builder.append(".").append(zn.image);
        }
        // no worries, toString() gets called!
        return builder;
    }

    @Override
    public Object visit(ASTImportStatement node, Object data) {
        Object from = node.jjtGetChild(0).jjtAccept(this, data);
        String as = ((ASTIdentifier) node.jjtGetChild(1)).image;
        MonadicContainer c = zImport(from.toString(), as);
        if (c.isNil()) throw new ZException.Import(node, from);
        ZContext ctx = frames.peek();
        Object r = c.value();
        ctx.set(as, r);
        return r;
    }

    @Override
    public Object visit(ASTAmbiguous node, Object data) {
        return null;
    }

    @Override
    public Object visit(ASTIfStatement node, Object data) {
        boolean condition;
        Object exprResult = node.jjtGetChild(0).jjtAccept(this, data);
        if ( exprResult == null ){
            exprResult = NIL;
        }
        if ( !(exprResult instanceof Boolean) ) {
            throw new ZException.ArithmeticLogicOperation(node,
                    new UnsupportedOperationException("Conditional expression yielded non boolean type: "
                            + exprResult.getClass()) , "conditional");
        }
        condition = (boolean)exprResult;
        if (condition) {
            return node.jjtGetChild(1).jjtAccept(this, data);
        }
        int num = node.jjtGetNumChildren();
        if (num > 2) {
            return node.jjtGetChild(2).jjtAccept(this, data);
        }
        return false;
    }

    @Override
    public Object visit(ASTWhileStatement node, Object data) {
        Object r = Void;
        int num = node.jjtGetNumChildren();
        Node cNode = node.jjtGetChild(0);
        boolean condition = (boolean) cNode.jjtAccept(this, data);
        if (num == 1) {
            while (condition) {
                condition = (boolean) cNode.jjtAccept(this, data);
            }
            return r;
        }
        Node body = node.jjtGetChild(1);
        while (condition) {
            try {
                r = Void;
                r = body.jjtAccept(this, data);
            } catch (ZException.Continue c) {
                if (!c.isNil()) {
                    r = c.value;
                }
            } catch (ZException.Break b) {
                if (!b.isNil()) {
                    r = b.value;
                }
                break;
            }
            condition = (boolean) cNode.jjtAccept(this, data);
        }
        return r;
    }

    @Override
    public Object visit(ASTForWithIterator node, Object data) {
        int num = node.jjtGetNumChildren();
        String loopVar = Function.THIS;
        String indexVar = "_$";
        Object it;
        Node body ;

        Node first = node.jjtGetChild(0);

        if ( num == 1 ){
            body = null;
            it = first.jjtAccept(this, data);
        }
        else if (num == 2) {
            if ( first instanceof ASTIdentifier ){
                it = node.jjtGetChild(1).jjtAccept(this, data);
                body = null;
            } else {
                it = first.jjtAccept(this, data);
                body = node.jjtGetChild(1);
            }

        } else {
            if ( num == 3 ) {
                loopVar = ((ASTIdentifier) first).image;
                it = node.jjtGetChild(1).jjtAccept(this, data);
                body = node.jjtGetChild(2);
            } else { // ( index, item )
                indexVar = ((ASTIdentifier) first).image;
                loopVar = ((ASTIdentifier) node.jjtGetChild(1)).image;
                it = node.jjtGetChild(2).jjtAccept(this, data);
                body = node.jjtGetChild(3);
            }
        }
        if (it instanceof ZScriptMethod) { // perhaps a generator??
            it = ((ZScriptMethod) it).instance(this);
        }
        ZContext ctx = frames.peek();
        MonadicContainer varBefore = ctx.get(loopVar);
        MonadicContainer indexVarBefore = ctx.get(indexVar);

        if (ZMethodInterceptor.Default.empty(it)) return Void;
        Iterator iterator;
        if (it instanceof Iterator) {
            iterator = (Iterator) it;
        } else if (it instanceof CharSequence) {
            iterator = new ZString.CharSequenceIterator((CharSequence) it);
        } else if (it instanceof Iterable) {
            iterator = ((Iterable) it).iterator();
        } else if (it instanceof Map) {
            iterator = ((Map) it).entrySet().iterator();
        } else if (it.getClass().isArray()) {
            iterator = new ZArray.ArrayIterator(it);
        } else {
            return Void;
        }
        Object r = Void;
        callStack.add( 0, "|- for_iterator" + node.locationInfo() );
        int _counter = -1;
        while (iterator.hasNext()) {
            try {
                Object o = iterator.next();
                if ( body == null){
                    continue;
                }
                ctx.set(indexVar, ++_counter);
                ctx.set(loopVar, o);
                r = body.jjtAccept(this, data);
            } catch (ZException.Break b) {
                if (!b.isNil()) {
                    r = b.value;
                }
                break;
            } catch (ZException.Continue c) {
                if (!c.isNil()) {
                    r = c.value;
                }
            } /* catch (Throwable th){
                throw th;
            } */
        }
        callStack.remove(0);
        if (varBefore.isNil()) {
            // no such variable before this for, so unset
            ctx.unSet(loopVar);
        } else {
            // replace with old value
            ctx.set( loopVar, varBefore.value());
        }
        if (indexVarBefore.isNil()) {
            // no such
            ctx.unSet(indexVar);
        } else {
            // replace with old value
            ctx.set(indexVar, indexVarBefore.value());
        }
        return r;
    }

    @Override
    public Object visit(ASTExpressionFor node, Object data) {
        int num = node.jjtGetNumChildren();
        Object r = true;
        for (int i = 0; i < num; i++) {
            // support multiple expression
            r = node.jjtGetChild(i).jjtAccept(this, data);
        }
        return r;
    }

    @Override
    public Object visit(ASTForWithCondition node, Object data) {
        Node pre = node.jjtGetChild(0);
        // must execute once
        pre.jjtAccept(this, data);
        Node cNode = node.jjtGetChild(1);
        Node post = node.jjtGetChild(2);
        int numChild = node.jjtGetNumChildren();
        Node body = null;
        if ( numChild > 3 ){
            body = node.jjtGetChild(3);
        }
        // now check condition
        boolean condition = ZTypes.bool(cNode.jjtAccept(this, data), false);
        Object r = Void;
        while (condition) {
            if( body != null){
                try {
                    r = Void;
                    r = body.jjtAccept(this, data);
                } catch (ZException.Break b) {
                    if (!b.isNil()) {
                        r = b.value;
                    }
                    break;
                } catch (ZException.Continue c) {
                    if (!c.isNil()) {
                        r = c.value;
                    }
                }
            }
            post.jjtAccept(this, data);
            condition = ZTypes.bool(cNode.jjtAccept(this, data), false);
        }
        return r;
    }

    @Override
    public Object visit(ASTForeachStatement node, Object data) {
        return node.jjtGetChild(0).jjtAccept(this, data);
    }

    @Override
    public Object visit(ASTReturnStatement node, Object data) {
        int num = node.jjtGetNumChildren();
        if (num == 0) throw RETURN;
        Object r = node.jjtGetChild(0).jjtAccept(this, data);
        throw new ZException.Return(r);
    }


    @Override
    public Object visit(ASTAssignment node, Object data) {
        Node left = node.jjtGetChild(0);
        Node right = node.jjtGetChild(1);
        Object value = right.jjtAccept(this, data);
        assignToNode(node, (ZoombaNode) left, value);
        return value;
    }

    @Override
    public Object visit(ASTReference node, Object data) {
        // could be array access, identifier or map literal
        // followed by zero or more ("." and array access, method, size,
        // identifier or integer literal)
        int numChildren = node.jjtGetNumChildren();
        // pass first piece of data in and loop through children
        Object result = null;

        for (int c = 0; c < numChildren; c++) {
            ZoombaNode theNode = (ZoombaNode) node.jjtGetChild(c);
            // integer literals may be part of an antish var name only if no bean was found so far
            if (result == null && theNode instanceof ASTNumberLiteral && ((ASTNumberLiteral) theNode).isInteger()) {
            } else {
                if ( c > 0 && result == null ){
                    // Sure null reference here?
                    Object[] m = new Object[] { "null reference : "
                            + ((ZoombaNode) node.jjtGetChild(c-1)).image } ;
                    throw new Panic(theNode,m);
                }
                result = theNode.jjtAccept(this, result);
            }
        }
        return result;
    }

    @Override
    public Object visit(ASTTernaryNode node, Object data) {
        boolean condition = (boolean) node.jjtGetChild(0).jjtAccept(this, data);
        if (condition) {
            return node.jjtGetChild(1).jjtAccept(this, data);
        }
        return node.jjtGetChild(2).jjtAccept(this, data);
    }

    @Override
    public Object visit(ASTNullCoalesce node, Object data) {
        try {
            Object v = node.jjtGetChild(0).jjtAccept(this, data);
            if (v != null) {
                return v;
            }
        } catch (ZException e) {
        }
        return node.jjtGetChild(1).jjtAccept(this, data);
    }

    @Override
    public Object visit(ASTOrNode node, Object data) {
        Object l = node.jjtGetChild(0).jjtAccept(this, data);
        boolean ret = ZTypes.bool(l, false);
        if (ret) return true;
        Object r = node.jjtGetChild(1).jjtAccept(this, data);
        ret = ZTypes.bool(r, false);
        return ret;
    }

    @Override
    public Object visit(ASTAndNode node, Object data) {
        Object l = node.jjtGetChild(0).jjtAccept(this, data);
        boolean ret = ZTypes.bool(l, false);
        if (!ret) return false;
        Object r = node.jjtGetChild(1).jjtAccept(this, data);
        ret = ZTypes.bool(r, false);
        return ret;
    }

    @Override
    public Object visit(ASTBitwiseOrNode node, Object data) {
        Object l = node.jjtGetChild(0).jjtAccept(this, data);
        Object r = node.jjtGetChild(1).jjtAccept(this, data);
        return arithmetic.or(l, r);
    }

    @Override
    public Object visit(ASTBitwiseXorNode node, Object data) {
        Object l = node.jjtGetChild(0).jjtAccept(this, data);
        Object r = node.jjtGetChild(1).jjtAccept(this, data);
        return arithmetic.xor(l, r);
    }

    @Override
    public Object visit(ASTBitwiseAndNode node, Object data) {
        Object l = node.jjtGetChild(0).jjtAccept(this, data);
        Object r = node.jjtGetChild(1).jjtAccept(this, data);
        return arithmetic.and(l, r);
    }

    @Override
    public Object visit(ASTISANode node, Object data) {
        Object l = node.jjtGetChild(0).jjtAccept(this, data);
        Object r = node.jjtGetChild(1).jjtAccept(this, data);
        return ZTypes.isa(l, r,this);
    }

    @Override
    public Object visit(ASTINNode node, Object data) {
        Object l = node.jjtGetChild(0).jjtAccept(this, data);
        Object r = node.jjtGetChild(1).jjtAccept(this, data);
        return arithmetic.in(l, r);
    }

    @Override
    public Object visit(ASTNINNode node, Object data) {
        Object l = node.jjtGetChild(0).jjtAccept(this, data);
        Object r = node.jjtGetChild(1).jjtAccept(this, data);
        return !arithmetic.in(l, r);
    }

    @Override
    public Object visit(ASTDividesNode node, Object data) {
        Object l = node.jjtGetChild(0).jjtAccept(this, data);
        Object r = node.jjtGetChild(1).jjtAccept(this, data);
        return arithmetic.divides(l, r);
    }

    @Override
    public Object visit(ASTAEQNode node, Object data) {
        Object l = node.jjtGetChild(0).jjtAccept(this, data);
        Object r = node.jjtGetChild(1).jjtAccept(this, data);
        if (l == null && r == null) return true;
        if (l == null || r == null) return false;
        if (l.getClass() != r.getClass()) return false;
        return arithmetic.eq(l, r);
    }

    @Override
    public Object visit(ASTSEQNode node, Object data) {
        Object l = node.jjtGetChild(0).jjtAccept(this, data);
        Object r = node.jjtGetChild(1).jjtAccept(this, data);
        if (l == null && r == null) return true;
        if (l == null || r == null) return false;
        return l.equals(r);
    }

    @Override
    public Object visit(ASTStartWithNode node, Object data) {
        Object l = node.jjtGetChild(0).jjtAccept(this, data);
        Object r = node.jjtGetChild(1).jjtAccept(this, data);
        return arithmetic.startsWith(l, r);
    }

    @Override
    public Object visit(ASTEndWithNode node, Object data) {
        Object l = node.jjtGetChild(0).jjtAccept(this, data);
        Object r = node.jjtGetChild(1).jjtAccept(this, data);
        return arithmetic.endsWith(l, r);
    }

    @Override
    public Object visit(ASTInOrderNode node, Object data) {
        Object l = node.jjtGetChild(0).jjtAccept(this, data);
        Object r = node.jjtGetChild(1).jjtAccept(this, data);
        return arithmetic.inOrder(l,r);
    }

    @Override
    public Object visit(ASTEQNode node, Object data) {
        Object l = node.jjtGetChild(0).jjtAccept(this, data);
        Object r = node.jjtGetChild(1).jjtAccept(this, data);
        return arithmetic.eq(l, r);
    }

    @Override
    public Object visit(ASTNENode node, Object data) {
        Object l = node.jjtGetChild(0).jjtAccept(this, data);
        Object r = node.jjtGetChild(1).jjtAccept(this, data);
        return arithmetic.ne(l, r);
    }

    @Override
    public Object visit(ASTLTNode node, Object data) {
        Object l = node.jjtGetChild(0).jjtAccept(this, data);
        Object r = node.jjtGetChild(1).jjtAccept(this, data);
        try {
            return arithmetic.lt(l, r);
        }catch (Throwable t){
            throw new ArithmeticLogicOperation(node,t,"<");
        }
    }

    @Override
    public Object visit(ASTGTNode node, Object data) {
        Object l = node.jjtGetChild(0).jjtAccept(this, data);
        Object r = node.jjtGetChild(1).jjtAccept(this, data);
        try {
            return arithmetic.gt(l, r);
        }catch (Throwable t){
            throw new ArithmeticLogicOperation(node,t,">");
        }
    }

    @Override
    public Object visit(ASTLENode node, Object data) {
        Object l = node.jjtGetChild(0).jjtAccept(this, data);
        Object r = node.jjtGetChild(1).jjtAccept(this, data);
        try {
            return arithmetic.le(l, r);
        }catch (Throwable t){
            throw new ArithmeticLogicOperation(node,t,"<=");
        }
    }

    @Override
    public Object visit(ASTGENode node, Object data) {
        Object l = node.jjtGetChild(0).jjtAccept(this, data);
        Object r = node.jjtGetChild(1).jjtAccept(this, data);
        try {
            return arithmetic.ge(l, r);
        }catch (Throwable t){
            throw new ArithmeticLogicOperation(node,t,">=");
        }
    }

    @Override
    public Object visit(ASTPatternLiteral node, Object data) {
        return node;
    }

    protected boolean match(Object l, Object r) {
        String left = String.valueOf(l);
        if (r instanceof ASTPatternLiteral) {
            ASTPatternLiteral literal = (ASTPatternLiteral) r;
            return literal.matches(left,this);
        }
        if (r instanceof CharSequence) {
            Pattern p = Pattern.compile(r.toString());
            return p.matcher(left).matches();
        }
        if (r instanceof Pattern) {
            return ((Pattern) r).matcher(left).matches();
        }
        return false;
    }

    @Override
    public Object visit(ASTERNode node, Object data) {
        Object l = node.jjtGetChild(0).jjtAccept(this, data);
        Object r = node.jjtGetChild(1).jjtAccept(this, data);
        return match(l, r);
    }

    @Override
    public Object visit(ASTNRNode node, Object data) {
        Object l = node.jjtGetChild(0).jjtAccept(this, data);
        Object r = node.jjtGetChild(1).jjtAccept(this, data);
        return !match(l, r);
    }

    @Override
    public Object visit(ASTAdditiveOperator node, Object data) {
        return NIL;
    }

    @Override
    public Object visit(ASTPowNode node, Object data) {
        Object l = node.jjtGetChild(0).jjtAccept(this, data);
        Object r = node.jjtGetChild(1).jjtAccept(this, data);
        try {
            return arithmetic.pow(l, r);
        }catch (Throwable t){
            throw new ArithmeticLogicOperation(node,t,"**");
        }
    }

    @Override
    public Object visit(ASTMulNode node, Object data) {
        Object l = node.jjtGetChild(0).jjtAccept(this, data);
        Object r = node.jjtGetChild(1).jjtAccept(this, data);
        try {
            return arithmetic.mul(l, r);
        }catch (Throwable t){
            throw new ArithmeticLogicOperation(node,t,"*");
        }
    }

    @Override
    public Object visit(ASTDivNode node, Object data) {
        Object l = node.jjtGetChild(0).jjtAccept(this, data);
        Object r = node.jjtGetChild(1).jjtAccept(this, data);
        try {
            return arithmetic.div(l, r);
        }catch (Throwable t){
            throw new ArithmeticLogicOperation(node,t,"/");
        }

    }

    @Override
    public Object visit(ASTModNode node, Object data) {
        Object l = node.jjtGetChild(0).jjtAccept(this, data);
        Object r = node.jjtGetChild(1).jjtAccept(this, data);
        try {
            return arithmetic.mod(l, r);
        }catch (Throwable t){
            throw new ArithmeticLogicOperation(node,t,"%");
        }
    }

    @Override
    public Object visit(ASTUnarySizeNode node, Object data) {
        Object l = node.jjtGetChild(0).jjtAccept(this, data);
        return ZMethodInterceptor.Default.cardinality(l);
    }

    @Override
    public Object visit(ASTUnaryMinusNode node, Object data) {
        Object l = node.jjtGetChild(0).jjtAccept(this, data);
        return arithmetic.not(l);
    }

    @Override
    public Object visit(ASTBitwiseComplNode node, Object data) {
        return null;
    }

    @Override
    public Object visit(ASTNotNode node, Object data) {
        Object l = node.jjtGetChild(0).jjtAccept(this, data);
        Boolean b = ZTypes.bool(l);
        if (b != null) return !b;
        return arithmetic.UNSUPPORTED_OPERATION(l, l, "LOGICAL NOT");
    }

    @Override
    public Object visit(ASTIdentifier node, Object data) {
        if (data != null) {
            // a field access probably ...
            MonadicContainer c = ZJVMAccess.getProperty(data, node.image);
            if (c.isNil()) throw new Property(node, data, node.image);
            return c.value();
        }

        String name = node.image;
        if ( MAIN_SCRIPT_VAR.equals(name)){
            return danceMaster;
        }
        // globals?
        if (ZScript.IS_GLOBAL_VAR(name)) {
            if (danceMaster.globals.containsKey(name)) {
                return danceMaster.globals.get(name);
            }
            // $$ is now allowed anywhere else anyways
            throw new ZException.Variable(node, name);
        }
        // else...
        ZContext ctx = frames.peek();
        if (ctx.has(name)) {
            return ctx.get(name).value();
        }
        // is it a method?
        if (danceMaster.methods.containsKey(name)) {
            return danceMaster.methods.get(name);
        }
        throw new ZException.Variable(node, name);
    }

    @Override
    public Object visit(ASTNullLiteral node, Object data) {
        return null;
    }

    @Override
    public Object visit(ASTTrueNode node, Object data) {
        return true;
    }

    @Override
    public Object visit(ASTFalseNode node, Object data) {
        return false;
    }

    @Override
    public Object visit(ASTNumberLiteral node, Object data) {
        if (data != null) {
            MonadicContainer c = ZJVMAccess.getProperty(data, node.getLiteral());
            if (c.isNil()) throw new Property(node, data, node.getLiteral());
            return c.value();
        }
        return node.getLiteral();
    }

    @Override
    public Object visit(ASTStringLiteral node, Object data) {
        return node.image;
    }

    @Override
    public Object visit(ASTArrayRange node, Object data) {
        int num = node.jjtGetNumChildren();
        Object b = node.jjtGetChild(0).jjtAccept(this, data);
        Object e = node.jjtGetChild(1).jjtAccept(this, data);
        if (num > 2) {
            Object s = node.jjtGetChild(2).jjtAccept(this, data);
            return ZTypes.range(b, e, s);
        }
        return ZTypes.range(b, e);
    }


    @Override
    public Object visit(ASTArrayLiteral node, Object data) {
        int num = node.jjtGetNumChildren();
        if (num == 0) return ZArray.EMPTY_Z_ARRAY;

        Object[] arr = new Object[num];
        for (int i = 0; i < num; i++) {
            arr[i] = node.jjtGetChild(i).jjtAccept(this, data);
        }
        return new ZArray(arr, false);
    }

    @Override
    public Object visit(ASTMapLiteral node, Object data) {
        Map m = new ZMap();
        int num = node.jjtGetNumChildren();
        for (int i = 0; i < num; i++) {
            Node n = node.jjtGetChild(i);
            Object[] pair = (Object[]) n.jjtAccept(this, data);
            m.put(pair[0], pair[1]);
        }
        return m;
    }

    @Override
    public Object visit(ASTMapEntry node, Object data) {
        Object[] pair = new Object[2];
        pair[0] = node.jjtGetChild(0).jjtAccept(this, data);
        pair[1] = node.jjtGetChild(1).jjtAccept(this, data);
        return pair;
    }

    private Object createAnon(Node n, Object data) {
        if (n instanceof ASTMethodDef) {
            ZScriptMethod m = (ZScriptMethod) n.jjtAccept(this, data);
            return m.instance(this);
        }
        if (n instanceof ASTIdentifier) {
            Object func = n.jjtAccept(this, data);
            if (func instanceof Function) return func;
            if (func instanceof ZScriptMethod) {
                return ((ZScriptMethod) func).instance(this);
            }
            throw new ZException.Function((ZoombaNode) n.jjtGetParent(), ((ASTIdentifier) n).image);
        }
        return null;
    }

    @Override
    public Object visit(ASTMapperLambda node, Object data) {
        Node n = node.jjtGetChild(0);
        Object func = createAnon(n, data);
        if (func != null) return func;
        return new AnonymousFunctionInstance.MapperLambda(this, (ASTBlock) node.jjtGetChild(0));
    }

    @Override
    public Object visit(ASTPredicateLambda node, Object data) {
        Node n = node.jjtGetChild(0);
        Object func = createAnon(n, data);
        if (func != null) return func;
        return new AnonymousFunctionInstance.PredicateLambda(this, (ASTBlock) node.jjtGetChild(0));
    }

    @Override
    public Object visit(ASTMethodArguments node, Object data) {
        return null;
    }


    protected Object[] processArgs(ASTMethodArguments arguments) {
        if (arguments == null) return ZArray.EMPTY_ARRAY;
        // no named params as of now... later job
        int num = arguments.jjtGetNumChildren();

        Object[] args = new Object[num];
        for (int i = 0; i < num; i++) {
            Node n = arguments.jjtGetChild(i);
            args[i] = n.jjtAccept(this, null);
            if (args[i] instanceof ZScriptMethod) {
                // create an instance ...
                args[i] = ((ZScriptMethod) args[i]).instance(this);
            }
        }
        if (args.length == 1 && args[0] instanceof ASTArgDef) {
            String name = ((ZoombaNode) ((ASTArgDef) args[0]).jjtGetChild(0)).image;
            if (ZScriptMethod.ARGS_VAR.equals(name)) {
                // we must repopulate args...
                Object o = ((ASTArgDef) args[0]).jjtGetChild(1).jjtAccept(this, null);
                if (o != null && o.getClass().isArray()) {
                    o = new ZArray(o, false);
                }
                if (o instanceof List) {
                    List l = ((List) o);
                    args = new Object[l.size()];
                    for (int i = 0; i < args.length; i++) {
                        args[i] = l.get(i);
                    }
                }
            }
        }
        return args;
    }

    @Override
    public MonadicContainer intercept(Object context, ZInterpret interpret, String methodName,
                                      Object[] args, List<Function> anons,
                                      ZoombaNode zoombaNode) {
        MonadicContainer r;
        ZScript zScript = danceMaster;
        Function anon = anons.isEmpty() ? null : anons.get(0);
        // if they are any of these babies?
        switch (methodName) {
            case "test":
                zScript.assertion.testAssert(ZAssertion.AssertionType.TEST,
                        zoombaNode, anon, args);
                return SUCCESS;
            case "assert":
                zScript.assertion.testAssert(ZAssertion.AssertionType.ASSERT,
                        zoombaNode, anon, args);
                return SUCCESS;
            case "panic":
                zScript.assertion.testAssert(ZAssertion.AssertionType.PANIC,
                        zoombaNode, anon, args);
                return SUCCESS;
            default:
                // no probs... move along
                break;
        }


        if (context != null) {
            zScript = (ZScript) context;
        }

        // can file function defined by *ME*, so ...
        if (zScript.methods.containsKey(methodName)) {
            // yes, I have a match ...
            //try instantiating and then calling
            ZScriptMethod method = zScript.methods.get(methodName);
            ZScriptMethod.ZScriptMethodInstance mI = method.instance(this);
            r = mI.execute(args);
            return r;
        }
        if ( context != null ){
            // x.y() but... could not do it...
            return UNSUCCESSFUL_INTERCEPT;
        }
        // can be a variable which is assigned a function
        ZContext ctx = frames.peek();
        if (ctx.has(methodName)) {
            Object o = ctx.get(methodName).value();
            if (o instanceof ZScriptMethod) {
                // yes, I have a match ...
                //try instantiating and then calling
                ZScriptMethod.ZScriptMethodInstance mI = ((ZScriptMethod) o).instance(this);
                r = mI.execute(args);
                return r;
            }
            // if this is a script then
            if (o instanceof ZScript) {
                ((ZScript) o).setExternalContext((ZContext.FunctionContext) ctx);
                return ((ZScript) o).execute(args);
            }
        }
        return UNSUCCESSFUL_INTERCEPT;
    }

    protected Object callJVMMethod(ASTMethodNode methodNode, Object context, String name, Object[] args) {
        try {
            // call JVM support ya hoooo
            Object ro = ZJVMAccess.callMethod(context, name, args);
            return ro;
        } catch (Throwable t) {
            String message = t.getMessage();
            t = t.getCause();
            Throwable cause = null;
            if (t instanceof InvocationTargetException) {
                cause = t.getCause();
                if (cause != null) {
                    message = cause.toString();
                }
            }
            throw new ZException.Function(methodNode, name + " : " + message, cause );
        }
    }


    protected Object callMethod(Object context, final ASTMethodNode methodNode, String name,
                                ASTMethodArguments arguments,
                                List<Function> anons) {

        Object[] args = processArgs(arguments);
        MonadicContainer r = NOTHING;
        if (context == null || context instanceof ZScript) {
            try {
                // can be a method defined in file...
                r = intercept(context, this, name, args, anons, methodNode);
                if (r != UNSUCCESSFUL_INTERCEPT) {
                    return r.value();
                }
                // must be context less : so interceptor
                r = DEFAULT_INTERCEPTOR.intercept(context, this, name, args, anons, methodNode);
                if (UNSUCCESSFUL_INTERCEPT == r) {
                    if (context != null) {
                        // I am calling ZScript programmatically
                        return callJVMMethod(methodNode, context, name, args);
                    }
                    // can it be a function passed as an arg or a function as variable in context??
                    MonadicContainer mayBe = frames.peek().get(name);
                    if ( mayBe.value() instanceof Function ){
                        Function func = (Function)mayBe.value();
                        r = func.execute(args);
                        return r.value();
                    }
                    // unknown function exception ... boo ha ha ha ha
                    throw new ZException.Function(methodNode, name);
                }
                return r.value();
            } catch (Throwable t) {
                if (t instanceof ZException) {
                    throw t;
                }
                final String message = t instanceof StackOverflowError ? "StackOverFlow" :  t.getMessage();
                throw new ZException.Function(methodNode, message + ": in method : " + name, t );
            }
        } else if (context instanceof ZMethodInterceptor) {
            r = ((ZMethodInterceptor) context).intercept(context, this, name, args, anons, methodNode);
            if (r != UNSUCCESSFUL_INTERCEPT) {
                return r.value();
            }
            return callJVMMethod(methodNode, context, name, args);
        } else {
            if (context instanceof Map) {
                try {
                    Object func = ((Map) context).get(name);
                    if (func instanceof ZScriptMethod) {
                        func = ((ZScriptMethod) func).instance(this, context);
                    }
                    if (func instanceof Function) {
                        r = ((Function) func).execute(args);
                        return r.value();
                    }
                } catch (Exception e) { /* Ignore... Type error may happen */ }
            }
            return callJVMMethod(methodNode, context, name, args);
        }
    }

    boolean isDefined(ASTMethodArguments arguments) {
        if (arguments == null) return true;
        try {
            Object o = arguments.jjtGetChild(0).jjtAccept(this, null);
            return !( o instanceof Nil );
        } catch (Throwable t) {
        }
        return false;
    }

    List<String> callStack = new ArrayList<>();

    public List<String> callStack(){ return callStack; }

    @Override
    public Object visit(ASTMethodNode node, Object data) {
        int num = node.jjtGetNumChildren();
        Node n = node.jjtGetChild(num - 1);
        Function instance = null;
        List composes = new ArrayList<>();
        int i = num - 1;
        while (i >= 0) {
            if (n instanceof ASTPredicateLambda) {
                instance = (Predicate) n.jjtAccept(this, data);
            } else if (n instanceof ASTMapperLambda) {
                instance = (Mapper) n.jjtAccept(this, data);
            } else {
                break;
            }
            composes.add(0, instance);
            n = node.jjtGetChild(--i);
        }
        ASTMethodArguments arguments = null;
        if (num > 1) {
            n = node.jjtGetChild(1);
            if (n instanceof ASTMethodArguments) {
                arguments = (ASTMethodArguments) n;
            }
        }
        String name = ((ASTIdentifier) node.jjtGetChild(0)).image;
        if ("type".equals(name) && data == null ) {
            Object[] args = new Object[1];
            num = arguments.jjtGetNumChildren();
            if (num == 0) return NIL.getClass();
            args[0] = arguments.jjtGetChild(0).jjtAccept(this, data);
            return type(args);
        }
        if ("is".equals(name) && data == null ) {
            return isDefined(arguments);
        }
        callStack.add( 0, "|- " + name + node.locationInfo() );
        Object o = callMethod(data, node, name, arguments, composes);
        if ( !callStack.isEmpty() ) {
            callStack.remove(0);
        }
        // find if
        composes.clear();
        return o;
    }

    private Object createZObject(ASTConstructorNode node, ZObject classType, Object[] args){
        try {
            return ZObject.createFrom(classType, this, args);
        } catch (Throwable t) {
            if ( t instanceof ZException ){ throw t;}
            throw new ZException.Function(node, t.getMessage());
        }
    }

    @Override
    public Object visit(ASTConstructorNode node, Object data) {
        int num = node.jjtGetNumChildren();
        Object classType = node.jjtGetChild(0).jjtAccept(this, data);
        Object[] args = new Object[num - 1];
        for (int i = 1; i < num; i++) {
            args[i - 1] = node.jjtGetChild(i).jjtAccept(this, data);
        }
        if (classType instanceof ZObject) {
            return createZObject(node, (ZObject)classType, args);
        } else {
            try {
                return ZJVMAccess.construct(classType, args);
            } catch (Throwable t) {
                // am I looking for ZObject as string ?
                if ( classType instanceof String ){
                    String[] splits = ((String) classType).split("\\.");
                    MonadicContainer mc = frames.peek().get(splits[0]);
                    if ( splits.length == 1 ){
                       if ( mc.value() instanceof ZObject ){
                           return createZObject(node,(ZObject)mc.value(),args );
                       }
                    } else {
                       if ( mc.value() instanceof ZScript ){
                           Object obj = ((ZScript) mc.value()).get(splits[1]);
                           if ( obj instanceof ZObject ){
                               return createZObject(node,(ZObject)obj, args);
                           }
                       }
                    }
                }

                String message = t.getMessage();
                throw new ZException.Function(node, message);
            }
        }
    }

    @Override
    public Object visit(ASTArrayIndex node, Object data) {
        Object l = node.jjtGetChild(0).jjtAccept(this, data);
        if (1 == node.jjtGetNumChildren()) {
            return l;
        }
        Object r = node.jjtGetChild(1).jjtAccept(this, data);
        return new Object[]{l, r};
    }

    @Override
    public Object visit(ASTArrayAccess node, Object data) {
        // first objectNode is the identifier
        Object object = node.jjtGetChild(0).jjtAccept(this, data);
        // can have multiple nodes - either an expression, integer literal or reference
        int numChildren = node.jjtGetNumChildren();
        for (int i = 1; i < numChildren; i++) {
            Node nindex = node.jjtGetChild(i);
            if (nindex instanceof ZoombaNode.Literal<?>) {
                object = nindex.jjtAccept(this, object);
            } else {
                Object index = nindex.jjtAccept(this, null);
                MonadicContainer mc = ZJVMAccess.getProperty(object, index);
                if (mc.isNil()) throw new Property((ZoombaNode) nindex, object , index);
                object = mc.value();
            }
        }

        return object;
    }

    @Override
    public Object visit(ASTReferenceExpression node, Object data) {
        int num = node.jjtGetNumChildren();
        if (num == 1) {
            // this is binding ( x ) --> x
            return node.jjtGetChild(0).jjtAccept(this, data);
        }
        ASTArrayAccess upper = node;
        return visit(upper, data);
    }

    @Override
    public Object visit(ASTClassDef node, Object data) {
        String className = ((ZoombaNode) node.jjtGetChild(0)).image;
        Map<String, Object> prop = new HashMap<>();
        int numChildren = node.jjtGetNumChildren();
        // JS Style...
        if (numChildren > 1 && node.jjtGetChild(1) instanceof ASTMapLiteral ){
            // this is JS style class definition
            Node mapLiteral = node.jjtGetChild(1);
            int num = mapLiteral.jjtGetNumChildren();
            for (int i = 0; i < num; i++) {
                Node astMapEntry = mapLiteral.jjtGetChild(i);
                Node propNode = astMapEntry.jjtGetChild(0).jjtGetChild(0);
                Node propValue = astMapEntry.jjtGetChild(1);

                String propName = ((ZoombaNode) propNode).image;
                propName = propName != null ? propName : String.valueOf(propNode.jjtAccept(this,data));
                Object value = propValue.jjtAccept(this, data);
                prop.put(propName, value);
            }
        } else {
            // classical class definition
            // all children are named functions
            for ( int i = 1 ; i < numChildren ; i++ ){
                ZScriptMethod zScriptMethod = (ZScriptMethod)node.jjtGetChild(i).jjtAccept(this,data);
                // if a function has no name, then it is not counted in :: silent ignore?
                if ( !zScriptMethod.name.isEmpty() ){
                    prop.put(zScriptMethod.name,zScriptMethod);
                } else {
                    System.err.printf("Class function [ %s ] has no name, ignored! %n",
                            ((ZoombaNode)zScriptMethod.methodNode).locationInfo() );
                }
            }
        }
        ZObject zo = new ZObject(className, prop, this);
        this.danceMaster.protoTypes.put(className, zo);
        ZContext ctx = frames.peek();
        ctx.set(className, zo);
        return zo;
    }

    @Override
    public Object visit(ASTParamDef node, Object data) {
        int num = node.jjtGetNumChildren();
        String name = ((ASTIdentifier) node.jjtGetChild(0)).image;
        if (num == 1) return new ZMap.Pair(name, NIL);
        Object value = node.jjtGetChild(1).jjtAccept(this, null);
        return new ZMap.Pair(name, value);
    }

    @Override
    public Object visit(ASTArgDef node, Object data) {
        int num = node.jjtGetNumChildren();
        if (num == 1) {
            // simple form
            Object o = node.jjtGetChild(0).jjtAccept(this, null);
            return o;
        }
        // x = y form : return itself : to be evaluated there later
        return node;
    }

    Map<String, Object> getDefaults(ASTMethodParams node) {
        Map m = new HashMap();
        int num = node.jjtGetNumChildren();
        for (int i = 0; i < num; i++) {
            ASTParamDef paramDef = (ASTParamDef) node.jjtGetChild(i);
            // all the args must be paired too...
            ZMap.Pair defaultArg = (ZMap.Pair) paramDef.jjtAccept(this, null);
            String argName = defaultArg.getKey().toString();
            Object value = defaultArg.getValue();
            m.put(argName, value);
        }
        return m;
    }

    Map<String, Object> getActualArgs(Object[] args) {
        Map m = new HashMap();
        int namedCount = 0;
        for (int i = 0; i < args.length; i++) {
            if (args[i] instanceof ASTArgDef) {
                String name = ((ASTIdentifier) ((ASTArgDef) args[i]).jjtGetChild(0)).image;
                Object v = ((ASTArgDef) args[i]).jjtGetChild(1).jjtAccept(this, null);
                m.put(name, v);
                namedCount++;
            }
        }
        if (namedCount > 0) {

            if (namedCount != args.length) {
                m.clear();
                throw new UnsupportedOperationException("One must provide all named args!" +
                        ((ASTArgDef) args[0]).locationInfo());
            }
            if (namedCount > 1 && m.containsKey(ZScriptMethod.ARGS_VAR)) {
                m.clear();
                throw new UnsupportedOperationException("One must provide only @ARGS as argument!" +
                        ((ASTArgDef) args[0]).locationInfo());
            }
        }
        return m;
    }


    @Override
    public Object visit(ASTMethodParams node, Object data) {
        Object[] args = (Object[]) data;
        int num = node.jjtGetNumChildren();
        if (num == 0) {
            if (args.length == 0)
                return ZContext.ArgContext.EMPTY_ARGS_CONTEXT;
            return new ZContext.ArgContext(args);
        }

        Map<String, Object> defMap = getDefaults(node);
        Map<String, Object> argMap = getActualArgs(args);
        List params = new ArrayList();
        List values = new ArrayList();

        if (!argMap.isEmpty()) {
            boolean didUnPack = false;
            // we have named args...
            if (argMap.containsKey(ZScriptMethod.ARGS_VAR)) {
                // we have @ARGS, treat accordingly
                Object a = argMap.get(ZScriptMethod.ARGS_VAR);
                if (a instanceof Iterable) {
                    Object[] packed = BaseZCollection.array((Iterable) a);
                    args = packed;
                    didUnPack = true;
                } else if (a instanceof Map) {
                    // just replace the argMap with this
                    argMap = (Map) a;
                } else {
                    // we dont know what we should do, so ...
                    throw new UnsupportedOperationException(
                            "No Clue what I should do with this @ARGS! : " + node.locationInfo());
                }
            }
            if (!didUnPack) {
                // other named args
                for (String name : defMap.keySet()) {
                    params.add(name);
                    if (argMap.containsKey(name)) {
                        values.add(new MonadicContainerBase(argMap.get(name)));
                    } else {
                        values.add(new MonadicContainerBase(defMap.get(name)));
                    }
                }
                return new ZContext.ArgContext(params, values);
            }
        }

        // now here... must match ...
        for (int i = 0; i < num; i++) {
            ASTParamDef paramDef = (ASTParamDef) node.jjtGetChild(i);
            String paramName = ((ASTIdentifier) paramDef.jjtGetChild(0)).image;
            params.add(paramName);
            if (i < args.length) {
                values.add(new MonadicContainerBase(args[i]));
            } else {
                Object defValue = defMap.get(paramName);
                if ( defValue instanceof Nil ){
                    // should say unassigned parameters..
                    throw new RuntimeException(String.format("parameter '%s' is not assigned!", paramName));
                }
                values.add(new MonadicContainerBase(defValue));
            }
        }
        return new ZContext.ArgContext(params, values);
    }

    @Override
    public Object visit(ASTMethodDef node, Object data) {
        if (frames.size() > 1) {
            ZContext ctx = frames.peek();
            // this is inner function for sure... freeze outer
            ZScriptMethod zm = new ZScriptMethod(danceMaster, node, ctx);
            if (!zm.name.isEmpty()) {
                ctx.set(zm.name, zm);
            }
            return zm; // return it
        }
        return new ZScriptMethod(danceMaster, node);
    }
}
