/*
 * Copyright 2017 zoomba-lang.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package zoomba.lang.core.interpreter;

import org.apache.commons.jxpath.JXPathContext;
import org.jscience.mathematics.number.LargeInteger;
import org.jscience.mathematics.number.Real;
import zoomba.lang.core.collections.*;
import zoomba.lang.core.io.ZDataBase;
import zoomba.lang.core.io.ZWeb;
import zoomba.lang.core.operations.Function;
import zoomba.lang.core.operations.ZCollection;
import zoomba.lang.core.operations.ZMatrix;
import zoomba.lang.core.operations.ZRandom;
import zoomba.lang.core.sys.ZProcess;
import zoomba.lang.core.sys.ZThread;
import zoomba.lang.core.types.*;
import zoomba.lang.parser.ASTArgDef;
import zoomba.lang.parser.ZoombaNode;
import zoomba.lang.core.io.ZFileSystem;

import java.io.File;
import java.io.PrintStream;
import java.lang.reflect.Array;
import java.util.*;

import static zoomba.lang.core.operations.Function.*;
import static zoomba.lang.core.types.ZTypes.bool;

/**
 * The Interceptor for all the fancy methods
 */
public interface ZMethodInterceptor {

    Function.MonadicContainer intercept(Object context, ZInterpret interpret,
                                        String methodName,
                                        Object[] args,
                                        List<Function> anons,
                                        ZoombaNode callNode);

    Function.MonadicContainer UNSUCCESSFUL_INTERCEPT = new Function.MonadicContainerBase();

    Default DEFAULT_INTERCEPTOR = new Default();

    final class Default implements ZMethodInterceptor {

        public static final String ASYNC = "async";

        public static final String BINARY_SEARCH = "bsearch";

        public static final String POLL = "poll";

        public static final String PRINTF = "printf";

        public static final String PRINTLN = "println";

        public static final String EPRINTF = "eprintf";

        public static final String EPRINTLN = "eprintln";


        public static final String SIZE = "size";

        public static final String EMPTY = "empty";

        public static final String LIST = "list";

        public static final String SET = "set";

        public static final String SORTED_SET = "sset";

        public static final String ORDERED_SET = "oset";

        public static final String MSET = "mset";

        public static final String GROUP_BY = "group";

        public static final String DICT = "dict";

        public static final String SORTED_DICT = "sdict";

        public static final String ORDERED_DICT = "odict";

        public static final String SELECT = "select";

        public static final String PARTITION = "partition";

        public static final String JOIN = "join";

        public static final String FOLD = "fold";

        public static final String LFOLD = "lfold";

        public static final String RFOLD = "rfold";

        public static final String LREDUCE = "reduce";

        public static final String RREDUCE = "rreduce";

        public static final String FIND = "find";

        public static final String LINDEX = "index";

        public static final String EXISTS = "exists";

        public static final String RINDEX = "rindex";

        public static final String INT = "int";

        public static final String FLOAT = "float";

        public static final String INT_L = "INT";

        public static final String FLOAT_L = "FLOAT";

        public static final String NUM = "num";

        public static final String BOOL = "bool";

        public static final String TIME = "time";

        public static final String ROUND = "round";

        public static final String FLOOR = "floor";

        public static final String CEIL = "ceil";

        public static final String LOG = "log";

        public static final String STRING = "str";

        public static final String JSON_STRING = "jstr";

        public static final String ENUM = "enum";

        public static final String THREAD = "thread";

        public static final String JSON = "json";

        public static final String XML = "xml";

        public static final String OPEN = "open";

        public static final String FILE = "file";

        public static final String READ = "read";

        public static final String WRITE = "write";

        public static final String RANDOM = "random";

        public static final String SHUFFLE = "shuffle";

        public static final String SORTA = "sorta";

        public static final String SORTD = "sortd";

        public static final String SUM = "sum";

        public static final String MINMAX = "minmax";

        public static final String SYSTEM = "system";

        public static final String POPEN = "popen";

        public static final String MATRIX = "matrix";

        public static final String BYE = "bye";

        public static final String RAISE = "raise";

        public static final String LOAD = "load";

        public static final String FROM = "from";

        public static final String HEAP = "heap";

        public static final String HASH = "hash";

        public static final String TOKENS = "tokens";

        public static final String SEQUENCE = "seq";

        public static final String SUB_SEQUENCES = "sequences";

        public static final String COMBINATION = "comb";

        public static final String PERMUTATION = "perm";

        public static final String TUPLE = "tuple";

        public static final String YAML = "yaml";

        public static final String YSTR = "ystr";

        public static final String SIGN = "sign";

        public static final String PRIORITY_QUEUE = "pqueue";

        public static final String JX_PATH = "xpath";

        public static final String JX_ELEMENT = "xelem";

        private Default() {
        }

        public static Object jxPath(Object... args) {
            if (args.length < 2) return null;
            JXPathContext context = JXPathContext.newContext(args[0]);
            String path = String.valueOf(args[1]);
            if (args.length > 2) {
                if (bool(args[2], false)) {
                    Iterator iterable = context.iterate(path);
                    return new ZList(iterable);
                }
            }
            return context.getValue(String.valueOf(args[1]));
        }


        public static Object jxElement(Object... args) {
            if (args.length < 2) return null;
            JXPathContext context = JXPathContext.newContext(args[0]);
            String path = String.valueOf(args[1]);
            if (args.length > 2) {
                if (bool(args[2], false)) {
                    Iterator iterable = context.iteratePointers(path);
                    return new ZList(iterable);
                }
            }
            return context.getPointer(String.valueOf(args[1]));
        }

        public static Object from(List<Function> anons, Object... args) {
            Object o;
            if (args.length > 1 && args[1] instanceof Collection) {
                o = BaseZCollection.compose(args[0], (Collection) args[1], anons);
            } else if (args.length > 0) {
                o = BaseZCollection.compose(args[0], null, anons);
            } else {
                o = Collections.EMPTY_LIST;
            }
            return o;
        }

        public static Object read(Object... args) {
            if (args.length == 0) { // in case no args, reads from stdin
               Scanner sc = new Scanner(System.in);
               if ( sc.hasNext() ){
                   return sc.next();
               }
               return NIL;
            }
            String path;
            NamedArgs params = null;
            if ( args[0] instanceof Map ){
                params = NamedArgs.fromMap( (Map)args[0] );
                path = params.string("path","");
            } else {
                path = String.valueOf(args[0]);
            }

            if (path.contains(":/") && path.startsWith("http")) {
                // web protocol
                int conTO = Integer.MAX_VALUE;
                int readTO = Integer.MAX_VALUE;
                Map<String,String> headers = Collections.emptyMap();
                if (args.length > 1) {
                    conTO = (int) ZNumber.integer(args[1], conTO);
                    if (args.length > 2) {
                        readTO = (int) ZNumber.integer(args[1], readTO);
                        if (args.length > 3) {
                            headers = (Map)args[2];
                        }
                    }
                }
                if ( params != null ){
                    conTO = params.number( "conTo", conTO).intValue();
                    readTO = params.number( "readTo", readTO).intValue();
                    headers = params.map( "headers", headers );
                }

                try {
                    return ZWeb.readUrl(path, headers, conTO, readTO);
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            }
            try {
                return ZFileSystem.read(args);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }

        static Object open(Object... args) {
            if (args.length == 0) return NIL;
            String path = String.valueOf(args[0]);
            if (path.toLowerCase().contains("db:")) {
                String dbLoc = path.replaceAll("[dD][bB]\\:", "");
                return new ZDataBase(dbLoc);
            }
            if (path.contains(":/") && path.startsWith("http")) {
                // web protocol
                return new ZWeb(path);
            }
            // file later...
            return ZFileSystem.open(args);
        }

        static Thread thread(Function anon, Object... args) {
            if (args.length == 0) {
                if (anon == null) return Thread.currentThread();
            }
            ZThread zt = null;
            if (anon != null) {
                zt = new ZThread(anon, args);
                zt.start();
            } else {
                if (args.length > 0 && args[0] instanceof Function) {
                    Function f = (Function) args[0];
                    args = ZTypes.shiftArgsLeft(args);
                    zt = new ZThread(f, args);
                    zt.start();
                }
            }
            if (zt == null) throw new UnsupportedOperationException("Can not create thread from these args!");

            return zt;
        }

        static boolean empty(Object o) {
            if (o == null) return true;
            if (o instanceof Collection) {
                return ((Collection) o).isEmpty();
            }
            if (o.getClass().isArray()) {
                return (0 == Array.getLength(o));
            }
            if (o instanceof CharSequence) {
                return ((CharSequence) o).length() == 0;
            }
            if (o instanceof Map) return ((Map) o).isEmpty();
            if (o instanceof Function.Nil) return true;
            if (o instanceof ZFileSystem.ZFile) {
                o = ((ZFileSystem.ZFile) o).file;
            }
            if (o instanceof File) {
                if (((File) o).exists()) {
                    return ((File) o).length() == 0;
                } else {
                    return true;
                }
            }
            return false;
        }

        static Number size(Object... args) {
            if (args.length == 0) return -1;
            Object o = args[0];
            if (o == null) return -1;
            if (o instanceof Collection) {
                return ((Collection) o).size();
            }
            if (o.getClass().isArray()) {
                return Array.getLength(o);
            }
            if (o instanceof ZRange) {
                return ((ZRange) o).size();
            }
            if (o instanceof CharSequence) {
                return ((CharSequence) o).length();
            }
            if (o instanceof Map) return ((Map) o).size();
            if (o instanceof Function.Nil) return 0;
            if (o instanceof ZFileSystem.ZFile) {
                o = ((ZFileSystem.ZFile) o).file;
            }
            if (o instanceof File) {
                if (((File) o).exists()) {
                    return ((File) o).length();
                } else {
                    return -1;
                }
            }
            if (o instanceof Number) {
                if (args.length > 1) {
                    Number n = ZNumber.integer(args[1]);
                    if (n != null && n.intValue() > 1) {
                        return ZTypes.string(o, n.intValue()).length();
                    }
                }
                return cardinality(o);
            }
            // fallback...
            throw new UnsupportedOperationException("Can not Computer Size for Object Type " + o.getClass());
        }

        static Number cardinality(Object o) {
            if (o == null) return 0;
            if (o instanceof Number) {
                ZNumber zn = new ZNumber(o);
                return zn._abs_();
            }
            return size(o);
        }

        static void print(boolean error, Object... f) {
            PrintStream ps = error ? System.err : System.out;
            if (f == null) {
                ps.println("null");
                return;
            }
            if (f.length == 0) {
                ps.println();
                return;
            }
            Object a = f[0];
            if (a == null) {
                ps.println("null");
                return;
            }
            if (a.getClass().isArray()) {
                a = new ZArray(a, false);
            }
            if (a instanceof Number) {
                a = new ZNumber(a);
            }
            ps.println(a);
        }

        static void println(Object... f) {
            print(false, f);
        }

        static void eprintln(Object... f) {
            print(true, f);
        }

        static void formatPrint(boolean error, Object... args) {
            if (args.length == 0) return;
            String format = String.valueOf(args[0]);
            args = ZTypes.shiftArgsLeft(args);
            try {
                if (error) {
                    System.err.printf(format, args);
                } else {
                    System.out.printf(format, args);
                }
            }catch (Exception e){
                if ( e instanceof java.util.IllegalFormatConversionException ){
                    // here, we should fix the formats, if need be
                    if ( e.getMessage().contains("d != ") ){
                        // here, change format...
                        format = format.replace("%d", "%s");
                    }

                    for ( int i = 0; i < args.length; i++ ){
                        if ( args[i] instanceof Real ){
                            args[i] = ((Number)args[i]).doubleValue();
                        }
                    } // try again
                    if (error) {
                        System.err.printf(format, args);
                    } else {
                        System.out.printf(format, args);
                    }
                }
            }
        }

        static void printf(Object... args) {
            formatPrint(false, args);
        }

        static void eprintf(Object... args) {
            formatPrint(true, args);
        }

        static Collection join(List<Function> anons, Object... args) {
            if (args.length == 0) return new ZList();
            if (args[0] == null || args.length < 2) return Collections.EMPTY_LIST;
            ZCollection initial;
            if (args[0] instanceof ZRange) {
                args[0] = ((ZRange) args[0]).yield().asList();
            }

            if (args[0].getClass().isArray()) {
                initial = new ZArray(args[0], false);
            } else if (args[0] instanceof ZCollection) {
                initial = (ZCollection) args[0];
            } else if (args[0] instanceof Iterable) {
                initial = new ZList((Iterable) args[0]);
            } else {
                throw new UnsupportedOperationException("Can not join non Collection!");
            }
            Collection[] cols = new Collection[args.length - 1];
            for (int i = 0; i < cols.length; i++) {
                int j = i + 1;
                if (args[j] instanceof ZRange) {
                    args[j] = ((ZRange) args[j]).yield().asList();
                }

                if (args[j].getClass().isArray()) {
                    cols[i] = new ZArray(args[j], false);
                } else if (args[j] instanceof ZCollection) {
                    cols[i] = (ZCollection) args[j];
                } else if (args[i] instanceof Iterable) {
                    cols[i] = new ZList((Iterable) args[j]);
                } else {
                    throw new UnsupportedOperationException("Can not join non Collection!");
                }
            }
            if (anons.isEmpty()) return initial.join(cols);
            Function predicate = TRUE;
            Function mapper = COLLECTOR_IDENTITY;
            for (Function f : anons) {
                if (f instanceof AnonymousFunctionInstance.PredicateLambda) {
                    predicate = f;
                }
                if (f instanceof AnonymousFunctionInstance.MapperLambda) {
                    mapper = f;
                }
            }
            if (mapper == COLLECTOR_IDENTITY) {
                return initial.join(predicate, cols);
            }
            return initial.join(predicate, mapper, cols);
        }



        static Map multiSet(Function anon, Object[] args) {
            if (args.length == 0) return new ZMap();
            Object a = args[0];
            if (args.length > 1) {
                a = args; // implicit set from args, no flattening
            }
            if (anon == null) {
                // this is clean multiset, list of (item_key, count)
                if (a instanceof Iterator) {
                    a = new BaseZCollection.ZIteratorWrapper((Iterator) a);
                }
                if (a instanceof Iterable) {
                    return new ZMap.MultiSet(BaseZCollection.multiSet((Iterable) a));
                }
                if (a.getClass().isArray()) {
                    return new ZMap.MultiSet(BaseZCollection.multiSet(new ZArray(a, false)));
                }
            }
            if (a instanceof Iterator) {
                a = new BaseZCollection.ZIteratorWrapper((Iterator) a);
            }
            if (a instanceof Iterable) {
                return BaseZCollection.multiSet(anon, (Iterable) a);
            }
            if (a instanceof Map) {
                return BaseZCollection.multiSet(anon, ((Map) a).entrySet());
            }
            if (a.getClass().isArray()) {
                return BaseZCollection.multiSet(anon, new ZArray(a, false));
            }
            throw new UnsupportedOperationException("Type can not be converted to a multi set!");
        }

        static Map groupBy(List<Function> anons, Object[] args) {
            if (args.length == 0) return Collections.EMPTY_MAP;
            if (anons.size() == 0) return multiSet(null, args);
            if (anons.size() == 1) return multiSet(anons.get(0), args);
            Object a = args[0];
            if (args.length > 1) {
                a = args; // implicit set from args, no flattening
            }
            if (a instanceof Iterator) {
                a = new BaseZCollection.ZIteratorWrapper((Iterator) a);
            }
            if (a instanceof Iterable) {
                return BaseZCollection.groupBy(anons.get(0), anons.get(1), (Iterable) a);
            }
            if (a instanceof Map) {
                return BaseZCollection.groupBy(anons.get(0), anons.get(1), ((Map) a).entrySet());
            }
            if (a.getClass().isArray()) {
                return BaseZCollection.groupBy(anons.get(0), anons.get(1), new ZArray(a, false));
            }
            throw new UnsupportedOperationException("Type can not be converted to a multi set!");
        }

        static Collection select(Object[] args, Function anon, List<Function> anons) {
            if (anons.size() > 1) {
                return (Collection) from(anons, args);
            }
            if (args.length == 0) return Collections.EMPTY_LIST;
            Object a = args[0];
            if (a == null || anon == null) return Collections.EMPTY_LIST;
            Collection collector;
            boolean collectorSpecified = false;
            if (args.length > 1 && args[1] instanceof Collection) {
                collector = (Collection) args[1];
                collectorSpecified = true;
            } else {
                collector = new ZList();
            }

            if (a instanceof ZRange) {
                a = ((ZRange) a).asList();
            }
            if (a instanceof Iterable) {
                if (a instanceof Set) {
                    if (!collectorSpecified) {
                        collector = new ZSet();
                    }
                    return BaseZCollection.select(collector, anon, (Set) a);
                }
                return BaseZCollection.select(collector, anon, (Iterable) a);
            }
            if (a instanceof Map) {
                return BaseZCollection.select(collector, anon, ((Map) a).entrySet());
            }
            if (a.getClass().isArray()) {
                return BaseZCollection.select(collector, anon, new ZArray(a, false));
            }
            throw new UnsupportedOperationException("Type can not be converted to a collection!");
        }

        public static Collection[] EMPTY_PARTITIONS = new Collection[]{
                Collections.EMPTY_LIST, Collections.EMPTY_LIST};

        static Collection[] partition(Function anon, Object[] args) {
            if (args.length == 0) return EMPTY_PARTITIONS;
            Object a = args[0];
            if (args.length > 1) {
                a = args; // implicit set from args, no flattening
            }
            if (a == null || anon == null) return EMPTY_PARTITIONS;

            if (a instanceof ZRange) {
                a = ((ZRange) a).asList();
            }
            if (a instanceof Iterable) {
                if (a instanceof Set) {
                    return BaseZCollection.partition(new Collection[]{new ZSet(), new ZSet()},
                            anon, (Iterable) a);
                }
                return BaseZCollection.partition(new Collection[]{new ZList(), new ZList()},
                        anon, (Iterable) a);
            }
            if (a instanceof Map) {
                return BaseZCollection.partition(new Collection[]{new ZList(), new ZList()},
                        anon, ((Map) a).entrySet());
            }
            if (a.getClass().isArray()) {
                return BaseZCollection.partition(new Collection[]{new ZList(), new ZList()},
                        anon, new ZArray(a, false));
            }
            throw new UnsupportedOperationException("Type can not be converted to a collection!");
        }

        static Object leftReduce(Object a, Function anon) {
            if (a == null || anon == null) return NIL;

            if (a instanceof ZRange) {
                a = ((ZRange) a).asList();
            }
            if (a instanceof Iterator) {
                a = BaseZCollection.fromIterator((Iterator) a);
            }
            if (a instanceof Iterable) {
                return BaseZCollection.leftReduce((Iterable) a, anon);
            }
            if (a instanceof Map) {
                return BaseZCollection.leftReduce(((Map) a).entrySet(), anon);
            }
            if (a.getClass().isArray()) {
                return BaseZCollection.leftReduce(new ZArray(a, false), anon);
            }
            throw new UnsupportedOperationException("Type can not be converted to a collection!");
        }

        static Object leftFold(Object a, Function anon, Object... args) {
            if (a == null || anon == null) return NIL;

            if (a instanceof ZRange) {
                a = ((ZRange) a).asList();
            }
            if (a instanceof Iterator) {
                a = BaseZCollection.fromIterator((Iterator) a);
            }
            if (a instanceof Iterable) {
                return BaseZCollection.leftFold((Iterable) a, anon, args);
            }
            if (a instanceof Map) {
                return BaseZCollection.leftFold(((Map) a).entrySet(), anon, args);
            }
            if (a.getClass().isArray()) {
                return BaseZCollection.leftFold(new ZArray(a, false), anon, args);
            }
            throw new UnsupportedOperationException("Type can not be converted to a collection!");
        }

        static Object rightFold(Object a, Function anon, Object... args) {
            if (a == null || anon == null) return NIL;

            if (a instanceof ZRange) {
                a = ((ZRange) a).asList();
            }
            if (a instanceof List) {
                return BaseZCollection.rightFold((List) a, anon, args);
            }

            if (a.getClass().isArray()) {
                return BaseZCollection.rightFold(new ZArray(a, false), anon, args);
            }
            throw new UnsupportedOperationException("Type can not be converted to a List!");
        }

        static Object rightReduce(Object a, Function anon) {
            if (a == null || anon == null) return NIL;

            if (a instanceof ZRange) {
                a = ((ZRange) a).asList();
            }
            if (a instanceof List) {
                return BaseZCollection.rightReduce((List) a, anon);
            }

            if (a.getClass().isArray()) {
                return BaseZCollection.rightReduce(new ZArray(a, false), anon);
            }
            throw new UnsupportedOperationException("Type can not be converted to a List!");
        }

        static Function.MonadicContainer find(Object a, Function anon) {
            if (a == null || anon == null) return NOTHING;
            if (a instanceof ZRange) {
                a = ((ZRange) a).asList();
            }
            if (a instanceof Iterator) {
                a = BaseZCollection.fromIterator((Iterator) a);
            }
            if (a instanceof Iterable) {
                return BaseZCollection.find((Iterable) a, anon);
            }
            if (a instanceof Map) {
                return BaseZCollection.find(((Map) a).entrySet(), anon);
            }
            if (a.getClass().isArray()) {
                return BaseZCollection.find(new ZArray(a, false), anon);
            }
            throw new UnsupportedOperationException("Type can not be converted to a collection!");
        }

        static int leftIndex(Object a, Function anon, Object... args) {
            if (a == null) return -1;
            if (anon == null) {
                if (a instanceof CharSequence) {
                    return a.toString().indexOf(String.valueOf(args[0]));
                }
                if (a instanceof Iterable) {
                    return BaseZCollection.leftIndex((Iterable) a, args[0]);
                }
                if (a.getClass().isArray()) {
                    return BaseZCollection.leftIndex(new ZArray(a, false), args[0]);
                }
            }
            if (a instanceof ZRange) {
                a = ((ZRange) a).asList();
            }
            if (a instanceof Iterator) {
                a = BaseZCollection.fromIterator((Iterator) a);
            }
            if (a instanceof Iterable) {
                return BaseZCollection.leftIndex((Iterable) a, anon);
            }
            if (a instanceof Map) {
                return BaseZCollection.leftIndex(((Map) a).entrySet(), anon);
            }
            if (a instanceof CharSequence) {
                a = a.toString().toCharArray();
            }

            if (a.getClass().isArray()) {
                return BaseZCollection.leftIndex(new ZArray(a, false), anon);
            }
            throw new UnsupportedOperationException("Type can not be converted to a collection!");
        }

        static int rightIndex(Object a, Function anon, Object... args) {
            if (a == null) return -1;
            if (anon == null) {
                if (a instanceof CharSequence) {
                    return a.toString().lastIndexOf(String.valueOf(args[0]));
                }
                if (a instanceof List) {
                    return BaseZCollection.rightIndex((List) a, args[0]);
                }
                if (a.getClass().isArray()) {
                    return BaseZCollection.rightIndex(new ZArray(a, false), args[0]);
                }
            }
            if (a instanceof ZRange) {
                a = ((ZRange) a).asList();
            }
            if (a instanceof List) {
                return BaseZCollection.rightIndex((List) a, anon);
            }
            if (a instanceof CharSequence) {
                a = a.toString().toCharArray();
            }
            if (a.getClass().isArray()) {
                return BaseZCollection.rightIndex(new ZArray(a, false), anon);
            }
            throw new UnsupportedOperationException("Type can not be converted to a List!");
        }

        public static Object[] namedArgs(ZInterpret interpret, Object[] args) {
            if (args.length > 0 && args[0] instanceof ASTArgDef) {
                NamedArgs namedArgs = new NamedArgs();
                for (int i = 0; i < args.length; i++) {
                    ASTArgDef a = (ASTArgDef) args[i];
                    String name = ((ZoombaNode) a.jjtGetChild(0)).image;
                    Object value = a.jjtGetChild(1).jjtAccept(interpret, null);
                    namedArgs.put(name, value);
                }
                return new Object[]{namedArgs};
            }
            return args;
        }

        @Override
        public Function.MonadicContainer intercept(Object context, ZInterpret interpret, String methodName,
                                                   Object[] args, List<Function> anons, ZoombaNode callNode) {
            Function anon = anons.isEmpty() ? null : anons.get(0);
            // create named args, when necessary
            args = namedArgs(interpret, args);
            switch (methodName) {
                case EMPTY:
                    return new Function.MonadicContainerBase(empty(args[0]));
                case SIZE:
                    return new Function.MonadicContainerBase(size(args));
                case PRINTF:
                    printf(args);
                    return Function.Void;
                case PRINTLN:
                    println(args);
                    return Function.Void;
                case EPRINTF:
                    eprintf(args);
                    return Function.Void;
                case EPRINTLN:
                    eprintln(args);
                    return Function.Void;
                case LIST:
                    return new Function.MonadicContainerBase(ZList.list(anon, args));
                case SET:
                    return new Function.MonadicContainerBase(ZSet.set(anon, args));
                case SORTED_SET:
                    return new Function.MonadicContainerBase(ZSet.sortedSet(anons, args));
                case ORDERED_SET:
                    return new Function.MonadicContainerBase(ZSet.orderedSet(anon, args));
                case DICT:
                    return new Function.MonadicContainerBase(ZMap.dict(anon, args));
                case SORTED_DICT:
                    return new Function.MonadicContainerBase(ZMap.sortedDict(anons, args));
                case ORDERED_DICT:
                    return new Function.MonadicContainerBase(ZMap.orderedDict(anon, args));
                case SELECT:
                    return new Function.MonadicContainerBase(select(args, anon, anons));
                case FOLD: // it is boring to do lfold all the time, default should be fold <-> lfold
                case LFOLD:
                    Object o = args[0];
                    Object[] others = ZTypes.shiftArgsLeft(args);
                    return new Function.MonadicContainerBase(leftFold(o, anon, others));
                case BOOL:
                    return new Function.MonadicContainerBase(bool(args));
                case INT:
                    return new Function.MonadicContainerBase(ZNumber.integer(args));
                case FLOAT:
                    return new Function.MonadicContainerBase(ZNumber.floating(args));
                case INT_L:
                    return new Function.MonadicContainerBase(ZNumber.INT(args));
                case FLOAT_L:
                    return new Function.MonadicContainerBase(ZNumber.FLOAT(args));
                case NUM:
                    return new Function.MonadicContainerBase(ZNumber.number(args));
                case TIME:
                    return new Function.MonadicContainerBase(ZDate.time(args));
                case FIND:
                    if (args.length == 0) return Function.FAILURE;
                    return new Function.MonadicContainerBase(find(args[0], anon));
                case EXISTS:
                    o = args[0];
                    others = ZTypes.shiftArgsLeft(args);
                    int i = leftIndex(o, anon, others);
                    if (i >= 0) return Function.SUCCESS;
                    return Function.FAILURE;
                case LINDEX:
                    o = args[0];
                    others = ZTypes.shiftArgsLeft(args);
                    i = leftIndex(o, anon, others);
                    return new Function.MonadicContainerBase(i);

                case LREDUCE:
                    if (args.length == 0) return Function.NOTHING;
                    o = leftReduce(args[0], anon);
                    return new Function.MonadicContainerBase(o);

                case RREDUCE:
                    if (args.length == 0) return Function.NOTHING;
                    o = rightReduce(args[0], anon);
                    return new Function.MonadicContainerBase(o);

                case PARTITION:
                    return new Function.MonadicContainerBase(partition(anon, args));
                case MSET:
                    return new Function.MonadicContainerBase(multiSet(anon, args));
                case GROUP_BY:
                    return new Function.MonadicContainerBase(groupBy(anons, args));
                case STRING:
                    if (anon != null && args.length > 1) {
                        return new Function.MonadicContainerBase(
                                ZTypes.string(anon, args[0], String.valueOf(args[1])));
                    }
                    return new Function.MonadicContainerBase(ZTypes.string(args));
                case JSON_STRING:
                    if (args.length != 1) {
                        return new Function.MonadicContainerBase(ZTypes.jsonPretty(args));
                    }
                    return new Function.MonadicContainerBase(ZTypes.jsonString(args[0]));
                case JSON:
                    return new Function.MonadicContainerBase(ZTypes.json(args));
                case OPEN:
                    return new Function.MonadicContainerBase(open(args));
                case RFOLD:
                    o = args[0];
                    others = ZTypes.shiftArgsLeft(args);
                    return new Function.MonadicContainerBase(rightFold(o, anon, others));
                case RINDEX:
                    o = args[0];
                    others = ZTypes.shiftArgsLeft(args);
                    i = rightIndex(o, anon, others);
                    return new Function.MonadicContainerBase(i);
                case JOIN:
                    return new Function.MonadicContainerBase(join(anons, args));
                case FROM:
                    return new Function.MonadicContainerBase(from(anons, args));
                case ROUND:
                    Number num = (Number) args[0];
                    args = ZTypes.shiftArgsLeft(args);
                    return new Function.MonadicContainerBase(ZNumber.round(num, args));
                case FLOOR:
                    return new Function.MonadicContainerBase(ZNumber.floor((Number) args[0]));
                case CEIL:
                    return new Function.MonadicContainerBase(ZNumber.ceil((Number) args[0]));
                case LOG:
                    return new Function.MonadicContainerBase(ZNumber.log(args));
                case THREAD:
                    return new Function.MonadicContainerBase(thread(anon, args));
                case FILE:
                    return new Function.MonadicContainerBase( args.length == 0 ? Collections.emptyList() :
                            ZFileSystem.file(args));
                case READ:
                    return new Function.MonadicContainerBase(read(args));
                case WRITE:
                    return new Function.MonadicContainerBase(ZFileSystem.write(args));
                case XML:
                    return new Function.MonadicContainerBase(ZXml.xml(args));
                case RANDOM:
                    return new Function.MonadicContainerBase(ZRandom.random(args));
                case SHUFFLE:
                    return new Function.MonadicContainerBase(ZRandom.shuffle(args[0]));
                case SORTA:
                    return new Function.MonadicContainerBase(ZRandom.sortAscending(args, anon));
                case SORTD:
                    return new Function.MonadicContainerBase(ZRandom.sortDescending(args, anon));
                case SUM:
                    return new Function.MonadicContainerBase(ZRandom.sum(anon, args));
                case MINMAX:
                    if (args.length == 0) return Function.FAILURE;
                    return new Function.MonadicContainerBase(ZRandom.minMax(anons, args));
                case SYSTEM:
                    return new Function.MonadicContainerBase(ZProcess.system(args));
                case POPEN:
                    return new Function.MonadicContainerBase(ZProcess.popen(args));
                case MATRIX:
                    return new Function.MonadicContainerBase(ZMatrix.Loader.load(args));
                case ENUM:
                    return new Function.MonadicContainerBase(ZTypes.enumCast(args));
                case BYE:
                    ZTypes.bye(args);
                    // unreachable code for sure
                    return Function.SUCCESS;
                case RAISE:
                    ZTypes.raise(args);
                    // unreachable
                    break;
                case HEAP:
                    return new Function.MonadicContainerBase(ZHeap.heap(anon, args));
                case LOAD:
                    return ZTypes.loadJar(args);

                case HASH:
                    return new Function.MonadicContainerBase(ZRandom.hash(args));
                case TOKENS:
                    return new Function.MonadicContainerBase(ZRandom.tokens(anons, args));
                case SEQUENCE:
                    return new Function.MonadicContainerBase(ZSequence.BaseSequence.sequence(anon, args));
                case POLL:
                    return new Function.MonadicContainerBase(ZThread.poll(anon, args));
                case ASYNC:
                    if (args.length != 0 && args[0] instanceof Function) {
                        return new Function.MonadicContainerBase(ZThread.async(anon, (Function) args[0]));
                    }
                    return Function.FAILURE;
                case PERMUTATION:
                    return new Function.MonadicContainerBase(Combinatorics.permutations(args));
                case COMBINATION:
                    return new Function.MonadicContainerBase(Combinatorics.combinations(args));
                case SUB_SEQUENCES:
                    return new Function.MonadicContainerBase(new Combinatorics.Sequences(args));
                case TUPLE:
                    return new Function.MonadicContainerBase(ZMatrix.Tuple.tuple(args));
                case YAML:
                    return new Function.MonadicContainerBase(ZTypes.yaml(args));
                case YSTR:
                    return new Function.MonadicContainerBase(ZTypes.yamlString(args));
                case SIGN:
                    return new Function.MonadicContainerBase(ZNumber.sign(args));
                case PRIORITY_QUEUE:
                    return new Function.MonadicContainerBase(ZTypes.pqueue(anon, args));

                case BINARY_SEARCH:
                    return new Function.MonadicContainerBase(ZRandom.binarySearch(args, anon));
                case JX_PATH:
                    return new Function.MonadicContainerBase(jxPath(args));
                case JX_ELEMENT:
                    return new Function.MonadicContainerBase(jxElement(args));

            }
            return UNSUCCESSFUL_INTERCEPT;
        }
    }
}
