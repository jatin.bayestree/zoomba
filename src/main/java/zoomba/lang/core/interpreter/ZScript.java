/*
 * Copyright 2017 zoomba-lang.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package zoomba.lang.core.interpreter;

import zoomba.lang.core.oop.ZObject;
import zoomba.lang.core.operations.Arithmetic;
import zoomba.lang.core.operations.Function;
import zoomba.lang.core.types.ZException;
import zoomba.lang.parser.*;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Consumer;
import java.util.regex.Pattern;
import zoomba.lang.core.interpreter.ZContext.*;

/**
 * Implementation of a Function ZoomBA script wise
 */
public class ZScript implements Function, Externalizable {

    /**
     * Defines if a name can be Global
     * @param s name of the variable
     * @return true if it is, false if it is not
     */
    public static boolean IS_GLOBAL_VAR(String s){
        return s.length() > 1 && s.charAt(0) == '$' ;
    }

    public static final String MAIN_SCRIPT = "__ZS__" ;

    public static final Pattern CURRY_PATTERN = Pattern.compile("#\\{(?<p>[^\\{]+)\\}" , Pattern.MULTILINE );

    public final Map<String,Object> globals = new ConcurrentHashMap<>();

    ZAssertion assertion;

    public ZAssertion assertion(){ return assertion; }

    public boolean isMain(){ return parent == null ;}

    protected boolean lenient = true ;

    /**
     * Should we error out on Ambiguous operations?
     * @return true/false
     */
    public boolean lenient(){
        return lenient ;
    }

    /**
     * Should we error out on Ambiguous operations?
     * @param  leniency true/false
     */
    public void lenient(boolean leniency){
        lenient = leniency ;
    }

    /**
     * Read from a reader into a local buffer and return a String with
     * the contents of the reader.
     *
     * @param scriptReader to be read.
     * @return the contents of the reader as a String.
     * @throws IOException on any error reading the reader.
     */
    public static String readerToString(Reader scriptReader) throws IOException {
        StringBuilder buffer = new StringBuilder();
        BufferedReader reader;
        if (scriptReader instanceof BufferedReader) {
            reader = (BufferedReader) scriptReader;
        } else {
            reader = new BufferedReader(scriptReader);
        }
        try {
            String line;
            while ((line = reader.readLine()) != null) {
                buffer.append(line).append('\n');
            }
            return buffer.toString();
        } finally {
            try {
                reader.close();
            } catch (IOException xio) {
                // ignore
            }
        }

    }

    /**
     * Trims the expression from front and ending spaces.
     *
     * @param str expression to clean
     * @return trimmed expression ending in a semi-colon
     */
    public static String cleanExpression(CharSequence str) {
        if (str != null) {
            int start = 0;
            int end = str.length();
            if (end > 0) {
                // trim front spaces
                while (start < end && str.charAt(start) == ' ') {
                    ++start;
                }
                // trim ending spaces
                while (end > 0 && str.charAt(end - 1) == ' ') {
                    --end;
                }
                return str.subSequence(start, end).toString();
            }
            return "";
        }
        return null;
    }


    protected final Parser parser = new Parser(new StringReader(";")); //$NON-NLS-1$

    protected String body;

    @Override
    public String body() {
        return body ; // later
    }

    @Override
    public String name() {
        return name ;
    }


    /**
     * The base directory of the script
     */
    private String baseDir;

    public String baseDir(){ return  baseDir; }

    private String location;

    /**
     * Location of the script
     * @return actual location of the script
     */
    public String location(){ return  location; }

    /**
     * The parent of this script
     */
    private ZScript parent;

    public ZScript parent(){ return parent; }

    /**
     * Name of the script
     */
    protected String name;

    /**
     * The script DOM under the hood
     */
    protected ASTZoombaScript script ;

    private void init(String text){
        try {
            baseDir = "." ;
            location = new File( System.getProperty("user.dir") + File.separator + "<inline>" ).getCanonicalPath() ;
            parent = null ;
            script = parse(text);
            script.sourceLocation(location);
            name = "__INLINE__" ;
            assertion = new ZAssertion();
            populateEntities(script);
            body = text ;
        }catch (Throwable e){
            throw new RuntimeException(e);
        }
    }

    public boolean inline(){
        return "__INLINE__".equals(name);
    }

    /**
     * One must never use this, ever.
     * This is only for Serialization
     */
    public ZScript(){}

    /**
     * From raw text, create a script
     * @param text the text string
     */
    public ZScript(String text)  {
        init(text);
    }

    /**
     * Create / Import a script from a file with a name for a prent
     * @param fileLocation location of the file
     * @param parent the parent script
     * @param as the import name
     */
    public ZScript(String fileLocation, ZScript parent, String as)  {
        File f = new File(fileLocation);
        try {
            // fix for one jar issues in loading
            String dir = f.getAbsoluteFile().getParent();
            location = f.getAbsolutePath();
            File d = new File(dir);
            baseDir = d.getCanonicalPath();
            String scriptText = readerToString( new FileReader(f) );
            script = parse(scriptText);
            script.sourceLocation(location);
            this.parent = parent;
            if ( parent == null ){
                assertion = new ZAssertion();
            }else{
                assertion = parent.assertion ;
            }
            name = as ;
            populateEntities(script);
            body = scriptText ;
        }catch (Throwable e){
            throw new RuntimeException(e);
        }
    }

    /**
     * Create a script from a location given parent
     * @param fileLocation the location
     * @param parent the parent
     */
    public ZScript(String fileLocation, ZScript parent){
        this(fileLocation,parent,MAIN_SCRIPT);
    }

    /**
     * The method hash name of the method with the method itself
     */
    protected Map<String,ZScriptMethod> methods;

    /**
     * The ZObject prototypes hash name of the prototype with the ZObject itself
     */
    protected Map<String,ZObject> protoTypes;

    /**
     * Given a name get the entity of the name
     * 1. Methods
     * 2. Classes
     * @param name of the entity in the script
     * @return the entity
     */
    public Object get(String name){
        if ( methods.containsKey( name ) ) return methods.get(name);
        if ( protoTypes.containsKey(name )) return protoTypes.get(name);
        if ( myThreadLocalContext.get().has(name) ){
            return myThreadLocalContext.get().get(name).value();
        }
        return NIL;
    }

    /**
     * Sets only existing variable values in a Script
     * @param name of the variable
     * @param value value of the variable
     * @return the value if it was set, or NIL if it was not
     */
    public Object set(String name, Object value){
        if ( myThreadLocalContext.get().has(name) ){
            myThreadLocalContext.get().set(name,value);
            return value;
        }
        return NIL;
    }

    /**
     * Populates the methods and Classes for a script from
     * This is necessary for back referencing.
     * One method defined later can be called from one method defined before.
     * @param script the script
     */
    protected void populateEntities(ASTZoombaScript script){
        methods = new HashMap<>();
        protoTypes = new HashMap<>();
        int num = script.jjtGetNumChildren();
        for ( int i = 0 ; i < num; i++ ){
            Node n = script.jjtGetChild(i);
            if ( n instanceof ASTMethodDef){
                ZScriptMethod method = new ZScriptMethod(this, n);
                methods.put( method.name , method );
            }
        }
    }

    /**
     * Parses an expression.
     *
     * @param expression the expression to parse
     * @return the parsed tree
     * @throws ZException if any error occured during parsing
     */
    protected ASTZoombaScript parse(CharSequence expression) {
        String expr = cleanExpression(expression);
        ASTZoombaScript script = null;
        synchronized (parser) {

            try {
                Reader reader = new StringReader(expr);
                // use first calling method of JexlEngine as debug info
                script = parser.parse(reader);
                // reaccess in case local variables have been declared

            } catch (TokenMgrError xtme) {
                throw new ZException.Tokenization(xtme, expression.toString());
            } catch (ParseException xparse) {
                throw new ZException.Parsing(xparse, expression.toString());
            } finally {

            }
        }
        return script;
    }

    private boolean debug = false ;

    /**
     * Unused debug flag
     * @return the debug flag
     */
    public boolean debug(){ return debug ; }

    /**
     * Sets the debug flag
     * @param debug the flag
     */
    public void debug(boolean debug){ this.debug = debug ; }

    /**
     * The external context
     */
    protected FunctionContext externalContext = null ;

    /**
     * Sets the external context
     * @param context external context
     */
    public void setExternalContext(ZContext.FunctionContext context){
        externalContext = context ;
    }

    /**
     * Gets the external context
     * @return the external context
     */
    public FunctionContext getExternalContext() {
        return externalContext;
    }

    private ThreadLocal<ZContext> myThreadLocalContext;

    private ThreadLocal<List<String>> trace = ThreadLocal.withInitial(ArrayList::new);

    public List<String> trace(){
        return trace.get();
    }

    public void consumeTrace(Consumer<String> consumer){
        trace.get().forEach(consumer);
    }

    @Override
    public MonadicContainer execute(Object...args){
        final ZContext functionContext;
        ArgContext argContext = new ArgContext(args) ;
        if ( externalContext == null ) {
            functionContext = new FunctionContext(ZContext.EMPTY_CONTEXT, argContext);
        }else{
            functionContext = externalContext ;
        }

        synchronized (this){
            myThreadLocalContext = ThreadLocal.withInitial(()-> functionContext );
        }
        // auto import all methods
        methods.entrySet().stream().forEach( entry -> functionContext.set(entry.getKey(), entry.getValue() ) );
        // proceed as is...
        ZInterpret current = new ZInterpret(this);
        current.prepareCall( functionContext );
        Object o = NIL;
        try {
            o = current.dance(script);
        }catch (Throwable t){
            o = t;
        }finally {
            trace.get().addAll(current.callStack);
            current.endCall();
            if ( externalContext == null ){
                // why ???
                //functionContext.clear();
                //argContext.clear();

            } /* else{
                System.gc();
            }

            if ( !(functionContext.parent() instanceof ZContext.EmptyContext) ){
                System.gc();
            }  */ // removed based on co-authors suggestion. No clue thought.
        }
        return new MonadicContainerBase(o);
    }

    @Override
    public void writeExternal(ObjectOutput out) throws IOException {
        out.writeObject(body);
    }

    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
       String text = (String)in.readObject();
       init(text);
    }

    @Override
    public String toString() {
        if ( inline() )  return body;
        return String.format("ZScript @ %s", location() );
    }
}
