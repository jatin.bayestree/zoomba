/*
 * Copyright 2017 zoomba-lang.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package zoomba.lang.core.collections;

import zoomba.lang.core.operations.Function;
import zoomba.lang.core.types.ZNumber;
import zoomba.lang.core.types.ZTypes;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;

/**
 * A Heap data Structure for small items,
 * Avoids the heapify procedure by sorting the items,
 * Thus making it unsuitable for large no of items in the heap
 */
public class ZHeap extends ZList {

    /**
     * Creates a heap using a comparator function and arguments
     * @param f the comparator function
     * @param args
     *       args[0] is size of the heap
     *       args[1] is the boolean flag to make it a min heap, if passed false, it is a max heap
     * @return a heap
     */
    public static ZHeap heap(Function f, Object...args){
        if ( args.length == 0 ) return new ZHeap(42); // waste...?
        if ( f == null ){
            if ( args.length == 1 )
                return new ZHeap(ZNumber.integer(args[0],42).intValue() );
            return new ZHeap( ZNumber.integer(args[0],42).intValue(), ZTypes.bool( args[1], false ) );
        }
        if ( args.length == 1 )
            return new ZHeap(ZNumber.integer(args[0],42).intValue(), false, f );

        return new ZHeap(ZNumber.integer(args[0],42).intValue(), ZTypes.bool( args[1], false ) , f );
    }

    final static class ComparatorNullAscending implements Comparator{
        @Override
        public int compare(Object o1, Object o2) {
            if ( o1 == null ){
                if ( o2 == null ) return 0;
                return -1;
            }
            if ( o2 == null ) return 1;
            if ( o1 instanceof Comparable ){
                return ((Comparable) o1).compareTo(o2);
            }
            if ( o2 instanceof Comparable ){
                return -1 * ((Comparable) o2).compareTo(o1);
            }
            throw new UnsupportedOperationException("Can not compare!");
        }

    }

    /**
     * Generic comparator capable of comparing null object against non null objects
     */
    public static ComparatorNullAscending ASC_COMP = new ComparatorNullAscending();

    /**
     * Heaps underlying comparator
     */
    public final Comparator comparator;

    /**
     * flag to see if it is a min heap or max heap
     */
    public final boolean min;

    /**
     * The size of the heap
     */
    public final int maxSize;

    /**
     * For almost sorted array with very small size less than 10ish ... this is faster!
     */
    protected void sort(){
        int len = size();
        if ( len <= 1 ) return;
        // TODO : ideal case would be doing insertion sort : one element would be off balance
        for( int i = 0 ; i < len ; i++ ){
            for  ( int j = i+1; j< len ; j++){
                Object I = get(i);
                Object J = get(j);
                if ( comparator.compare(I, J ) < 0 ){
                    set(j, I) ;
                    set(i, J) ;
                }
            }
        }
    }

    /**
     * Creates a new max heap
     * @param size with this value
     */
    public ZHeap(int size){
        this(size,false);
    }

    /**
     * Creates a new heap
     * @param size with value
     * @param minHeap true means min, false means max
     */
    public ZHeap(int size, boolean minHeap){
        this(size,minHeap,ASC_COMP);
    }

    /**
     * Creates a new heap
     * @param size with value
     * @param minHeap true means min, false means max
     * @param comparator  using this Comparator to compare elements
     */
    public ZHeap(int size, boolean minHeap, Comparator comparator){
        super();
        maxSize = size ;
        min = minHeap ;
        this.comparator = min ? comparator.reversed(): comparator ;
    }

    /**
     * Creates a new heap
     * @param size with value
     * @param minHeap true means min, false means max
     * @param f using this function to compare elements -1,0,1 or true/false
     */
    public ZHeap(int size, boolean minHeap, Function f){
        super();
        maxSize = size ;
        Comparator cmp = new Function.ComparatorLambda( this, f) ;
        min = minHeap ;
        this.comparator = min ? cmp.reversed(): cmp ;
    }

    /**
     * Gets the min value of the heap
     * @return min value
     */
    public  Object min(){
        return min? get(0) : get(size()-1);
    }

    /**
     * Gets the max value of the heap
     * @return max value
     */
    public  Object max(){
        return min? get(size()-1) : get(0) ;
    }

    /**
     * Gets the top element of the heap
     * @return top value
     */
    public Object top(){
        return get(size()-1);
    }

    @Override
    public Object get(int index) {
        try {
            return super.get(index);
        }catch (Exception e){
            return Function.NIL  ;
        }
    }

    @Override
    public boolean add(Object o) {
        if ( size() < maxSize ) {
            super.add(o);
            sort();
            return true ;
        }
        Object top = get(0);
        int cmp = comparator.compare( o, top ) ;
        if ( cmp >= 0 ) { return false ; }
        set(0,o);
        sort();
        return true ;
    }

    @Override
    public boolean remove(Object o) {
        if ( !contains(o) ) return false ;
        super.remove(o);
        sort();
        return true ;
    }

    @Override
    public boolean addAll(Collection c) {
        boolean ret = true ;
        for ( Object item : c ){
            ret &= this.add(item );
        }
        return ret ;
    }

    @Override
    public boolean removeAll(Collection c) {
        boolean ret = true ;
        for ( Object item : c ){
            ret &= this.remove(item );
        }
        return ret ;
    }


    @Override
    public String toString() {
        return String.format("h<%s|%s>",top(), super.toString());
    }
}
