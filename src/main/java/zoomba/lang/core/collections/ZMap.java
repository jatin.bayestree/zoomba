/*
 * Copyright 2017 zoomba-lang.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package zoomba.lang.core.collections;
import zoomba.lang.core.interpreter.AnonymousFunctionInstance;
import zoomba.lang.core.operations.Arithmetic;
import zoomba.lang.core.operations.Function;
import zoomba.lang.core.operations.ZCollection;
import zoomba.lang.core.operations.ZJVMAccess;
import zoomba.lang.core.types.ZRange;
import zoomba.lang.core.types.ZTypes;
import java.util.*;

/**
 * A Map class implementing various ZoomBA protocols to make it inherently useful
 */
public class ZMap implements SortedMap, NavigableMap, Arithmetic.BasicArithmeticAware, Arithmetic.LogicAware, ZTypes.StringX {

    private static final ZMap EMPTY = new ZMap( Collections.EMPTY_MAP );

    /**
     * An Implementation of the Entry interface
     */
    public static class Pair extends ZArray implements Entry {

        /**
         * Creates a pair from two objects
         * @param k key object
         * @param v value object
         */
        public Pair(Object k, Object v){
            super( new Object[ ]{ k,v } );
        }

        /**
         * Creates a pair from an Entry
         * @param e the entry
         */
        public Pair(Entry e){
            this(e.getKey(), e.getValue());
        }

        /**
         * From an array/list like structure creates a pair
         * @param p the array object
         */
        public Pair(Object p){
            super(p);
        }

        @Override
        public Object getKey() {
            return get(0);
        }

        @Override
        public Object getValue() {
            return get(1);
        }

        @Override
        public Object setValue(Object value) {
            return set(1,value);
        }
    }

    /**
     * Creates a map from a Collection of Entries
     * @param m the collector map
     * @param c the collection of entries
     * @return the m, collector map
     */
    private static Map fromEntries(Map m, Collection c){
        Iterator iterator = c.iterator();
        while ( iterator.hasNext() ){
            Object o = iterator.next();
            if ( o == null )
                throw new UnsupportedOperationException("Map entry can not be null!");
            if ( o instanceof Map.Entry ){
                m.put( ((Map.Entry) o).getKey(), ((Map.Entry) o).getValue() );
                continue;
            }
            if ( o.getClass().isArray() ){
                o = new ZArray(o,false);
            }
            if ( o instanceof List ){
                List l = (List)o;
                if ( l.size() < 2 ) throw new UnsupportedOperationException("Map entry can not be less than 2!");
                m.put(l.get(0) ,l.get(1));
            }
        }
        return m;
    }

    /**
     * Create a map by joining two collections, item by item
     * first collection is the key list, second is the value list
     * @param c1 key collection
     * @param c2 value collection
     * @return a map combining [ (key,value) ]
     */
    private static Map joinEntries(Map m, Collection c1, Collection c2){
        Iterator iterator1 = c1.iterator();
        Iterator iterator2 = c2.iterator();

        while ( iterator1.hasNext() ){
            Object k = iterator1.next();
            if ( !iterator2.hasNext() ) {
                m.clear();
                throw new UnsupportedOperationException("Key,Value collection size mismatch!");
            }
            Object v = iterator2.next();
            m.put(k,v);
        }
        if ( iterator2.hasNext() ){
            m.clear();
            throw new UnsupportedOperationException("Key,Value collection size mismatch!");
        }
        return m;
    }

    /**
     * Creates a dictionary
     * @param f using function
     * @param args using arguments
     * @return the dictionary or map
     */
    public static Map dict(Function f, Object...args){
        if (args.length == 0 ) return new ZMap();
        if ( args[0] instanceof Map && f == null ){
            if ( args.length == 2 &&  ZTypes.bool(args[1],false)  ){
                return new ZMap( new HashMap( (Map)args[0]) );
            }
            if ( args[0] instanceof ZMap ){
                return new ZMap( ((ZMap)args[0]).map );
            }
            return new ZMap( (Map)args[0] );
        }
        if ( f != null ){
            Object o = args[0];
            if ( o == null ) return EMPTY ;
            if ( o.getClass().isArray() ){
                o = new ZArray(o,false);
            }
            if ( o instanceof ZRange ){
                o = ((ZRange) o).asList();
            }
            else if ( o instanceof Map ){
                o = ((Map)o).entrySet();
            }
            if ( !(o instanceof Iterable) ){
                throw new UnsupportedOperationException("Can not covert to Map!");
            }
            return BaseZCollection.dict(new ZMap(), f, (Iterable)o );

        }
        if ( args.length == 1 ){
            if ( args[0].getClass().isArray() ){
                return new ZMap(args[0]) ;
            }
            if ( args[0] instanceof Collection ){
                return new ZMap((Collection)args[0]);
            }
            return ZJVMAccess.dict(args[0]);
        }
        return new ZMap(args[0],args[1]);
    }

    /**
     * Creates an ordered dictionary - where entries remain in the order of inclusion
     * @param f using function
     * @param args using arguments
     * @return the ordered dictionary or map
     */
    public static Map orderedDict(Function f, Object...args){
        if (args.length == 0 ) return new ZMap( new LinkedHashMap() );
        if ( args[0] instanceof Map && f == null ){
            if ( args[0] instanceof ZMap ){
                return new ZMap( new LinkedHashMap( ((ZMap)args[0]).map ) );
            }
            return new ZMap( new LinkedHashMap( (Map)args[0] ) );
        }
        if ( f != null ){
            Object o = args[0];
            if ( o == null ) return EMPTY ;
            if ( o.getClass().isArray() ){
                o = new ZArray(o,false);
            }
            if ( o instanceof ZRange ){
                o = ((ZRange) o).asList();
            }
            else if ( o instanceof Map ){
                o = ((Map)o).entrySet();
            }
            if ( !(o instanceof Iterable) ){
                throw new UnsupportedOperationException("Can not covert to Sorted Map!");
            }
            return BaseZCollection.dict(new ZMap( new LinkedHashMap() ), f, (Iterable)o );
        }

        if ( args[0] instanceof Collection ){
            if ( args.length == 1 ) {
                return new ZMap(fromEntries(new LinkedHashMap(), (Collection) args[0]));
            }
            if ( args[1] instanceof Collection ){
                return joinEntries( new LinkedHashMap(), (Collection)args[0], (Collection)args[1] );
            }
        }
        // now - go bonkers
        throw new UnsupportedOperationException("Can not covert to Ordered Map, Provide a collection of pairs!");
    }

    /**
     * Creates an sorted dictionary - where keys are sorted
     * @param anons using functions list
     *              anons can be either empty or null
     *              if it has only one entry, then use this as the mapper
     *              if it is two one is the mapper, another is the comparator
     *              The type of the function matters
     * @param args using arguments
     * @return the ordered dictionary or map
     */
    public static Map sortedDict(List<Function> anons, Object...args){
        if (args.length == 0 ) return new ZMap( new TreeMap() );
        if ( args.length == 1 && args[0] instanceof Comparator ){
            return new ZMap( new TreeMap( (Comparator)args[0] ) ) ;
        }
        if ( args[0] instanceof Map && anons.isEmpty() ){
            if ( args[0] instanceof ZMap ){
                return new ZMap( new TreeMap( ((ZMap)args[0]).map ) );
            }
            return new ZMap( new TreeMap( (Map)args[0] ) );
        }
        if ( anons.isEmpty() && args[0] instanceof Collection ){
            if ( args.length == 1 ){
                return fromEntries( new TreeMap( Arithmetic.INSTANCE ), (Collection)args[0]);
            }
            if ( args[1] instanceof Collection ){
                return joinEntries( new TreeMap( Arithmetic.INSTANCE ), (Collection)args[0], (Collection)args[1]);
            }
        }

        Function f =  Function.COLLECTOR_IDENTITY ;
        Comparator cmp = null ;
        TreeMap seed ;
        for ( Function function : anons ){
            if ( function instanceof AnonymousFunctionInstance.PredicateLambda ){
                cmp = (Comparator) function ;
            } else {
                f  = function ;
            }
        }
        if ( cmp != null ){
            seed = new TreeMap(cmp);
        } else {
            seed = new TreeMap( Arithmetic.INSTANCE );
        }
        Object o = args[0];
        if ( o == null ) return EMPTY ;
        if ( o.getClass().isArray() ){
            o = new ZArray(o,false);
        }
        if ( o instanceof ZRange ){
            o = ((ZRange) o).asList();
        }
        else if ( o instanceof Map ){
            o = ((Map)o).entrySet();
        }
        if ( !(o instanceof Iterable) ){
            throw new UnsupportedOperationException("Can not covert to Sorted Map!");
        }
        return BaseZCollection.dict(new ZMap( seed ), f, (Iterable)o );
    }

    protected Map map;

    @Override
    public Comparator comparator() {
        if ( map instanceof SortedMap ){ return ((SortedMap)map).comparator() ; }
        return null;
    }

    @Override
    public SortedMap subMap(Object fromKey, Object toKey) {
        if ( map instanceof SortedMap ){ return ((SortedMap)map).subMap(fromKey, toKey ) ; }
        return EMPTY ;
    }

    @Override
    public SortedMap headMap(Object toKey) {
        if ( map instanceof SortedMap ){ return ((SortedMap)map).headMap(toKey ) ; }
        return EMPTY;
    }

    @Override
    public SortedMap tailMap(Object fromKey) {
        if ( map instanceof SortedMap ){ return ((SortedMap)map).tailMap( fromKey ) ; }
        return EMPTY;
    }

    @Override
    public Object firstKey() {
        if ( map instanceof SortedMap ){ return ((SortedMap)map).firstKey() ; }
        return Function.NIL ;
    }

    @Override
    public Object lastKey() {
        if ( map instanceof SortedMap ){ return ((SortedMap)map).lastKey() ; }
        return Function.NIL ;
    }

    @Override
    public Entry lowerEntry(Object key) {
        if ( map instanceof NavigableMap ){ return ((NavigableMap)map).lowerEntry(key) ; }
        return null;
    }

    @Override
    public Object lowerKey(Object key) {
        if ( map instanceof NavigableMap ){ return ((NavigableMap)map).lowerKey(key) ; }
        return Function.NIL;
    }

    @Override
    public Entry floorEntry(Object key) {
        if ( map instanceof NavigableMap ){ return ((NavigableMap)map).floorEntry(key) ; }
        return null;
    }

    @Override
    public Object floorKey(Object key) {
        if ( map instanceof NavigableMap ){ return ((NavigableMap)map).floorKey(key) ; }
        return Function.NIL;
    }

    @Override
    public Entry ceilingEntry(Object key) {
        if ( map instanceof NavigableMap ){ return ((NavigableMap)map).ceilingEntry(key) ; }
        return null;
    }

    @Override
    public Object ceilingKey(Object key) {
        if ( map instanceof NavigableMap ){ return ((NavigableMap)map).ceilingKey(key) ; }
        return Function.NIL;
    }

    @Override
    public Entry higherEntry(Object key) {
        if ( map instanceof NavigableMap ){ return ((NavigableMap)map).higherEntry(key) ; }
        return null;
    }

    @Override
    public Object higherKey(Object key) {
        if ( map instanceof NavigableMap ){ return ((NavigableMap)map).higherKey(key) ; }
        return Function.NIL;
    }

    @Override
    public Entry firstEntry() {
        if ( map instanceof NavigableMap ){ return ((NavigableMap)map).firstEntry() ; }
        return null;
    }

    @Override
    public Entry lastEntry() {
        if ( map instanceof NavigableMap ){ return ((NavigableMap)map).lastEntry() ; }
        return null;
    }

    @Override
    public Entry pollFirstEntry() {
        if ( map instanceof NavigableMap ){ return ((NavigableMap)map).pollFirstEntry() ; }
        return null;
    }

    @Override
    public Entry pollLastEntry() {
        if ( map instanceof NavigableMap ){ return ((NavigableMap)map).pollLastEntry() ; }
        return null;
    }

    @Override
    public NavigableMap descendingMap() {
        if ( map instanceof NavigableMap ){ return ((NavigableMap)map).descendingMap() ; }
        return EMPTY;
    }

    @Override
    public NavigableSet navigableKeySet() {
        if ( map instanceof NavigableMap ){ return ((NavigableMap)map).navigableKeySet() ; }
        return ZSet.EMPTY;
    }

    @Override
    public NavigableSet descendingKeySet() {
        if ( map instanceof NavigableMap ){ return ((NavigableMap)map).descendingKeySet() ; }
        return ZSet.EMPTY;
    }

    @Override
    public NavigableMap subMap(Object fromKey, boolean fromInclusive, Object toKey, boolean toInclusive) {
        if ( map instanceof NavigableMap ){
            return ((NavigableMap)map).subMap(fromKey,fromInclusive,toKey,toInclusive) ;
        }
        return EMPTY;
    }

    @Override
    public NavigableMap headMap(Object toKey, boolean inclusive) {
        if ( map instanceof NavigableMap ){
            return ((NavigableMap)map).headMap(toKey,inclusive) ;
        }
        return EMPTY;
    }

    @Override
    public NavigableMap tailMap(Object fromKey, boolean inclusive) {
        if ( map instanceof NavigableMap ){
            return ((NavigableMap)map).tailMap(fromKey,inclusive) ;
        }
        return EMPTY;
    }

    /**
     * Relates a dictionary against another
     * @param m1 one dictionary
     * @param m2 another
     * @return Set relation between them
     */
    public static ZCollection.Relation relate(Map m1, Map m2){
        Set keySet1 = m1.keySet();
        Set keySet2 = m2.keySet();
        ZCollection.Relation relation = BaseZCollection.relate(keySet1,keySet2);
        if ( ZCollection.Relation.INDEPENDENT  == relation ) { return  relation ; }
        // do they really?
        Set intersection = BaseZCollection.intersection( keySet1,keySet2 );
        for ( Object k : intersection ){
            Object v1 = m1.get(k);
            Object v2 = m2.get(k);
            if ( !ZTypes.equals(v1,v2) ) return ZCollection.Relation.INDEPENDENT ;
        }
        return relation;
    }

    /**
     * Relates a dictionary against another collection entry
     * @param m one dictionary
     * @param c a collection of entries
     * @return Set relation between them
     */
    public static ZCollection.Relation relate(Map m, Collection<Entry> c){
        ZCollection zc = new ZSet( m.entrySet() );
        return zc.relate(c);
    }

    /**
     * Calculates intersection of a dictionary against another
     * Key,value must match
     * @param m1 one dictionary
     * @param m2 another
     * @return intersection between them
     */
    public static ZMap intersection( Map m1, Map m2){
        Set intersection = BaseZCollection.intersection(m1.keySet(), m2.keySet() );
        ZMap map = new ZMap();
        for ( Object k : intersection ){
            Object v1 = m1.get(k);
            Object v2 = m2.get(k);
            if ( ZTypes.equals(v1,v2) ){
                map.map.put(k,v1);
            }
        }
        return map;
    }

    /**
     * Mutably calculates intersection of a dictionary against another
     * Key,value must match
     * @param m1 one dictionary
     * @param m2 another
     * @return intersection between them as m1
     */
    public static Map intersectionMutable( Map m1, Map m2){
        Set intersection = BaseZCollection.intersection(m1.keySet(), m2.keySet() );
        Set ks = new HashSet<>( m1.keySet() );
        for ( Object k : ks ) {
            if ( !intersection.contains(k) ){
                m1.remove(k);
            }
        }

        for ( Object k : intersection ){
            Object v1 = m1.get(k);
            Object v2 = m2.get(k);
            if ( ZTypes.equals(v1,v2) ){
                m1.put(k,v1);
            }
        }
        return m1;
    }

    /**
     * Calculates union of a dictionary against another
     * Key,value must match
     * @param m1 one dictionary
     * @param m2 another
     * @return union between them
     */
    public static ZMap union( Map m1, Map m2){
        Set intersection = BaseZCollection.intersection(m1.keySet(), m2.keySet() );
        ZMap map = new ZMap();
        for ( Object k : intersection ){
            Object v1 = m1.get(k);
            Object v2 = m2.get(k);
            if ( !ZTypes.equals(v1,v2) ){
                map.map.put(k, new Pair( v1, v2 ) );
            }else{
                map.map.put(k,v1);
            }
        }
        for ( Object k : m1.keySet() ){
            if ( !intersection.contains(k) ){
                map.map.put(k, m1.get(k));
            }
        }
        for ( Object k : m2.keySet() ){
            if ( !intersection.contains(k) ){
                map.map.put(k, m2.get(k));
            }
        }
        return map;
    }

    /**
     * Mutably calculates union of a dictionary against another
     * Key,value must match
     * @param m1 one dictionary
     * @param m2 another
     * @return union between them as m1
     */
    public static Map unionMutable( Map m1, Map m2){
        Set intersection = BaseZCollection.intersection(m1.keySet(), m2.keySet() );
        for ( Object k : intersection ){
            Object v1 = m1.get(k);
            Object v2 = m2.get(k);
            if ( !ZTypes.equals(v1,v2) ){
                m1.put(k, new Pair( v1, v2 ) );
            }else{
                m1.put(k, v1 );
            }
        }

        for ( Object k : m2.keySet() ){
            if ( !m1.containsKey(k) ){
                m1.put(k, m2.get(k));
            }
        }
        return m1;
    }

    /**
     * Calculates difference between a dictionary against another
     * Key,value must match
     * @param m1 one dictionary
     * @param m2 another
     * @return difference between them
     */
    public static ZMap difference( Map m1, Map m2){
        Set intersection = BaseZCollection.intersection(m1.keySet(), m2.keySet() );
        ZMap map = new ZMap();
        for ( Object k : intersection ){
            Object v1 = m1.get(k);
            Object v2 = m2.get(k);
            if ( !ZTypes.equals(v1,v2) ){
                map.map.put(k, v1 );
            }
        }
        for ( Object k : m1.keySet() ){
            if ( !intersection.contains(k) ){
                map.map.put(k, m1.get(k));
            }
        }

        return map;
    }

    /**
     * Mutably calculates difference between a dictionary against another
     * Key,value must match
     * @param m1 one dictionary
     * @param m2 another
     * @return difference between them as m1
     */
    public static Map differenceMutable( Map m1, Map m2){
        for ( Object k : m2.keySet() ){
            if ( m1.containsKey(k)){
                Object v1 = m1.get(k);
                Object v2 = m2.get(k);
                if ( ZTypes.equals(v1,v2)){
                    m1.remove(k);
                }
            }
        }
        return m1;
    }


    /**
     * Calculates Symmetric difference between a dictionary against another
     * Key,value must match
     * @param m1 one dictionary
     * @param m2 another
     * @return symmetric difference between them
     */
    public static ZMap symmetricDifference( Map m1, Map m2){
        Set intersection = BaseZCollection.intersection(m1.keySet(), m2.keySet() );
        ZMap map = new ZMap();
        for ( Object k : m1.keySet() ){
            if ( !intersection.contains(k) ){
                map.map.put(k, m1.get(k));
            }
        }
        for ( Object k : m2.keySet() ){
            if ( !intersection.contains(k) ){
                map.map.put(k, m2.get(k));
            }
        }
        return map;
    }

    /**
     * Mutably calculates Symmetric difference between a dictionary against another
     * Key,value must match
     * @param m1 one dictionary
     * @param m2 another
     * @return symmetric difference between them as m1
     */
    public static Map symmetricDifferenceMutable( Map m1, Map m2){
        Set intersection = BaseZCollection.intersection(m1.keySet(), m2.keySet() );
        for ( Object k : m1.keySet() ){
            if ( intersection.contains(k)){
                m1.remove(k);
            }
        }
        for ( Object k : m2.keySet() ){
            if ( !intersection.contains(k)){
                m1.put(k,m2.get(k));
            }
        }
        return m1;
    }

    /**
     * Creates a ZMap from array
     * @param arr the array
     */
    public ZMap(Object arr){
        this( fromEntries( new HashMap(), new ZArray(arr,false)));
    }

    /**
     * Creates a ZMap from underlying map
     * @param m the underlying map
     */
    public ZMap(Map m) {
        if ( m instanceof ZMap ){
            map = new HashMap<>(((ZMap) m).map);
        }else {
            map = m;
        }
    }

    /**
     * Creates a ZMap from underlying Entry collection
     * @param c the underlying Entry collection
     */
    public ZMap(Collection c) {
        map = fromEntries(new HashMap(),c) ;
    }

    /**
     * Create a map using
     * @param keys array/collection of keys
     * @param values array/collection of values
     */
    public ZMap(Object keys, Object values){
        if ( keys == null ) throw new UnsupportedOperationException("null as keys!");
        if ( values == null ) throw new UnsupportedOperationException("null as keys!");
        if ( keys.getClass().isArray() ){
            keys = new ZArray(keys,false);
        }
        if ( values.getClass().isArray() ){
            values = new ZArray(values,false);
        }
        if ( keys instanceof Collection && values instanceof Collection ) {
            map = joinEntries(new HashMap(), (Collection)keys,(Collection)values);
        } else {
            throw new UnsupportedOperationException("Unsupported types as keys or values!");
        }
    }

    /**
     * Create an empty map
     */
    public ZMap() {
        this(new HashMap());
    }

    @Override
    public boolean containsKey(Object key) {
        return map.containsKey(key);
    }

    @Override
    public boolean containsValue(Object value) {
        return map.containsValue(value);
    }

    @Override
    public Object get(Object key) {
        return map.get(key);
    }

    @Override
    public Object put(Object key, Object value) {
        return map.put(key,value);
    }

    @Override
    public void putAll(Map m) {
        map.putAll(m);
    }

    @Override
    public Set keySet() {
        return new ZSet(map.keySet());
    }

    @Override
    public Collection values() {
        return new ZList(map.values());
    }

    @Override
    public Set entrySet() {
        return new ZSet(map.entrySet());
    }

    @Override
    public int size() {
        return map.size();
    }

    @Override
    public boolean isEmpty() {
        return map.isEmpty();
    }

    @Override
    public void clear() {
        map.clear();
    }

    @Override
    public Object remove(Object key) {
        return map.remove(key);
    }

    @Override
    public int compareTo(Object o) {
        if ( o == null ) return 1;
        if ( o == this ) return 0 ;
        if ( o instanceof Map ) {
            ZCollection.Relation r ;
            if ( o instanceof ZMap ){
                r = relate(this.map, ((ZMap) o).map);
            }else{
                r = relate(this.map, (Map) o);
            }
            if ( r == ZCollection.Relation.SUB ) return -1;
            if ( r == ZCollection.Relation.EQUAL ) return 0;
            if ( r == ZCollection.Relation.SUPER ) return 1;
            throw new UnsupportedOperationException("Can not compare unrelated Maps!");
        }
        if ( o instanceof Collection ) {
            ZCollection.Relation r = relate(this.map, (Collection<Entry>) o);
            if ( r == ZCollection.Relation.SUB ) return -1;
            if ( r == ZCollection.Relation.EQUAL ) return 0;
            if ( r == ZCollection.Relation.SUPER ) return 1;
            throw new UnsupportedOperationException("Can not compare unrelated Maps!");
        }
        throw new UnsupportedOperationException("Can not compare Map against another object!");
    }

    @Override
    public Object _add_(Object o) {
        if ( o instanceof Map ){
            return union(this.map,(Map)o);
        }
        add_mutable(o);
        return this;
    }

    @Override
    public Object _sub_(Object o) {
        if ( o instanceof Map ){
            return difference(this.map,(Map)o);
        }
        sub_mutable(o);
        return this;
    }

    @Override
    public Object _mul_(Object o) {
        throw new UnsupportedOperationException("Can not multiply Map!");
    }

    @Override
    public Object _div_(Object o) {
        Set s = new ZSet();
        for ( Object k : map.keySet() ){
            Object v = map.get(k);
            if ( ZTypes.equals(v,o) ){
                s.add(k);
            }
        }
        return s;
    }

    @Override
    public Object _pow_(Object o) {
        throw new UnsupportedOperationException("No support for exponentiation in Map!");
    }

    @Override
    public void add_mutable(Object o) {
        Pair p = new Pair(o);
        map.put(p.get(0),p.get(1));
    }

    @Override
    public void sub_mutable(Object o) {
        if ( o instanceof Map ){
            map = differenceMutable(map,(Map)o);
            return;
        }
        if ( map.containsKey( o ) ){
            map.remove(o);
        }
    }

    @Override
    public void mul_mutable(Object o) {
        throw new UnsupportedOperationException("Can not multiply Map!");
    }

    @Override
    public void div_mutable(Object o) {
        throw new UnsupportedOperationException("Division of a Map is immutable!");
    }

    @Override
    public Object _and_(Object o) {
        if ( !(o instanceof Map) ) throw new UnsupportedOperationException("Intersection with a Non Map!");
        return intersection(this.map, (Map)o);
    }

    @Override
    public Object _or_(Object o) {
        if ( !(o instanceof Map) ) throw new UnsupportedOperationException("Union with a Non Map!");
        return union(this.map, (Map)o);
    }

    @Override
    public Object _xor_(Object o) {
        if ( !(o instanceof Map) ) throw new UnsupportedOperationException("Symmetric Difference with a Non Map!");
        return symmetricDifference(this.map, (Map)o);
    }

    @Override
    public void and_mutable(Object o) {
        if ( !(o instanceof Map) ) throw new UnsupportedOperationException("Intersection with a Non Map!");
        map = intersectionMutable(map,(Map)o);
    }

    @Override
    public void or_mutable(Object o) {
        if ( !(o instanceof Map) ) throw new UnsupportedOperationException("Union with a Non Map!");
        map = unionMutable(map,(Map)o);
    }

    @Override
    public void xor_mutable(Object o) {
        if ( !(o instanceof Map) ) throw new UnsupportedOperationException("Symmetric Difference with a Non Map!");
        map = symmetricDifferenceMutable(map,(Map)o);
    }

    /* Java 8 Exotics ... */

    @Override
    public Object getOrDefault(Object key, Object defaultValue) {
        if ( map.containsKey( key ) ) return map.get(key);
        return defaultValue;
    }

    @Override
    public Object putIfAbsent(Object key, Object value) {
        if ( map.containsKey( key ) )  return map.get(key);
        return map.put(key,value);
    }

    @Override
    public boolean remove(Object key, Object value) {
        if ( map.containsKey(key)){
            Object v = map.get(key);
            if ( Objects.deepEquals( value, v) ){
                map.remove(key);
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean replace(Object key, Object oldValue, Object newValue) {
        if ( map.containsKey(key)){
            Object v = map.get(key);
            if ( Objects.deepEquals( oldValue, v) ){
                map.put(key,newValue);
                return true;
            }
        }
        return false;
    }

    @Override
    public Object replace(Object key, Object value) {
        if ( map.containsKey(key)){
            return map.put(key,value);
        }
        return null;
    }

    @Override
    public String toString() {
        return map.toString();
    }

    @Override
    public String string(Object... args) {
        if  ( args.length == 0 ) return map.toString();
        boolean json = ZTypes.bool(args[0],false);
        if ( json ){
            return ZTypes.jsonString(this.map);
        }
        return map.toString();
    }

    @Override
    public int hashCode() {
        return map.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if ( obj == null ) return false;
        if ( obj == this ) return true;
        // check the class? Nonsense...
        return compareTo(obj) == 0 ;
    }

    /* For MultiSet() */
    public static class MultiSet extends ZMap {

        public static ZCollection.Relation relate(MultiSet ms1, MultiSet ms2){
            Map<Object,Integer> m1 = ms1;
            Map<Object,Integer> m2 = ms2;
            ZSet ks1 = (ZSet) m1.keySet();
            ZSet ks2 = (ZSet) m2.keySet();
            // case 1 : ks1 and ks2 just overlap and nothing more
            ZCollection.Relation r = ks1.relate(ks2);
            if ( r == ZCollection.Relation.OVERLAP || r == ZCollection.Relation.INDEPENDENT ) return r;
            if ( r == ZCollection.Relation.SUB ){
                // is it really ?
                for ( Object key : ks1 ){
                    int leftCount = m1.get(key);
                    int rightCount = m2.get(key);
                    if ( leftCount > rightCount ) return ZCollection.Relation.OVERLAP;
                }
                return r;
            }
            if ( r == ZCollection.Relation.SUPER ){
                // is it really ?
                for ( Object key : ks2 ){
                    int leftCount = m1.get(key);
                    int rightCount = m2.get(key);
                    if ( leftCount < rightCount ) return ZCollection.Relation.OVERLAP;
                }
                return r;
            }
            // this is where they are equal, are they ?
            // it can be all of them, super, sub, equal, now?
            boolean sup = true;
            for ( Object key : ks2 ){
                int leftCount = m1.get(key);
                int rightCount = m2.get(key);
                if ( leftCount < rightCount ) {
                    sup = false ;
                    break;
                }
            }
            boolean sub = true;
            for ( Object key : ks1 ){
                int leftCount = m1.get(key);
                int rightCount = m2.get(key);
                if ( leftCount > rightCount ) {
                    sub = false;
                    break;
                }
            }
            if ( sub && sup ) return ZCollection.Relation.EQUAL ;
            if ( sub  ) return ZCollection.Relation.SUB ;
            if ( sup  ) return ZCollection.Relation.SUPER ;
            return ZCollection.Relation.OVERLAP;
        }

        public MultiSet(Map<Object,Integer> src){
            super(src);
        }

        public MultiSet(){
            this(new ZMap());
        }

        @Override
        public Object _and_(Object o) {
            if ( !(o instanceof MultiSet) ){
                return super._and_(o);
            }
            MultiSet ms = new MultiSet();
            Map<Object,Integer> m1 = this;
            Map<Object,Integer> m2 = (MultiSet)o;
            ZSet ks1 = (ZSet) keySet();
            ZSet ks2 = (ZSet) m2.keySet();
            ZCollection i = ks1.intersection(ks2);
            for ( Object k : i ){
                int leftCount = m1.get(k);
                int rightCount = m1.get(k);
                int count = leftCount;
                if ( leftCount > rightCount ){
                    count = rightCount;
                }
                ms.put(k,count);
            }
            return ms;
        }

        @Override
        public Object _or_(Object o) {
            if ( !(o instanceof MultiSet) ){
                return super._or_(o);
            }
            MultiSet ms = new MultiSet();
            Map<Object,Integer> m1 = this;
            Map<Object,Integer> m2 = (MultiSet)o;
            ZSet ks1 = (ZSet) keySet();
            ZSet ks2 = (ZSet) m2.keySet();
            ZCollection i = ks1.union(ks2);
            for ( Object k : i ){
                int leftCount = m1.get(k);
                int rightCount = m2.get(k);
                int count = leftCount;
                if ( leftCount < rightCount ){
                    count = rightCount;
                }
                ms.put(k,count);
            }
            return ms;
        }

        @Override
        public int compareTo(Object o) {
            if ( o instanceof MultiSet ){
                ZCollection.Relation r = relate(this,(MultiSet)o);
                switch ( r ){
                    case SUB:
                        return -1;
                    case SUPER:
                        return 1;
                    case EQUAL:
                        return 0;
                    default:
                        throw new UnsupportedOperationException("Multisets are not comparable!");
                }
            }
            return super.compareTo(o);
        }
    }
}
