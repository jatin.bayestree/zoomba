/*
 * Copyright 2017 zoomba-lang.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package zoomba.lang.core.collections;

import zoomba.lang.core.operations.Function;
import zoomba.lang.core.operations.ZCollection;
import zoomba.lang.core.operations.Function;
import zoomba.lang.core.operations.ZCollection;
import zoomba.lang.core.types.ZRange;
import zoomba.lang.core.types.ZTypes;
import zoomba.lang.core.types.ZException.Break;
import java.util.*;

import static zoomba.lang.core.operations.Function.NIL;

/**
 * A Generic List Class to be a wrapper over any List interface implementation
 */
public class ZList extends BaseZCollection implements List{


    /**
     * Creates a list from
     * @param anon anonymous function
     * @param args using the args
     *             either the args is empty - then empty list
     *             the args is a collection, creates a list containing collection
     *             the args themselves are taken as list
     * @return a list
     */
    public static List list( Function anon, Object... args){
        if ( args.length == 0 ) return new ZList() ;
        Object a = args[0];
        if ( args.length > 1 ){
            a = args ; // implicit list from args, no flattening
        }
        if ( anon == null ){
            if ( a instanceof ZRange) return ((ZRange) a).list();
            if ( a instanceof Collection ){
                return new ZList((Collection)a);
            }
            if ( a == null ) return Collections.EMPTY_LIST;
            return new ZList(a);
        }
        if ( a instanceof ZRange ){
            a = ((ZRange)a).asList(); // intercept and convert
        }
        if ( a instanceof Iterable ){
            return (List) BaseZCollection.map(new ZList(), anon, (Iterable)a);
        }
        if ( a instanceof Map ){
            return (List)BaseZCollection.map(new ZList(), anon, ((Map) a).entrySet() );
        }
        if ( a.getClass().isArray() ){
            return (List)BaseZCollection.map(new ZList(), anon, new ZArray(a,false) );
        }
        throw new UnsupportedOperationException("Type can not be converted to a list!");
    }


    /**
     * Creates a list from underlying array object
     * @param arr the array
     */
    public ZList(Object arr) {
        super(arr);
        col = new ArrayList<>(col);
    }

    /**
     * Creates a list out of collection
     * @param c the collection
     */
    public ZList(Collection c) {
        super(c);
        col = new ArrayList<>(col);
    }

    /**
     * Creates a list out of an Iterator
     * @param i the iterator
     */
    public ZList(Iterator i) {
        super(new ArrayList() );
        while ( i.hasNext() ){
            col.add(i.next());
        }
    }

    /**
     * Creates a list out of an Iterable
     * @param i the iterable
     */
    public ZList(Iterable i) {
        this(i.iterator());
    }

    /**
     * Generates an empty list
     */
    public ZList() {
        super(new ArrayList());
    }

    @Override
    public boolean addAll(int index, Collection c) {
        return ((List)col).addAll(index, c );
    }

    @Override
    public Object get(int index) {
        try {
            return ((List) col).get(index);
        }catch (IndexOutOfBoundsException e){
            if ( !col.isEmpty() && index < 0 ){
                return ((List) col).get(col.size() + index );
            }
            throw e;
        }
    }

    @Override
    public Object set(int index, Object element) {
        try {
            return ((List) col).set(index, element);
        }catch (IndexOutOfBoundsException e){
            if ( !col.isEmpty() && index < 0 ){
                return set(col.size() + index, element );
            }
            throw e;
        }
    }

    @Override
    public void add(int index, Object element) {
        ((List)col).add(index,element);
    }

    @Override
    public Object remove(int index) {
        return ((List)col).remove(index);
    }

    /**
     * Like the normal indexOf but
     * given the parameter is a list, then
     * returns the index where the list starts within itself
     * @param o the item to be found
     * @return the index (if o is a collection, returns the right most index of match )
     * [0,1,2,3,4] when indexOf [2,3], it will yield 2
     * [0,1,2,3,4] when indexOf [2,3,4], it will yield 2
     */
    @Override
    public int indexOf(Object o) {
        if ( o == null ) return ((List)col).indexOf(o);

        List l = null;
        if ( o.getClass().isArray() ){
             l = new ZArray(o,false);
        }else if ( o instanceof List ){
            l = (List)o;
        }
        if ( l == null ) return ((List)col).indexOf(o);

        if ( l.isEmpty() ) return 0;
        if ( isEmpty() ) return -1;

        int mySize = size();
        if ( l.size() > mySize ) return -1;
        int j = 0 ;
        int startIndex = 0 ;
        for ( int i = 0 ; i < size(); ){

            Object mine = get(i);
            Object her = l.get(j);
            if ( ZTypes.equals(mine,her ) ){
                if ( j == 0 ){ startIndex = j ; }
                i++;
                j++;
                if ( j == l.size() ) return startIndex ;
            }else{
                if ( j != 0 ){ // has to start from i-j'th index again
                    i -= j;
                }
                i++;
                j=0;
            }
        }
        return -1;
    }

    /**
     * Like the normal lastIndexOf but
     * given the parameter is a list, then
     * returns the index where the list ends within itself
     * @param o the item to be found
     * @return the index (if o is a collection, returns the right most index of match )
     * [0,1,2,3,4] when lastIndexOf [2,3], it will yield 3, instead of 2
     * [0,1,2,3,4] when lastIndexOf [2,3,4], it will yield 4, instead of 2
     */
    @Override
    public int lastIndexOf(Object o) {
        if ( o == null ) return ((List)col).lastIndexOf(o);

        List l = null;
        if ( o.getClass().isArray() ){
            l = new ZArray(o,false);
        }else if ( o instanceof List ){
            l = (List)o;
        }
        if ( l == null ) return ((List)col).lastIndexOf(o);

        if ( l.isEmpty() ) return size() - 1;
        if ( isEmpty() ) return -1;

        int mySize = size();
        if ( l.size() > mySize ) return -1;
        int j = l.size() - 1 ;
        int startIndex = size() - 1 ;
        for ( int i = size() -1 ; i >= 0 ; ){

            Object mine = get(i);
            Object her = l.get(j);
            if ( ZTypes.equals(mine,her ) ){
                if ( j == l.size() - 1 ){ startIndex = i ; }
                i--;
                j--;
                if ( j < 0 ) return startIndex ;
            }else{
                i--;
                j = l.size() -1 ;
            }
        }
        return -1;
    }

    @Override
    public ListIterator listIterator() {
        return ((List)col).listIterator();
    }

    @Override
    public ListIterator listIterator(int index) {
        return ((List)col).listIterator(index);
    }

    @Override
    public List subList(int fromIndex, int toIndex) {
        return ((List)col).subList(fromIndex,toIndex);
    }

    @Override
    public ZCollection collector() {
        return new ZList();
    }

    @Override
    public Set setCollector() {
        return new ZSet();
    }

    @Override
    public Object rightFold(Function fold, Object... arg) {
        return rightFold(this,fold,arg);
    }

    @Override
    public int rightIndex(Function predicate) {
        return rightIndex(this,predicate);
    }

    @Override
    public Object rightReduce(Function reduce) {
        return rightReduce( this, reduce);
    }

    @Override
    public ZCollection reverse() {
        ZList reverse = new ZList();
        for ( int i = size() - 1; i >= 0 ; i--){
            reverse.add( get(i));
        }
        return reverse ;
    }

    @Override
    public ZCollection myCopy() {
        return new ZList(new ArrayList<>(col));
    }

    @Override
    public String containerFormatString() {
        return "[ %s ]";
    }

    /*
    * Stack and Queue Manipulation Technique
    * https://upload.wikimedia.org/wikipedia/commons/5/52/Data_Queue.svg
    * */

    /**
     * Treat the List as a Queue
     * Enqueues an item
     * @param item the item to be queued
     * @return true if it could enqueue
     */
    public boolean enqueue(Object item){
        return add(item);
    }

    /**
     * Treat the List as a Queue
     * Dequeue from the queue an item
     * @return the item
     */
    public Object dequeue(){
        try {
            return remove(0);
        }catch (IndexOutOfBoundsException i){
            return NIL;
        }
    }

    /**
     * Treat the List as a Queue
     * Gets the item from the front of the queue without removing it
     * @return the item in the front
     */
    public Object front(){
        try {
            return get(0);
        }catch (IndexOutOfBoundsException i){
            return NIL;
        }
    }

    /**
     * Treat the List as a Queue
     * Gets the item from the back of the queue without removing it
     * @return the item in the back
     */
    public Object back(){
        try {
            return get(size()-1);
        }catch (IndexOutOfBoundsException i){
            return NIL;
        }
    }

    /**
     * Treat the List as a Stack
     * Gets the item top of the stack without removing it
     * @return the item in the top
     */
    public Object top(){
        return back();
    }

    /**
     * Treat the List as a Stack
     * Puts the item on the stack
     * @param item the item to be pushed
     * @return true if could push, false otherwise
     */
    public boolean push(Object item){
        return enqueue(item);
    }

    /**
     * Treat the List as a Stack
     * Pops the top item from the stack
     * @return the top item in the Stack
     */
    public Object pop(){
        try {
            return remove(size()-1);
        }catch (IndexOutOfBoundsException i){
            return NIL;
        }
    }
}
