/*
 * Copyright 2017 zoomba-lang.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package zoomba.lang.core.operations;

import zoomba.lang.core.collections.BaseZCollection;
import zoomba.lang.core.collections.ZArray;
import zoomba.lang.core.oop.ZObject;
import zoomba.lang.core.types.*;
import zoomba.lang.core.collections.ZList;
import zoomba.lang.core.collections.ZMap;
import zoomba.lang.core.types.ZNumber;
import zoomba.lang.core.types.ZString;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.*;

/**
 * The Miracle class for all ALU Operations
 */
public class Arithmetic implements Comparator {

    public boolean strictOnAmbiguity = false;

    /**
     * The instance which can be used by anyone else - no need to create instance all around
     */
    public static final Arithmetic INSTANCE = new Arithmetic();

    /**
     * Checks if an object is infinity type
     * @param o the object
     * @return true if it is, false if it is not
     */
    public static boolean isInfinity(Object o){
        return o instanceof Infinity ;
    }

    private static class Infinity extends Number {

        Infinity(){}

        @Override
        public int intValue() {
            throw new UnsupportedOperationException("Infinity can not be contained in container!");
        }

        @Override
        public long longValue() {
            throw new UnsupportedOperationException("Infinity can not be contained in container!");
        }

        @Override
        public float floatValue() {
            throw new UnsupportedOperationException("Infinity can not be contained in container!");
        }

        @Override
        public double doubleValue() {
            throw new UnsupportedOperationException("Infinity can not be contained in container!");
        }
    }

    static final class PositiveInfinity extends Infinity implements Comparable{

        private PositiveInfinity(){}

        @Override
        public int compareTo(Object o) {
            if ( this == o ) return 0 ;
            return 1;
        }

        @Override
        public String toString() {
            return "inf";
        }
    }

    static final class NegativeInfinity extends Infinity implements Comparable{

        private NegativeInfinity(){}

        @Override
        public int compareTo(Object o) {
            if ( this == o ) return 0 ;
            return -1;
        }

        @Override
        public String toString() {
            return "-inf";
        }
    }

    public static final PositiveInfinity POSITIVE_INFINITY = new PositiveInfinity();

    public static final NegativeInfinity NEGATIVE_INFINITY  = new NegativeInfinity();

    /**
     * Basic Arithmetic interface to mingle with ZoomBA operator overloading
     */
    public interface BasicArithmeticAware extends Comparable {

        /**
         * Adds an object to self
         * @param o the object
         * @return the addition result
         */
        Object _add_(Object o);

        /**
         * Subtracts an object to self
         * @param o the object
         * @return the subtraction result
         */
        Object _sub_(Object o);

        /**
         * Multiplies an object to self
         * @param o the object
         * @return the multiplication result
         */
        Object _mul_(Object o);

        /**
         * Division of self by an object
         * @param o the object
         * @return the division result
         */
        Object _div_(Object o);

        /**
         * Exponentiation of self by an object
         * @param o the object
         * @return the exponentiation result
         */
        Object _pow_(Object o);

        /**
         * Mutable addition of self to object
         * @param o the object
         */
        void add_mutable(Object o);

        /**
         * Mutable subtraction of self to object
         * @param o the object
         */
        void sub_mutable(Object o);

        /**
         * Mutable multiplication of self to object
         * @param o the object
         */
        void mul_mutable(Object o);

        /**
         * Mutable division of self to object
         * @param o the object
         */
        void div_mutable(Object o);
    }

    /**
     * Interface capable of doing some more Numeric operations
     */
    public interface NumericAware extends Comparable {

        /**
         * Gets the absolute value as number
         * @return the absolute value
         */
        Number _abs_();

        /**
         * Gets the not of the number
         * @return the negation
         */
        Number _not_();

        /**
         * Negates the object
         */
        void not_mutable();

        /**
         * Modulo operation using the object
         * @param o the object
         * @return the result of modulo
         */
        Number _mod_(Number o);

        /**
         * Mutable modulo of the self with the object
         * @param o the object
         */
        void mod_mutable(Number o);

        /**
         * The actual number, narrowed down
         * @return the actual number
         */
        Number actual();

        /**
         * BigDecimal representation for JDK compatibility
         * @return big decimal representation
         */
        BigDecimal bigDecimal();

        /**
         * BigInteger representation for JDK compatibility
         * @return big integer representation
         */
        BigInteger bigInteger();

        /**
         * Method to define if there is a fractional non zero part
         * @return true if it is fractional, else false
         */
        boolean fractional();

        /**
         * Ceiling of the object
         * @return ceiling
         */
        Number ceil();

        /**
         * Floor function
         * @return floor of the object
         */
        Number floor();
    }

    /**
     * The Logical Interface
     */
    public interface LogicAware {

        /**
         * And or intersection of self with object
         * @param o the object
         * @return the result
         */
        Object _and_(Object o);

        /**
         * Or or Union of self with object
         * @param o the object
         * @return the result
         */
        Object _or_(Object o);

        /**
         * XOR or Symmetric Delta of self with object
         * @param o the object
         * @return the result
         */
        Object _xor_(Object o);

        /**
         * Mutable And or intersection of self with object
         * @param o the object
         */
        void and_mutable(Object o);

        /**
         * Mutable Or or Union of self with object
         * @param o the object
         */
        void or_mutable(Object o);

        /**
         * Mutable XOR or Symmetric Delta of self with object
         * @param o the object
         */
        void xor_mutable(Object o);
    }

    private void NULL_CHECK(Object o){
        if ( o == null ) throw new UnsupportedOperationException("Operation on null not supported!");
    }

    /**
     * Throws unsupported operation exception
     * @param l left object
     * @param r right object
     * @param op operation
     * @return noting, it will always throw exception
     */
    public int UNSUPPORTED_OPERATION(Object l, Object r, String op){
        String message = String.format( "Can not do operation ( %s ) : ( %s ) with ( %s ) !",
                op,l,r);
        throw new UnsupportedOperationException(message);
    }

    public void CHECK_AMBIGUOUS_OPERATION(Object l, Object r, String op){
        if ( !(l instanceof Number) && r instanceof Number && ZNumber.number(l) != null ){
            final String message =
                    String.format("Ambiguous Operation : [%s] : '%s' of type %s with number %s of type %s !", op, l,
                   l.getClass(), r, r.getClass() );
            if (strictOnAmbiguity){
                throw new UnsupportedOperationException(message);
            }
            System.err.println(message);
        }
    }

    /**
     * Adds two objects
     * @param left object left
     * @param right object right
     * @return result
     */
    public Object add(Object left, Object right) {
        // start with arithmetic ...
        NULL_CHECK(left);
        if ( left instanceof CharSequence ){
            CHECK_AMBIGUOUS_OPERATION(left,right,"+");
            return left.toString() + right;
        }
        try{
            ZNumber l = new ZNumber(left);
            ZNumber r = new ZNumber(right);
            return l._add_(r);
        }catch (Exception e){
            left = customZConvert(left);
            if ( left instanceof BasicArithmeticAware ){
                return ((BasicArithmeticAware) left)._add_(right);
            }
        }
        return UNSUPPORTED_OPERATION(left,right,"ADD");
    }

    /**
     * Adds right object into left object and returns mutated left object
     * @param left object left
     * @param right object right
     * @return left object, mutated
     */
    public Object addMutable(Object left, Object right) {
        // start with arithmetic ...
        NULL_CHECK(left);

        if ( left instanceof CharSequence ){
            CHECK_AMBIGUOUS_OPERATION(left,right,"+=");
            if ( left instanceof ZString){
                ((ZString) left).add_mutable( right );
                return left ;
            }
            ZString zs = new ZString(left);
            zs.add_mutable( right );
            return zs;
        }
        try{
            if ( left instanceof Number ){
                if ( left instanceof ZNumber ){
                    ((ZNumber) left).add_mutable(right);
                    return left;
                }
                ZNumber l = new ZNumber(left);
                return l._add_(right);
            }
            if ( left instanceof BasicArithmeticAware ){
                ((BasicArithmeticAware) left).add_mutable(right);
                return left ;
            }
            if ( left instanceof Collection ){
                if ( right instanceof Collection ){
                    ((Collection)left).addAll((Collection)right);
                } else {
                    ((Collection)left).add(right);
                }
                return left;
            }
        }catch (Exception e){ }
        return UNSUPPORTED_OPERATION(left,right,"ADD");
    }

    /**
     * Subtracts two objects
     * @param left object left
     * @param right object right
     * @return result
     */
    public Object sub(Object left, Object right) {
        NULL_CHECK(left);
        CHECK_AMBIGUOUS_OPERATION(left,right,"-");
        try{
            ZNumber l = new ZNumber(left);
            ZNumber r = new ZNumber(right);
            return l._sub_(r);
        }catch (Exception e){
            left = customZConvert(left);
            if ( left instanceof BasicArithmeticAware ){
                return ((BasicArithmeticAware) left)._sub_(right);
            }
        }
        return UNSUPPORTED_OPERATION(left,right,"SUBTRACT");
    }

    /**
     * Subtracts right object from left object and returns mutated left object
     * @param left object left
     * @param right object right
     * @return left object, mutated
     */
    public Object subMutable(Object left, Object right) {
        NULL_CHECK(left);
        CHECK_AMBIGUOUS_OPERATION(left,right,"-=");
        try{
            if ( left instanceof Number ){
                if ( left instanceof ZNumber ){
                    ((ZNumber) left).sub_mutable(right);
                    return left;
                }
                ZNumber l = new ZNumber(left);
                return l._sub_(right);
            }
            if ( left instanceof BasicArithmeticAware ){
                ((BasicArithmeticAware) left).sub_mutable(right);
                return left ;
            }
            if ( left instanceof Collection ){
                if ( right instanceof Collection ){
                    ((Collection)left).removeAll((Collection)right);
                } else {
                    ((Collection)left).remove(right);
                }
                return left;
            }
            if ( left instanceof Map ){
                ((Map)left).remove(right);
                return left;
            }
        }catch (Exception e){
        }
        return UNSUPPORTED_OPERATION(left,right,"SUBTRACT");
    }

    /**
     * Multiplies two objects
     * @param left object left
     * @param right object right
     * @return result
     */
    public Object mul(Object left, Object right) {
        NULL_CHECK(left);
        CHECK_AMBIGUOUS_OPERATION(left,right,"*");
        try{
            ZNumber l = new ZNumber(left);
            ZNumber r = new ZNumber(right);
            return l._mul_(r);
        }catch (Exception e){
            if ( left instanceof BasicArithmeticAware ){
                return ((BasicArithmeticAware) left)._mul_(right);
            }

        }
        return UNSUPPORTED_OPERATION(left,right,"MULTIPLY");
    }

    /**
     * Multiplies right object to left object and returns mutated left object
     * @param left object left
     * @param right object right
     * @return left object, mutated
     */
    public Object mulMutable(Object left, Object right) {
        NULL_CHECK(left);
        CHECK_AMBIGUOUS_OPERATION(left,right,"*=");
        try{
            ZNumber l = new ZNumber(left);
            ZNumber r = new ZNumber(right);
            l.mul_mutable(r);
            if ( !(left instanceof ZNumber) ) {
                return l.actual();
            }
            return l;
        }catch (Exception e){
            if ( left instanceof BasicArithmeticAware ){
                ((BasicArithmeticAware) left).mul_mutable(right);
                return left;
            }

        }
        return UNSUPPORTED_OPERATION(left,right,"MULTIPLY");
    }

    /**
     * Divides two objects
     * @param left object left
     * @param right object right
     * @return result
     */
    public Object div(Object left, Object right) {
        NULL_CHECK(left);
        CHECK_AMBIGUOUS_OPERATION(left,right,"/");
        try{
            ZNumber l = new ZNumber(left);
            ZNumber r = new ZNumber(right);
            return l._div_(r);
        }catch (Exception e){
            if ( left instanceof BasicArithmeticAware){
                return ((BasicArithmeticAware) left)._div_(right);
            }
        }
        return UNSUPPORTED_OPERATION(left,right,"DIVISION");
    }

    /**
     * Divides left object by right object and returns mutated left object
     * @param left object left
     * @param right object right
     * @return left object, mutated
     */
    public Object divMutable(Object left, Object right) {
        NULL_CHECK(left);
        CHECK_AMBIGUOUS_OPERATION(left,right,"/=");
        try{
            ZNumber l = new ZNumber(left);
            ZNumber r = new ZNumber(right);
            l.div_mutable(r);
            if ( !(left instanceof ZNumber) ) {
                return l.actual();
            }
            return l ;
        }catch (Exception e){
            if ( left instanceof BasicArithmeticAware){
                ((BasicArithmeticAware) left).div_mutable(right);
                return left ;
            }
        }
        return UNSUPPORTED_OPERATION(left,right,"DIVISION");
    }

    /**
     * Computers result of the left object raised to the power of right
     * @param left object left
     * @param right object right
     * @return result
     */
    public Object pow(Object left, Object right) {
        if ( left instanceof CharSequence ){
            ZString zs = new ZString((CharSequence)left);
            return zs._pow_(right);
        }
        NULL_CHECK(left);
        try{
            ZNumber l = new ZNumber(left);
            ZNumber r = new ZNumber(right);
            return l._pow_(r);
        }catch (Exception e){
            // try convert...
            left = customZConvert(left);
            if ( left instanceof BasicArithmeticAware ){
                return ((BasicArithmeticAware) left)._pow_(right);
            }
        }
        return UNSUPPORTED_OPERATION(left,right,"EXP");
    }

    private static Object customZConvert(Object o){
        // do not convert ZObjects or Tuples and what not ...
        if ( o instanceof ZObject ||
                o instanceof ZMatrix.Tuple ) return o;

        // now rest
        if ( o.getClass().isArray() ){
            return new ZArray(o,false);
        }
        if ( o instanceof ZCollection || o instanceof ZMap ){
            return o;
        }
        if ( o instanceof List ){
            return new ZList((List)o);
        }
        if ( o instanceof Set ){
            return new ZList((Set)o);
        }
        if ( o instanceof Collection ){
           // it is unknown collection
           return new BaseZCollection.ZWrappedCollection((Collection)o);
        }
        if ( o instanceof Map ){
            return new ZMap((Map)o);
        }
        if ( o instanceof Date ){
            return new ZDate((Date)o);
        }

        return o;
    }

    /**
     * Compare left object with right,
     * throws error when they are not comparable
     * @param left left object
     * @param right right object
     * @return
     *     left less than right then -1
     *     left equals right then 0
     *     left greater than right then +1
     */
    @Override
    public int compare(Object left, Object right){
        // intercept ... infinities
        if ( left instanceof Infinity ){ return ((Comparable)left).compareTo(right) ; }
        if ( right == POSITIVE_INFINITY ) return -1;
        if ( right == NEGATIVE_INFINITY ) return 1;

        if ( left == null && right == null ) return 0;
        if ( left == null || right == null ){
            throw new UnknownFormatConversionException("null can not be compared for or against non null!");
        }

        if ( left instanceof Number ){
            ZNumber l = new ZNumber(left);
            ZNumber r = new ZNumber(right);
            return l.compareTo(r);
        }
        if ( left instanceof Character ){
            if ( right instanceof Number ){
                int i = ((Number) right).intValue();
                return ((Character) left).charValue() - i;
            }
            return left.toString().compareTo( String.valueOf(right));
        }

        if ( left instanceof CharSequence ){
            ZString s = new ZString(left);
            return s.compareTo(right);
        }
        left = customZConvert(left);
        if ( left instanceof Comparable ){
            return ((Comparable) left).compareTo(right);
        }
        return UNSUPPORTED_OPERATION(left,right,"COMPARING!");
    }

    /**
     * The operator less than
     * @param left left object
     * @param right right object
     * @return true if left is less than right
     */
    public boolean lt(Object left, Object right) {
        try{
            return compare(left,right) < 0 ;
        }catch (UnknownFormatConversionException fme){
            throw fme;
        }
        catch (Throwable t){
            return false;
        }
    }

    /**
     * The operator less than equal to
     * @param left left object
     * @param right right object
     * @return true if left is less than or equals to right
     */
    public boolean le(Object left, Object right) {
        try{
            return compare(left,right) <= 0 ;
        }catch (UnknownFormatConversionException fme){
            throw fme;
        }
        catch (Throwable t){
            return false;
        }
    }

    /**
     * The operator greater than
     * @param left left object
     * @param right right object
     * @return true if left is greater than right
     */
    public boolean gt(Object left, Object right) {
        try{
            return compare(left,right) > 0 ;
        }catch (UnknownFormatConversionException fme){
            throw fme;
        }
        catch (Throwable t){
            return false;
        }
    }

    /**
     * The operator greater than equal to
     * @param left left object
     * @param right right object
     * @return true if left is greater than or equals to right
     */
    public boolean ge(Object left, Object right) {
        try{
            return compare(left,right) >= 0 ;
        }catch (UnknownFormatConversionException fme){
            throw fme;
        }
        catch (Throwable t){
            return false;
        }
    }

    /**
     * The operator equals
     * @param left left object
     * @param right right object
     * @return true if left is equals to right
     */
    public boolean eq(Object left, Object right) {
        if ( left == null ){
            return ( right == null ) ;
        }

        try{
            if ( left instanceof Boolean ){
                return (boolean)left == ZTypes.bool(right) ;
            }
            return ( compare(left,right) == 0 ) ;
        }catch (Throwable t){
        }
        return left.equals(right);
    }

    /**
     * The operator not equals
     * @param left left object
     * @param right right object
     * @return true if left is not equal to right
     */
    public boolean ne(Object left, Object right) {
        return !eq(left, right);
    }

    /**
     * The operator and or intersection
     * @param left left object
     * @param right right object
     * @return the and or intersection of left with right object
     */
    public Object and(Object left, Object right) {
        if ( left instanceof Boolean ){
            return (boolean)left && ZTypes.bool(right,false);
        }
        if ( left instanceof Number ){
            ZNumber n = new ZNumber(left);
            return n._and_(right);
        }
        left = customZConvert(left);
        if ( left instanceof LogicAware ){
           return  ((LogicAware) left)._and_(right);
        }
        return UNSUPPORTED_OPERATION(left,right,"AND");
    }

    /**
     * The operator and or intersection, mutably
     * @param left left object
     * @param right right object
     * @return the and or intersection of left with right object, mutably stored in left object
     */
    public Object andMutable(Object left, Object right) {
        if ( left instanceof Boolean ){
            return (boolean)left && ZTypes.bool(right,false);
        }
        if ( left instanceof Number ){
            if ( left instanceof ZNumber ){
                ((ZNumber) left).and_mutable(right);
                return left;
            }
            ZNumber n = new ZNumber(left);
            return n._and_(right);
        }
        left = customZConvert(left);
        if ( left instanceof LogicAware ){
            ((LogicAware) left).and_mutable(right);
            return left ;
        }
        return UNSUPPORTED_OPERATION(left,right,"AND");
    }

    /**
     * The operator or or union
     * @param left left object
     * @param right right object
     * @return the or or union of left with right object
     */
    public Object or(Object left, Object right) {
        if ( left instanceof Boolean ){
            return (boolean)left || ZTypes.bool(right,false);
        }
        if ( left instanceof Number ){
            ZNumber n = new ZNumber(left);
            return n._or_(right);
        }
        left = customZConvert(left);
        if ( left instanceof LogicAware ){
            return  ((LogicAware) left)._or_(right);
        }
        return UNSUPPORTED_OPERATION(left,right,"OR");
    }

    /**
     * The operator and or intersection, mutably
     * @param left left object
     * @param right right object
     * @return the or or union of left with right object, mutably stored in left object
     */
    public Object orMutable(Object left, Object right) {
        if ( left instanceof Boolean ){
            return (boolean)left || ZTypes.bool(right,false);
        }
        if ( left instanceof Number ){
            if ( left instanceof ZNumber ){
                ((ZNumber) left).or_mutable(right);
                return left;
            }
            ZNumber n = new ZNumber(left);
            return n._or_(right);
        }
        left = customZConvert(left);
        if ( left instanceof LogicAware ){
            ((LogicAware) left).or_mutable(right);
            return left;
        }
        return UNSUPPORTED_OPERATION(left,right,"OR");
    }

    /**
     * The operator xor or symmetric delta
     * @param left left object
     * @param right right object
     * @return the and or intersection of left with right object
     */
    public Object xor(Object left, Object right) {
        if ( left instanceof Boolean ){
            return (boolean)left != ZTypes.bool(right,false);
        }
        if ( left instanceof Number ){
            ZNumber n = new ZNumber(left);
            return n._xor_(right);
        }
        left = customZConvert(left);
        if ( left instanceof LogicAware ){
            return  ((LogicAware) left)._xor_(right);
        }
        return UNSUPPORTED_OPERATION(left,right,"XOR");
    }

    /**
     * The operator xor or symmetric delta, mutably
     * @param left left object
     * @param right right object
     * @return the xor or symmetric delta of left with right object, mutably stored in left object
     */
    public Object xorMutable(Object left, Object right) {
        if ( left instanceof Boolean ){
            return (boolean)left != ZTypes.bool(right,false);
        }
        if ( left instanceof Number ){
            ZNumber n = new ZNumber(left);
            return n._xor_(right);
        }
        left = customZConvert(left);
        if ( left instanceof LogicAware ){
            ((LogicAware) left).xor_mutable(right);
            return left;
        }
        return UNSUPPORTED_OPERATION(left,right,"XOR");
    }

    /**
     * Negation of an object
     * @param o the object
     * @return negation of the object
     */
    public Number not(Object o) {
        if ( o instanceof ZNumber ) { return ((ZNumber) o)._not_() ; }
        if ( o instanceof Number ){
            ZNumber n = new ZNumber(o);
            return n._not_();
        }
        return UNSUPPORTED_OPERATION(o,o,"NEGATE");
    }

    /**
     * The modulo operation of left object modulo of right object
     * @param left left object
     * @param right right object
     * @return the modulo result of left modulo to right
     */
    public Number mod(Object left, Object right) {
        NULL_CHECK(left);
        NULL_CHECK(right);
        ZNumber l = new ZNumber(left);
        ZNumber r = new ZNumber(right);
        return l._mod_(r);
    }

    /**
     * The modulo operation of left object modulo of right object
     * @param left left object
     * @param right right object
     * @return the modulo result of left modulo to right, stored in the left
     */
    public Number modMutable(Object left, Object right) {
        if ( left instanceof ZNumber && right instanceof Number ){
            ((ZNumber) left).mod_mutable( (Number) right );
            return (Number) left;
        }
        NULL_CHECK(left);
        NULL_CHECK(right);
        ZNumber l = new ZNumber(left);
        ZNumber r = new ZNumber(right);
        return l._mod_(r);
    }

    /**
     * The easy division check  left object with respect to right
     * @param left left object
     * @param right right object
     * @return true if the left object is divisible by right object
     */
    public boolean divides(Object left, Object right){
        if ( !( left instanceof Number && right instanceof Number ) ) return false ;
        ZNumber l = new ZNumber( left );
        ZNumber r = new ZNumber( right );
        if ( l.fractional() || r.fractional() ) return true ; // mathematically, we are correct
        return ( r._mod_( l ).intValue() == 0 ) ;
    }

    /**
     * Left is in as a element in the right
     * @param left element
     * @param right container
     * @return true if left in right, false if not
     */
    public boolean in(Object left, Object right){
        if ( right == null ) return false ;
        if ( right.getClass().isArray() ){
            ZArray za = new ZArray(right,false);
            return za.contains( left );
        }
        if ( right instanceof ZRange ){
            right = ((ZRange) right).asList();
        }
        if ( right instanceof Map){
            return ((Map) right).containsKey(left);
        }
        if ( right instanceof Collection){
            return ((Collection) right).contains(left);
        }
        if ( left == null ) return false ;

        if ( right instanceof CharSequence ){
            return right.toString().contains( String.valueOf(left));
        }
        return false ;
    }

    /**
     * Left tuple starts with the right tuple
     * @param left left tuple
     * @param right right tuple
     * @return true if left starts with right, false if not
     */
    public boolean startsWith(Object left, Object right){
        if ( left == null ) return false ;
        if ( left instanceof Map ) return false ;
        if ( left instanceof Set) return false ;

        if ( left instanceof CharSequence ){
            if ( right == null ) return false ;
            return left.toString().startsWith( right.toString() );
        }
        if ( left.getClass().isArray() ){
            left = new ZArray(left,false);
        }
        if ( left instanceof List ){
            ZList zl = new ZList((List)left);
            return ( zl.indexOf(right) == 0 );
        }
        return false ;
    }

    /**
     * Left tuple ends with the right tuple
     * @param left left tuple
     * @param right right tuple
     * @return true if left ends with right, false if not
     */
    public boolean endsWith(Object left, Object right){
        if ( left == null ) return false ;
        if ( left instanceof Map ) return false ;
        if ( left instanceof Set) return false ;

        if ( left instanceof CharSequence ){
            if ( right == null ) return false ;
            return left.toString().endsWith( right.toString() );
        }
        if ( left.getClass().isArray() ){
            left = new ZArray(left,false);
        }
        if ( left instanceof List ){
            ZList zl = new ZList((List)left);
            return ( zl.lastIndexOf(right) == zl.size() - 1 );
        }
        return false ;
    }

    /**
     * Left tuple is a sub tuple in order within the right tuple
     * [1,2] in order [3,3,1,2,4]
     * but
     * [1,2] not in order [3,3,1,4,2]
     * That is, if left tuple is a sub tuple of the right tuple
     * @param left left tuple
     * @param right right tuple
     * @return true if left tuple is in order present within right, false if not
     */
    public boolean inOrder(Object left, Object right){
        if ( right == null ) return false ;
        if ( right instanceof Map ) return false ;
        if ( right instanceof Set) return false ;

        if ( right instanceof CharSequence ){
            return (left != null) &&
                    right.toString().contains( String.valueOf(left) );
        }
        if ( right.getClass().isArray() ){
            right = new ZArray(right,false);
        }
        if ( right instanceof List ){
            ZList zr = new ZList(right);
            return ( zr.indexOf(left) >= 0 );
        }
        return false ;
    }
}
