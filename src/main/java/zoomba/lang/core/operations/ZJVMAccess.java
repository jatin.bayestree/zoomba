/*
 * Copyright 2017 zoomba-lang.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package zoomba.lang.core.operations;

import zoomba.lang.core.collections.ZArray;
import zoomba.lang.core.collections.ZList;
import zoomba.lang.core.collections.ZMap;
import zoomba.lang.core.interpreter.ZScript;
import zoomba.lang.core.types.ZException;

import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.*;


/**
 * The one who connects ZoomBA to JVM
 */
public final class ZJVMAccess {


    private static final Map<String,Method>  classMethodMap = new HashMap<>();

    @SuppressWarnings("unchecked")
    private static void disableAccessWarnings() {
        /*
        https://stackoverflow.com/questions/46454995/how-to-hide-warning-illegal-reflective-access-in-java-9-without-jvm-argument
        * */
        try {
            Class unsafeClass = Class.forName("sun.misc.Unsafe");
            Field field = unsafeClass.getDeclaredField("theUnsafe");
            field.setAccessible(true);
            Object unsafe = field.get(null);

            Method putObjectVolatile = unsafeClass.getDeclaredMethod("putObjectVolatile", Object.class, long.class, Object.class);
            Method staticFieldOffset = unsafeClass.getDeclaredMethod("staticFieldOffset", Field.class);

            Class loggerClass = Class.forName("jdk.internal.module.IllegalAccessLogger");
            Field loggerField = loggerClass.getDeclaredField("logger");
            Long offset = (Long) staticFieldOffset.invoke(unsafe, loggerField);
            putObjectVolatile.invoke(unsafe, loggerClass, offset, null);
        } catch (Exception ignored) {
        }
    }

    static {
        disableAccessWarnings();
        Method[] methods = Class.class.getDeclaredMethods() ;
        for ( Method m : methods ){
            classMethodMap.put( m.getName(), m );
        }
    }

    /**
     * Given a type name returns the class type
     * @param type  type name
     * @return type class
     * @throws Exception when any error has occurred - for example class not found exception
     */
    public static Class findClass(String type) throws Exception {
        switch (type){
            case "int":
                return int.class;
            case "byte":
                return byte.class;
            case "short":
                return short.class;
            case "long":
                return long.class;
            case "float":
                return float.class;
            case "double":
                return double.class;
            case "char":
                return char.class;
            case "bool" :
                return boolean.class;
        }
        return Class.forName(type);
    }

    /**
     * Creates a dictionary out of an object as if it is a property bucket
     * @param o the object
     * @return a map with key fields and value the value of the fields
     */
    public static Map dict(Object o){
        if ( o == null ) return null  ;

        Map map = new HashMap();
        map.put("type", o.getClass().getName());
        map.put("hash", o.hashCode());
        Map mFields = new HashMap();
        map.put("fields", mFields );
        Field[] fields = o.getClass().getDeclaredFields();
        for ( Field f : fields ){
            f.setAccessible(true);
            String name = f.getName();
            try {
                Object value = f.get(o);
                mFields.put(name, new ZMap.Pair(f.getType().getName(), value) );
            }catch (Exception e){ }
        }
        Method[] methods = o.getClass().getDeclaredMethods();
        Set mMethods = new HashSet();
        map.put("methods", mMethods );
        for ( Method m : methods ){
            m.setAccessible(true);
            mMethods.add(m.toString());
        }
        return Collections.unmodifiableMap(map);
    }

    private static boolean isPrimitiveMatch(Class pType, Class aType){
        if ( pType == boolean.class && aType == Boolean.class ) return true;
        if ( Number.class.isAssignableFrom(aType) ){
            return  pType == int.class   ||  pType == long.class ||
                    pType == short.class ||  pType == byte.class ||
                    pType == float.class ||  pType == double.class ;
        }
        return ( pType == char.class && aType == Character.class );
    }

    private static boolean parameterMatches(Class[] pTypes, Object[] args){
        boolean match = true ;
        for ( int i = 0 ; i < args.length; i++ ){
            if ( args[i] == null ) continue; // legal
            Class pType = pTypes[i];
            Class aType = args[i].getClass();
            if ( pType.isAssignableFrom( aType ) ) continue;
            if ( pType.isPrimitive() && isPrimitiveMatch(pType,aType) ) continue ;
            match = false;
            break;
        }
        return match ;
    }

    /**
     * Constructs an object from arguments using constructor
     * @param classType the type of the object
     * @param args the constructor arguments
     * @return an instance of the classType
     */
    public static Object construct(Object classType, Object[] args){
        try{
            Class clazz = null;
            if ( classType instanceof CharSequence ){
                clazz = Class.forName(classType.toString());
            }
            if ( clazz == null ){
                if ( classType instanceof Class ){
                    clazz = (Class) classType ;
                }
            }
            if ( clazz == null )
                throw new UnsupportedOperationException("Unknown type to create : " + classType.getClass());

            if ( args == null || args.length == 0 ){
                // even if private :: I am no mood for privacy
                return clazz.getDeclaredConstructor().newInstance();
            }
            Constructor[] constructors = clazz.getDeclaredConstructors();
            for ( Constructor c : constructors ){
                try{
                    // violate every norm of OOPS!
                    c.setAccessible(true);
                }catch (Throwable t){}
                if ( args.length == c.getParameterCount() ){
                    // reasonable guess, now type match
                    Class[] pTypes =  c.getParameterTypes();
                    boolean match = parameterMatches(pTypes,args);
                    if ( match ) {
                        Object instance = c.newInstance(args);
                        return instance;
                    }
                }
            }
        }catch (Exception e){
            throw new RuntimeException(e);
        }
        throw new UnsupportedOperationException("args did not match any known constructor for class : " + classType.toString() );
    }

    private static void before(ZEventAware before, Method m, Object[] args){
        ZEventAware.ZEventArg event = new ZEventAware.ZEventArg( before, ZEventAware.When.BEFORE, m, args);
        try {
            before.before(event);
        }catch (Throwable t){

        }
    }

    private static void after(ZEventAware after, Method m, Object[] args, Object r, Throwable th){
        ZEventAware.ZEventArg event = new ZEventAware.ZEventArg( after, ZEventAware.When.AFTER, m, args,
                new Function.MonadicContainerBase(r),th);
        try {
            after.after(event);
        }catch (Throwable t){

        }
    }

    private static Object wrapForVarArgs(Class c, Object arr ) {
        if  ( arr == null || !arr.getClass().isArray() ){
            Object o = Array.newInstance( c.getComponentType(), 1 );
            Array.set(o, 0, arr);
            return o;
        }

        int len = Array.getLength(arr);
        Object o = Array.newInstance( c.getComponentType(), len );
        for ( int i = 0 ; i < len ; i++ ){
            Object item = Array.get( arr, i );
            Array.set(o, i, item);
        }
        return o;
    }

    /**
     * Calls a method on an object
     * @param instance the object, or a class type to call static method
     * @param name name of the method
     * @param args arguments to the method
     * @return result of the function call
     */
    public static Object callMethod(Object instance, String name, Object[] args){
        boolean methodFound = false;
        try{
            if ( instance == null )
                throw new UnsupportedOperationException("Can not call method on null! ");

            boolean isStatic = false ;
            Method[] methods ;
            Class clazz = null;
            if ( instance instanceof Class ){
                clazz = ((Class)instance);
                methods = ((Class)instance).getDeclaredMethods();
                isStatic = true ;
            }else{
                clazz = instance.getClass();
                methods = clazz.getDeclaredMethods();
            }

            while ( clazz != Object.class ){
                for ( Method m : methods ){
                    try{
                        m.setAccessible(true);
                    }catch (Throwable t){}

                    String mName = m.getName();
                    if ( mName.equals(name)  ){
                        methodFound = true ;
                        boolean isVarArgs = m.isVarArgs();
                        boolean proceed = args.length == m.getParameterCount()
                                || isVarArgs ;
                        if ( !proceed ) continue;

                        // reasonable guess, now type match
                        Class[] pTypes =  m.getParameterTypes();
                        boolean match = isVarArgs  || parameterMatches(pTypes,args) ;
                        if ( match ) {
                            if ( isVarArgs ){
                                int numPar = m.getParameterCount();
                                boolean wrapLast = args.length == 0 || (  (args.length) > 0 &&  numPar <= args.length ) ;
                                // last stuff must be wrapped inside Object[] so...
                                if ( wrapLast ) {
                                    if ( numPar > 1 ) {
                                        Object varArgs = args[args.length - 1];
                                        args[args.length - 1] =
                                                wrapForVarArgs( pTypes[pTypes.length-1], varArgs );
                                    }else{
                                        // fully variable length, then wrap into the
                                        args = new Object[]{wrapForVarArgs(pTypes[pTypes.length - 1], args)};
                                    }
                                }
                                else{
                                    // last param must be a empty Object[] ...
                                    Object[] newArgs = new Object[ args.length + 1];
                                    for ( int i = 0 ; i < args.length ; i++ ){
                                        newArgs[i] = args[i];
                                    }
                                    newArgs[newArgs.length-1] = ZArray.EMPTY_ARRAY ;
                                    args = newArgs ;
                                }
                            }
                            Object instanceObject = isStatic ? null : instance ;
                            if ( instanceObject instanceof ZEventAware ){
                                before((ZEventAware)instanceObject, m, args);
                            }
                            Object r = Function.NIL;
                            Throwable th = ZException.Return.RETURN ;
                            try {
                                r = m.invoke(instanceObject, args);
                                if (m.getReturnType() == Void.TYPE) {
                                    return (r = Function.Void);
                                }
                                return r;
                            }catch (Throwable t){
                                th = t ;
                              // for debugging we should keep it
                                //System.err.println(t);
                                throw t;
                            } finally {
                                if ( instanceObject instanceof ZEventAware ){
                                    after((ZEventAware)instanceObject, m, args,r,th.getCause());
                                }
                            }
                        }
                    }
                }
                clazz = clazz.getSuperclass();
                methods = clazz.getDeclaredMethods();
            }

        }catch (Exception e){
            throw new RuntimeException(e);
        }
        // in here, do we have methods to be invoked on a Class itself?
        if ( instance instanceof Class ){
            Method m = classMethodMap.get( name );
            if ( m != null ){
                try {
                    return m.invoke(instance, args);
                }catch (Exception e){}
            }
        }
        final String errorMessage;
        if ( methodFound ) {
            errorMessage = "args did not match any known method! : " ;
        } else {
            errorMessage = "no such method! : " ;
        }
        final String addendum ;
        if ( instance instanceof ZScript ){
            addendum = " : in Script : " + ((ZScript) instance).location();
        } else {
            addendum = " : in class : " + instance.getClass() ;
        }

        throw new NoSuchMethodError
                ( errorMessage + name + addendum );
    }

    /**
     * Invalid accessor
     */
    public final static Function.MonadicContainer INVALID_ACCESSOR = new Function.MonadicContainerBase();

    /**
     * Invalid property
     */
    public final static Function.MonadicContainer INVALID_PROPERTY = new Function.MonadicContainerBase();

    private static boolean VALID_ACCESS(Function.MonadicContainer c){
        return !c.isNil() ;
    }

    static Field getField(Object o, String prop){
        if ( o == null ) return null;
        if ( o instanceof Class ){
            if ( o == Object.class ) return null;
            Field[] fields = ((Class) o).getDeclaredFields();
            for ( Field f : fields ){
                f.setAccessible(true);
                if ( f.getName().equals(prop) ) return f;
            }
            return getField(((Class) o).getSuperclass(),prop);
        }
        return getField(o.getClass(),prop);
    }

    static Method getGetterMethod(Object o, String prop){
        if ( o == null ) return null;
        if ( o instanceof Class ){
            if ( o == Object.class ) return null;
            Method[] methods = ((Class) o).getDeclaredMethods();
            for ( Method m : methods ){
                m.setAccessible(true);
                if ( m.getName().equals(prop) && m.getParameterCount() == 0 ) return m;
            }
            return getGetterMethod(((Class) o).getSuperclass(),prop);
        }
        return getGetterMethod(o.getClass(),prop);
    }

    static Method getSetterMethod(Object o, String prop){
        if ( o == null ) return null;
        if ( o instanceof Class ){
            if ( o == Object.class ) return null;
            Method[] methods = ((Class) o).getDeclaredMethods();
            for ( Method m : methods ){
                m.setAccessible(true);
                if ( m.getName().equals(prop) && m.getParameterCount() > 0 ) return m;
            }
            return getSetterMethod(((Class) o).getSuperclass(),prop);
        }
        return getSetterMethod(o.getClass(),prop);
    }

    /**
     * Named Property
     */
    public interface NameProperty{

        /**
         * Gets the value
         * @param instance from the instance
         * @param propertyName of the property name
         * @return the value
         */
        Function.MonadicContainer getValue(Object instance, String propertyName);

        /**
         * Sets value
         * @param instance of the instance
         * @param propertyName name of the property
         * @param value the new value
         * @return Function.SUCCESS if successful else Function.FAILURE
         */
        Function.MonadicContainer setValue(Object instance, String propertyName, Object value);

    }

    final static class FieldAccess implements NameProperty {

        @Override
        public Function.MonadicContainer getValue(Object instance, String propertyName) {
            Field f = getField(instance,propertyName);
            if ( f == null ) return INVALID_PROPERTY ;
            try{
                return new Function.MonadicContainerBase( f.get(instance) ) ;
            }catch (Exception e){
                return INVALID_PROPERTY;
            }
        }

        @Override
        public Function.MonadicContainer setValue(Object instance, String propertyName, Object value) {
            Field f = getField(instance,propertyName);
            if ( f == null ) return INVALID_PROPERTY ;
            try{
                f.set(instance,value) ;
                return Function.SUCCESS ;
            }catch (Exception e){
                return Function.FAILURE ;
            }
        }
    }

    final static FieldAccess FIELD_ACCESS = new FieldAccess() ;

    final static class MethodAccess implements NameProperty {

        public static String doCamelCasing(String propertyName){
            return  Character.toUpperCase( propertyName.charAt(0) ) + propertyName.substring(1) ;
        }

        @Override
        public Function.MonadicContainer getValue(Object instance, String propertyName) {
            String methodName = "get" + doCamelCasing(propertyName);
            Method m = getGetterMethod(instance, methodName );
            if ( m == null ) {
                // perhaps it has a function instance.propertyName()?
                m = getGetterMethod(instance, propertyName );
                if ( m == null ) {
                    // perhaps boolean isXXX?
                    methodName = "is" + doCamelCasing(propertyName);
                    m = getGetterMethod(instance, methodName );
                    if ( instance instanceof Class && m == null ){
                        methodName = "get" + doCamelCasing(propertyName);
                        m = classMethodMap.get( methodName );
                    }
                    if ( m == null ) return INVALID_PROPERTY ;
                }
            }
            try{
                return new Function.MonadicContainerBase( m.invoke(instance) ) ;
            }catch (Exception e){
                return INVALID_PROPERTY;
            }
        }

        @Override
        public Function.MonadicContainer setValue(Object instance, String propertyName, Object value) {
            String methodName = "set" + doCamelCasing(propertyName);
            Method m = getSetterMethod(instance, methodName );
            if ( m == null ) {
                // perhaps it has a function instance.propertyName()?
                m = getSetterMethod(instance, propertyName );
                if ( m == null ) return INVALID_PROPERTY ;
            }
            try{
                Object o = m.invoke(instance,value) ;
                if ( Void.TYPE.equals(m.getReturnType() ) ){
                    return Function.SUCCESS ;
                }
                return new Function.MonadicContainerBase(o);
            }catch (Exception e){
                return Function.FAILURE ;
            }
        }
    }

    final static MethodAccess METHOD_ACCESS = new MethodAccess();

    /**
     * Indexed property - object index
     */
    public interface IndexProperty{

        /**
         * Gets the value
         * @param instance of the instance
         * @param index at the index
         * @return the value
         */
        Function.MonadicContainer getIndexedValue(Object instance, Object index);

        /**
         * Sets the value
         * @param instance of the instance
         * @param index at the index
         * @param value the new value
         * @return Function.SUCCESS if successful else Function.FAILURE
         */
        Function.MonadicContainer setIndexedValue(Object instance, Object index, Object value);
    }

    final static class PropertyBucket implements IndexProperty{

        public static Method getDuckGet(Object o){
            if ( o == null ) return null;
            if ( o instanceof Class ){
                if ( o == Object.class ) return null;
                Method[] methods = ((Class) o).getDeclaredMethods();
                for ( Method m : methods ){
                    m.setAccessible(true);
                    if ( m.getName().equals("get") && m.getParameterCount() == 1 ) return m;
                }
                return getDuckGet(((Class) o).getSuperclass());
            }
            return getDuckGet(o.getClass());
        }

        public static Method getDuckSet(Object o){
            if ( o == null ) return null;
            if ( o instanceof Class ){
                if ( o == Object.class ) return null;
                Method[] methods = ((Class) o).getDeclaredMethods();
                for ( Method m : methods ){
                    m.setAccessible(true);
                    if ( m.getName().equals("set") && m.getParameterCount() == 2 ) return m;
                }
                return getDuckSet(((Class) o).getSuperclass());
            }
            return getDuckSet(o.getClass());
        }


        @Override
        public Function.MonadicContainer getIndexedValue(Object instance, Object propertyName) {
            try{
                if ( instance == null ) return INVALID_PROPERTY ;
                if ( instance instanceof Map ) return MAP_ACCESS.getIndexedValue(instance,propertyName);
                Method m =  getDuckGet( instance );
                if ( m == null ) return INVALID_PROPERTY ;
                return new Function.MonadicContainerBase( m.invoke(instance, propertyName )  ) ;
            }catch (Exception e){
                return INVALID_PROPERTY;
            }
        }

        @Override
        public Function.MonadicContainer setIndexedValue(Object instance, Object propertyName, Object value) {
            try{
                if ( instance == null ) return INVALID_PROPERTY ;
                if ( instance instanceof Map ) return MAP_ACCESS.setIndexedValue(instance,propertyName,value );
                Method m = getDuckSet( instance );
                if ( m == null ) return INVALID_PROPERTY ;
                Object o = m.invoke(instance, propertyName, value) ;
                if ( Void.TYPE.equals( m.getReturnType() ) ){
                    return Function.SUCCESS ;
                }
                return new Function.MonadicContainerBase(o) ;
            }catch (Exception e){
                return Function.FAILURE ;
            }
        }
    }

    final static PropertyBucket PROPERTY_BUCKET = new PropertyBucket();

    final static class MapAccess implements IndexProperty{

        @Override
        public Function.MonadicContainer getIndexedValue(Object instance, Object index) {
            if ( !(instance instanceof Map )) return INVALID_ACCESSOR ;
            try {
                if (((Map) instance).containsKey(index)) // can throw exception in case of sorted maps
                    return new Function.MonadicContainerBase(((Map) instance).get(index));
            }catch (Throwable t){

            }
            return INVALID_PROPERTY ;
        }

        @Override
        public Function.MonadicContainer setIndexedValue(Object instance, Object index, Object value) {
            if ( !(instance instanceof Map )) return INVALID_ACCESSOR;
            Object o = ((Map) instance).put(index,value);
            return new Function.MonadicContainerBase(o) ;
        }
    }

    final static MapAccess MAP_ACCESS = new MapAccess();

    final static class ArrayAccess implements IndexProperty{

        @Override
        public Function.MonadicContainer getIndexedValue(Object instance, Object index) {
            if (! instance.getClass().isArray() ) return INVALID_ACCESSOR ;
            if ( index instanceof Integer ){
                try {
                    return new Function.MonadicContainerBase(Array.get(instance, (int) index));
                }catch (ArrayIndexOutOfBoundsException e){
                    try {
                        int len = Array.getLength(instance);
                        return new Function.MonadicContainerBase(Array.get(instance, len + (int) index));
                    }catch (ArrayIndexOutOfBoundsException e1){
                    }
                    return INVALID_PROPERTY ;
                }
            }

            if ( index != null && index.getClass().isArray() ){
                try{
                    int left = (int)((Object[])index)[0];
                    int right = (int)((Object[])index)[1];
                    int len = Array.getLength(instance);
                    if ( left < 0 ){ left += len ; }
                    if ( right < 0 ){ right += len ; }
                    if ( left <= right && left >=0 ){
                        // inclusive
                        Object[] arr = new Object[ right -left + 1 ];
                        for ( int i = left; i<=right; i++ ){
                            arr[i-left] = Array.get(instance, i);
                        }
                        return new Function.MonadicContainerBase( arr );
                    }

                }catch (Exception e){

                }
            }
            return INVALID_PROPERTY;
        }

        @Override
        public Function.MonadicContainer setIndexedValue(Object instance, Object index, Object value) {
            if (! instance.getClass().isArray() ) return INVALID_ACCESSOR ;
            if ( index instanceof Integer ){
                try {
                    Array.set(instance, (int) index, value);
                    return Function.SUCCESS;
                }catch (ArrayIndexOutOfBoundsException e){
                    try {
                        int len = Array.getLength(instance);
                        Array.set(instance, len + (int)index,value);
                        return Function.SUCCESS;
                    }catch (ArrayIndexOutOfBoundsException e1){
                    }
                    return INVALID_PROPERTY ;
                }
            }
            return INVALID_ACCESSOR ;
        }
    }

    private final static ArrayAccess ARRAY_ACCESS = new ArrayAccess();

    final static class ListAccess implements IndexProperty{

        @Override
        public Function.MonadicContainer getIndexedValue(Object instance, Object index) {
            if (! (instance instanceof List) ) return INVALID_ACCESSOR ;
            List myList = (List)instance;
            if ( index instanceof Integer ){
                try {
                    return new Function.MonadicContainerBase(myList.get((int) index));
                }catch (IndexOutOfBoundsException e){
                    if ( (int)index < 0 ){
                        try{
                            return new Function.MonadicContainerBase(myList.get(myList.size() + (int)index));
                        }catch (IndexOutOfBoundsException e1){ }
                    }
                    return INVALID_PROPERTY ;
                }
            }
            if ( index != null && index.getClass().isArray() ){
                try{
                    int left = (int)((Object[])index)[0];
                    int right = (int)((Object[])index)[1];
                    int len = myList.size() ;
                    if ( left < 0 ){ left += len ; }
                    if ( right < 0 ){ right += len ; }
                    List slice = new ZList();
                    if ( left <= right && left >=0 ){
                        // inclusive
                        for ( int i = left; i<=right; i++ ){
                            slice.add( myList.get(i) );
                        }
                        return new Function.MonadicContainerBase( slice );
                    }

                }catch (Exception e){

                }
            }
            return INVALID_PROPERTY;
        }

        @Override
        public Function.MonadicContainer setIndexedValue(Object instance, Object index, Object value) {
            if (! (instance instanceof List)  ) return INVALID_ACCESSOR ;
            List myList = (List)instance;
            if ( index instanceof Integer ){
                try {
                    Object o = myList.set((int) index, value);
                    return new Function.MonadicContainerBase(o);
                }catch (IndexOutOfBoundsException e){
                    if ( (int)index < 0 ){
                        try{
                            return new Function.MonadicContainerBase(myList.set(myList.size() + (int)index, value));
                        }catch (IndexOutOfBoundsException e1){ }
                    }
                    return INVALID_PROPERTY ;
                }
            }
            return INVALID_PROPERTY ;
        }
    }

    private final static ListAccess LIST_ACCESS = new ListAccess();

    /**
     * Gets the value of the property of the object instance
     * @param instance the object instance
     * @param property the name of the property
     * @return the property value in a container, or INVALID_PROPERTY
     */
    public static Function.MonadicContainer getProperty(Object instance, Object property){
        if ( instance == null ) throw new UnsupportedOperationException("Can not get property of null!");
        String prop = String.valueOf(property);
        switch ( prop ){
            case "hashCode" :
                return new Function.MonadicContainerBase( instance.hashCode() );
            case "class" :
                return new Function.MonadicContainerBase(instance.getClass());
            case "toString" :
                return new Function.MonadicContainerBase(instance.toString());
        }

        Function.MonadicContainer r ;
        if ( instance.getClass().isArray() ){
            r = ARRAY_ACCESS.getIndexedValue( instance, property );
            if ( VALID_ACCESS(r) ) return r;
            if ( "length".equals(property) ) return new Function.MonadicContainerBase( Array.getLength(instance));
        }
        boolean isMap = false ;
        if ( instance instanceof Map ){
            isMap = true ;
            r = MAP_ACCESS.getIndexedValue( instance, property );
            if ( VALID_ACCESS(r) ) return r;

            switch (prop){
                case "size" :
                case "length" :
                    return new Function.MonadicContainerBase( ((Map) instance).size() );
                case "keys" :
                case "keySet":
                    return new Function.MonadicContainerBase( ((Map) instance).keySet() );
                case "values" :
                    return new Function.MonadicContainerBase( ((Map) instance).values() );
                case "entries" :
                case "entrySet" :
                    return new Function.MonadicContainerBase( ((Map) instance).entrySet() );
            }

        }
        if ( instance instanceof List ){
            r = LIST_ACCESS.getIndexedValue( instance, property );
            if ( VALID_ACCESS(r) ) return r;
            switch (prop) {
                case "size":
                case "length":
                    return new Function.MonadicContainerBase(((List) instance).size());
            }
        }
        // for CharSequences
        if ( instance instanceof CharSequence ){
            String s = instance.toString();
            if ( property instanceof Integer ) {
                int index = (int) property;
                try {
                    return new Function.MonadicContainerBase(s.charAt(index));
                } catch (IndexOutOfBoundsException e) {
                    index += s.length(); // perhaps -ve index ?
                    try {
                        return new Function.MonadicContainerBase(s.charAt(index));
                    } catch (IndexOutOfBoundsException e1) {
                    }
                }
            }
            if ( property.getClass().isArray() ){
                try {
                    int start = (int) Array.get(property, 0);
                    int end = (int) Array.get(property, 1);
                    int len = s.length() ;
                    if ( start < 0 ) { start = len + start ; }
                    if ( end < 0 ) { end = len + end ; }
                    return new Function.MonadicContainerBase(s.substring(start, end + 1));
                }catch (Exception e){}
            }
        }
        // for Map.Entry
        if ( instance instanceof Map.Entry ){
            if ( property instanceof CharSequence ){
                switch ( property.toString() ){
                    case "k":
                    case "key":
                        return new Function.MonadicContainerBase(((Map.Entry) instance).getKey());
                    case "v":
                    case "value":
                        return new Function.MonadicContainerBase(((Map.Entry) instance).getValue());
                }
            }
            if ( property instanceof Number ){
                switch ( ((Number) property).intValue() ){
                    case 0:
                        return new Function.MonadicContainerBase(((Map.Entry) instance).getKey());
                    case 1:
                        return new Function.MonadicContainerBase(((Map.Entry) instance).getValue());
                }
            }
        }

        // now try getter/setter
        r = METHOD_ACCESS.getValue(instance,prop);
        if ( VALID_ACCESS(r)) return r;
        if ( !isMap ) {
            // try Bucket Access only when NOT map?
            r = PROPERTY_BUCKET.getIndexedValue(instance, property);
            if (VALID_ACCESS(r)) return r;
        }
        // and now, go back to field access
        r = FIELD_ACCESS.getValue(instance,prop);
        if ( VALID_ACCESS(r)) return r;
        // nothing... hence...
        return INVALID_PROPERTY ;
    }

    /**
     * Sets the value of the property of the object instance
     * @param instance the instance of the object
     * @param property name of the property
     * @param value the value we want to set
     * @return Function.SUCCESS or INVALID_PROPERTY
     */
    public static Function.MonadicContainer setProperty(Object instance, Object property, Object value){
        if ( instance == null ) throw new UnsupportedOperationException("Can not set property of null!");
        if ( instance.getClass().isArray() ){
            return ARRAY_ACCESS.setIndexedValue(instance,property,value);
        }
        if ( instance instanceof List ){
            return LIST_ACCESS.setIndexedValue(instance,property,value);
        }
        if ( instance instanceof Map ){
            return MAP_ACCESS.setIndexedValue(instance,property,value);
        }
        String prop = String.valueOf(property);

        Function.MonadicContainer r = METHOD_ACCESS.setValue(instance,prop,value);
        if ( VALID_ACCESS(r) ) return r;

        r = PROPERTY_BUCKET.setIndexedValue(instance,property,value);
        if ( VALID_ACCESS(r) ) return r;

        r = FIELD_ACCESS.setValue(instance,prop,value);
        if ( VALID_ACCESS(r) ) return r;

        return INVALID_PROPERTY ;
    }
}
