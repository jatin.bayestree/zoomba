/*
 * Copyright 2017 zoomba-lang.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package zoomba.lang.core.operations;

import zoomba.lang.core.collections.BaseZCollection;
import zoomba.lang.core.collections.ZArray;
import zoomba.lang.core.io.ZFileSystem;
import zoomba.lang.core.io.ZTextDataFile;
import zoomba.lang.core.io.ZXlsxDataFile;
import zoomba.lang.core.types.ZException;
import zoomba.lang.core.collections.ZList;
import zoomba.lang.core.types.ZRange;
import zoomba.lang.core.types.ZTypes;
import java.io.PrintStream;
import java.util.*;
import java.util.regex.Pattern;

/**
 * A matrix implementation
 */
public interface ZMatrix extends Collection {

    /**
     * Selecting all requires this function
     */
    Function ALL = Function.TRUE ;

    /**
     * Selecting none will require this function
     */
    Function NONE = Function.FALSE ;

    /**
     * Default separator for the values
     */
    String SPACING = "\u00F8\u00D8" ;

    /**
     * A Tuple class, name value pair with integer indexing
     */
    final class Tuple implements Map{

        static final Tuple EMPTY = new Tuple( ZArray.EMPTY_ARRAY );

        Map<String,Integer> columns;

        List values;

        final int size;

        /**
         * Creates a tuple from arguments
         * @param args the arguments
         * @return a tuple
         */
        public static Tuple tuple(Object...args){

            switch (args.length){
                case 0 :
                    return EMPTY;
                case 1 :
                    return new Tuple(args[0]);
                case 2:
                    if ( args[1] instanceof List ){
                        if ( args[0] instanceof Map ) {
                            return new Tuple((Map) args[0], (List) args[1]);
                        }
                        if ( args[0] instanceof List ) {
                            return new Tuple((List) args[0], (List) args[1]);
                        }
                    }
                default:
                    return new Tuple( args );
            }
        }

        Tuple(Object o){
            if ( o == null )
                throw new IllegalArgumentException("Can not make tuple with null");
            if ( o instanceof List ){
                values = (List)o;
            } else if ( o.getClass().isArray() ){
                values = new ZArray( o, false );
            } else {
                values = Arrays.asList(o);
            }
            size = values.size();
            columns = new HashMap<>();
            for ( int i = 0 ; i < size; i++ ){
                columns.put(String.valueOf(i),i);
            }
        }


        Tuple(Map<String,Integer> columns, List values){
            size = columns.size();
            if ( size != values.size() )
                throw new UnsupportedOperationException("Can not make tuple with non matching name/value sizes :" +
                        ZTypes.jsonString(columns) + "->" + ZTypes.jsonString(values) );
            this.columns = columns ;
            this.values = values ;
        }

        Tuple(List<String> columns, List values){
            size = columns.size();
            if ( size != values.size() )
                throw new UnsupportedOperationException("Can not make tuple with non matching name/value sizes :" +
                        ZTypes.jsonString(columns) + "->" + ZTypes.jsonString(values) );
            this.columns = new HashMap<>() ;
            this.values = values ;
            for ( int i = 0; i < size; i++ ){
                this.columns.put(columns.get(i),i);
            }
        }

        /**
         * Project operation on Tuple
         * @param columns given the columns
         * @return a list of values corresponding to the columns
         */
        public List project(Object...columns){
            if ( columns.length == 0 ) return Collections.EMPTY_LIST ;
            Object[] p = new Object[ columns.length ];
            for ( int i = 0 ; i < columns.length ;i++ ){
                p[i] = get( columns[i] );
            }
            return new ZArray(p,false);
        }

        @Override
        public void clear() {

        }

        @Override
        public int size() {
            return size ;
        }

        @Override
        public boolean isEmpty() {
            return values.isEmpty() ;
        }

        @Override
        public boolean containsKey(Object key) {
            if ( key instanceof Integer ){
                int i = (int)key;
                return  ( i >=0 && i < size ) ;
            }
            if ( key instanceof String ){
                return columns.containsKey(key);
            }
            return false ;
        }

        @Override
        public boolean containsValue(Object value) {
            return values.contains(value );
        }

        @Override
        public Object get(Object key) {
            if ( key instanceof Integer ){
                int i = (int)key;
                if  ( i >=0 && i < size ) {
                    return values.get(i);
                }
                return Function.NIL;
            }
            if ( key instanceof String ){
                if ( columns.containsKey(key ) ) return values.get( columns.get(key) );
                return Function.NIL;
            }
            return Function.NIL;
        }

        @Override
        public Object put(Object key, Object value) {
            return Function.NIL ;
        }

        @Override
        public Object remove(Object key) {
            return Function.NIL;
        }

        @Override
        public void putAll(Map m) {

        }

        @Override
        public Set keySet() {
            return columns.keySet() ;
        }

        @Override
        public Collection values() {
            return values;
        }

        @Override
        public Set<Entry> entrySet() {
            // wrong, will fix later..
            return Collections.emptySet() ;
        }

        @Override
        public boolean equals(Object obj) {
            if ( obj == null ) return false ;
            Tuple o ;
            if ( obj instanceof Tuple ) {
                o = (Tuple)obj;
            } else {
                o = new Tuple(obj);
            }
            // size mismatch ?
            if ( o.size != size ) return false ;
            // order mismatch ?
            for ( int i = 0 ; i < size; i++ ){
                Object her = o.get(i);
                Object mine = get(i);
                if ( !mine.equals(her) ) return false ;
            }
            return true;
        }

        @Override
        public String toString() {
            return String.format( "< %s >" , this.values );
        }
    }

    /**
     * An iterator over matrix
     * One row at a time
     */
    final class MatrixIterator implements Iterator{

        final ZMatrix matrix ;

        int pos;

        /**
         * Given a matrix creates an iterator
         * @param matrix the matrix
         */
        public MatrixIterator(ZMatrix matrix){
            this.matrix = matrix ;
            pos = 0 ;
        }

        @Override
        public boolean hasNext() {
            return pos + 1 < matrix.rows();
        }

        @Override
        public Object next() {
            try {
                return matrix.tuple(++pos);
            }catch (UnsupportedOperationException ex){
                throw new UnsupportedOperationException( "At Matrix Data Row : " + pos + " : " +  ex.getMessage());
            }
        }
    }

    /**
     * Gets the columns of the matrix, mapping between Column and integer
     * @return the map
     */
    Map<String,Integer> columns();

    /**
     * Number of rows in the Matrix
     * @return the number of rows
     */
    int rows();

    /**
     * Creates a tuple corresponding the row
     * @param rowNum the row number
     * @return the tuple
     */
    Tuple tuple(int rowNum);

    /**
     * Selecting from a matrix and then projecting the columns
     * @param f the selection function
     * @param columns the column identifiers
     * @return a list of selected tuples
     */
    List select(Function f, Object...columns);

    /**
     * Finds the row where the predicate function is true
     * @param f the predicate function
     * @return the row index of the match
     */
    int index(Function f);

    /**
     * Generate keys for the matrix
     * @param f the function to generate the key
     * @return a mapping of key with list of rows where they have the same key
     */
    Map<String,List<Integer>> keys(Function f);

    /**
     * Generate keys for the matrix
     * @param columns the columns to select to generate the key
     * @return a mapping of key with list of rows where they have the same key
     */
    Map<String,List<Integer>> keys(Object...columns);

    /**
     * A basic matrix implementation
     */
    final class Matrix extends BaseZMatrix{

        List headers;

        List<List> data;

        public Matrix(List headers, List data){
            this.headers = headers ;
            this.data = data ;
        }

        @Override
        public List row(int rowNum) {
            if ( rowNum == 0 ) return headers;
            return data.get(rowNum - 1 );
        }

        @Override
        public int rows() {
            return data.size() + 1 ;
        }

        @Override
        public String toString() {
            return String.format( "Matrix: [ %s , rows %d ]" , headers , rows() );
        }
    }

    /**
     * An abstract base class for other matrices to implement
     */
    abstract class BaseZMatrix implements ZMatrix{

        /**
         * Gets the default headers
         * @param colSize given the size of the rows
         * @return indices from 1, in the form _1, _2, ....
         */
        public static List<String> defaultHeaders(int colSize){
            List<String> headers = new ArrayList<>();
            for ( int i = 1; i <=colSize ; i++){
                headers.add("_" + String.valueOf(i));
            }
            return headers;
        }

        /**
         * Given a row index get the row data
         * @param rowNum the index of the row
         * @return the row data as list
         */
        public abstract List row(int rowNum);

        protected Map<String,List<Integer>> keys;

        private Map<String,Integer> columns;

        /**
         * Gets the headers as a list
         * @return the headers
         */
        public List<String> headers(){
            return row(0);
        }

        @Override
        public Map<String, Integer> columns() {
            if ( columns == null ){
                columns = new HashMap<>();
                List<String> data = headers();
                for ( int i = 0 ; i < data.size(); i++ ){
                    columns.put( data.get(i), i);
                }
                if ( data.size() != columns.size() ){
                    throw new UnsupportedOperationException("Duplicate Column Names!");
                }
            }
            return columns ;
        }

        @Override
        public Map<String, List<Integer>> keys(Object... columns) {
            if ( keys != null ) keys.clear();
            keys = new HashMap<>();
            if ( columns.length == 1 && columns[0] instanceof Function ){
                return keys((Function)columns[0]);
            }
            if ( columns.length == 0 ){
                int size = this.columns().size();
                // we must now use all the columns...
                columns = new Object[ size ];
                for ( int i = 0 ; i < size; i++ ){ columns[i] = i ;}
            }

            Iterator i = iterator();
            StringBuilder buf = new StringBuilder();
            int rowNum = 1 ;
            while ( i.hasNext() ){
                Tuple t = (Tuple)i.next();
                for ( Object c : columns ){
                    buf.append( t.get(c)).append(SPACING);
                }
                String key = buf.toString();
                buf.setLength(0);
                if ( keys.containsKey(key ) ){
                    List l = keys.get(key);
                    l.add( rowNum );
                }else{
                    List l = new ArrayList();
                    l.add(rowNum);
                    keys.put(key,l);
                }
                rowNum++;
            }
            return keys;
        }

        @Override
        public Map<String, List<Integer>> keys(Function f) {
            if ( keys != null ) keys.clear();
            keys = new HashMap<>();
            int index = 1 ;
            Iterator i = iterator() ;
            while ( i.hasNext() ){
                Object o = i.next();
                Function.MonadicContainer result = f.execute(index,o,this,keys);
                if ( !result.isNil() ){
                    String key = String.valueOf(result.value());
                    if ( keys.containsKey(key ) ){
                        List l = keys.get(key);
                        l.add( index );
                    }else{
                        List l = new ArrayList();
                        l.add(index);
                        keys.put(key,l);
                    }
                }
                if ( result instanceof ZException.Break) return keys ;
                index++;
            }
            return keys;
        }

        @Override
        public int index(Function f) {
            return BaseZCollection.leftIndex( this, f );
        }

        @Override
        public List select(Function f,Object...columns) {
            boolean shouldProject = ( columns.length != 0 ) ;
            int index = 1 ;
            ZList into = new ZList();
            Iterator i = iterator() ;
            while ( i.hasNext() ){
                Object o = i.next();
                Function.MonadicContainer result = f.execute(index,o,this,into);
                if ( !result.isNil() && ZTypes.bool(result.value() ) ){
                    List l ;
                    if ( shouldProject ){
                        l = ((Tuple)o).project(columns);
                    }else{
                        l = ((Tuple)o).values ;
                    }
                    into.add(l);
                }
                if ( result instanceof ZException.Break) return into;
                index++;
            }
            return into;
        }

        /**
         * Given columns projects and create another sub matrix
         * @param columns the column identifiers
         * @return a sub matrix
         */
        public ZMatrix project(Object...columns){
            return sub( ALL, columns );
        }

        /**
         * SubMatrix from current using predicate function and columns
         * @param f given predicate selected function
         * @param columns column identifiers
         * @return a sub matrix
         */
        public ZMatrix sub(Function f, Object...columns){
            List<List> data = select(f,columns);
            Tuple ht = new Tuple( columns() , headers() );
            return new Matrix( ht.project( columns ) , data );
        }

        @Override
        public Tuple tuple(int rowNum) {
            List row = row(rowNum);
            Map<String,Integer> columns = columns();
            return new Tuple(columns,row);
        }

        @Override
        public boolean add(Object o) {
            return false;
        }

        @Override
        public boolean addAll(Collection c) {
            return false;
        }

        @Override
        public void clear() {

        }

        @Override
        public boolean contains(Object o) {
            return false;
        }

        @Override
        public boolean containsAll(Collection c) {
            return false;
        }

        @Override
        public boolean isEmpty() {
            // header is not counted in...
            return rows() < 2 ;
        }

        @Override
        public Iterator iterator() {
            return new MatrixIterator(this);
        }

        @Override
        public boolean remove(Object o) {
            return false;
        }

        @Override
        public boolean removeAll(Collection c) {
            return false;
        }

        @Override
        public boolean retainAll(Collection c) {
            return false;
        }

        @Override
        public int size() {
            return rows() ;
        }

        @Override
        public Object[] toArray() {
            return new Object[0];
        }

        @Override
        public Object[] toArray(Object[] a) {
            return new Object[0];
        }

        /**
         * Given a file, and a separator writes back to the file
         * @param file the file path
         * @param sep the column value separator
         */
        public void write(String file,String sep){
            PrintStream ps = (PrintStream)ZFileSystem.open(file,"w");
            for ( int i = 0 ; i < size() - 1 ; i++ ){
                ps.printf("%s\n", ZTypes.string( row(i), sep) );
            }
            ps.close();
        }

        /**
         * Writes to a file path with default separator tab
         * @param file the file path
         */
        public void write(String file){
            write(file,"\t");
        }
    }

    /**
     * An empty matrix
     */
    final class EmptyMatrix extends BaseZMatrix{

        @Override
        public List row(int rowNum) {
            return Collections.EMPTY_LIST ;
        }

        private EmptyMatrix(){}

        @Override
        public Map<String,Integer> columns() {
            return Collections.EMPTY_MAP  ;
        }

        @Override
        public int rows() {
            return 0;
        }


        @Override
        public List select(Function f, Object...columns) {
            return Collections.EMPTY_LIST;
        }

        @Override
        public int index(Function f) {
            return -1;
        }

        @Override
        public Iterator iterator() {
            return Collections.EMPTY_LIST.iterator();
        }

        @Override
        public String toString() {
            return "EmptyMatrix{}";
        }

    }

    /**
     * Single instance of an empty matrix
     */
    EmptyMatrix EMPTY_MATRIX = new EmptyMatrix();

    /**
     * Loader for matrices
     */
    interface ZMatrixLoader{

        /**
         * Loads when the pattern matches
         * @return the matching pattern
         */
        Pattern loadPattern();

        /**
         * Load a matrix from
         * @param path the path
         * @param args using the arguments
         * @return a matrix
         */
        ZMatrix load(String path, Object...args);

    }

    /**
     * A Loader implementation
     */
    final class Loader{

        /**
         * Loader registry
         */
        public static final Map<Pattern,ZMatrixLoader> loaders = new HashMap<>( );

        static {
            // some hard coding -- bad later should go to config
            add( ZTextDataFile.ZTextDataFileLoader.LOADER  );
            add(ZXlsxDataFile.XlsxDataLoader.LOADER);
        }

        /**
         * Adds a loader to the mix
         * @param l the loader
         * @return true if could add, false if did not or pattern already registered
         */
        public static boolean add(ZMatrixLoader l){
            Pattern p = l.loadPattern();
            if ( loaders.containsKey(p) ) return false ;
            loaders.put(p,l);
            return true;
        }

        /**
         * Removes a loader from the mix
         * @param l the loader
         * @return true if could remove, false if did not or pattern is not registered
         */
        public static boolean remove(ZMatrixLoader l){
            Pattern p = l.loadPattern();
            if ( !loaders.containsKey(p) ) return false ;
            loaders.remove(p);
            return true;
        }

        /**
         * Loads a matrix from a path
         * @param path the path
         * @param args arguments to the matrix
         * @return a matrix
         */
        public static ZMatrix load(String path, Object...args){
            for ( Pattern p : loaders.keySet() ){
                if ( p.matcher(path).matches() ){
                    ZMatrixLoader loader = loaders.get(p);
                    return loader.load(path,args);
                }
            }
            return EMPTY_MATRIX;
        }

        /**
         * loads a Matrix
         * @param args the arguments
         * @return a matrix
         */
        public static ZMatrix load(Object...args){
            if ( args.length == 0 ) return EMPTY_MATRIX;
            if ( args[0] instanceof List ){
                // Load appropriate from data
                if ( args.length > 1 ){
                    return new Matrix( (List)args[0], (List)args[1] );
                }
                List<List> all = ((List<List>)args[0]);
                List headers = all.get(0);
                List<List> data  = all.subList(1,all.size());
                return new Matrix( headers, data );
            }
            String path = String.valueOf( args[0] );
            args = ZTypes.shiftArgsLeft(args);
            return load(path,args);
        }
    }

}
