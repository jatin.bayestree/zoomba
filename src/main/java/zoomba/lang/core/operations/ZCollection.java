/*
 * Copyright 2017 zoomba-lang.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package zoomba.lang.core.operations;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * The Interface for All Collections in ZoomBA
 */
public interface ZCollection extends Collection , Comparable {

    /**
     * The relationship that may exist between two collections
     */
    enum Relation{
        /**
         * As in Sub Set relation
         */
        SUB,
        /**
         * As in Super Set relation
         */
        SUPER,

        /**
         * As in Overlap Set relation - some Intersection exists
         */
        OVERLAP,

        /**
         * As in collections are equal
         */
        EQUAL,

        /**
         * As in NO Overlap - No Intersection exists
         */
        INDEPENDENT
    }

    /**
     * Converts the Collection into a MultiSet
     * which is ( key , List of objects with same key )
     * @param hash using the hash function
     * @return the multiset -
     */
    Map<Object,List> toMultiSet(Function hash);

    /**
     * Converts the Collection into a MultiSet
     * which is ( key , List of objects with same key )
     * Then apply the apply function for each (key,value)
     * To generate the value for the Map
     * @param hash hash function
     * @param apply apply function
     * @return the multiset - with key from hash,
     *     value from the result of the applied function
     */
    Map<Object,Object> groupBy(Function hash, Function apply);

    /**
     * Converts the collection into a set using a key hash function
     * @param hash the function
     * @return the set
     */
    Set toSet(Function hash);

    /**
     * Use the default hashCode() as the function to convert
     * the collection into a multiset
     * @return the multiset
     */
    Map<Object,Integer> toMultiSet();

    /**
     * Use the default hashCode() as the function to convert
     * the collection into a set
     * @return the set
     */
    Set toSet();

    /**
     * Relates this against other collection
     * @param c the other collection
     * @return the relation
     */
    Relation relate(Collection c);

    /**
     * Unions this with the other collection
     * @param c the other collection
     * @return the Union
     */
    ZCollection union(Collection c);

    /**
     * Intersection of this with the other collection
     * @param c the other collection
     * @return the Intersection
     */
    ZCollection intersection(Collection c);

    /**
     * Difference of this with the other collection
     * @param c the other collection
     * @return the Difference
     */
    ZCollection difference(Collection c);

    /**
     * Cartesian Product of this with the other collection
     * @param c the other collection
     * @return the Product
     */
    ZCollection product(Collection c);

    /**
     * Cartesian Product of this with the other collections
     * @param cc the other collections
     * @return the Product
     */
    ZCollection join(Collection... cc);

    /**
     * Cartesian Product of this with the other collections when
     * the predicate condition is true
     * @param predicate the condition function
     * @param cc the other collections
     * @return the Product
     */
    ZCollection join(Function predicate, Collection... cc);

    /**
     * Cartesian Product of this with the other collections when
     * the predicate condition is true and them maps the result using the mapper
     * @param predicate the condition function
     * @param map the mapper function
     * @param cc the other collections
     * @return the Product
     */
    ZCollection join(Function predicate, Function map, Collection... cc);

    /**
     * FoEach item maps the item using the map, create a newer collection
     * @param map the mapper function
     * @return a new collection of same type
     */
    ZCollection map(Function map);

    /**
     * FoEach item maps the item using the map, create a newer collection
     * which will not have any nested collection
     * @param map the mapper function
     * @return a new collection of same type
     */
    ZCollection flatMap(Function map);

    /**
     * FoEach item maps the item using the map, create a newer collection
     * using Identity as the map
     * which will not have any nested collection
     * @return a new collection of same type
     */
    ZCollection flatten();

    /**
     * Apply the function apply for each element
     * @param apply the function
     */
    void forEach( Function apply );

    /**
     * Folds left, with arguments
     * @param fold the fold function
     * @param arg args, only one will be taken if multiple are provided
     * @return the result of the fold
     */
    Object leftFold( Function fold , Object... arg);

    /**
     * Folds right, with arguments
     * @param fold the fold function
     * @param arg args, only one will be taken if multiple are provided
     * @return the result of the fold
     */
    Object rightFold( Function fold , Object... arg);

    /**
     * Reduces from left, initial argument being the first item of the collection
     * @param reduce the reduction function
     * @return the result of the reduction
     */
    Object leftReduce( Function reduce);

    /**
     * Reduces from right, initial argument being the last item of the collection
     * @param reduce the reduction function
     * @return the result of the reduction
     */
    Object rightReduce( Function reduce);

    /**
     * Partitions of the collection using predicate function
     * selected items goes in the 0th item of an array
     * rejected item goes in the 1st item of an array
     * @param predicate the function
     * @return an array of size 2 [selected,rejected]
     */
    ZCollection[] partition( Function predicate );

    /**
     * Selects items of the collection using predicate function
     * @param predicate the function
     * @return an selected items
     */
    ZCollection select( Function predicate );

    /**
     * Returns index of the location in the collection from left where
     * a condition happened defined in the
     * @param predicate function
     * @return the index, -1 when match not found
     */
    int leftIndex(Function predicate);

    /**
     * Returns index of the location in the collection from right where
     * a condition happened defined in the
     * @param predicate function
     * @return the index, -1 when match not found
     */
    int rightIndex(Function predicate);

    /**
     * The item in the collection from left where
     * a condition happened defined in the
     * @param predicate function
     * @return the item, or nil value
     */
    Function.MonadicContainer find(Function predicate);

    /**
     * Reverses the collection
     * @return returns the reversed collection
     */
    ZCollection reverse();

    /**
     * Serializes the collection in a string using the separator
     * @param separator the separator
     * @return A string representation of the collection
     */
    String string(String separator);

    /**
     * Compose from a collection using multitudes of functions
     * @param functions map and predicate functions chained order
     * @return the final result, which will be another collection
     */
    ZCollection compose(Function... functions);

    /**
     * Converts the collection into an array of type[]
     * @param type the type, either a class or a string
     * @return an array of type[]
     */
    Object array(Object type);
}
