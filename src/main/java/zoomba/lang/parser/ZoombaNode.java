package zoomba.lang.parser;

/**
 */
public class ZoombaNode extends SimpleNode{

    protected String sourceLocation;

    public String sourceLocation(){
        if ( sourceLocation == null ){
            if ( parent == null )return  null;
            return parent.sourceLocation();
        }
        return sourceLocation;
    }

    public void sourceLocation(String sourceLocation){
        this.sourceLocation = sourceLocation;
    }

    /** A marker interface for literals.
     * @param <T> the literal type
     */
    public interface Literal<T> {
        T getLiteral();
    }
    /** token value. */
    public String image;

    public ZoombaNode(int id) {
        super(id);
    }

    public ZoombaNode(Parser p, int id) {
        super(p, id);
    }

    public String locationInfo(){
        Token t1 = jjtGetFirstToken();
        Token t2 = jjtGetLastToken();
        String locInfo = "" ;
        if ( t1.beginLine != t2.endLine ) {
            locInfo = String.format("at line %d, col %d to line %d, col %d",
                    t1.beginLine, t1.beginColumn, t2.endLine, t2.endColumn);
        }
        else{
            locInfo = String.format("at line %d, cols %d:%d",
                    t1.beginLine, t1.beginColumn,t2.endColumn);
        }
        return String.format(" --> %s %s", sourceLocation(), locInfo);
    }

    public String body(String scriptBody){
        String[] lines = scriptBody.split("\n");
        if ( firstToken.beginLine == lastToken.endLine ){
            return lines[firstToken.beginLine-1].substring(firstToken.beginColumn-1, lastToken.endColumn );
        }
        String startLine = lines[firstToken.beginLine-1].substring( firstToken.beginColumn -1) ;
        StringBuilder builder = new StringBuilder();
        builder.append(startLine);
        for ( int i = firstToken.beginLine ; i < lastToken.endLine - 1; i++ ){
            builder.append("\n").append( lines[i] );
        }
        String endLine = lines[lastToken.endLine-1].substring( 0, lastToken.endColumn ) ;
        builder.append("\n").append( endLine);
        return builder.toString();
    }

    /**
     * Whether this node is a constant node
     * Its value can not change after the first evaluation and can be cached indefinitely.
     * @return true if constant, false otherwise
     */
    public final boolean isConstant() {
        return isConstant(this instanceof ZoombaNode.Literal<?>);
    }

    protected boolean isConstant(boolean literal) {
        if (literal) {
            if (children != null) {
                for (ZoombaNode child : children) {
                    if (child instanceof ASTReference) {
                        boolean is = child.isConstant(true);
                        if (!is) {
                            return false;
                        }
                    } else if (child instanceof ASTMapEntry) {
                        boolean is = child.isConstant(true);
                        if (!is) {
                            return false;
                        }
                    } else if (!child.isConstant()) {
                        return false;
                    }
                }
            }
            return true;
        }
        return false;
    }
}
