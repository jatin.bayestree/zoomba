/*
 * Copyright 2019 zoomba-lang.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package zoomba.lang.scripting;

import zoomba.lang.core.interpreter.ZContext;

import javax.script.Bindings;
import javax.script.ScriptContext;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.Writer;
import java.util.*;

public class ZScriptContext implements ScriptContext {

    private final Map<Integer,Bindings> bindingsMap = new HashMap<>();

    @Override
    public void setBindings(Bindings bindings, int scope) {
       bindingsMap.put(scope,bindings);
    }

    @Override
    public Bindings getBindings(int scope) {
        return bindingsMap.get(scope);
    }

    @Override
    public void setAttribute(String name, Object value, int scope) {
       Bindings bindings = getBindings(scope);
       if ( bindings == null ) {
           bindings = new ZContext.MapContext();
           bindingsMap.put(scope,bindings);
       }
       bindings.put(name,value);
    }

    @Override
    public Object getAttribute(String name, int scope) {
        Bindings bindings = getBindings(scope);
        if ( bindings == null ) return null;
        return bindings.get(name);
    }

    @Override
    public Object removeAttribute(String name, int scope) {
        Bindings bindings = getBindings(scope);
        if ( bindings == null ) return null;
        return bindings.remove(name);
    }

    @Override
    public Object getAttribute(String name) {
        for ( int key : bindingsMap.keySet() ){
            Bindings bindings = bindingsMap.get(key);
            if ( bindings.containsKey(name) ) return bindings.get(name);
        }
        return null;
    }

    @Override
    public int getAttributesScope(String name) {
        for ( int key : bindingsMap.keySet() ){
            Bindings bindings = bindingsMap.get(key);
            if ( bindings.containsKey(name) ) return key;
        }
        return -1;
    }

    @Override
    public Writer getWriter() {
        return new PrintWriter(System.out) ;
    }

    @Override
    public Writer getErrorWriter() {
        return new PrintWriter(System.err) ;
    }

    @Override
    public void setWriter(Writer writer) {

    }

    @Override
    public void setErrorWriter(Writer writer) {

    }

    @Override
    public Reader getReader() {
        return null;
    }

    @Override
    public void setReader(Reader reader) {

    }

    @Override
    public List<Integer> getScopes() {
        return new ArrayList<>( bindingsMap.keySet() );
    }
}
