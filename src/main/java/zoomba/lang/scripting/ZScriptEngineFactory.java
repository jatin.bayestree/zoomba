/*
 * Copyright 2019 zoomba-lang.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package zoomba.lang.scripting;

import zoomba.lang.core.collections.ZArray;
import zoomba.lang.core.types.ZTypes;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineFactory;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ZScriptEngineFactory implements ScriptEngineFactory {

    private static List<String> immutableList(String... elements) {
        return Collections.unmodifiableList(Arrays.asList(elements));
    }

    private static final List<String> names = immutableList("zoomba", "ZoomBA", "zm", "ZM", "ZMB", "zmscript", "zoombascript");
    private static final List<String> mimeTypes = immutableList("application/zoombascript", "application/zmscript", "text/zoombascript");
    private static final List<String> extensions = immutableList("zm", "zmb", "zz", "z" );

    static final ZScriptEngineFactory INSTANCE = new ZScriptEngineFactory();

    private static final String VERSION = "0.1-beta5" ;

    @Override
    public String getEngineName() {
        return "ZoomBA";
    }

    @Override
    public String getEngineVersion() {
        return VERSION;
    }

    @Override
    public List<String> getExtensions() {
        return extensions;
    }

    @Override
    public List<String> getMimeTypes() {
        return mimeTypes;
    }

    @Override
    public List<String> getNames() {
        return names;
    }

    @Override
    public String getLanguageName() {
        return "zoomba";
    }

    @Override
    public String getLanguageVersion() {
        return VERSION;
    }

    @Override
    public Object getParameter(String key) {
        return null;
    }

    @Override
    public String getMethodCallSyntax(String obj, String m, String... args) {
        return String.format("%s.%s( %s )", obj, m,
                ZTypes.string( new ZArray(args,false)));
    }

    @Override
    public String getOutputStatement(String toDisplay) {
        return String.format("println(%s)", toDisplay );
    }

    @Override
    public String getProgram(String... statements) {
        return ZTypes.string( new ZArray(statements,false), "\n");
    }

    @Override
    public ScriptEngine getScriptEngine() {
        return new ZScriptEngine();
    }
}
