{
  "#atomic" : {
     "desc" : "atomic block",
     "syn" : "#atomic{ /* code here */ }" ,
     "samples" :  ["See url"],
     "url" : "https://gitlab.com/non.est.sacra/zoomba/blob/master/_wiki/08-Execution.md#atomic"
   },
  "assert" : {
    "desc" : "fire assertion when condition is false",
    "syn" : "assert ( condition, message ) " ,
    "samples" :  [ "assert(false,'I will crash!')" , "assert(1==1,'Tautology! will always pass!')" ],
    "url" : "https://gitlab.com/non.est.sacra/zoomba/blob/master/_wiki/03-Iteration.md#error-handling"
  },
  "panic" : {
    "desc" : "fire assertion when condition is true",
    "syn" : "panic ( condition, message ) " ,
    "samples" :  [ "panic(false,'I will not crash!')" , "panic(1==1,'Tautology! will always crash!')" ],
    "url" : "https://gitlab.com/non.est.sacra/zoomba/blob/master/_wiki/03-Iteration.md#error-handling"
  },
  "test" : {
    "desc" : "logs test pass/fail when condition is true/false",
    "syn" : "test ( condition, message ) " ,
    "samples" :  [ "test(1==1,'I will not be logged')", "test(0==1,'I will be logged!')" ],
    "url" : "https://gitlab.com/non.est.sacra/zoomba/blob/master/_wiki/03-Iteration.md#error-handling"
  },
  "async" : {
    "desc" : "execute block of code in asynchronous mode",
    "syn" : "async( handler ) -> { /* block */ } " ,
    "samples" :  [ "See url" ],
    "url" : "https://gitlab.com/non.est.sacra/zoomba/blob/master/_wiki/08-Execution.md#being-asynchronous"
  },
  "bool" : {
    "desc" : "casts object into boolean : true|false|null \n if fails, returns null or default",
    "syn" : "bool( object [, default ] ) | bool( object, [option_true, option_false] )" ,
    "samples" :  ["bool(true) // true ", "bool('none') // null" ],
    "url" : "https://gitlab.com/non.est.sacra/zoomba/blob/master/_wiki/02-types.md#logical"
  },
  "break" : {
    "desc" : "breaks iteration flow when expression is true",
    "syn" : "break [ ( expression ) [ { /* after executing body */ } ] ]" ,
    "samples" :  ["See url"],
    "url" : "https://gitlab.com/non.est.sacra/zoomba/blob/master/_wiki/03-Iteration.md#break-and-continue"
  },
  "bye" : {
    "desc" : "Exits ZoomBA system with messages",
    "syn" : "bye ( message1, message2,... )" ,
    "samples" :  [ "bye('I am going bye bye!')"],
    "url" : ""
  },
  ".md#clock" : {
    "desc" : "Clocks a ZoomBA block",
    "syn" : ".#( t, r ) = #clock { /* block */ }\n t is time in sec(double), r is the result of the block" ,
    "samples" :  ["#(t,o) = #clock{ for([0:10000]){ x = $ } } \n printf('Time: %f Output %s', t,o )"],
    "url" : "https://gitlab.com/non.est.sacra/zoomba/blob/master/_wiki/08-Execution.md#clocking-execution-time"
  },
  "case" : {
    "desc" : "See switch",
    "syn" : "case @$ | case <option> | case expression" ,
    "samples" :  ["See url"],
    "url" : "https://gitlab.com/non.est.sacra/zoomba/blob/master/_wiki/03-Iteration.md#pattern-matching"
  },
  "comb" : {
    "desc" : "Combinatorial iterator",
    "syn" : "comb(collection,sample_size)" ,
    "samples" :  ["See url"],
    "url" : "https://gitlab.com/non.est.sacra/zoomba/blob/master/_wiki/03-Iteration.md#combinations"
  },
  "continue" : {
    "desc" : "Continues from current point in an iteration when expression is true",
    "syn" : "continue [ ( expression ) [ { /* after executing body */ } ] ]" ,
    "samples" :  ["See url"],
    "url" : "https://gitlab.com/non.est.sacra/zoomba/blob/master/_wiki/03-Iteration.md#break-and-continue"
  },
  "ceil" : {
    "desc" : "Ceiling function : smallest integer not smaller than argument",
    "syn" : "ceil(argument)" ,
    "samples" :  [ "ciel(1.1) // 2.0" ],
    "url" : "https://gitlab.com/non.est.sacra/zoomba/blob/master/_wiki/02-types.md#ceiling-and-floor"
  },
  "def" : {
    "desc" : "Defines a function or a class",
    "syn" : "def ClassName : { /* class body */ } | def [method_name] ( args... ) " ,
    "samples" :  [ "def X : {} // creates class X" , "def func(){ println('Hey!') }" ,
                   "func = def (){ println('cool') }" ],
    "url" : "https://gitlab.com/non.est.sacra/zoomba/blob/master/_wiki/04-Functions\nhttps://gitlab.com/non.est.sacra/zoomba/blob/master/_wiki/11-oops"
  },
  "del" : {
    "desc" : "Deletes a variable",
    "syn" : "del var_name | del $global_var" ,
    "samples" :  [ "del x" , "del $someVar" ],
    "url" : "https://gitlab.com/non.est.sacra/zoomba/blob/master/_wiki/01-basic.md#delete-variable"
  },
  "dict" : {
    "desc" : "Defines a Dictionary",
    "syn" : "dict(args...) [ (as| -> ) { [ key_expression, value_expression ] } ]" ,
    "samples" :  [ "dict()" , "dict([0:10] ) -> { [$.o, $.o ** 2 ] } " ],
    "url" : "https://gitlab.com/non.est.sacra/zoomba/blob/master/_wiki/02-types.md#dictionary"
  },
  "empty" : {
    "desc" : "Gets if an expression is empty ",
    "syn" : "empty(expression)" ,
    "samples" :  [ "empty(null) // true" , "empty('abc') // false" , "empty([]) // true" , "empty({:}) // true "],
    "url" : ""
  },
  "enum" : {
    "desc" : "Gets an Enum ",
    "syn" : "enum(enum_class)|enum('enum_class.enumValue')" ,
    "samples" :  [ "See url" ],
    "url" : "https://gitlab.com/non.est.sacra/zoomba/blob/master/_wiki/05-Imports.md#enum"
  },
  "exists" : {
    "desc" : "If condition exists in a collection return true else false",
    "syn" : "exists( collection ) (where | :: ) { /* condition */ } " ,
    "samples" :  [ "exists([0:10]) :: { 5 /? $.o }" ],
    "url" : "https://gitlab.com/non.est.sacra/zoomba/blob/master/_wiki/03-Iteration.md#exists"
  },
  "file" : {
    "desc" : "Gets a file iterator over a file/directory",
    "syn" : "file(file_path)" ,
    "samples" :  ["f = file(.)" , "f.file.directory //true" ],
    "url" : "https://gitlab.com/non.est.sacra/zoomba/blob/master/_wiki/07-IO.md#file"
  },
  "find" : {
    "desc" : "Finds element with condition in a collection returns OptionMonad",
    "syn" : "find( collection ) (where | :: ) { /* condition */ } " ,
    "samples" :  [ "div_5 = find([0:10]) :: { 5 /? $.o }" ],
    "url" : "https://gitlab.com/non.est.sacra/zoomba/blob/master/_wiki/03-Iteration.md#monadic-containers-while-finding-item"
  },
  "floor" : {
    "desc" : "Highest Integer lower than or equal to the argument",
    "syn" : "floor(argument)" ,
    "samples" :  ["floor(1.9) // 1.0" ],
    "url" : "https://gitlab.com/non.est.sacra/zoomba/blob/master/_wiki/02-types.md#ceiling-and-floor"
  },
  "for" : {
    "desc" : "Classic for loop",
    "syn" : "(for( [x :] collection ) | for (start ; check; end ) ) { /* block */ } " ,
    "samples" :  ["for([0:10){ println($) }"],
    "url" : "https://gitlab.com/non.est.sacra/zoomba/blob/master/_wiki/03-Iteration.md#for"
  },
  "fold" : {
    "desc" : "Folds the collection, lfold is same as fold, to fold from right use rfold",
    "syn" : "fold(collection [, seed ] as { /* fold body */ } " ,
    "samples" :  [ "fold([0:10]) -> { println($.o)}", "factorial_10 = fold([1:11], 1) -> { $.p * = $.o }" ],
    "url" : "https://gitlab.com/non.est.sacra/zoomba/blob/master/_wiki/03-Iteration.md#folds"
  },
  "float" : {
    "desc" : "Converts argument to Floating Point : Double",
    "syn" : "float( argument [, optional_value ] )" ,
    "samples" :  [ "float('12.34')" , "float(12)" , "float('aba',42.0)" ],
    "url" : "https://gitlab.com/non.est.sacra/zoomba/blob/master/_wiki/02-types.md#floating-point"
  },
  "FLOAT" : {
    "desc" : "Converts argument to Large Floating Point : Apfloat",
    "syn" : "FLOAT( argument [, optional_value ] )" ,
    "samples" :  [ "FLoAT(1.0)" ],
    "url" : "https://gitlab.com/non.est.sacra/zoomba/blob/master/_wiki/02-types.md#numeric-conversion"
  },
  "from" : {
    "desc" : "Collection collector for ZoomBA",
    "syn" : "from ( collection [, collector_collection ] ) ( as { /* mapper */ } | where {/* predicate */} )+ " ,
    "samples" :  ["l = from ( [1:10] , set() ) as {  $.o ** 2  }"],
    "url" : "https://gitlab.com/non.est.sacra/zoomba/blob/master/_wiki/03-Iteration.md#from"
  },
  "group" : {
    "desc" : "Group by  collections for ZoomBA",
    "syn" : "group ( collection ) [ as { /* hash */ } [ as {  /* apply for each key,value */ } ] " ,
    "samples" :  ["x = group(1,2,3,3,1,2) -> { $.o } -> { sum($.value) }"],
    "url" : "https://gitlab.com/non.est.sacra/zoomba/blob/master/_wiki/03-Iteration.md#from"
  },
  "hash" : {
    "desc" : "Encoding/Decoding for ZoomBA",
    "syn" : "hash ( string ) | hash ( scheme , string ) \n schemes are 'e64','b64' , ... " ,
    "samples" :  ["hash('e64' ,'Hello, World')"],
    "url" : "https://gitlab.com/non.est.sacra/zoomba/blob/master/_wiki/07-IO.md#encoding-and-decoding"
  },
  "heap" : {
    "desc" : "Priority Queue for ZoomBA",
    "syn" : "heap ( size [, min_heap = true|false ] ) [ where { /* comparator */ } ] " ,
    "samples" :  ["h = heap(4) \n for([0:10]){  h += $ }" ],
    "url" : "https://gitlab.com/non.est.sacra/zoomba/blob/master/_wiki/02-types.md#heap-or-priority-queue"
  },
  "import" : {
    "desc" : "Imports an object into ZoomBA",
    "syn" : "import <object_path> as <Identifier>" ,
    "samples" :  [ "import java.lang.Integer as INT" ],
    "url" : "https://gitlab.com/non.est.sacra/zoomba/blob/master/_wiki/05-Imports.md#import-syntax"
  },
  "int" : {
    "desc" : "Converts argument to Integer: Integer/Long : returns default_value or null if failed",
    "syn" : "int( argument [, default_value ] )" ,
    "samples" :  ["int(23.33)" , "int('baaa',0)" ],
    "url" : "https://gitlab.com/non.est.sacra/zoomba/blob/master/_wiki/02-types.md#integer"
  },
  "INT" : {
    "desc" : "Converts argument to Large Integer: Apint : returns default_value or null if failed",
    "syn" : "INT( argument [, default_value ] )" ,
    "samples" :  [ "INT(1)" ],
    "url" : "https://gitlab.com/non.est.sacra/zoomba/blob/master/_wiki/02-types.md#integer"
  },
  "index" : {
    "desc" : "Gets the index from left/right (rindex) in a collection given a condition, returns non negative if successful",
    "syn" : "index(collection,item) | index(collection) where { /* condition */ } , rindex for right" ,
    "samples" :  [ "index('abc','a') // 0" , "index('1234') :: { int($.o) == 2 } // 1" ],
    "url" : "https://gitlab.com/non.est.sacra/zoomba/blob/master/_wiki/03-Iteration.md#index"
  },
  "is" : {
    "desc" : "is a variable/expression exists? then true, else false",
    "syn" : "is(var) | is ( var.method() ) | is(obj.field)" ,
    "samples" :  [ "is(x)//false" , "x=10; is(x) // true" ],
    "url" : "https://gitlab.com/non.est.sacra/zoomba/blob/master/_wiki/01-basic.md#check-if-variable-exists"
  },
  "isa" : {
    "desc" : "variable/expression isa (of the) type T ? then true, else false ",
    "syn" : " expression isa type_expression" ,
    "samples" :  [ "1 isa 0 // true", "list(1,2,3) isa []"],
    "url" : "https://gitlab.com/non.est.sacra/zoomba/blob/master/_wiki/03-Iteration.md#isa-operator"
  },
  "join" : {
    "desc" : "conditional cartesian product of collections",
    "syn" : " join(collection1, collection2, ... ) [ [where { /* */ } [ as {/* mapper */ }] ]" ,
    "samples" :  ["join([0,1,2],[0,1,2]) where { $.o.0 != $.o.1 } as { str($.o) }"],
    "url" : "https://gitlab.com/non.est.sacra/zoomba/blob/master/_wiki/03-Iteration.md#multiple-iteration-join"
  },
  "json" : {
    "desc" : "From a String or from Path loads JSON object",
    "syn" : " json(string, from_file=false ) " ,
    "samples" :  [" json('[1,2,3]') "],
    "url" : "https://gitlab.com/non.est.sacra/zoomba/blob/master/_wiki/07-IO.md#json"
  },
  "lfold" : {
    "desc" : "See fold",
    "syn" : " See fold" ,
    "samples" :  [],
    "url" : ""
  },
  "list" : {
    "desc" : "Generates a list",
    "syn" : " list ( args... ) [ as { /* mapper */ } ]" ,
    "samples" :  ["list(1,2,3,3,4)" , "list([0:5]) -> { $.o ** 2 }" ],
    "url" : "https://gitlab.com/non.est.sacra/zoomba/blob/master/_wiki/03-Iteration.md#map"
  },
  "load" : {
    "desc" : "Loads a Jar into ZoomBA : true if successful, false if not",
    "syn" : " load ( jar_path ) " ,
    "samples" :  [],
    "url" : ""
  },
  "log" : {
    "desc" : "Mathematical Log function base some other number",
    "syn" : " log(number,base=e)" ,
    "samples" :  [ "log(100)" , "log(100,10) // 2" ],
    "url" : "https://gitlab.com/non.est.sacra/zoomba/blob/master/_wiki/02-types.md#log"
  },
  "matrix" : {
    "desc" : "Loads a Matrix from Data file",
    "syn" : " matrix( file_path [, separator_string ]) " ,
    "samples" :  ["See url"],
    "url" : "https://gitlab.com/non.est.sacra/zoomba/blob/master/_wiki/07-IO.md#matrix"
  },
  "minmax" : {
    "desc" : "Gets minimum and maximum from a Collection",
    "syn" : " #(min,MAX) = minmax(collection) [ where { /* comparator */ } ] " ,
    "samples" :  [ "#(min,MAX) = minmax(1,2,3,4,5,-1,0) // min:= -1, MAX:= 5" ],
    "url" : "https://gitlab.com/non.est.sacra/zoomba/blob/master/_wiki/03-Iteration.md#min-and-max"
  },
  "mset" : {
    "desc" : "Gets a multi-set from a Collection",
    "syn" : " mset(collection) [ as { /* key function */ } ] " ,
    "samples" :  [ "mset( 'hello,ZoomBA'.value )" ],
    "url" : "https://gitlab.com/non.est.sacra/zoomba/blob/master/_wiki/03-Iteration.md#multiple-element-set"
  },
  "new" : {
    "desc" : "Creates and returns a new Object by calling appropriate initializer",
    "syn" : " new ( obj_identifier [, args... ] )" ,
    "samples" :  ["z = new( 'java.lang.Integer', 0 )"],
    "url" : ""
  },
  "num" : {
    "desc" : "Creates an appropriate numeric type from args, returns null or default if failed ",
    "syn" : " num ( arg [, default_value ] )" ,
    "samples" :  ["num('2.31')" , "num('xxx') // null" ],
    "url" : "https://gitlab.com/non.est.sacra/zoomba/blob/master/_wiki/02-types.md#numeric-types"
  },
  "open" : {
    "desc" : "Opens a resource",
    "syn" : " open ( file_path [, mode = 'r' ] ) : modes <- ( read : 'r' , write :'w', append : 'a' )" ,
    "samples" :  [ "open('foo.txt')" , "open('foo.txt', 'w')",  "open(_/'../db:db_con.json')" ],
    "url" : "https://gitlab.com/non.est.sacra/zoomba/blob/master/_wiki/07-IO.md#database"
  },
  "oset" : {
    "desc" : "Ordered Set : The insertion order is preserved ",
    "syn" : " oset ( collection ) [ where { /* key function */ } ] " ,
    "samples" :  ["oset(1,2,3,4)"],
    "url" : ""
  },
  "odict" : {
    "desc" : "Ordered Dictionary : The insertion order is preserved ",
    "syn" : " odict ( [ collection ] ) [ where { /* [ key, value ] */ } ] " ,
    "samples" :  [],
    "url" : ""
  },
  "partition" : {
    "desc" : "Partitions a collection using condition, haves, have not ",
    "syn" : " .md#(have, have_not) = partition (  collection ) where { /* condition */ } " ,
    "samples" :  [ ".md#(select,reject) = partition([0:10) where {  2 /? $.o }" ],
    "url" : "https://gitlab.com/non.est.sacra/zoomba/blob/master/_wiki/03-Iteration.md#partitioning"
  },
  "perm" : {
    "desc" : "Permutations iterator of a collection having k items ",
    "syn" : " perm ( collection [, k = size(collection)] ) " ,
    "samples" :  [ "perm( [0,1,2,3], 3 )" ],
    "url" : "https://gitlab.com/non.est.sacra/zoomba/blob/master/_wiki/03-Iteration.md#permutations"
  },
  "popen" : {
    "desc" : "Process open",
    "syn" : " popen( args... )  " ,
    "samples" :  ["See url"],
    "url" : "https://gitlab.com/non.est.sacra/zoomba/blob/master/_wiki/08-Execution.md#popen"
  },
  "poll" : {
    "desc" : "Polls for a condition to be true using num_iteration, poll_interval, true if successful, false otherwise",
    "syn" : " poll( [ num_iteration , [ poll_interval ] ] ) where { /* polling condition */ } " ,
    "samples" :  ["See url"],
    "url" : "https://gitlab.com/non.est.sacra/zoomba/blob/master/_wiki/08-Execution.md#poll"
  },
  "printf" : {
    "desc" : "Classical printf()",
    "syn" : " printf( fmt_string [,args...])" ,
    "samples" :  [],
    "url" : ""
  },
  "println" : {
    "desc" : "Classical println()",
    "syn" : " println(object)" ,
    "samples" :  [],
    "url" : ""
  },
  "random" : {
    "desc" : "Cipher quality random number Generator",
    "syn" : " random() | random(collection [,count] ) " ,
    "samples" :  ["random(100)" , "random( [0,1,2,3], 3)"],
    "url" : "https://gitlab.com/non.est.sacra/zoomba/blob/master/_wiki/03-Iteration.md#randomisation"
  },
  "raise" : {
    "desc" : "Raise Throwable : used for Error throwing",
    "syn" : " raise( exception_identifier) " ,
    "samples" :  ["raise('java.lang.Exception', 'yep, throw it!' )"],
    "url" : "https://gitlab.com/non.est.sacra/zoomba/blob/master/_wiki/03-Iteration.md#raise"
  },
  "read" : {
    "desc" : "Reads from path/url",
    "syn" : " read( url|path )" ,
    "samples" :  ["See url"],
    "url" : "https://gitlab.com/non.est.sacra/zoomba/blob/master/_wiki/07-IO.md#read"
  },
  "reduce" : {
    "desc" : "Reduces a collection, to reduce from right use rreduce",
    "syn" : " reduce ( collection ) as { /* reduction function */ } " ,
    "samples" :  ["my_sum = reduce([1:10] ) -> { $.p += $.o }"],
    "url" : "https://gitlab.com/non.est.sacra/zoomba/blob/master/_wiki/03-Iteration.md#reduce"
  },
  "rfold" : {
    "desc" : "See fold",
    "syn" : " See fold " ,
    "samples" :  [],
    "url" : ""
  },
  "rindex" : {
    "desc" : "See index",
    "syn" : " See index " ,
    "samples" :  [],
    "url" : ""
  },
  "round" : {
    "desc" : "Rounds off a floating point number optionally to number of digits after decimal",
    "syn" : " round(number [, digits = 0 ])" ,
    "samples" :  [" round(1234.567, 2) "],
    "url" : ""
  },
  "rreduce" : {
    "desc" : "See Reduce",
    "syn" : " See Reduce " ,
    "samples" :  [],
    "url" : ""
  },
  "sdict" : {
    "desc" : "A Sorted Dictionary",
    "syn" : " sdict( [collection] ) [ where { /* comparator*/ } ] " ,
    "samples" :  [],
    "url" : ""
  },
  "sset" : {
    "desc" : "A Sorted Set",
    "syn" : " sset( [collection] ) [ where { /* comparator*/ } ] " ,
    "samples" :  [],
    "url" : ""
  },
  "set" : {
    "desc" : "A Set ",
    "syn" : " set( [collection] ) [ as { /* key function */ } ] " ,
    "samples" :  [],
    "url" : ""
  },
  "select" : {
    "desc" : "Selects from a collection using condition, filters elements having the condition",
    "syn" : " filtered = select (  collection ) where { /* condition */ } " ,
    "samples" :  [ "filtered = select([0:10) where {  2 /? $.o }" ],
    "url" : "https://gitlab.com/non.est.sacra/zoomba/blob/master/_wiki/03-Iteration.md#find-all"
  },
  "send" : {
    "desc" : "Sends data to url on web ",
    "syn" : " send ( url, protocol, data_map , body )" ,
    "samples" :  [],
    "url" : ""
  },
  "seq" : {
    "desc" : "Generates an infinite iterator based on sequence",
    "syn" : " seq (args... ) as { /* definition of sequence */ } " ,
    "samples" :  [ "fibonacci = seq(0,1) -> { $.p[-1] + $.p[-2] }" ],
    "url" : "https://gitlab.com/non.est.sacra/zoomba/blob/master/_wiki/03-Iteration.md#sequences"
  },
  "sequences" : {
    "desc" : "Generates an  iterator based on all possible sub sequences of a collection",
    "syn" : " sequences ( collection )" ,
    "samples" :  [ "iter = sequences([0,1,2,3,4])" ],
    "url" : "https://gitlab.com/non.est.sacra/zoomba/blob/master/_wiki/03-Iteration.md#sub-sequences-and-power-sets"
  },
  "sign" : {
    "desc" : "Gets the sign of a numeric object ",
    "syn" : " sign(expression) " ,
    "samples" :  [ "sign(-2.3) //-1" , "sign(0) // 0" , "sign(10) // 1"  ],
    "url" : ""
  },
  "size" : {
    "desc" : "Gets the size of a collection ",
    "syn" : " size(expression) " ,
    "samples" :  [ "size('abc')" , "size(null)" , "size(10)"  ],
    "url" : ""
  },
  "sort" : {
    "desc" : "Sorts in place a Collection : sorta for ascending, sortd for descending",
    "syn" : " sorta|sortd ( collection ) [ where { /* comparator */ } ]" ,
    "samples" :  [ "a = [9,1,0,2,3] ; sorta(a) // in place sorting" ],
    "url" : "https://gitlab.com/non.est.sacra/zoomba/blob/master/_wiki/03-Iteration.md#sorting"
  },
  "shuffle" : {
    "desc" : "Randomly Shuffles in-place a collection : true if shuffled, false if failed",
    "syn" : " shuffle ( collection ) " ,
    "samples" :  [ "a = [0,1,2,3,4] ; shuffle(a)" ],
    "url" : "https://gitlab.com/non.est.sacra/zoomba/blob/master/_wiki/03-Iteration.md#shuffling"
  },
  "str" : {
    "desc" : "Converts to String objects, collections, maps... ",
    "syn" : " str( object [ , separator] ) [ as { /* mapper per item */ } ] " ,
    "samples" :  ["See Wiki"],
    "url" : ""
  },
  "sum" : {
    "desc" : "Sums up a  collection",
    "syn" : " sum( collection ) [ as { /* mapper per item */ } ] " ,
    "samples" :  ["x = sum([0:10])" , "s = sum( [0:10] ) -> { 2 /? $.o ? 1:0 }" ],
    "url" : "https://gitlab.com/non.est.sacra/zoomba/blob/master/_wiki/03-Iteration.md#sum"
  },
  "switch" : {
    "desc" : "Conditional flow",
    "syn" : " switch( expression ) {\n  case <expression> : body1 \n case @$ : default_body \n}" ,
    "samples" :  [ "See url" ],
    "url" : ""
  },
  "system" : {
    "desc" : "Executes system command, returns the exit status",
    "syn" : " system ( args... ) " ,
    "samples" :  [],
    "url" : ""
  },
  "tokens" : {
    "desc" : "Tokenize a string, using regex, optionally maps and returns the list of mapped match",
    "syn" : " tokens ( string, regex ) [ as {/* mapper */} ] " ,
    "samples" :  [ "See url"],
    "url" : "https://gitlab.com/non.est.sacra/zoomba/blob/master/_wiki/03-Iteration.md#tokenization"
  },
  "thread" : {
    "desc" : "Creates a thread object to run asynchronously : returns the thread ",
    "syn" : " thread( [args...] ) [ as { /* thread body */ } ]" ,
    "samples" :  ["See url"],
    "url" : "https://gitlab.com/non.est.sacra/zoomba/blob/master/_wiki/08-Execution.md#threads"
  },
  "time" : {
    "desc" : "Get time object",
    "syn" : " time( [args...] )" ,
    "samples" : [ "time() // get current time" , "time('15-AUG-1947', 'dd-MMM-yyyy') // parses and creates" ],
    "url" : "https://gitlab.com/non.est.sacra/zoomba/blob/master/_wiki/02-types.md#date-and-time-type"
  },
  "type" : {
    "desc" : "Gets type of an object",
    "syn" : " type( arg )" ,
    "samples" : ["type(0) // Integer" , "type(null) // nil" ] ,
    "url" : "https://gitlab.com/non.est.sacra/zoomba/blob/master/_wiki/02-types.md#finding-types"
  },
  "tuple" : {
    "desc" : "Converts into a tuple",
    "syn" : " tuple( args...) | tuple(col_to_int_map, item_list) " ,
    "samples" : ["tuple(1,2,3)" , "tuple({'a':0,'b':1},[1,2] ) ; t.a // 1" ] ,
    "url" : "NA"
  },
  "write" : {
    "desc" : "Writes to path",
    "syn" : " write( path , string_data , mode )" ,
    "samples" : [ "write('foo.txt', 'I am cool!' ) // create n then writes to foo.txt" ,
                 "write('foo.txt', 'I am cooler!' , 'a' ) // appends on the same file " ],
    "url" : "https://gitlab.com/non.est.sacra/zoomba/blob/master/_wiki/07-IO.md#write"
  },
  "xml" : {
    "desc" : "From a String or from Path loads Xml object",
    "syn" : " xml(string, from_file=false ) " ,
    "samples" :  [" xml('some/path',true) "],
    "url" : "https://gitlab.com/non.est.sacra/zoomba/blob/master/_wiki/07-IO.md#xml"
  },
  "xelem" : {
    "desc" : "From a String or from Object runs xpath to return element[s]",
    "syn" : " xelem(arg, path, multiple=false ) " ,
    "samples" :  [" xelem(obj,'//x', true ) "],
    "url" : "https://gitlab.com/non.est.sacra/zoomba/blob/master/_wiki/07-IO.md#xml"
  },
  "xpath" : {
    "desc" : "From a String or from Object runs xpath ",
    "syn" : " xpath(arg, path, multiple=false ) " ,
    "samples" :  [" xpath(obj,'//x', true ) "],
    "url" : "https://gitlab.com/non.est.sacra/zoomba/blob/master/_wiki/07-IO.md#xml"
  },
  "yaml" : {
    "desc" : "From a String or from Path loads Yaml object",
    "syn" : " yaml(string, from_file=false ) " ,
    "samples" :  [" yaml('some/path',true) "],
    "url" : "NA"
  }
}